<style>
    .btnBack {
        margin-top: 30px;
        margin-bottom: 50px;
    }

    #btnHold {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnHold:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnUnhold {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnUnhold>a {
        color: white;
    }

    #btnUnhold:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnUnhold:hover>a {
        color: #151A48;
    }

    #btnClose {
        background-color: #ee5744;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnClose:hover {
        border: 2px solid #ee5744;
        color: #ee5744;
        background-color: white;
    }

    #btnPreclose {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnPreclose:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReassign {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReassign:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReturn {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturn:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReturnCustCare {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturnCustCare:hover {
        border: 2px solid #151A48;
        color: #151A48;
        font-weight: 600;
        background-color: white;
    }


    #btnOther>a {
        margin-top: 5px;
    }

    #panelIncident>span {
        word-break: break-all;
    }

    #lineShadow {
        width: 98%;
        height: 10px;
        border: 0;
        box-shadow: 0 10px 10px -10px #8c8c8c inset;

    }

    #btnComment {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    #btnComment:hover {
        background-color: white;
        color: #151A48;
        border: 2px solid #151A48;
    }

    textarea {
        resize: none;
        border: 4px solid gray;
    }

    textarea:focus {
        border: 4px solid #151A48;
    }

    #attc1 {
        height: 100px;
        border: 4px solid gray;
    }

    #attc1:hover {
        border: 4px solid #151A48;
    }

    #panelHeader {
        background-color: #151A48;
        overflow: hidden;
    }

    #panelBasic {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #borderPanel {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblLogsAssign {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblComment {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    table>tbody>tr:hover>td {
        background-color: lightblue;
        color: black;
    }

    table>thead>tr>th {
        background-color: #151A48;
        color: white;
    }

    .modal-header {
        border-bottom: 1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }

    .form-group>.form-control:focus {
        border: 2px solid #151A48;
    }

    .modal-footer>button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }

    .modal-footer>button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }

    .modal-footer>button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }

    .modal-footer>button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnTopo_1 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_1:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnTopo_2 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_2:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnTopo_3 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_3:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnExportLogExternal {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }

    #btnExportLogExternal:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnReschedule {
        background-color: #fdba4b;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnReschedule:hover {
        background-color: white;
        color: #fdba4b;
        border: 2px solid #fdba4b;
        font-weight: 700;
    }

    #btnCancel {
        background-color: #ee5744;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnCancel:hover {
        background-color: white;
        color: #ee5744;
        border: 2px solid #ee5744;
        font-weight: 700;
    }

    #btnAttachment>a {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    .form-body>ul>li {
        color: red;
    }
</style>


@extends('layout.master')
@section('content')

<!--alert success -->
@if (session('status'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ session('status') }}</strong>
</div>
@endif
<!--alert success -->
<!--validasi form-->
@if (count($errors)>0)
<div class="alert alert-danger alert-dismissible" role="alert">
    <ul>
        <li><strong>Submit Failed !</strong></li>
        @foreach ($errors->all() as $error)
        <li><strong>{{ $error }}</strong></li>
        @endforeach
    </ul>
</div>
@endif
<!--end validasi form-->

<div class="btnBack">
    <div class="container-fluid">
        <div class="bs-component mb20" align="right">
            <a href="{{ url()->previous() }}" class="btn btn-danger btn-md">
                <i class="fa fa-angle-double-left"></i>
                <span class="nav-text" style="font-weight: 700">
                    Back To Ticket List
                </span>
            </a>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row mb20">
        <div class="col-md-12" align="right" id="BtnOther">
            <div class="bs-component mb20" align="center">
                @if($buttonOn == '1')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnReschedule" data-target="#myModalReschedule" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-calendar" aria-hidden="true"></i> Reschedule
                </button>

                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnCancel" data-target="#myModalCancel" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-ban" aria-hidden="true"></i> Cancel
                </button>

                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnClose" data-target="#myModalClose" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-close" aria-hidden="true"></i> Close Ticket
                </button>
                @else

                @endif
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="panelBasic">
        <div class="panel-body">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>No Ticket</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->no_ticket}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Status</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>
                        @if (($tickets_maintenance[0]->status=='1') && date('d-M-Y', strtotime($tickets_maintenance[0]->created_date))==date('d-M-Y'))
                        <font color='green'><b>New !</b></font>
                        @elseif (($tickets_maintenance[0]->status=='1') && date('d-M-Y', strtotime($tickets_maintenance[0]->created_date))<>date('d-M-Y'))
                            <font color='green'><b>In Progress</b></font>
                            @elseif ($tickets_maintenance[0]->status=='2')
                            <font color='blue'><b>Unassign</b></font>
                            @elseif ($tickets_maintenance[0]->status=='3')
                            <font color='orange'><b>Hold</b></font><br>
                            {{-- {{ date('d-M-Y H:i:s', strtotime($tickets[0]->hold_date)) }} --}}
                            @else
                            <font color='red'><b>Closed</b></font>
                            @endif
                    </span>
                </div>
            </div>

            <div class="row" align="left">
                <div class="col-md-2">
                    <span>
                        @if($tickets_maintenance[0]->status=='4')
                        <b>Close Date</b>
                        @elseif ($tickets_maintenance[0]->status=='3')
                        <b>Hold Until</b>
                        @else
                        <b>Create Date</b>
                        @endif
                    </span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>
                        @if($tickets_maintenance[0]->status=='3')
                        {{ date('d-M-Y H:i:s', strtotime($tickets_maintenance[0]->hold_date)); }}
                        @elseif($tickets_maintenance[0]->status=='4')
                        {{ date('d-M-Y H:i:s', strtotime($tickets_maintenance[0]->closed_date)); }}
                        @else
                        {{ date('d-M-Y H:i:s', strtotime($tickets_maintenance[0]->created_date)); }}
                        @endif
                    </span>
                </div>
            </div>
            <br>
            <hr id="lineShadow">
            <div class="bs-component mb20" align="left">
                <form action="{{url('/tickets/exportLogAssign')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <button type="submit" class="btn btn-primary" id="btnExportLogExternal">
                        <i class="fa fa-table" aria-hidden="true"></i>
                        <input type="hidden" name="id_ticket" value="{{ $tickets_maintenance[0]->id_ticket }}" id="id_ticket" style="">
                        <span class="nav-text">
                            Export Log to Excel
                        </span>
                    </button>
                </form>
            </div>

            <div class="w3l-table-info">
                <table id="tblLogsAssign" class="display">
                    <thead>
                        <tr>
                            <th>Assign By</th>
                            <th>Assign to Department</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($assign_history as $assign)
                        <tr>
                            <td>{{ $assign->int_emp_name }}</td>
                            <td>{{ $assign->department_name }}</td>
                            <td>{{ $assign->message }}</td>
                            <td><strong>Assign date</strong> : {{date('d-M-Y H:i:s', strtotime($assign->assign_date))}} <br>
                                @if($assign->preclosed_status == '1')
                                <strong>Preclose date</strong> : {{date('d-M-Y H:i:s', strtotime($assign->preclosed_date))}}
                                @else
                                <span><b>Preclose date</b> : still hasn't pre-closed </span>
                                @endif
                            </td>
                            <td>
                                @if($assign->preclosed_status == '1')
                                <span class="badge" style="background-color: green"><b>Pre-Close</b></span>
                                @else
                                <span class="badge" style="background-color: orange"><b>Active</b></span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <br>

    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Detail Ticket</h2>
        </div>

        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Start Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->start_date_mtc}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Finish Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->finish_date_mtc}}</span>
                </div>
            </div>

            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Location</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->outage_location}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Category</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->category}}</span>
                </div>
            </div>

            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Sub Category</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->name_incident}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Created by</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->int_emp_name}}</span>
                </div>
            </div>

            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Assign To Dept</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->department_name}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Work Description</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets_maintenance[0]->remarks}}</span>
                </div>
            </div>

            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Attachment</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <a href="{{ url('/tickets_Maintenance/'.$tickets_maintenance[0]->file_1.'/download') }}">
                        <span>
                            {{$tickets_maintenance[0]->file_1}}
                        </span>
                    </a>
                    {{-- <a href="{{  url('/tickets/'.$tickets[0]->file_2.'/download') }}">
                    <span>
                        {{$tickets[0]->file_2}}
                    </span>
                    </a>
                    <a href="{{  url('/tickets/'.$tickets[0]->file_3.'/download') }}">
                        <span>
                            {{$tickets[0]->file_3}}
                        </span>
                    </a>
                    <a href="{{  url('/tickets/'.$tickets[0]->file_4.'/download') }}">
                        <span>
                            {{$tickets[0]->file_4}}
                        </span>
                    </a>
                    <a href="{{  url('/tickets/'.$tickets[0]->file_5.'/download') }}">
                        <span>
                            {{$tickets[0]->file_5}}
                        </span>
                    </a> --}}
                </div>
            </div>
            <!--row-->

            <!--Reschedule table-->
            <div class="w3l-table-info">
                <table id="tblLogsAssign" class="display">
                    <thead>
                        <tr>
                            <th>Start Date Maintenance</th>
                            <th>Finish Date Maintenance</th>
                            <th>Message</th>
                            <!-- <th>Created By</th> -->
                            <th>Status Maintenance</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reschedules as $reschedule)
                        <tr>
                            <td>{{ date('d-M-Y H:i:s', strtotime($reschedule->start_date_mtc)) }}</td>
                            <td>{{ date('d-M-Y H:i:s', strtotime($reschedule->finish_date_mtc)) }}</td>
                            <td>{{ $reschedule->notes }}</td>
                            <!-- <td>
                            // ini isi create by kalo dibutuhkan
                        </td> -->
                            <td>
                                @if($reschedule->status_mtc == '1')
                                <span class="badge" style="background-color: green"><b>Open</b></span>
                                @elseif($reschedule->status_mtc == '2')
                                <span class="badge" style="background-color: green"><b>Reschedule</b></span>
                                @elseif($reschedule->status_mtc == '3')
                                <span class="badge" style="background-color: orange"><b>Cancel</b></span>
                                @else
                                <span class="badge" style="background-color: red"><b>Closed</b></span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!--end reschedule table-->

        </div>
    </div>
    <!--panel primary -->
</div>
{{-- </div> --}}

<br>
@if($panelCustomer == '1')
<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2>Detail Customer</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>CID</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->cid}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Company Name</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->company_name}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Priority</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->priority}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Company Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->company_type}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Service</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->service}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>PIC Name</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->pic_name}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>A-End</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->a_end}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Capacity</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->capacity}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>B-End</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->b_end}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Network Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->network_type}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>PIC Contact</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->pic_contact}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Regional</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets_maintenance[0]->regional}}</span>
                </div>
            </div>
            @foreach ($tickets_maintenance as $ticket)
            <div class="row mb10" align="left">
                @php
                $file1=base64_encode($ticket->topology_file1);
                $file2=base64_encode($ticket->topology_file2);
                $file3=base64_encode($ticket->topology_file3);

                $cust=base64_encode($ticket->company_name);
                @endphp

                <div class="col-lg-12" align="left">
                    @if ($file1<>'')
                        <a href="{{ url('/masters/topology/view/'.$file1.'/'.$cust) }}" class="btn btn-info btn-md" id="btnTopo_1" target="_blank">
                            Topology 1
                        </a>
                        @endif

                        @if ($file2<>'')
                            <a href="{{ url('/masters/topology/view/'.$file2.'/'.$cust) }}" class="btn btn-info btn-md" id="btnTopo_2" target="_blank">
                                Topology 2
                            </a>
                            @endif

                            @if ($file3<>'')
                                <a href="{{ url('/masters/topology/view/'.$file3.'/'.$cust) }}" class="btn btn-info btn-md" id="btnTopo_3" target="_blank">
                                    Topology 3
                                </a>
                                @endif
                </div>
            </div>
            @endforeach
        </div><!-- panel body -->
    </div>
</div>
@else

@endif

<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2>Update Progress</h2>
        </div>
        <div class="panel-body">
            <br><br>
            <div class="bs-component mb20">
                <div class="row">
                    <form action="{{ url('/tickets_Maintenance/addComment') }}" method="post" id="formComment" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-8">
                            <input type="hidden" name="id_ticket" value="{{ $tickets_maintenance[0]->id_ticket }}" id="id_ticket" style="">
                            <textarea name="message_comment" id="message_comment" cols="10" rows="5" placeholder=" Write a comment"></textarea>
                            <span style="color: red">*required</span>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="file" name="attc1" id="attc1" class="form-control">
                                <span style="color: red">*not required | file must : mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif | max:10048 </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <button type="submit" class="btn btn-primary btn-block" id="BtnComment"><span class="fa fa-comments"></span> Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="main-grid">
                <div class="agile-grids">
                    <div class="w3l-table-info">
                        <table id="tblComment" class="display">
                            <thead style="background-color:#151A48; color:white;">
                                <tr>
                                    <th style="background-color:#151A48; color:white;">Create by</th>
                                    <th style="background-color:#151A48; color:white;">Message</th>
                                    <th style="background-color:#151A48; color:white;">File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($comment_history as $comment)
                                <tr id="hoverRowComment">
                                    <td><b>{{ $comment->int_emp_name }} - {{ $comment->department_name }}</b><br>
                                        <i>{{ $comment->created_at }}</i>
                                    </td>
                                    <td><b>{{ $comment->message}}</b><br>
                                        <i>{{ $comment->description}}</i>
                                    </td>
                                    <td>
                                        <a href="{{ url('/tickets_Maintenance/'.$comment->attachment_1.'/downloadFileComment') }}">{{ $comment->attachment_1}}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- end table -->
                </div>
            </div> <!-- main grid -->
        </div><!-- panel body-->
    </div><!-- panel primary -->
</div>
<!--container fluid -->

<!-- Modal Reschedule-->
<div class="modal fade" id="myModalReschedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Reschedule Maintenance</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalRechedule" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets_Maintenance/reschedule') }}" method="post" id="myform" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id_employee }}">
                <input type="hidden" name="id_ticket" id="id_ticket" value="{{ $tickets_maintenance[0]->id_ticket }}">
                <div class="modal-body">

                    <div class="row mb20">
                        <div class="col-md-6">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Date Start</label>
                                        <input type="date" name="start_date" id="date_start" class="form-control date_start" value="{{ date('Y-m-d', strtotime($tickets_maintenance[0]->start_date_mtc))}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Time Start</label>
                                        <input type="time" name="time_start" id="time_start" class="form-control time_start" value="{{ date('H:i:s', strtotime($tickets_maintenance[0]->start_date_mtc))}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Date Finish</label>
                                        <input type="date" name="finish_date" id="date_finish" class="form-control date_finish" value="{{ date('Y-m-d', strtotime($tickets_maintenance[0]->finish_date_mtc))}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Time Finish</label>
                                        <input type="time" name="time_finish" id="time_finish" class="form-control time_finish" value="{{ date('H:i:s', strtotime($tickets_maintenance[0]->finish_date_mtc))}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->

                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-reschedule">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalReschedule"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Reschedule-->

<!-- Modal Cancel-->
<div class="modal fade" id="myModalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Cancel Maintenance</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalCancel" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets_Maintenance/cancel') }}" method="post" id="myrom" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id_employee }}">
                <input type="hidden" name="id_ticket" id="id_ticket" value="{{ $tickets_maintenance[0]->id_ticket }}">
                <input type="hidden" name="start_date_mtc" id="start_date_mtc" value="{{ $tickets_maintenance[0]->start_date_mtc }}">
                <input type="hidden" name="finish_date_mtc" id="finish_date_mtc" value="{{ $tickets_maintenance[0]->finish_date_mtc }}">
                <input type="hidden" name="created_date" id="created_date" value="{{ $tickets_maintenance[0]->created_date }}">
                <div class="modal-body">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-cancel">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalCancel"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Cancel-->

<!-- Modal Close Ticket-->
<div class="modal fade" id="myModalClose" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Close Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalClose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets_Maintenance/closeticket') }}" method="post" id="myform" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id_employee }}">
                <input type="hidden" name="id_ticket" id="id_ticket" value="{{ $tickets_maintenance[0]->id_ticket }}">
                <input type="hidden" name="start_date_mtc" id="start_date_mtc" value="{{ $tickets_maintenance[0]->start_date_mtc }}">
                <input type="hidden" name="finish_date_mtc" id="finish_date_mtc" value="{{ $tickets_maintenance[0]->finish_date_mtc }}">
                <input type="hidden" name="created_date" id="created_date" value="{{ $tickets_maintenance[0]->created_date }}">
                <div class="modal-body">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-close">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalClose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Close Ticket-->

<script>
    function showhide(elem) {
        console.log(elem.getAttribute("href"));
        var div = document.getElementById(elem.getAttribute("href").replace("#", ""));

        if (div.style.display !== "none") {
            div.style.display = "none";
        } else {
            div.style.display = "block";
        }
    }

    $(document).ready(function() {
        var table = $('#tblComment').DataTable({
            "order": [],
            responsive: true
        });

        var tableLogs = $('#tblLogsAssign').DataTable({
            "order": [],
            responsive: true
        });

        $('#btnResetModalReschedule').off('click').on('click', resetModalReschedule);
        $('#closeModalRechedule').off('click').on('click', resetModalReschedule);

        function resetModalReschedule() {
            $('.date_start').val('');
            $('.time_start').val('');
            $('.date_finish').val('');
            $('.time_finish').val('');
            $('.message-reschedule').val('');
        }

        $('#btnResetModalCancel').off('click').on('click', resetModalCancel);
        $('#closeModalCancel').off('click').on('click', resetModalCancel);

        function resetModalCancel() {
            $('.message-cancel').val('');
        }

        $('#btnResetModalClose').off('click').on('click', resetModalClose);
        $('#closeModalClose').off('click').on('click', resetModalClose);

        function resetModalClose() {
            $('.message-close').val('');
        }
    });

    //select sub outage from outage 1
    $('select[name="outage_reason1"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if (outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="outage_sub1"]').empty();
                    $('select[name="outage_sub1"]').append(
                        '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub1"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="outage_sub1"]').empty();
        }
    });

    //select sub outage from outage 2
    $('select[name="outage_reason2"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if (outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="outage_sub2"]').empty();
                    $('select[name="outage_sub2"]').append(
                        '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub2"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
            $('#btnComment').click(function() {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('select[name="outage_sub2"]').empty();
        }
    });

    //select sub SLR from select SLR
    $('select[name="link_respond"]').on('change', function() {
        var slrID = $(this).val();
        var url = '{{ route("slr", ":id") }}';
        url = url.replace(':id', slrID);
        if (slrID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="sub_link_respond"]').empty();
                    $('select[name="sub_link_respond"]').append(
                        '<option value="">- Select Sub Status Link Respond -</option>'
                    );
                    $.each(data, function(slr_data, value) {
                        $('select[name="sub_link_respond"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="sub_link_respond"]').empty();
        }
    });
</script>
@endsection