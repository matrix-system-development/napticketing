<!DOCTYPE html>
<html>

<head>
	<title>REASON FOR OUTAGE</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<style>
		#footer {
			position: fixed;
			padding: 10px 10px 0px 10px;
			bottom: 0;
			width: 100%;
			/* Height of the footer*/
			height: 40px;
		}

		tr,
		td {
			padding-left: 15px;
		}
	</style>
</head>
@foreach($ticket as $data)

<body style="min-height: 100%">
	<img <img src="{{public_path('admin/images/header_mail.JPG')}}" alt="" width="700" height="140" />
	<br>
	<p class="text-left">
		Date: {{ date('d F Y', strtotime($data->created_date)); }}
	</p>
	<br>
	<center>
		<h5>Maintenance Report</h5>
	</center>
	<table border='1' style="width: 100%">
		<tbody>
			<tr>
				<td>Ticket Number</td>
				<td>{{ $data->no_ticket }}</td>
			</tr>
			@if ($data->no_ticket<>'NULL')
				<tr>
					<td>CID Impacted</td>
					<td>{{ $data->cid }}</td>
				</tr>
				<tr>
					<td>Customer Impacted</td>
					<td>{{ $data->company_name }}</td>
				</tr>
				@endif
				<tr>
					<td>Impact</td>
					<td>Down</td>
				</tr>
				<tr>
					<td>Start Time</td>
					<td>{{ date('l,d F Y H:i:s', strtotime($data->start_date_mtc)).' (GMT+7)' }}</td>
				</tr>
				<tr>
					<td>Finish Time</td>
					<td>{{ date('l,d F Y H:i:s', strtotime($data->finish_date_mtc)).' (GMT+7)' }}</td>
				</tr>
				<tr>
					<td>Total Maintenance</td>
					<td>{{ $data->duration_mtc }}</td>
				</tr>
				<tr>
					<td>Location</td>
					<td>{{ $data->outage_location }}</td>
				</tr>
				<tr>
					<td>Category Maintenance</td>
					<td>{{ $data->name_incident }}</td>
				</tr>
				<tr>
					<td>Description of Work</td>
					<td>{{ $data->remarks }}</td>
				</tr>
		</tbody>
	</table>
	<br>
	<div id="footer">
		<img <img src="{{public_path('admin/images/footer_mail.JPG')}}" alt="" width="700" height="70" />
	</div>
</body>
@endforeach

</html>