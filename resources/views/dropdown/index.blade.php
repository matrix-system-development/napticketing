<style>
    #dropdownTable {
        border: 4px solid gray; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }

    table > thead > tr > th {
        background-color:#151A48; 
        color:white;
    }
    #btnDropdown {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnDropdown:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnDropdown:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-footer > button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">

        <!--alert success -->
        @if (session('status'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session('status') }}</strong>
        </div> 
        @endif
        <!--alert success -->

        <!--validasi form-->
        @if (count($errors)>0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    <li><strong>Saved Data Failed !</strong></li>
                    @foreach ($errors->all() as $error)
                        <li><strong>{{ $error }}</strong></li>
                    @endforeach
                </ul>
            </div>   
        @endif
        <!--end validasi form-->
        
        <div class="buttons-heading">
            <h2>Master Dropdown</h2>
        </div>
        
        <!-- Button trigger modal Name -->
        <div class="bs-component mb20">
            <button type="button" class="btn btn-primary" id="btnDropdown" data-toggle="modal" data-target="#myModalName">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span class="nav-text">
                Add Item
                </span>
            </button>
        </div>
        <!-- Button trigger modal -->

        <!-- table -->
                <table id="dropdownTable" class="display" style="width:100%">
                <thead>
                    <tr>
                    <th>Category</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dropdowns as $dropdown)
                    <tr>
                        <td>{{ $dropdown -> category }}</td>
                        <td>{{ $dropdown -> name_value }}</td>
                        <td>{{ $dropdown -> code_format }}</td>
                        <td>
                            <a href="{{url('/masters/dropdowns/'.$dropdown->id.'/edit')}}" class="btn btn-sm btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i> edit
                            </a>
                            <form action="{{url('/masters/dropdowns/'.$dropdown->id)}}" method="post">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                </button>
                            </form>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
        <!-- table-->
    </div>
</div>

<!-- Modal Name-->
<div class="modal fade" id="myModalName" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Dropdown Item</b>
                <button type="button" class="close" data-dismiss="modal" id="closeModalDropdown" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <form action="{{url('/masters/dropdowns')}}" method="post">
            @csrf
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <label>Category</label>
                        <select name="group_incident" id="group_incident" class="form-control">
                            <option value="" selected>-- Select Category --</option>
                            @foreach ($categories as $item)
                                <option value="{{ $item->category_name }}" {{ old('category') == $item->category_name ? "selected" :""}}>{{ $item->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" name="item_name" id="item_name" class="form-control" value="{{ old('item_name') }}">
                    </div>
                    <div class="form-group">
                        <label>Code format</label>
                        <input type="text" name="code_format" id="code_format" class="form-control" value="{{ old('code_format') }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnResetModalDropdown"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- Modal Name-->
<script>
   $(document).ready(function() {
        var table = $('#dropdownTable').DataTable( {
            "order": [],
            responsive: true
        } );

        $('#btnResetModalDropdown').off('click').on('click',resetModalDropdown);
        $('#closeModalDropdown').off('click').on('click',resetModalDropdown);
        
        function resetModalDropdown() 
        {
            $('#group_incident').val('');
            $('#item_name').val('');
            $('#code_format').val('');
        }
    } );
</script>
@endsection