@extends('layout.master')
@section('content')
<div class="main-grid">
    <div class="agile-grids">	
        <!-- grids -->
        <div class="grids">
            <h2>Detail Category</h2><br>

            <!--validasi form-->
            @if (count($errors)>0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        <li><strong>Updated Data Failed !</strong></li>
                        @foreach ($errors->all() as $error)
                            <li><strong>{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>   
            @endif
            <!--end validasi form-->

            <form action="{{url('/masters/dropdowns/'.$dropdown->id)}}" method="post">
                @method('patch')
                @csrf
            <div class="panel panel-widget top-grids">
                <div class="chute chute-center">
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="category" id="category" class="form-control">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->category_name }}" 
                                                @if ($dropdown->category== $category->category_name)
                                                    selected
                                                @endif>{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group"> 
                                        <label>Item Name</label> 
                                        <input type="text" name="item_name" class="form-control" id="item_name" value="{{ $dropdown->name_value }}"> 
                                    </div>
                                    <div class="form-group"> 
                                        <label>Code</label> 
                                        <input type="text" name="code" class="form-control" id="code" value="{{ $dropdown->code_format }}"> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
         
            <br>
            <div class="bs-component mb20" align="center">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            </form>
        </div>
        <!-- //grids -->
    </div>
</div>
@endsection