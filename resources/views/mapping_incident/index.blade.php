<style>
    #mappingTable {
        border: 4px solid gray; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }

    table > thead > tr > th {
        background-color:#151A48; 
        color:white;
    }
    #btnMapping {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnMapping:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnMapping:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-footer > button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
</style>

@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <!--alert success -->
        @if (session('status'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session('status') }}</strong>
        </div> 
        @endif
        <!--alert success -->

        <!--validasi form-->
        @if (count($errors)>0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    <li><strong>Saved Data Failed !</strong></li>
                    @foreach ($errors->all() as $error)
                        <li><strong>{{ $error }}</strong></li>
                    @endforeach
                </ul>
            </div>   
        @endif
        <!--end validasi form-->

        <div class="buttons-heading">
            <h2>Master Mapping Category</h2>
        </div>
        
        <!-- Button trigger modal Name -->
        <div class="bs-component mb20">
            <button type="button" class="btn btn-primary" data-toggle="modal" id="btnMapping" data-target="#myModalName">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span class="nav-text">
                Add Item
                </span>
            </button>
        </div>
        <!-- Button trigger modal -->
        
        <!-- table-->
        <table id="mappingTable" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Group</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mappings as $item)
                <tr>
                    <td>{{ $item->category_name }}</td>
                    <td>{{ $item->name_value }}</td>
                    <td>{{ $item->name_incident }}</td>
                    <td>
                        <a href="{{url('/masters/mapping_incident/'.$item->id.'/edit')}}" class="btn btn-sm btn-info">
                            <i class="fa fa-edit" aria-hidden="true"></i> edit
                        </a>
                        <form action="{{url('/masters/mapping_incident/'.$item->id)}}" method="post">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- table-->
    </div>
</div>

<!-- Modal Name-->
<div class="modal fade" id="myModalName" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Mapping Category</b>
                <button type="button" class="close" data-dismiss="modal" id="closeModalMapping" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <div class="modal-body">
            <form action="{{url('/masters/mapping_incident')}}" method="post">
                @csrf
                <div class="form-body">
                    <div class="form-group">
                        <label>Category</label>
                        <select name="category" id="category" class="form-control">
                            <option value="">- Please Select Category -</option>
                            @foreach ($dropdowns['category'] as $data)
                                <option value="{{ $data->category_name }}" {{ old('category') == $data->category_name ? 'selected' : '' }}>{{ $data->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sub Category</label>
                        <select name="group" id="group" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Type Category</label>
                        <input type="text" name="name_incident" id="name_incident" class="form-control">
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnResetModalMapping"><i class="fa fa-refresh"></i> Reset</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
        </div>
        </form>
        </div>
    </div>
</div>
<!-- Modal Name-->
<script>
    $(document).ready(function() {
        var table = $('#mappingTable').DataTable( {
            "order": [],
            responsive: true
        } );

        $('#btnResetModalMapping').off('click').on('click',resetModalMapping);
        $('#closeModalMapping').off('click').on('click',resetModalMapping);
        
        function resetModalMapping() 
        {
            $('#category').val('');
            $('#group').val('');
            $('#name_incident').val('');
        }

    } );

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    //select incident from select group_incident
    $('select[name="category"]').on('change', function() {
        var categoryID = $(this).val();
        var url = '{{ route("mapping", ":id") }}';
        url = url.replace(':id', categoryID);
        if(categoryID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success:function(data) {

                    $('select[name="group"]').empty();
                    $('select[name="group"]').append(
                            '<option value="">- Select Group -</option>'
                    );
                    $.each(data, function(incident_data, value) {
                        $('select[name="group"]').append(
                            '<option value="'+ value.id +'">'+ value.name_value +'</option>'
                        );
                    });

                }
            });
        }else{
            $('select[name="group"]').empty();
        }
    });
</script>
@endsection