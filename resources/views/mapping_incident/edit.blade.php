@extends('layout.master')
@section('content')
<div class="main-grid">
    <div class="agile-grids">	
        <!-- grids -->
        <div class="grids">
            <h2>Detail Category</h2><br>

            <!--validasi form-->
            @if (count($errors)>0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        <li><strong>Updated Data Failed !</strong></li>
                        @foreach ($errors->all() as $error)
                            <li><strong>{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>   
            @endif
            <!--end validasi form-->

            <form action="{{url('/masters/mapping_incident/'.$mappings[0]->id)}}" method="post">
                @method('patch')
                @csrf
            <div class="panel panel-widget top-grids">
                <div class="chute chute-center">
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Category Incident</label>
                                        <select name="group_incident" id="group_incident" class="form-control">
                                            @foreach ($dropdowns as $dropdown)
                                                <option value="{{ $dropdown->id }}" 
                                                @if ($dropdown->name_value == $mappings[0]->name_value)
                                                    selected
                                                @endif>{{ $dropdown->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Type Incident</label>
                                        <input type="text" name="name_incident" id="name_incident" class="form-control" value="{{ $mappings[0]->name_incident }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
         
            <br>
            <div class="bs-component mb20" align="center">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            </form>
        </div>
        <!-- //grids -->
    </div>
</div>
@endsection