<style>
    
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <!--alert success -->
        @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{ session('status') }}</strong>
            </div> 
        @endif
        <!--alert success -->
        
        <!--validasi form-->
            @if (count($errors)>0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        <li><strong>Saved Data Failed !</strong></li>
                        @foreach ($errors->all() as $error)
                            <li><strong>{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>   
            @endif
        <!--end validasi form-->

        <div class="buttons-heading">
            <h2>History Tickets</h2>
        </div>

        <!-- table-->
            <table id="history_ticket" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Request ID</th>
                        <th>Requester</th>
                        <th>Time</th>
                        <th>Created By</th>
                        <th>Closed By</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($old_tickets as $old_ticket)
                    <tr>
                        {{-- <a href="{{url('/old_tickets/'.$old_ticket->id)}}">{{ $old_ticket->request_id }}</a> --}}
                        <td>{{ $old_ticket->request_id }}</td>
                        <td>
                            <b>{{ $old_ticket->requester }}</b><br>
                            <i>{{ $old_ticket->inquiry."/".$old_ticket->service }}</i>
                        </td>
                        <td>
                            <span>
                                <b>Created: </b><i>{{ date('d-M-Y H:i:s', strtotime($old_ticket->created_time)) }}</i><br>
                                <b>Completed: </b><i>{{ date('d-M-Y H:i:s', strtotime($old_ticket->completed_time)) }}</i><br>
                                <b>Resolved: </b><i>{{ date('d-M-Y H:i:s', strtotime($old_ticket->resolved_time)) }}</i>
                            </span>
                        </td>   
                        <td>{{ $old_ticket->cc_open_ticket }}</td>
                        <td>{{ $old_ticket->cc_close_ticket }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <!-- table-->
    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $('#history_ticket').DataTable( {
            "order": [],
            responsive: true
        });
    });
</script>
@endsection