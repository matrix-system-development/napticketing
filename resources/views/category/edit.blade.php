@extends('layout.master')
@section('content')
<div class="main-grid">
    <div class="agile-grids">	
        <!-- grids -->
        <div class="grids">
            <h2>Detail Category</h2><br>

            <!--validasi form-->
            @if (count($errors)>0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        <li><strong>Updated Data Failed !</strong></li>
                        @foreach ($errors->all() as $error)
                            <li><strong>{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>   
            @endif
            <!--end validasi form-->

            <form action="{{url('/masters/categories/'.$category->id)}}" method="post">
                @method('patch')
                @csrf
            <div class="panel panel-widget top-grids">
                <div class="chute chute-center">
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group"> 
                                        <label>Category</label> 
                                        <input type="text" name="category_name" class="form-control" id="category_name" value="{{ $category->category_name }}"> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
         
            <br>
            <div class="bs-component mb20" align="center">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            </form>
        </div>
        <!-- //grids -->
    </div>
</div>
@endsection