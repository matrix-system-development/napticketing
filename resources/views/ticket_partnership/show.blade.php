<style>
    .btnBack {
        margin-top: 30px;
        margin-bottom: 50px;
    }

    #btnHold {
        background-color: #fdba4b;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnHold:hover {
        border: 2px solid #fdba4b;
        color: #fdba4b;
        background-color: white;
    }

    #btnUnhold {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnUnhold>a {
        color: white;
    }

    #btnUnhold:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnUnhold:hover>a {
        color: #151A48;
    }

    #btnClose {
        background-color: #ee5744;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnClose:hover {
        border: 2px solid #ee5744;
        color: #ee5744;
        background-color: white;
    }

    #btnPreclose {
        background-color: #11a8bb;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnPreclose:hover {
        border: 2px solid #11a8bb;
        color: #11a8bb;
        background-color: white;
    }

    #btnReassign {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReassign:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReturn {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturn:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReturnCustCare {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturnCustCare:hover {
        border: 2px solid #151A48;
        color: #151A48;
        font-weight: 600;
        background-color: white;
    }

    #btnEditRFOHead {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnEditRFOHead:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnEditRFOStaff {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnEditRFOStaff:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnOther>a {
        margin-top: 5px;
    }

    #panelIncident>span {
        word-break: break-all;
    }

    #lineShadow {
        width: 98%;
        height: 10px;
        border: 0;
        box-shadow: 0 10px 10px -10px #8c8c8c inset;

    }

    #btnComment {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    #btnComment:hover {
        background-color: white;
        color: #151A48;
        border: 2px solid #151A48;
    }

    textarea {
        resize: none;
        border: 4px solid gray;
    }

    textarea:focus {
        border: 4px solid #151A48;
    }

    #attc1 {
        height: 100px;
        border: 4px solid gray;
    }

    #attc1:hover {
        border: 4px solid #151A48;
    }

    #panelHeader {
        background-color: #151A48;
        overflow: hidden;
    }

    #panelBasic {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #borderPanel {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblLogsAssign {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblComment {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    table>tbody>tr:hover>td {
        background-color: lightblue;
        color: black;
    }

    table>thead>tr>th {
        background-color: #151A48;
        color: white;
    }

    .modal-header {
        border-bottom: 1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }

    .form-group>.form-control:focus {
        border: 2px solid #151A48;
    }

    .modal-footer>button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }

    .modal-footer>button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }

    .modal-footer>button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }

    .modal-footer>button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnTopo_1 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_1:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnTopo_2 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_2:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnTopo_3 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_3:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnExportLogExternal {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }

    #btnExportLogExternal:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnrfo {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }

    #btnrfo:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnAttachment>a {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    .form-body>ul>li {
        color: red;
    }
</style>

@extends('layout.master')
@section('content')

<!--alert success -->
@if (session('status'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ session('status') }}</strong>
</div>
@endif
<!--alert success -->

<!--alert success -->
@if (session('statusError'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ session('statusError') }}</strong>
</div>
@endif
<!--alert success -->

<!--validasi form-->
@if (count($errors)>0)
<div class="alert alert-danger alert-dismissible" role="alert">
    <ul>
        <li><strong>Submit Failed !</strong></li>
        @foreach ($errors->all() as $error)
        <li><strong>{{ $error }}</strong></li>
        @endforeach
    </ul>
</div>
@endif
<!--end validasi form-->

<div class="btnBack">
    <div class="container-fluid">
        <div class="bs-component mb20" align="right">
            <a href="{{url()->previous()}}" class="btn btn-danger btn-md">
                <i class="fa fa-angle-double-left"></i>
                <span class="nav-text" style="font-weight: 700">
                    Back To Ticket List
                </span>
            </a>
        </div>
    </div>
</div>


{{-- <div class="panelIncident"> --}}
<div class="container-fluid">
    <div class="row mb20">
        <div class="col-md-12" align="right" id="BtnOther">
            <div class="bs-component mb20" align="center">
                @if ($ticket->status != '3' && $ticket->status != '9')    
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="btnClose" data-target="#myModalClose" data-backdrop="static" data-keyboard="false">
                        Update Status Ticket
                    </button>
                @endif

                <!-- Modal Close-->
                <div class="modal fade" id="myModalClose" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Update Status Ticket</b>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalClose" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <form action="{{ url('tickets_partnership/close/'.$ticket->id) }}" method="post" id="myform" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="modal-body">
                                    <div class="row mb40">
                                        <div class="col-md-12">
                                            <div class="demo-grid">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label>Status</label>
                                                        <select name="ticket_status" id="status" class="form-control">
                                                            <option value="">-- Select Status --</option>
                                                            <option value="8">Request to 3rd Party</option>
                                                            <option value="1">Follow Up</option>
                                                            <option value="6">Inquiry</option>
                                                            <option value="7">Feedback</option>
                                                            <option value="9">Purchase Request</option>
                                                            <option value="3">Purchase Order</option>
                                                            <option value="2">Cover</option>
                                                            <option value="4">Not Cover</option>
                                                            <option value="5">No Response</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Status Date</label>
                                                        <input type="datetime-local" name="last_status_date" id="last_status_date" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Notes</label>
                                                        <textarea name="notes" class="form-control" id="" cols="5" rows="5"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- row mb40-->

                                    {{-- Row Purchase Order --}}
                                    <div class="row mb40" id="row_purchase">
                                        <div class="col-md-12">
                                            <div class="demo-grid">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label>Best Price OTC</label>
                                                        <input type="text" name="best_price_otc" id="price_otc" class="form-control">
                                                        <span class="text-danger"> *Note: for decimal separated with (.) ex: 2,000,000.87</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Best Price MRC</label>
                                                        <input type="text" name="best_price_mrc" id="price_mrc" class="form-control">
                                                        <span class="text-danger"> *Note: for decimal separated with (.) ex: 2,000,000.87</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>RFS</label>
                                                        <input type="date" name="rfs" id="rfs" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Contract Period</label>
                                                        <input type="date" name="contract_period" id="contract_period" class="form-control">
                                                    </div>
                                                    <div class="form-group" id="input_po">
                                                        <label>No Purchase Order</label>
                                                        <input type="text" name="no_po" id="no_po" class="form-control">
                                                    </div>
                                                    <div class="form-group" id="input_pr">
                                                        <label>No Purchase Request</label>
                                                        <input type="text" name="no_pr" id="no_pr" class="form-control">
                                                    </div>
                                                    <div class="form-group" id="upload_po">
                                                        <label>Upload Purchase Order</label>
                                                        <input type="file" name="file_po" id="file_po" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- row mb40-->

                                    <!--panel-widget-->
                                </div><!-- modal body-->
                                <div class="modal-footer" align="right">    
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </form>
                        </div><!-- modal-content-->
                    </div><!-- modal dialog-->
                </div><!-- modal fade-->
                <!-- End Modal Close-->

                @if ($ticket->status == '3')
                    @if ($ticket->sub_status == null)
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="" data-target="#myModalRFS" data-backdrop="static" data-keyboard="false">
                        Update Info
                    </button>
                    @elseif ($ticket->sub_status != null)
                    @endif
                @elseif ($ticket->status == '9') 
                    @if ($ticket->sub_status == null)
                        <button type="button" class="btn btn-primary" data-toggle="modal" id="" data-target="#myModalInfo" data-backdrop="static" data-keyboard="false">
                            Update Info
                        </button>
                    @elseif ($ticket->sub_status == 'Success')
                        <button type="button" class="btn btn-primary" data-toggle="modal" id="btnClose" data-target="#myModalClose" data-backdrop="static" data-keyboard="false">
                            Update Status Ticket
                        </button>
                    @elseif ($ticket->sub_status == 'Cancel')
                    @endif
                @endif

                <!-- Modal Close-->
                {{-- Modal PO --}}
                <div class="modal fade" id="myModalRFS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Update Info</b>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalClose" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <form action="{{ url('tickets_partnership/update-rfs/'.$ticket->id) }}" method="post" id="myform" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="modal-body">
                                    <div class="row mb40">
                                        <div class="col-md-12">
                                            <div class="demo-grid">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label>RFS</label>
                                                        <input type="date" name="rfs" id="rfs" class="form-control" value="{{ $ticket->rfs }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Sub Status</label>
                                                            <select name="sub_status" id="sub_status" class="form-control">
                                                                <option value="">- Please Select Sub Status -</option>
                                                                @foreach ($dropdownsSubStatus as $item)
                                                                <option value="{{ $item->name_value }}" {{ old('sub_status') == $item->name_value ? 'selected' : '' }}>{{ $item->name_value }}</option>
                                                                @endforeach
                                                            </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No Purchase Order</label>
                                                        <input type="text" name="no_po" id="no_po" class="form-control" value="{{ $ticket->no_po }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- row mb40-->
                                    <!--panel-widget-->
                                </div><!-- modal body-->
                                <div class="modal-footer" align="right">    
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </form>
                        </div><!-- modal-content-->
                    </div><!-- modal dialog-->
                </div><!-- modal fade-->

                {{-- Modal PR --}}
                <div class="modal fade" id="myModalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Update Info</b>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalClose" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <form action="{{ url('tickets_partnership/update-rfs/'.$ticket->id) }}" method="post" id="myform" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="modal-body">
                                    <div class="row mb40">
                                        <div class="col-md-12">
                                            <div class="demo-grid">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label>RFS</label>
                                                        <input type="date" name="rfs" id="rfs" class="form-control" value="{{ $ticket->rfs }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Sub Status</label>
                                                            <select name="sub_status" id="sub_status" class="form-control">
                                                                <option value="">- Please Select Sub Status -</option>
                                                                @foreach ($dropdownsSubStatus as $item)
                                                                <option value="{{ $item->name_value }}" {{ old('sub_status') == $item->name_value ? 'selected' : '' }}>{{ $item->name_value }}</option>
                                                                @endforeach
                                                            </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No Purchase Request</label>
                                                        <input type="text" name="no_pr" id="no_pr" class="form-control" value="{{ $ticket->no_pr }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- row mb40-->
                                    <!--panel-widget-->
                                </div><!-- modal body-->
                                <div class="modal-footer" align="right">    
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </form>
                        </div><!-- modal-content-->
                    </div><!-- modal dialog-->
                </div><!-- modal fade-->
                <!-- End Modal Close-->
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="panelBasic">
        <div class="panel-body">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>No Ticket</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->no_ticket }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Status</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>
                        @if ($ticket->status == '0')
                            <span class="badge" style="background-color: green"><b>In Progress</b></span>
                        @elseif ($ticket->status == '1')
                            <span class="badge" style="background-color: green"><b>Follow Up</b></span>
                        @elseif ($ticket->status == '2')
                            <span class="badge" style="background-color: green"><b>Cover</b></span>
                        @elseif ($ticket->status == '3')
                            <span class="badge" style="background-color: green"><b>Purchase Order</b></span>
                        @elseif ($ticket->status == '4')
                            <span class="badge" style="background-color: green"><b>Not Cover</b></span>
                        @elseif ($ticket->status == '5')
                            <span class="badge" style="background-color: green"><b>No Response</b></span>
                        @elseif ($ticket->status == '6')
                            <span class="badge" style="background-color: green"><b>Inquiry</b></span>
                        @elseif ($ticket->status == '7')
                            <span class="badge" style="background-color: green"><b>Feedback</b></span>
                        @elseif ($ticket->status == '8')
                            <span class="badge" style="background-color: green"><b>Req 3rd Party</b></span>
                        @elseif ($ticket->status == '9')
                            <span class="badge" style="background-color: green"><b>Purchase Request</b></span>
                        @else
                            <span class="badge" style="background-color:black"><b>Undefined</b></span>
                        @endif
                    </span>

                    <span>
                        @if ($ticket->status == '3' || $ticket->status == '9')
                            @if ($ticket->sub_status == 'Success')
                                <span class="badge" style="background-color: green"><b>Success</b></span>
                            @elseif ($ticket->sub_status == 'Cancel')
                                <span class="badge" style="background-color: red"><b>Cancel</b></span>
                            @elseif ($ticket->sub_status == null)
                            @endif
                        @endif
                    </span>
                </div>

            </div>
        </div>
    </div>

    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Time Info</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="col-md-2">
                <span><b>Ticket Created</b></span><br><i>{{ $ticket->created_at }}</i> 
            </div>
            <div class="col-md-2">
                <span><b>Inquiry From Sales</b></span><br><i>{{ $ticket->sales_inquiry }}</i> 
            </div>
            <div class="col-md-2">
                <span><b>Request to 3rd Party</b></span><br><i>{{ $ticket->request_third_party_time }}</i> 
            </div>
            <div class="col-md-2">
                <span><b>Partnership Response time</b></span><br><i>{{ $ticket->partnership_response_time }}</i> 
            </div>
            <div class="col-md-2">
                <span><b>Feedback from 3rd party</b></span><br><i>{{ $ticket->feedback_third_party_time }}</i> 
            </div>
            <div class="col-md-2">
                <span><b>3rd Party Response time</b></span><br><i>{{ $ticket->third_party_response_time }}</i> 
            </div>
        </div>
    </div>


    @if ($ticket->status != '0' && $ticket->status != '3')
        <br>
        <div class="panel panel-primary" id="borderPanel">
            <div class="panel-heading" id="panelHeader">
                <h2 style="font-weight: 700">Last Status Info</h2>
            </div>
            <div class="panel-body" id="panelIncident">
                <div class="col-md-2">
                    <span><b>Notes</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->close_notes }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Last Status Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->last_status_date }}</span>
                </div>
            </div>
        </div>
    @elseif ($ticket->status == '3')
        <br>
        <div class="panel panel-primary" id="borderPanel">
            <div class="panel-heading" id="panelHeader">
                <h2 style="font-weight: 700">Last Status Info</h2>
            </div>
            <div class="panel-body" id="panelIncident">
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>Notes</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $ticket->close_notes }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Last Status Date</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $ticket->last_status_date }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>Best Price OTC</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{  "(".$ticket->currency.") ".number_format($ticket->best_price_otc,2,",",".")  }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Best Price MRC</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{  "(".$ticket->currency.") ".number_format($ticket->best_price_mrc,2,",",".")  }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>RFS</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $ticket->rfs }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Contract Period</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $ticket->contract_period }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>No PO</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $ticket->no_po }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>File PO</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        @if ($ticket->file_po != '')
                        <span><a href="{{  url('/tickets_partnership/'.$ticket->file_po.'/download') }}" class="btn btn-sm btn-primary"><i class="fa fa-paperclip" aria-hidden="true"></i> Download</a></span>
                        @endif 
                    </div>
                </div>
            </div>
        </div>
    @endif

    <br>

    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Detail Ticket</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Building Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->building_type }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Coordinate</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->coordinate }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>OTC</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ "(".$ticket->currency.") ".number_format($ticket->otc,2,",",".") }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>MRC</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ "(".$ticket->currency.") ".number_format($ticket->mrc,2,",",".") }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Term of Contract</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->term_of_contract }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Spesific Requirement</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->spesific_requirement }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Network Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->network_type }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Network Owner</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    @if ($ticket->network_type == 'OFFNET')
                        <span>{{ $ticket->network_owner." - ".$ticket->cid_third_party }}</span>
                    @else
                        <span>{{ $ticket->network_owner }}</span>
                    @endif
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Requestor</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->requestor }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Notes</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $ticket->notes }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Quotation File</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    @if ($ticket->quotation_file != '')
                        <span><a href="{{  url('/tickets_partnership/'.$ticket->quotation_file.'/download') }}" class="btn btn-sm btn-primary"><i class="fa fa-paperclip" aria-hidden="true"></i> Download</a></span>
                    @endif    
                </div>
            </div>
        </div>
    </div>
</div>
{{-- </div> --}}

<br>

<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Detail Customer</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>CID</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->cid }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>CID New</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->cid_new }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Customer Code</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->customer_code }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Company Name</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->company_name }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Address</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->address }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Priority</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->priority }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Company Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->company_type }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Service</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->service }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Broadband</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->broadband }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>PIC Name</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->pic_name }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>PIC Contact</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->pic_contact }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>PIC Email</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-lg-4">
                    {{ str_replace(";", "\r\n", $customer->pic_email) }}
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Capacity</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->capacity }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Capacity International</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->capacity_international }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>A-End</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->a_end }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>B-End</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->b_end }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Network Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->network_type }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Network Owner</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->network_owner }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Status Customer</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->status_customer }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Start Date Customer</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->start_date_customer }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Finish Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->finish_date }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Partner CID</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->partner_cid }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Username PPOE</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->username_ppoe }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>Password PPOE</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->password_ppoe }}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-1">
                    <span><b>Regional</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->regional }}</span>
                </div>
                <div class="col-md-1">
                    <span><b>City</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-4">
                    <span>{{ $customer->city }}</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Update Progress</h2>
        </div>
        <div class="panel-body">
            <br><br>
            <div class="bs-component mb20">
                <div class="row">
                    <form action="{{ url('tickets_partnership/addComment') }}" method="post" id="formComment" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-8">
                            <input type="hidden" name="id_ticket" value="{{ $ticket->id }}" id="id_ticket" style="">
                            <textarea name="message_comment" id="message_comment" cols="10" rows="5" placeholder=" Write a comment"></textarea>
                            <span style="color: red">*required</span>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="file" name="attc1" id="attc1" class="form-control">
                                <span style="color: red">*not required | file must : mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif | max:10048 </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <button type="submit" class="btn btn-primary btn-block" id="BtnComment"><span class="fa fa-comments"></span> Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            {{-- table log tickets  --}}
            <div class="main-grid">
                <div class="agile-grids">

                    <!-- Per Departemen-->
                    <div class="w3l-table-info">
                        <table id="tblComment" class="display">
                            <thead>
                                <tr>
                                    <th>Create by</th>
                                    <th>Message</th>
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($comments as $comment)
                                <tr id="hoverRowComment">
                                    <td><b>{{ $comment->int_emp_name }} - {{ $comment->department_name }}</b><br>
                                        <i>{{ $comment->created_at }}</i>
                                    </td>
                                    <td><b>{{ $comment->message}}</b><br>
                                        <i>{{ $comment->description }}</i>
                                    </td>
                                    <td>
                                        <a href="{{ url('/tickets_partnership/'.$comment->attachment_1.'/downloadFileComment') }}">{{ $comment->attachment_1}}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end per departemen-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Preclosed-->
<div class="modal fade" id="myModalPreclosed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Preclosed Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalPreclose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" id="user" name="user" value="">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Message (max 255 character)</label><br>
                            <textarea name="message" id="message" cols="20" rows="5" class="form-control message-preclose">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalPreclose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Hold-->

<script>
    function showhide(elem) {
        console.log(elem.getAttribute("href"));
        var div = document.getElementById(elem.getAttribute("href").replace("#", ""));

        if (div.style.display !== "none") {
            div.style.display = "none";
        } else {
            div.style.display = "block";
        }
    }

    $(document).ready(function() {
        var table = $('#tblComment').DataTable({
            "order": [],
            responsive: true
        });

        var tableLogs = $('#tblLogsAssign').DataTable({
            "order": [],
            responsive: true
        });

        $('#btnResetModalHold').off('click').on('click', resetModalHold);
        $('#closeModalHold').off('click').on('click', resetModalHold);

        function resetModalHold() {
            $('#holdtime').val('');
            $('#hold_time').val('');
            $('#category_hold').val('');
            $('.message-hold').val('');
        }

        $('#btnResetModalReturn').off('click').on('click', resetModalReturn);
        $('#closeModalReturn').off('click').on('click', resetModalReturn);

        function resetModalReturn() {
            $('.message-return').val('');
        }

        $('#btnResetModalReassign').off('click').on('click', resetModalReassign);
        $('#closeModalReassign').off('click').on('click', resetModalReassign);

        function resetModalReassign() {
            $('#department').val('');
            $('#message').val('');
        }

        $('#btnResetModalClose').off('click').on('click', resetModalClose);
        $('#closeModalClose').off('click').on('click', resetModalClose);

        function resetModalClose() {
            $('#finish_date').val('');
            $('#finish_time').val('');
            $('#baseonrfo_date').val('');
            $('#baseonrfo_time').val('');
            $('#gen_rfo').val('1');
            $('#outage_reason1').val('');
            $('#outage_sub1').val('');
            $('#outage_reason2').val('');
            $('#outage_sub2').val('');
            $('#solved_by1').val('');
            $('#solved_by2').val('');
            $('#action_close1').val('');
            $('#action_close2').val('');
            $('#outage_loc').val('');
            $('.message-close').val('');
        }

        $('#btnResetModalPreclose').off('click').on('click', resetModalPreclose);
        $('#closeModalPreclose').off('click').on('click', resetModalPreclose);

        function resetModalPreclose() {
            $('.message-preclose').val('');
        }

        $('#formComment').submit(function() {
            $("#btnComment", this)
                .html("Please Wait...")
                .attr('disabled', 'disabled');
            return true;
        });
    });

    // no issue change solved by 1
    $('#outage_reason1').click(function() {
        var id = $(this).val();
        var text = $('option:selected', this).text(); //to get selected text
        // alert(text)
        if (text == 'No Issue') {
            $('#solvedby1').show();

        } else {
            $('#solvedby1').hide();
        }

    });

    // no issue change solved by 2
    $('#outage_reason2').click(function() {
        var id = $(this).val();
        var text = $('option:selected', this).text(); //to get selected text
        // alert(text)
        if (text == 'No Issue') {
            $('#solvedby2').show();

        } else {
            $('#solvedby2').hide();
        }

    });

    //select staff from select dept
    $('select[name="department"]').on('change', function() {
        var deptID = $(this).val();
        var url = '{{ route("dept", ":id") }}';
        url = url.replace(':id', deptID);
        if (deptID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="pic_staff"]').empty();
                    $('select[name="pic_staff"]').append(
                        '<option value="all">- All Staff in Department -</option>'
                    );
                    $.each(data, function(dept_data, value) {
                        $('select[name="pic_staff"]').append(
                            '<option value="' + value.int_emp_id + '">' + value.int_emp_name + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="pic_staff"]').empty();
        }
    });

    //select sub outage from outage 1
    $('select[name="outage_reason1"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if (outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="outage_sub1"]').empty();
                    $('select[name="outage_sub1"]').append(
                        '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub1"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="outage_sub1"]').empty();
        }
    });

    //select sub outage from outage 2
    $('select[name="outage_reason2"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if (outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="outage_sub2"]').empty();
                    $('select[name="outage_sub2"]').append(
                        '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub2"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
            $('#btnComment').click(function() {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('select[name="outage_sub2"]').empty();
        }
    });

    //select sub SLR from select SLR
    $('select[name="link_respond"]').on('change', function() {
        var slrID = $(this).val();
        var url = '{{ route("slr", ":id") }}';
        url = url.replace(':id', slrID);
        if (slrID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="sub_link_respond"]').empty();
                    $('select[name="sub_link_respond"]').append(
                        '<option value="">- Select Sub Status Link Respond -</option>'
                    );
                    $.each(data, function(slr_data, value) {
                        $('select[name="sub_link_respond"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="sub_link_respond"]').empty();
        }
    });

    //select sub final category from select final category
    $('select[name="final_category"]').on('change', function() {
        var categoryID = $(this).val();
        var url = '{{ route("mappingIncident", ":id") }}';
        url = url.replace(':id', categoryID);
        if (categoryID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="sub_final_category"]').empty();
                    $('select[name="sub_final_category"]').append(
                        '<option value="">- Select Sub Category -</option>'
                    );
                    $.each(data, function(category, value) {
                        $('select[name="sub_final_category"]').append(
                            '<option value="' + value.id + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="sub_final_category"]').empty();
        }
    });

    // checkbox ticket for ca
    $('#department').on('change', function() {
        // alert($(this).val())
        if ($(this).val() == 34) {
            $('#checkbox-container').show();
        } else {
            $('#checkbox-container').hide();
        }
    });

    $(function() {
    $('#row_purchase').hide();
    $('#status').change(function(){
        if($('#status').val() == '3') {
            $('#row_purchase').show();
            $('#input_pr').hide();
            $('#input_po').show();
            $('#upload_po').show();
        } else if($('#status').val() == '9'){
            $('#row_purchase').show();
            $('#input_pr').show();
            $('#input_po').hide();
            $('#upload_po').hide();
        } else {
            $('#row_purchase').hide();
        } 
    });

    var price_otc = document.getElementById('price_otc');

    price_otc.addEventListener('keyup', function(e) {
      price_otc.value = formatCurrency(this.value, ' ');
    });

    var price_mrc = document.getElementById('price_mrc');

    price_mrc.addEventListener('keyup', function(e) {
      price_mrc.value = formatCurrency(this.value, ' ');
    });

    function formatCurrency(number, prefix) {
      var number_string = number.replace(/[^.\d]/g, '').toString(),
        split = number_string.split('.'),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

      if (ribuan) {
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
      }

      rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }
});
</script>
@endsection