@extends('layout.master')
@section('content')

<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>Create Ticket Partnership</h2>
        </div>
        <br>
        <!-- grids -->
        <div class="grids">
            <h2>Customer Information</h2>
            <!--validasi form-->
            @if (count($errors)>0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    <li><strong>Saved Data Failed !</strong></li>
                    @foreach ($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                    @endforeach
                </ul>
            </div>
            @endif
            <!--end validasi form-->
            <form action="{{ url('/tickets_partnership/store') }}" method="post" id="myform" enctype="multipart/form-data">
                @csrf
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label>Search by Name / CID / CID New</label>
                                            <input type="text" class="form-control" name='customer_search' id='customer_search' value="{{ old('customer_search') }}">
                                            <div class="form-group-append">
                                                <input class="btn btn-primary" type="button" id="reset" value="Reset Search">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label>CID</label>
                                                <input type="text" name="cid" class="form-control" id="cid" value="{{ old('cid') }}" readonly>
                                                <input type="hidden" name="id_cust" class="form-control" id="id_cust" value="{{ old('id_cust') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>CID New</label>
                                                <input type="text" name="cid_new" class="form-control" id="cid_new" value="{{ old('cid_new') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Cust Code*</label>
                                                <input type="text" name="cust_code" class="form-control" id="cust_code" value="{{ old('cust_code') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Company / Customer Name</label>
                                                <input type="text" name="company_name" class="form-control" id="company_name" value="{{ old('company_name') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Priority</label>
                                                <input type="text" name="priority" class="form-control" id="priority" value="{{ old('priority') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Company Type</label>
                                                <input type="text" name="company_type" class="form-control" id="company_type" value="{{ old('company_type') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Service</label>
                                                <input type="text" name="service" class="form-control" id="service" value="{{ old('service') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Network Owner</label>
                                                <input type="text" name="network_owner" class="form-control" id="network_owner" value="{{ old('network_owner') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <input type="text" name="start_date" class="form-control" id="start_date" value="{{ old('start_date') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>3rd Party Circuit ID*</label>
                                                <input type="text" name="partner_cid" class="form-control" id="partner_cid" value="{{ old('partner_cid') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Username PPoE</label>
                                                <input type="text" name="username_ppoe" class="form-control" id="username_ppoe" value="{{ old('username_ppoe') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Regional</label>
                                                <input type="text" name="regional" class="form-control" id="regional" value="{{ old('regional') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>AM Name</label>
                                                <input type="text" name="am_name" class="form-control" id="am_name" value="{{ old('am_name') }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label>PIC Name</label>
                                                <input type="text" name="pic_name" class="form-control" id="pic_name" value="{{ old('pic_name') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>PIC Contact*</label>
                                                <input type="text" name="pic_contact" class="form-control" id="pic_contact" value="{{ old('pic_contact') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>PIC Email*</label>
                                                <input type="text" name="pic_email" class="form-control" id="pic_email" value="{{ old('pic_email') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Capacity Local / IXX (Mbps)</label>
                                                <input type="text" name="capacity" class="form-control" id="capacity" value="{{ old('capacity') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Capacity International / IX (Mbps)</label>
                                                <input type="text" name="capacity_international" class="form-control" id="capacity_international" value="{{ old('capacity_international') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Capacity Mix</label>
                                                <input type="text" name="capacity_mix" class="form-control" id="capacity_mix" value="{{ old('capacity_mix') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>A-End</label>
                                                <input type="text" name="a_end" class="form-control" id="a_end" value="{{ old('a_end') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>B-End</label>
                                                <input type="text" name="b_end" class="form-control" id="b_end" value="{{ old('b_end') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Finish Date</label>
                                                <input type="text" name="finish_date" class="form-control" id="finish_date" value="{{ old('finish_date') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Network Type</label>
                                                <input type="text" name="network_type" class="form-control" id="network_type" value="{{ old('network_type') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Password PPoE</label>
                                                <input type="text" name="password_ppoe" class="form-control" id="password_ppoe" value="{{ old('password_ppoe') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" name="city" class="form-control" id="city" value="{{ old('city') }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>Notes</label>
                                                <input type="text" name="notes" class="form-control" id="notes" value="{{ old('notes') }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h2>Detail Ticket</h2>
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label>Building Type</label>
                                            <select name="building_type" id="building_type" class="form-control">
                                                <option value="">- Please Select Building Type -</option>
                                                @foreach ($dropdowns['building type'] as $item)
                                                <option value="{{ $item->name_value }}" {{ old('building_type') == $item->name_value ? 'selected' : '' }}>{{ $item->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Coordinate</label>
                                            <input type="text" name="coordinate" id="coordinate" class="form-control" value="{{ old('coordinate') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Currency</label>
                                            <select name="currency" id="currency" class="form-control">
                                                <option value="">- Please Select Currency -</option>
                                                @foreach ($dropdowns['currency'] as $item)
                                                <option value="{{ $item->name_value }}" {{ old('currency') == $item->name_value ? 'selected' : '' }}>{{ $item->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>OTC</label>
                                            <input type="text" name="otc" id="price" class="form-control" value="{{ old('otc') }}">
                                            <span class="text-danger"> *Note: for decimal separated with (.) ex: 2,000,000.87</span>
                                        </div>
                                        <div class="form-group">
                                            <label>MRC</label>
                                            <input type="text" name="mrc" id="price2" class="form-control" value="{{ old('mrc') }}">
                                            <span class="text-danger"> *Note: for decimal separated with (.) ex: 2,000,000.87</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Term of Contract</label>
                                            <select name="toc" id="toc" class="form-control" onchange="showValOther(this)">
                                                <option value="">- Please Select Term of Contract -</option>
                                                @foreach ($dropdowns['toc'] as $item)
                                                <option value="{{ $item->name_value }}" {{ old('toc') == $item->name_value ? 'selected' : '' }}>{{ $item->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="value_other" style="display:none;">
                                            <div class="form-inline">
                                                <input type="text" name="value_toc" id="value_toc" class="form-control">
                                                <select name="unit_toc" id="unit_toc" class="form-control">
                                                    <option value="days">Days</option>
                                                    <option value="month">Month</option>
                                                </select>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="form-group">
                                            <label>Spesific Requirements</label>
                                            <input type="text" name="requirement" id="requirement" class="form-control" value="{{ old('requirement') }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Type</label> 
                                            <select name="net_type" id="net_type" class="form-control" onchange="showValCIDThird(this)">
                                                <option value="">- Please Select Network Type -</option>
                                                @foreach ($dropdowns['network type'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ old('net_type') == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Owner</label> 
                                            <select name="net_owner" id="net_owner" class="form-control">
                                            </select>
                                        </div>
                                        <div id="value_cid_third" style="display:none;">
                                            <div class="form-group">
                                                <label>CID 3rd Party</label> 
                                                <input type="text" name="cid_third_party" id="cid_third_party" class="form-control">
                                            </div>
                                            <br>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Requestor</label> 
                                            <select name="requestor" id="requestor" class="form-control">
                                                <option value="">- Please Select Requestor -</option>
                                                @foreach ($dataSales as $sales)
                                                    <option value="{{ $sales->name }}" @if (old('requestor') == $sales->name) {{ 'selected' }} @endif>{{ $sales->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Inquiry From Sales</label>
                                            <input type="datetime-local" class="form-control" id="inquiry_date" name="inquiry_date" value="{{ old('inquiry_date') }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Upload Quotation</label>
                                            <input type="file" class="form-control" id="quotation_file" name="quotation_file" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Notes</label> 
                                            <textarea name="notes" class="form-control" id="notes" cols="20" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bs-component mb20" align="center">
                    <button type="submit" id="btnSubmitExternal" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
        <!-- //grids -->
    </div>
</div>

<script type="text/javascript">
    function showValOther(select){
       if(select.value == 'Other'){
        document.getElementById('value_other').style.display = "block";
       } else{
        document.getElementById('value_other').style.display = "none";
       }
    }
    function showValCIDThird(select){
       if(select.value == 'OFFNET'){
        document.getElementById('value_cid_third').style.display = "block";
       } else{
        document.getElementById('value_cid_third').style.display = "none";
       }
    }
</script>

<script type="text/javascript">
    // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function() {
        //customer search
        $("#customer_search").autocomplete({

            source: function(request, response) {
                // Fetch data
                $.ajax({
                    url: "{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                        _token: CSRF_TOKEN,
                        search: request.term
                    },
                    success: function(data) {

                        response(data);

                    }
                });
            },
            select: function(event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.label); // display the selected text
                $('#id_cust').val(ui.item.value_id);
                $('#cid').val(ui.item.value_cid); // save selected id to input
                $('#cust_code').val(ui.item.value_custcode);
                $('#company_name').val(ui.item.value_compname); // save selected id to input
                $('#priority').val(ui.item.value_priority);
                $('#company_type').val(ui.item.value_comptype);
                $('#service').val(ui.item.value_service);
                $('#pic_name').val(ui.item.value_picname);
                $('#pic_contact').val(ui.item.value_piccontact);
                $('#pic_email').val(ui.item.value_picemail);
                $('#capacity').val(ui.item.value_capacity);
                $('#capacity_international').val(ui.item.value_capacityInt);
                $('#capacity_mix').val(ui.item.value_capacitymix);
                $('#am_name').val(ui.item.value_am);
                $('#a_end').val(ui.item.value_a_end);
                $('#b_end').val(ui.item.value_b_end);
                $('#network_type').val(ui.item.value_nettype);
                $('#network_owner').val(ui.item.value_netowner);
                $('#start_date').val(ui.item.value_startdate);
                $('#finish_date').val(ui.item.value_finishdate);
                $('#partner_cid').val(ui.item.value_partnercid);
                $('#username_ppoe').val(ui.item.value_usernameppoe);
                $('#password_ppoe').val(ui.item.value_passwordppoe);
                $('#regional').val(ui.item.value_regional);
                $('#city').val(ui.item.value_city);
                $('#cid_new').val(ui.item.value_cid_new);
                $('#notes').val(ui.item.value_notes);
                return false;
            },

        });

        //select network owner from network type
        $('select[name="net_type"]').on('change', function() {
                var categoryID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', categoryID);
                if(categoryID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="net_owner"]').empty();
                            $('select[name="net_owner"]').append(
                                    '<option value="">- Select Network Owner -</option>'
                            );
                            $.each(data, function(category, value) {
                                $('select[name="net_owner"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="net_owner"]').empty();
                }
            });

        //select sub category from select category
        $('select[name="category"]').on('change', function() {
            var categoryID = $(this).val();
            var url = '{{ route("mappingIncident", ":id") }}';
            url = url.replace(':id', categoryID);
            if (categoryID) {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {

                        $('select[name="sub_category"]').empty();
                        $('select[name="sub_category"]').append(
                            '<option value="">- Select Sub Category -</option>'
                        );
                        $.each(data, function(category, value) {
                            $('select[name="sub_category"]').append(
                                '<option value="' + value.id + '">' + value.name_incident + '</option>'
                            );
                        });

                    }
                });
            } else {
                $('select[name="sub_category"]').empty();
            }
        });

        var price = document.getElementById('price');

        price.addEventListener('keyup', function(e) {
            price.value = formatCurrency(this.value, ' ');
        });

        var price2 = document.getElementById('price2');

        price2.addEventListener('keyup', function(e) {
            price2.value = formatCurrency(this.value, ' ');
        });

        function formatCurrency(number, prefix) {
        var number_string = number.replace(/[^.\d]/g, '').toString(),
            split = number_string.split('.'),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }

        //select sub SLR from select SLR
        $('select[name="link_respond"]').on('change', function() {
            var slrID = $(this).val();
            var url = '{{ route("slr", ":id") }}';
            url = url.replace(':id', slrID);
            if (slrID) {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {

                        $('select[name="sub_link_respond"]').empty();
                        $('select[name="sub_link_respond"]').append(
                            '<option value="">- Select Sub Status Link Respond -</option>'
                        );
                        $.each(data, function(slr_data, value) {
                            $('select[name="sub_link_respond"]').append(
                                '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                            );
                        });

                    }
                });
            } else {
                $('select[name="sub_link_respond"]').empty();
            }
        });

        //reset search customer
        $('#reset').click(function() {
            $(':input', '#myform')
                .not(':button, :submit, :reset, :hidden')
            $('#customer_search').val('');
            $('#cid').val('');
            $('#company_name').val('');
            $('#priority').val('');
            $('#company_type').val('');
            $('#service').val('');
            $('#pic_name').val('');
            $('#pic_contact').val('');
            $('#capacity').val('');
            $('#capacity_international').val('');
            $('#capacity_mix').val('');
            $('#am_name').val('');
            $('#a_end').val('');
            $('#b_end').val('');
            $('#network_type').val('');
            $('#network_owner').val('');
            $('#start_date').val('');
            $('#finish_date').val('');
            $('#partner_cid').val('');
            $('#username_ppoe').val('');
            $('#password_ppoe').val('');
            $('#regional').val('');
            $('#city').val('');
            $('#cid_new').val('');
            $('#notes').val('');
        });

        $('#myform').submit(function() {
            $("#btnSubmitExternal", this).html("Please Wait...").attr('disabled', 'disabled');
            return true;
        });

        // checkbox ticket for ca
        $('#department').on('change', function() {
            // alert($(this).val())
            if ($(this).val() == 34) {
                $('#checkbox-container').show();
            } else {
                $('#checkbox-container').hide();
            }
        });
    });
</script>
@endsection