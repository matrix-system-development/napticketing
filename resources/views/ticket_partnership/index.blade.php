<style>
    #tracking {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    thead>tr>th {
        background-color: #151A48;
        color: white;
    }

    table>tbody>tr:hover>td {
        background-color: lightblue;
        color: black;
    }

    #btnExportExcel {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnExportExcel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnExportExcel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnFilterPeriod:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    .modal-header {
        border-bottom: 1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }

    .modal-footer>button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    .modal-footer>button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
    }

    .form-group>.form-control:focus {
        border: 2px solid #151A48;
    }
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>Tickets Tracking Partnership</h2>
        </div>
        <!--validasi form-->
        @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                <li><strong>Export Data Failed !</strong></li>
                @foreach ($errors->all() as $error)
                <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>
        @endif
        <!--end validasi form-->

        <!--alert success -->
        @if (session('status'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session('status') }}</strong>
        </div>
        @endif
        <!--alert success -->
        <!-- Button trigger modal-->
        <center>
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnExportExcel" data-toggle="modal" data-target="#myModalExport" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Export Tickets to Excel
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            <!-- Button trigger modal-->
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnFilterPeriod" data-toggle="modal" data-target="#myModalPeriod" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Filter Ticket Period
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            Ticket Period From <b><i>{{ date('d F Y', strtotime($date_start2)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_now)) }}</i></b>
        </center>

        <table id="tracking" class="display" style="width: 100%">
            <thead>
                <tr>
                    <th>Info</th>
                    <th>Status</th>
                    <th>Network Info</th>
                    <th>Requestor</th>
                    <th>Created By</th>
                    <th>Created At</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tickets as $ticket)
                <tr>
                    <td>
                        <b>No Ticket: </b><a href="{{url('/tickets_partnership/'.encrypt($ticket->id))}}"><i>{{ $ticket->no_ticket }}</i></a><br>
                        <b>Customer: </b><i>{{ $ticket->company_name }}</i></a>
                    </td>
                    <td>
                        @if ($ticket->status == '0')
                            <span class="badge" style="background-color: green"><b>In Progress</b></span>
                        @elseif ($ticket->status == '1')
                            <span class="badge" style="background-color: green"><b>Follow Up</b></span>
                        @elseif ($ticket->status == '2')
                            <span class="badge" style="background-color: green"><b>Cover</b></span>
                        @elseif ($ticket->status == '3')
                            <span class="badge" style="background-color: green"><b>Purchase Order</b></span>
                        @elseif ($ticket->status == '4')
                            <span class="badge" style="background-color: green"><b>Not Cover</b></span>
                        @elseif ($ticket->status == '5')
                            <span class="badge" style="background-color: green"><b>No Response</b></span>
                        @elseif ($ticket->status == '6')
                            <span class="badge" style="background-color: green"><b>Inquiry</b></span>
                        @elseif ($ticket->status == '7')
                            <span class="badge" style="background-color: green"><b>Feedback</b></span>
                        @elseif ($ticket->status == '8')
                            <span class="badge" style="background-color: green"><b>Req 3rd Party</b></span>
                        @elseif ($ticket->status == '9')
                            <span class="badge" style="background-color: green"><b>Purchase Request</b></span>
                        @else
                            <span class="badge" style="background-color:black"><b>Undefined</b></span>
                        @endif
                        <br>
                        @if ($ticket->status != '0')
                            Last Update at {{ $ticket->last_status_date }}
                        @endif
                    </td>
                    <td>
                        <b>{{ $ticket->network_type }}</b><br>{{ $ticket->network_owner }}
                    </td>
                    <td>
                        <b>{{ $ticket->requestor }}</b><br>
                        Inquiry at {{ $ticket->sales_inquiry }}
                    </td>
                    <td>{{ $ticket->int_emp_name }}</td>
                    <td>{{ $ticket->created_at }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal Export-->
<div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Export to Excel (Filter by Created Date)</b>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/tickets_partnership/export')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        {{-- <div class="form-group">
                            <label>Ticket Type</label>
                            <select name="type" id="type" class="form-control">
                                <option value="all" selected>All</option>
                                @foreach ($dropdowns['ticket_type'] as $ticket_type )
                                <option value="{{ $ticket_type->ticket_type }}">{{ $ticket_type->ticket_type }}</option>
                                @endforeach
                            </select>
                        </div> --}}
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" id="date_start" name="date_start" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Until</label>
                            <input type="date" id="date_finish" name="date_finish" class="form-control">
                        </div>
                        {{-- <div class="form-group">
                            <label>Report Type</label>
                            <select name="report_type" id="report_type" class="form-control">
                                <option value="" selected>-- Choose Report Type --</option>
                                <option value="summary_type">Tickets</option>
                                <option value="logAssign_type">Logs Ticket</option>
                            </select>
                        </div> --}}
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnResetModalExport" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Export Now</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Export-->

<!-- Modal Period-->
<div class="modal fade" id="myModalPeriod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Ticket Period (Filter by Ticket Created Date)</b>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/tickets_partnership')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" id="date_start" name="date_start" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Until</label>
                            <input type="date" id="date_finish" name="date_finish" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <a href="{{url('/tickets_partnership')}}" class="btn btn-primary">Last 2 Month</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Submit Period</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Period-->

<script>
    $(document).ready(function() {
        var table = $('#tracking').DataTable({
            // order dipakai untuk mengurutkan data table
            "order": [],
            responsive: true,
        });

        $('#btnResetModalExport').off('click').on('click', resetExport);

        function resetExport() {
            $('#type').val('all');
            $('#date_start').val('');
            $('#date_finish').val('');
            $('#report_type').val('');
        }
        // $('#btnResetModalExport').off('click').on('click',resetExport);
    });

    $('#btnResetModalExport').off('click').on('click', resetExport);

    function resetExport() {
        $('#type').val('all');
        $('#date_start').val('');
        $('#date_finish').val('');
        $('#report_type').val('');
    }

    console.log(resetExport);
</script>
@endsection