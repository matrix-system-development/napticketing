<style>
    /* span {
        color: white;
        font-weight: 700;
    }

    .subnav-text {
        color: white;
        font-weight: 700;
    } */
</style>
<nav class="main-menu">
    @if(auth()->user()->role == 'Super Admin')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
        {{-- <li>
                <a href="{{url('/dashboard')}}">
        <i class="fa fa-bar-chart nav_icon"></i>
        <span class="nav-text">
            Dashboard
        </span>
        </a>
        </li> --}}
        <li class="has-subnav">
            <a href="javascript:;">
                <i class="icon-table nav-icon"></i>
                <span class="nav-text">Tickets Tracking</span>
                <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li>
                    <a class="subnav-text" href="{{url('/summary/matrix')}}">
                        Matrix
                    </a>
                </li>
                {{-- <li>
                    <a class="subnav-text" href="{{url('/tickets')}}">
                        Matrix
                        </a>
                </li> --}}
                <li>
                    <a class="subnav-text" href="{{url('/tickets_fs')}}">
                        FiberStar
                    </a>
                </li>
                <li>
                    <a class="subnav-text" href="{{url('/selfcare')}}">
                        Matrix Self Care
                    </a>
                </li>
            </ul>
        </li>
    <li>
        <a href="{{url('/old_tickets')}}">
            <i class="icon-table nav-icon nav_icon"></i>
            <span class="nav-text">
                Tickets History
            </span>
        </a>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="fa fa-file-text-o nav_icon"></i>
            <span class="nav-text">Create Ticket</span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            <li>
                <a class="subnav-text" href="{{url('/tickets_internal/create')}}">
                    Internal
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets_internal_maintenance/create')}}">
                    Internal Maintenance
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets/create')}}">
                    Incident or Request
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets_maintenance/create')}}">
                    3rd party maintenance
                </a>
            </li>
            {{-- <li>
                        <a class="subnav-text" href="{{url('/tickets_fs/create')}}">
            FiberStar
            </a>
    </li> --}}
    </ul>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="nav-text">
                Check ONT
            </span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            {{-- <li>
                        <a class="subnav-text" href="{{url('')}}">
            Matrix
            </a>
    </li> --}}
    <li>
        <a class="subnav-text" href="{{url('/ont-fs')}}">
            FiberStar
        </a>
    </li>
    </ul>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="icon-table nav-icon"></i>
            <span class="nav-text">Logs</span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            <li>
                <a class="subnav-text" href="{{url('/Log/audit-log/')}}">
                    Audit Logs
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/Log/api-log/')}}">
                    API Logs
                </a>
            </li>
        </ul>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="nav-text">
                Master Configuration
            </span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            <li>
                <a class="subnav-text" href="{{url('/masters/categories')}}">
                    Category
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/masters/dropdowns')}}">
                    Dropdowns
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/masters/mapping_incident')}}">
                    Mapping Category
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/masters/rule')}}">
                    App Rules
                </a>
            </li>
        </ul>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="fa fa-list-ul" aria-hidden="true"></i>
            <span class="nav-text">
                Master Data
            </span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            <li>
                <a class="subnav-text" href="{{url('/masters/topology')}}">
                    Topology
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/masters/users')}}">
                    User
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/masters/customers')}}">
                    Customers
                </a>
            </li>
        </ul>
    </li>
    </ul>
    @endif

    @if (auth()->user()->role == 'User')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
        {{-- <li>
                <a href="{{url('/dashboard')}}">
        <i class="fa fa-bar-chart nav_icon"></i>
        <span class="nav-text">
            Dashboard
        </span>
        </a>
        </li> --}}
        <li>
            <a href="{{url('/tickets')}}">
                <i class="icon-table nav-icon nav_icon"></i>
                <span class="nav-text">
                    Tickets Tracking
                </span>
            </a>
        </li>
        {{-- <li>
            <a class="subnav-text" href="{{url('/summary/matrix')}}">
        Matrix new
        </a>
        </li> --}}
        <li class="has-subnav">
            <a href="javascript:;">
                <i class="fa fa-file-text-o nav_icon"></i>
                <span class="nav-text">Create Ticket</span>
                <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li>
                    <a class="subnav-text" href="{{url('/tickets_internal/create')}}">
                        Internal
                    </a>
                </li>
                @if (auth()->user()->id_department == '16' || auth()->user()->id_department == '17' || auth()->user()->id_department == '18' || auth()->user()->id_department == '19' || auth()->user()->id_department == '20' || auth()->user()->id_department == '21' || auth()->user()->id_department == '22' || auth()->user()->id_department == '23' || auth()->user()->id_department == '37' )
                <li>
                    <a class="subnav-text" href="{{url('/tickets_internal_maintenance/create')}}">
                        Internal Maintenance
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @if (auth()->user()->id_department == '20'||auth()->user()->id_department == '18'||auth()->user()->id_department == '2')
        <li>
            <a href="{{url('/masters/topology')}}">
                <i class="fa fa-cogs"></i>
                <span class="nav-text">
                    Master Topology
                </span>
            </a>
        </li>
        @endif
    </ul>
    @endif

    @if (auth()->user()->role == 'Partnership')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
        <li class="has-subnav">
            <a href="javascript:;">
                <i class="icon-table nav-icon"></i>
                <span class="nav-text">Tickets Tracking</span>
                <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li>
                    <a class="subnav-text" href="{{url('/tickets')}}">
                        Internal
                    </a>
                </li>
                <li>
                    <a class="subnav-text" href="{{url('/tickets_partnership')}}">
                        Partnership
                    </a>
                </li>
            </ul>
        </li>
        <li class="has-subnav">
            <a href="javascript:;">
                <i class="fa fa-file-text-o nav_icon"></i>
                <span class="nav-text">Create Ticket</span>
                <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li>
                    <a class="subnav-text" href="{{url('/tickets_internal/create')}}">
                        Internal
                    </a>
                </li>
                <li>
                    <a class="subnav-text" href="{{url('/tickets_partnership/create')}}">
                        Partnership
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{url('/masters/customers')}}">
                <i class="fa fa-cogs"></i>
                <span class="nav-text">
                    Master Customers
                </span>
            </a>
        </li>
    </ul>
    @endif

    @if (auth()->user()->role == 'Customer Care')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
        {{-- <li>
                <a href="{{url('/dashboard')}}">
        <i class="fa fa-bar-chart nav_icon"></i>
        <span class="nav-text">
            Dashboard
        </span>
        </a>
        </li> --}}
        <li class="has-subnav">
            <a href="javascript:;">
                <i class="icon-table nav-icon"></i>
                <span class="nav-text">Tickets Tracking</span>
                <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li>
                    <a class="subnav-text" href="{{url('/summary/matrix')}}">
                        Matrix
                    </a>
                </li>
                <li>
                    <a class="subnav-text" href="{{url('/tickets_fs')}}">
                        FiberStar
                    </a>
                </li>
                <li>
                    <a class="subnav-text" href="{{url('/selfcare')}}">
                        Matrix Self Care
                    </a>
                </li>
                @if (auth()->user()->functional == 'Customer Care Dept Head | Dept Head Property & Partnership (Pjs)')
                <li>
                    <a class="subnav-text" href="{{url('/tickets_partnership')}}">
                        Partnership
                    </a>
                </li>
                @endif
            </ul>
        </li>
    <li>
        <a href="{{url('/old_tickets')}}">
            <i class="icon-table nav-icon nav_icon"></i>
            <span class="nav-text">
                Tickets History
            </span>
        </a>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="fa fa-file-text-o nav_icon"></i>
            <span class="nav-text">Create Ticket</span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            <li>
                <a class="subnav-text" href="{{url('/tickets_internal/create')}}">
                    Internal
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets_internal_maintenance/create')}}">
                    Internal Maintenance
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets/create')}}">
                    Incident or Request
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets_maintenance/create')}}">
                    Maintenance 3rd Party
                </a>
            </li>
            <li>
                <a class="subnav-text" href="{{url('/tickets_partnership/create')}}">
                    Partnership
                </a>
            </li>
            {{-- GAK PERLU --}}
            {{-- <li>
                        <a class="subnav-text" href="{{url('/tickets_fs/create')}}">
            FiberStar
            </a>
    </li> --}}
    </ul>
    </li>
    <li class="has-subnav">
        <a href="javascript:;">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="nav-text">
                Check ONT
            </span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
        </a>
        <ul>
            {{-- <li>
                        <a class="subnav-text" href="{{url('')}}">
            Matrix
            </a>
    </li> --}}
    <li>
        <a class="subnav-text" href="{{url('/ont-fs')}}">
            FiberStar
        </a>
    </li>
    </ul>
    </li>
    </ul>
    @endif

    @if (auth()->user()->role == 'BOD')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
        {{-- <li>
                <a href="{{url('/dashboard')}}">
        <i class="fa fa-bar-chart nav_icon"></i>
        <span class="nav-text">
            Dashboard
        </span>
        </a>
        </li> --}}
        <li>
            <a href="{{url('/summary/matrix')}}">
                <i class="icon-table nav-icon nav_icon"></i>
                <span class="nav-text">
                    Tickets Tracking
                </span>
            </a>
        </li>
    </ul>
    @endif

    @if (auth()->user()->role == 'Guest')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
    </ul>
    @endif

    @if (auth()->user()->role == 'Sales Admin')
    <ul>
        <li>
            <a href="{{url('/home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                    Home
                </span>
            </a>
        </li>
        {{-- <li>
                <a href="{{url('/dashboard')}}">
        <i class="fa fa-bar-chart nav_icon"></i>
        <span class="nav-text">
            Dashboard
        </span>
        </a>
        </li> --}}
        <li>
            <a href="{{url('/tickets')}}">
                <i class="icon-table nav-icon nav_icon"></i>
                <span class="nav-text">
                    Tickets Tracking
                </span>
            </a>
        </li>
        <li>
            <a href="{{url('/tickets_internal/create')}}">
                <i class="fa fa-file-text-o nav_icon"></i>
                <span class="nav-text">
                    Create Ticket Internal
                </span>
            </a>
        </li>
        <li>
            <a href="{{url('/masters/customers')}}">
                <i class="fa fa-cogs"></i>
                <span class="nav-text">
                    Master Customers
                </span>
            </a>
        </li>
    </ul>
    @endif



    <!--dimatikan kalau pakai SSO-->
    <ul class="logout">
        <li>
            <a href="{{ url('/logout') }}">
                <i class="icon-off nav-icon"></i>
                <span class="nav-text">
                    Logout
                </span>
            </a>
        </li>
    </ul>

</nav>