<style>
    #tracking {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    thead>tr>th {
        background-color: #151A48;
        color: white;
    }

    table>tbody>tr:hover>td {
        background-color: lightblue;
        color: black;
    }

    #btnCategory {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnExportExcel {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnExportExcel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnExportExcel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnFilterPeriod:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    .modal-header {
        border-bottom: 1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }

    .modal-footer>button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    .modal-footer>button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
    }

    .form-group>.form-control:focus {
        border: 2px solid #151A48;
    }
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>Tickets Summary Matrix</h2>
        </div>
        <!--validasi form-->
        @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                <li><strong>Export Data Failed !</strong></li>
                @foreach ($errors->all() as $error)
                <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>
        @endif
        <!--end validasi form-->

        <!--alert success -->
        @if (session('status'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session('status') }}</strong>
        </div>
        @endif
        <!--alert success -->
        <!-- Button trigger modal-->
        <center>
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnExportExcel" data-toggle="modal" data-target="#myModalExport" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Export Tickets to Excel
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            <!-- Button trigger modal-->
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnFilterPeriod" data-toggle="modal" data-target="#myModalPeriod" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Filter Ticket Period
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            Ticket Period From <b><i>{{ date('d F Y', strtotime($date_start2)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_now)) }}</i></b>
        </center>

        <table id="summary" class="display" style="width: 100%">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Total</th>
                    <th>In Progress</th>
                    <th>Hold</th>
                    <th>Unassigned</th>
                    <th>Closed</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($summary as $data)
                <tr>
                    <td>
                        <button type="button" class="btn btn-primary" id="btnCategory">
                            {{ $data->category }}
                            @if ($data->new_ticket > 0)
                            <span class="badge badge-light">{{ $data->new_ticket }}</span>
                            @endif
                        </button>
                    </td>
                    <td><a href="{{ url('/summary/matrix/total/'.$data->category.'/'.date('Y-m-d', strtotime($date_start2)).'/'.date('Y-m-d', strtotime($date_now))) }}" class="badge badge-pill bg-system">{{ $data->total }}</a></td>
                    <td><a href="{{ url('/summary/matrix/progress/'.$data->category.'/'.date('Y-m-d', strtotime($date_start2)).'/'.date('Y-m-d', strtotime($date_now))) }}" class="badge badge-pill bg-success">{{ $data->in_progress }}</a></td>
                    <td><a href="{{ url('/summary/matrix/hold/'.$data->category.'/'.date('Y-m-d', strtotime($date_start2)).'/'.date('Y-m-d', strtotime($date_now))) }}" class="badge badge-pill bg-alert">{{ $data->hold }}</a></td>
                    <td><a href="{{ url('/summary/matrix/unassign/'.$data->category.'/'.date('Y-m-d', strtotime($date_start2)).'/'.date('Y-m-d', strtotime($date_now))) }}" class="badge badge-pill bg-system">{{ $data->unassigned }}</a></td>
                    <td><a href="{{ url('/summary/matrix/closed/'.$data->category.'/'.date('Y-m-d', strtotime($date_start2)).'/'.date('Y-m-d', strtotime($date_now))) }}" class="badge badge-pill bg-danger">{{ $data->closed }}</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal Export-->
<div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Export to Excel (Filter by Created Date)</b>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/tickets/exportSummary')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Ticket Type</label>
                            <select name="type" id="type" class="form-control">
                                <option value="all" selected>All</option>
                                @foreach ($dropdowns['ticket_type'] as $ticket_type )
                                <option value="{{ $ticket_type->ticket_type }}">{{ $ticket_type->ticket_type }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" id="date_start" name="date_start" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Until</label>
                            <input type="date" id="date_finish" name="date_finish" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Report Type</label>
                            <select name="report_type" id="report_type" class="form-control">
                                <option value="" selected>-- Choose Report Type --</option>
                                <option value="summary_type">Tickets</option>
                                <option value="logAssign_type">Logs Ticket</option>
                                <option value="logComment_type">Logs Ticket Journey</option>
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnResetModalExport" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Export Now</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Export-->

<!-- Modal Period-->
<div class="modal fade" id="myModalPeriod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Ticket Period (Filter by Ticket Created Date)</b>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/summary/matrix/filter')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" id="date_start" name="date_start" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Until</label>
                            <input type="date" id="date_finish" name="date_finish" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <a href="{{url('/summary/matrix')}}" class="btn btn-primary">Last 2 Month</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Submit Period</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Period-->

<script>
    $(document).ready(function() {
        var table = $('#summary').DataTable({
            // order dipakai untuk mengurutkan data table
            "order": [],
            responsive: true,
        });

        $('#btnResetModalExport').off('click').on('click', resetExport);

        function resetExport() {
            $('#type').val('all');
            $('#date_start').val('');
            $('#date_finish').val('');
            $('#report_type').val('');
        }
        // $('#btnResetModalExport').off('click').on('click',resetExport);
    });

    $('#btnResetModalExport').off('click').on('click', resetExport);

    function resetExport() {
        $('#type').val('all');
        $('#date_start').val('');
        $('#date_finish').val('');
        $('#report_type').val('');
    }

    console.log(resetExport);
</script>
@endsection