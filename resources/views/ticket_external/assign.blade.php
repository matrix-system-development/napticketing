<!doctype html>
<html lang="en">
  <head>
    <title>Napticketing</title>
  </head>
  <body>
    <span>
        Dear {{ $data['assign_to'] }}, <br><br> 

        Please help this ticket with detail: <br><br>
        <b>No Ticket</b>  : {{ $data['no_ticket'] }}<br>

        @if($data['cust_name'] == null)
            
        @else
            <b>Customer Name</b>  : {{ $data['cust_name'] }}<br>
        @endif

        <b>Status Complain</b>    : {{ $data['status_complain'] }}<br>
        <b>Category</b>  : {{ $data['category'] }}<br>
        <b>Sub Category</b>    : {{ $data['subcategory'] }}<br>
        <b>Status Link Reported</b>    : {{ $data['link_respond'] }}<br>
        <b>Notes Ticket</b>    : {{ $data['notes'] }}<br>
        <b>Notes Re-Assign</b>    : {{ $data['message'] }}<br>
        <br>
        <br>
        Thank you,<br>
        Regards <br>
        [{{ $data['created_by'] }}]
        <br>
        <br>
    </span>
  </body>
</html>
