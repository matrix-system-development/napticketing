<!doctype html>
<html lang="en">
  <head>
    <title>Napticketing</title>
  </head>
  <body>
    <span>
        Dear {{ $data['assign_to'] }}, <br><br> 

        This ticket has been closed: <br><br>
        <b>No Ticket</b>  : {{ $data['no_ticket'] }}<br>

        @if($data['cust_name'] == null)
            
        @else
            <b>Customer Name</b>  : {{ $data['cust_name'] }}<br>
        @endif

        <b>Status Complain</b>    : {{ $data['status_complain'] }}<br>
        <b>Category Ticket</b>  : {{ $data['category'] }}<br>
        <b>Sub Category Ticket</b>    : {{ $data['subcategory'] }}<br>
        <b>Status Link Reported</b>    : {{ $data['link_respond'] }}<br>
        <b>Close Ticket Time</b>    : {{ $data['closed_date'] }}<br>
        <b>Duration</b>    : {{ $data['duration'] }}<br>
        <br>
        <br>
        Thank you,<br>
        Regards <br>
        [{{ $data['created_by'] }}]
        <br>
        <br>
    </span>
  </body>
</html>
