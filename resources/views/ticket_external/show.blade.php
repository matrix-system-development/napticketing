<style>
    .btnBack {
        margin-top: 30px;
        margin-bottom: 50px;
    }

    #btnHold {
        background-color: #fdba4b;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnHold:hover {
        border: 2px solid #fdba4b;
        color: #fdba4b;
        background-color: white;
    }

    #btnUnhold {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnUnhold>a {
        color: white;
    }

    #btnUnhold:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnUnhold:hover>a {
        color: #151A48;
    }

    #btnClose {
        background-color: #ee5744;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnClose:hover {
        border: 2px solid #ee5744;
        color: #ee5744;
        background-color: white;
    }

    #btnPreclose {
        background-color: #11a8bb;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnPreclose:hover {
        border: 2px solid #11a8bb;
        color: #11a8bb;
        background-color: white;
    }

    #btnReassign {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReassign:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReturn {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturn:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnReturnCustCare {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturnCustCare:hover {
        border: 2px solid #151A48;
        color: #151A48;
        font-weight: 600;
        background-color: white;
    }

    #btnEditRFOHead {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnEditRFOHead:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnEditRFOStaff {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnEditRFOStaff:hover {
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }

    #btnOther>a {
        margin-top: 5px;
    }

    #panelIncident>span {
        word-break: break-all;
    }

    #lineShadow {
        width: 98%;
        height: 10px;
        border: 0;
        box-shadow: 0 10px 10px -10px #8c8c8c inset;

    }

    #btnComment {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    #btnComment:hover {
        background-color: white;
        color: #151A48;
        border: 2px solid #151A48;
    }

    textarea {
        resize: none;
        border: 4px solid gray;
    }

    textarea:focus {
        border: 4px solid #151A48;
    }

    #attc1 {
        height: 100px;
        border: 4px solid gray;
    }

    #attc1:hover {
        border: 4px solid #151A48;
    }

    #panelHeader {
        background-color: #151A48;
        overflow: hidden;
    }

    #panelBasic {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #borderPanel {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblLogsAssign {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblComment {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    table>tbody>tr:hover>td {
        background-color: lightblue;
        color: black;
    }

    table>thead>tr>th {
        background-color: #151A48;
        color: white;
    }

    .modal-header {
        border-bottom: 1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }

    .form-group>.form-control:focus {
        border: 2px solid #151A48;
    }

    .modal-footer>button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }

    .modal-footer>button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }

    .modal-footer>button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }

    .modal-footer>button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnTopo_1 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_1:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnTopo_2 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_2:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnTopo_3 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }

    #btnTopo_3:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnExportLogExternal {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }

    #btnExportLogExternal:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnrfo {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }

    #btnrfo:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnAttachment>a {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    .form-body>ul>li {
        color: red;
    }
</style>

@extends('layout.master')
@section('content')

<!--alert success -->
@if (session('status'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ session('status') }}</strong>
</div>
@endif
<!--alert success -->

<!--alert success -->
@if (session('statusError'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ session('statusError') }}</strong>
</div>
@endif
<!--alert success -->

<!--validasi form-->
@if (count($errors)>0)
<div class="alert alert-danger alert-dismissible" role="alert">
    <ul>
        <li><strong>Submit Failed !</strong></li>
        @foreach ($errors->all() as $error)
        <li><strong>{{ $error }}</strong></li>
        @endforeach
    </ul>
</div>
@endif
<!--end validasi form-->

<div class="btnBack">
    <div class="container-fluid">
        <div class="bs-component mb20" align="right">
            <a href="{{url()->previous()}}" class="btn btn-danger btn-md">
                <i class="fa fa-angle-double-left"></i>
                <span class="nav-text" style="font-weight: 700">
                    Back To Ticket List
                </span>
            </a>
        </div>
    </div>
</div>


{{-- <div class="panelIncident"> --}}
<div class="container-fluid">
    <div class="row mb20">
        <div class="col-md-12" align="right" id="BtnOther">
            <div class="bs-component mb20" align="center">
                @if($buttonUnhold == '1')
                <button type="button" class="btn btn-primary" id="btnUnhold">
                    <a href="{{url('/tickets/unhold/'.$tickets[0]->id_ticket.'/'.$log_holds[0]->id)}}">
                        <i class="fa fa-play"></i> Unhold Ticket
                    </a>
                </button>
                @else

                @endif
                {{-- apabila id.login = id.create_by maka buttonStatus = 1 --}}
                @if($buttonHold == '1')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnHold" data-target="#myModalHold" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-pause" aria-hidden="true"></i> Hold Ticket
                </button>
                @else

                @endif

                @if($buttonClosed == '1')
                    @if ($tickets[0]->category != 'Request')
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="btnClose" data-target="#myModalClose" data-backdrop="static" data-keyboard="false">
                        <i class="fa fa-close" aria-hidden="true"></i> Close Ticket
                    </button>
                    @else
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="btnClose" data-target="#myModalCloseRequest" data-backdrop="static" data-keyboard="false">
                        <i class="fa fa-close" aria-hidden="true"></i> Close Ticket
                    </button>
                    @endif
                @endif

                @if($buttonPreclose == '1')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnPreclose" data-target="#myModalPreclosed" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-edit" aria-hidden="true"></i> Pre-Closed Ticket
                </button>
                @else

                @endif

                @if($buttonReassign == '1')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnReassign" data-target="#myModalReassign" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-user" aria-hidden="true"></i> Re-Assign Ticket
                </button>
                @elseif($buttonReassign == '2')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnReassign" data-target="#myModalReassign" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-user" aria-hidden="true"></i> Re-Assign Ticket
                </button>
                @else

                @endif

                @if($buttonReturn == '1')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnReturn" data-target="#myModalReturn" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-undo" aria-hidden="true"></i> Return Ticket
                </button>
                @elseif($buttonReturn == '2')
                <button type="button" class="btn btn-primary" data-toggle="modal" id="btnReturnCustCare" data-target="#myModalReturn" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-undo" aria-hidden="true"></i> Return Ticket
                </button>
                @else

                @endif

                @if($buttonEditRfoHead == '1')
                    @if ($tickets[0]->category != 'Request')
                    <button type="button" class="btn btn-primary" data-toggle="modal" id="btnEditRFOHead" data-target="#myModalEditRFOHead" data-backdrop="static" data-keyboard="false">
                        <i class="fa fa-undo" aria-hidden="true"></i> Edit RFO Head
                    </button>
                    @endif
                @endif

                @if($buttonEditRfoStaff == '1')
                    @if ($tickets[0]->category != 'Request')
                        <button type="button" class="btn btn-primary" data-toggle="modal" id="btnEditRFOStaff" data-target="#myModalEditRFOStaff" data-backdrop="static" data-keyboard="false">
                            <i class="fa fa-undo" aria-hidden="true"></i> Edit RFO Staff
                        </button>
                    @endif
                @endif
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="panelBasic">
        <div class="panel-body">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>No Ticket</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->no_ticket}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Status</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>
                        @if (($tickets[0]->status=='1') && date('d-M-Y', strtotime($tickets[0]->created_date))==date('d-M-Y'))
                        <font color='green'><b>New !</b></font>
                        @elseif (($tickets[0]->status=='1') && date('d-M-Y', strtotime($tickets[0]->created_date))<>date('d-M-Y'))
                            <font color='green'><b>In Progress</b></font>
                            @elseif ($tickets[0]->status=='2')
                            <font color='blue'><b>Unassign</b></font>
                            @elseif ($tickets[0]->status=='3')
                            <font color='orange'><b>Hold</b></font><br>
                            {{-- {{ date('d-M-Y H:i:s', strtotime($tickets[0]->hold_date)) }} --}}
                            @else
                            <font color='red'><b>Closed</b></font>
                            @endif
                    </span>
                </div>
            </div>

            @php
            $created_date=strtotime($ticket->created_date);
            $aging=time()-$created_date;

            if(date('d-M-Y H:i:s')>date('d-M-Y H:i:s', strtotime($ticket->due_date))){
            $labeldue="red";
            }
            else{
            $labeldue="green";
            }
            @endphp

            <div class="row" align="left">
                <div class="col-md-2">
                    <span><b>Due Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>
                        <font color='{{ $labeldue }}'><b>{{ date('d-M-Y H:i:s', strtotime($ticket->due_date)); }}</b></font>
                    </span>
                </div>
                <div class="col-md-2">
                    <span>
                        @if($tickets[0]->status=='4')
                        <b>Close Date</b>
                        @elseif ($tickets[0]->status=='3')
                        <b>Hold Until</b>
                        @else
                        <b>Create Date</b>
                        @endif
                    </span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>
                        @if($tickets[0]->status=='3')
                        {{ date('d-M-Y H:i:s', strtotime($tickets[0]->hold_date)); }}
                        @elseif($tickets[0]->status=='4')
                        {{ date('d-M-Y H:i:s', strtotime($tickets[0]->closed_date)); }}
                        @else
                        {{ date('d-M-Y H:i:s', strtotime($tickets[0]->created_date)); }}
                        @endif
                    </span>
                </div>
            </div>
            <br>
            <hr id="lineShadow">
            <div class="bs-component mb20" align="left">
                <form action="{{url('/tickets/exportLogAssign')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <button type="submit" class="btn btn-primary" id="btnExportLogExternal">
                        <i class="fa fa-table" aria-hidden="true"></i>
                        <input type="hidden" name="id_ticket" value="{{ $tickets[0]->id_ticket }}" id="id_ticket" style="">
                        <span class="nav-text">
                            Export Log to Excel
                        </span>
                    </button>
                </form>


                {{-- BUTTON DOWNLOAD RFO
                    @if (($tickets[0]->status=='4')&&($tickets[0]->rfo<>'NON RFO'))
                        @if ($tickets[0]->ticket_type=='Maintenance')
                            <a href="{{url('/tickets/rfo_maintenance/'.$tickets[0]->id_ticket)}}" id="btnrfo" class="btn btn-primary">Download RFO</a>
                @elseif($tickets[0]->ticket_type=='External')
                <a href="{{url('/tickets/rfo/'.$tickets[0]->id_ticket)}}" id="btnrfo" class="btn btn-primary">Download RFO</a>
                @endif
                @endif --}}
            </div>

            <div class="w3l-table-info">
                <table id="tblLogsAssign" class="display">
                    <thead>
                        <tr>
                            <th>Assign By</th>
                            <th>Assign to Department</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($assign_history as $assign)
                        <tr>
                            <td>{{ $assign->int_emp_name }}</td>
                            <td>{{ $assign->department_name }}</td>
                            <td>{{ $assign->message }}</td>
                            <td><strong>Assign date</strong> : {{date('d-M-Y H:i:s', strtotime($assign->assign_date))}} <br>
                                @if($assign->preclosed_status == '1')
                                <strong>Preclose date</strong> : {{date('d-M-Y H:i:s', strtotime($assign->preclosed_date))}} <br>
                                <strong>Duration</strong> : {{$assign->duration}}
                                @else
                                <span><b>Preclose date</b> : still hasn't pre-closed </span>
                                @endif
                            </td>
                            <td>
                                @if($assign->preclosed_status == '1')
                                <span class="badge" style="background-color: green"><b>Pre-Close</b></span>
                                @else
                                <span class="badge" style="background-color: orange"><b>Active</b></span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <br>

    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Detail Incident</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Request Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->request}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Type</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->category}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>CID</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->cid}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Service</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->service}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Capacity</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->capacity}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Status Link Respond</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->status_link_respond}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Created By</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->int_emp_name}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Created Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->created_date}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Attachment</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3" id="btnAttachment">
                    @if ($tickets[0]->file_1 != '')
                    <a href="{{ url('/tickets/'.$tickets[0]->file_1.'/download') }}" class="btn btn-primary btn-xs">
                        <span>
                            <i class="fa fa-paperclip" aria-hidden="true"></i> 1
                        </span>
                    </a>
                    @endif
                    @if ($tickets[0]->file_2 != '')
                    <a href="{{  url('/tickets/'.$tickets[0]->file_2.'/download') }}" class="btn btn-primary btn-xs">
                        <span>
                            <i class="fa fa-paperclip" aria-hidden="true"></i> 2
                        </span>
                    </a>
                    @endif
                    @if ($tickets[0]->file_3 != '')
                    <a href="{{  url('/tickets/'.$tickets[0]->file_3.'/download') }}" class="btn btn-primary btn-xs">
                        <span>
                            <i class="fa fa-paperclip" aria-hidden="true"></i> 3
                        </span>
                    </a>
                    @endif
                    @if ($tickets[0]->file_4 != '')
                    <a href="{{  url('/tickets/'.$tickets[0]->file_4.'/download') }}" class="btn btn-primary btn-xs">
                        <span>
                            <i class="fa fa-paperclip" aria-hidden="true"></i> 4
                        </span>
                    </a>
                    @endif
                    @if ($tickets[0]->file_5 != '')
                    <a href="{{  url('/tickets/'.$tickets[0]->file_5.'/download') }}" class="btn btn-primary btn-xs">
                        <span>
                            <i class="fa fa-paperclip" aria-hidden="true"></i> 5
                        </span>
                    </a>
                    @endif
                </div>
                <div class="col-md-2">
                    <span><b>Remarks</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $tickets[0]->remarks}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- </div> --}}

<br>

<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Detail Customer</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>A-End</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets[0]->a_end}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>B-End</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets[0]->b_end}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>PIC Name</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets[0]->pic_name}}</span>
                </div>
                <div class="col-md-2">
                    <span><b>PIC Contact</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets[0]->pic_contact}}</span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Regional</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{$tickets[0]->regional}}</span>
                </div>
            </div>
            @foreach ($tickets as $ticket)
            <div class="row mb10" align="left">
                @php
                $file1=base64_encode($ticket->topology_file1);
                $file2=base64_encode($ticket->topology_file2);
                $file3=base64_encode($ticket->topology_file3);

                $cust=base64_encode($ticket->company_name);
                @endphp

                <div class="col-lg-12" align="left">
                    @if ($file1<>'')
                        <a href="{{ url('/masters/topology/view/'.$file1.'/'.$cust) }}" class="btn btn-info btn-md" id="btnTopo_1" target="_blank">
                            Topology 1
                        </a>
                        @endif

                        @if ($file2<>'')
                            <a href="{{ url('/masters/topology/view/'.$file2.'/'.$cust) }}" class="btn btn-info btn-md" id="btnTopo_2" target="_blank">
                                Topology 2
                            </a>
                            @endif

                            @if ($file3<>'')
                                <a href="{{ url('/masters/topology/view/'.$file3.'/'.$cust) }}" class="btn btn-info btn-md" id="btnTopo_3" target="_blank">
                                    Topology 3
                                </a>
                                @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@if($datacall == 0)

@else
<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Detail Retention Call</h2>
        </div>
        <div class="panel-body" id="panelIncident">
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Call Status</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $datacall['CallStatus'] }} </span>
                </div>
                <div class="col-md-2">
                    <span><b>Call Decision</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $datacall['CallDecision'] }} </span>
                </div>
            </div>
            <div class="row mb20" align="left">
                <div class="col-md-2">
                    <span><b>Call Date</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $datacall['CallDate'] }}</span>
                </div>
                <div class="col-md-2">
                    <span><b>Call By</b></span>
                </div>
                <div class="col-md-1">
                    <span>:</span>
                </div>
                <div class="col-md-3">
                    <span>{{ $datacall['CallBy'] }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
@endif

<div class="container-fluid">
    <div class="panel panel-primary" id="borderPanel">
        <div class="panel-heading" id="panelHeader">
            <h2 style="font-weight: 700">Update Progress</h2>
        </div>
        <div class="panel-body">
            <br><br>
            <div class="bs-component mb20">
                <div class="row">
                    <form action="{{ url('/tickets/addComment') }}" method="post" id="formComment" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-8">
                            <input type="hidden" name="id_ticket" value="{{ $tickets[0]->id_ticket }}" id="id_ticket" style="">
                            <textarea name="message_comment" id="message_comment" cols="10" rows="5" placeholder=" Write a comment"></textarea>
                            <span style="color: red">*required</span>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="file" name="attc1" id="attc1" class="form-control">
                                <span style="color: red">*not required | file must : mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif | max:10048 </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <button type="submit" class="btn btn-primary btn-block" id="BtnComment"><span class="fa fa-comments"></span> Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            {{-- table log tickets  --}}
            <div class="main-grid">
                <div class="agile-grids">

                    <!-- Per Departemen-->
                    <div class="w3l-table-info">
                        <table id="tblComment" class="display">
                            <thead>
                                <tr>
                                    <th>Create by</th>
                                    <th>Message</th>
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($comment_history as $comment)
                                <tr id="hoverRowComment">
                                    <td><b>{{ $comment->int_emp_name }} - {{ $comment->department_name }}</b><br>
                                        <i>{{ $comment->created_at }}</i>
                                    </td>
                                    <td><b>{{ $comment->message}}</b><br>
                                        <i>{{ $comment->description}}</i>
                                    </td>
                                    <td>
                                        <a href="{{ url('/tickets/'.$comment->attachment_1.'/downloadFileComment') }}">{{ $comment->attachment_1}}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end per departemen-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Re-Assign-->
<div class="modal fade" id="myModalReassign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Re-Assign Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalReassign" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/reassign') }}" method="post" id="myform" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id_ticket" value="{{ $tickets[0]->id_ticket }}" id="id_ticket">
                <input type="hidden" name="status_ticket" value="{{ $tickets[0]->status }}" id="id_ticket">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Department NAP</label>
                            <select name="department" id="department" class="form-control">
                                <option value="">- Please Select Department -</option>
                                @foreach ($dropdowns['dept'] as $dept_data )
                                <option value="{{ $dept_data->department_id }}">{{ $dept_data->department_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div id="checkbox-container" style="display:none;">
                                <input type="checkbox" id="myCheckbox_ca" name="for_ca" class="form-check-input form-control-lg" value="1">
                                <label for="myCheckbox_ca" class="font-boldfont-weight-bold text-primary ">Ticket For Customer Assistant</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Remarks (max 255 character)</label>
                            <textarea name="message" id="message" cols="20" rows="5" class="form-control">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalReassign"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Re-Assign-->

<!-- Modal Return-->
<div class="modal fade" id="myModalReturn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Return Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalReturn" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            @foreach ($return as $return_data)
            <form action="{{ url('/tickets/return/'.$return_data->id) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" name="id_ticket" value="{{ $return_data->id_ticket }}" id="id_ticket">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Last Assign To Department</label><br>
                            {{ $return_data->department_name }}
                        </div>
                        <div class="form-group">
                            <label>Last Assign Date</label><br>
                            {{ $return_data->assign_date }}
                        </div>
                        <div class="form-group">
                            <label>Message (max 255 character)</label><br>
                            <textarea name="message" id="message" cols="20" rows="5" class="form-control message-return">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalReturn"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Return-->

<!-- Modal Hold-->
<div class="modal fade" id="myModalHold" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Hold Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" id="closeModalHold" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/hold/'.$tickets[0]->id_ticket) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Hold Date From</label>
                            <input type="date" id="holdtime" name="hold_date_from" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Hold Time From</label>
                            <input type="time" id="hold_time" name="hold_time_from" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Hold Date Until</label>
                            <input type="date" id="holdtime" name="hold_date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Hold Time Until</label>
                            <input type="time" id="hold_time" name="hold_time" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Category Hold</label>
                            <select name="category_hold" id="category_hold" class="form-control">
                                <option value="">- Please Select Category -</option>
                                @foreach ($dropdowns['hold'] as $hold )
                                <option value="{{ $hold->name_value }}">{{ $hold->name_value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Message (max 255 character)</label><br>
                            <textarea name="message" id="message" cols="20" rows="5" class="form-control message-hold">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalHold"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Hold-->

<!-- Modal Close Selain Request-->
<div class="modal fade" id="myModalClose" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Close Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalClose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/close/'.$tickets[0]->id_ticket) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" id="cid" name="cid" value="{{ $ticket->cid }}">
                <input type="hidden" id="ticket_date" name="ticket_date" value="{{ $ticket->created_date }}">
                <input type="hidden" id="hold_date" name="hold_date" value="{{ $ticket->hold_date }}">
                <input type="hidden" id="unhold_date" name="unhold_date" value="{{ $ticket->unhold_date }}">
                <input type="hidden" id="req_time" name="req_time" value="{{ $ticket->request_at }}">
                <input type="hidden" id="category" name="category" value="{{ $ticket->category }}">
                <input type="hidden" id="sub_category" name="sub_category" value="{{ $ticket->type_incident }}">
                <input type="hidden" id="no_ticket" name="no_ticket" value="{{ $ticket->no_ticket }}">
                <input type="hidden" id="req_by" name="req_by" value="{{ $ticket->request }}">
                <div class="modal-body">
                    {{-- <div class="panel panel-widget top-grids"> --}}
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Reporting Date</label><br>
                                        @if ($ticket->request_at == '')
                                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->created_date)) }}</i>
                                        @else
                                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->request_at)) }}</i>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Resolving Date</label>
                                        <input type="date" id="finish_date" name="finish_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Resolving Time</label>
                                        <input type="time" id="finish_time" name="finish_time" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Generate RFO</label>
                                        <select name="gen_rfo" id="gen_rfo" class="form-control">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Based on RFO Date</label>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" id="hour" name="baseonrfo_hour" placeholder="Hour">
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" id="minute" name="baseonrfo_minute">
                                                    <option value="">Minute</option>
                                                    @for ($i = 0; $i < 60; $i++) <option value="{{ $i }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                                        @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Outage Reason 1</label>
                                        <select name="outage_reason1" id="outage_reason1" class="form-control">
                                            <option value="">- Select Outage Reason -</option>
                                            @foreach ($dropdowns['outage reason'] as $outage )
                                            <option value="{{ $outage->name_value }}">{{ $outage->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Outage Reason 1</label>
                                        <select name="outage_sub1" id="outage_sub1" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Action 1</label>
                                        <select name="action_close1" id="action_close1" class="form-control">
                                            <option value="">- Please Select Action -</option>
                                            @foreach ($dropdowns['close'] as $close )
                                            <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="form-group" id="solvedby1">
                                        <label>Solved By 1</label>
                                        <select name="solved_by1" id="solved_by1" class="form-control">
                                            <option value="">- Please Select Solved By -</option>
                                            @foreach ($dropdowns['solved_by'] as $solved )
                                            <option value="{{ $solved->name_value }}">{{ $solved->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}
                                    <div class="form-group">
                                        <a href="#collapse1" class="btn btn-info btn-xs" onclick="showhide(this)">Add Reason</a>
                                    </div>
                                    <div id="collapse1" style="display:none">
                                        <div class="form-group">
                                            <label>Outage Reason 2</label>
                                            <select name="outage_reason2" id="outage_reason2" class="form-control">
                                                <option value="">- Select Outage Reason -</option>
                                                @foreach ($dropdowns['outage reason'] as $outage )
                                                <option value="{{ $outage->name_value }}">{{ $outage->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Outage Reason 2</label>
                                            <select name="outage_sub2" id="outage_sub2" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Action 2</label>
                                            <select name="action_close2" id="action_close2" class="form-control">
                                                <option value="">- Please Select Action -</option>
                                                @foreach ($dropdowns['close'] as $close )
                                                <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="form-group" id="solvedby2">
                                            <label>Solved By 2</label>
                                            <select name="solved_by2" id="solved_by2" class="form-control">
                                                <option value="">- Please Select Solved By -</option>
                                                @foreach ($dropdowns['solved_by'] as $solved )
                                                <option value="{{ $solved->name_value }}">{{ $solved->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Responsible</label>
                                        <select name="responsible" id="responsible" class="form-control">
                                            <option value="">- Please Select Responsible -</option>
                                            @foreach ($dropdowns['responsible'] as $responsible )
                                            <option value="{{ $responsible->name_value }}">{{ $responsible->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Solved By</label>
                                        <select name="solved_by[0]" id="solved_by" class="form-control"></select>
                                        {{-- Test --}}
                                        <div id="add_solved_by"></div>
                                        <button type="button" name="add-btn" id="add-btn" class="btn btn-success btn-xs" style="display: none; margin-top: 1rem;">Add</button>
                                        <button type="button" name="rem-btn" id="rem-btn" class="btn btn-danger btn-xs" style="display: none; margin-top: 1rem;">Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <a href="#collapse2" class="btn btn-info btn-xs" onclick="showhide(this)">Add Final Category</a>
                                    </div>
                                    <div id="collapse2" style="display:none">
                                        <div class="form-group">
                                            <label>Final Category</label>
                                            <select name="final_category" id="final_category" class="form-control">
                                                <option value="">- Please Select Category -</option>
                                                @foreach ($dropdowns['final category'] as $close )
                                                <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Final Category Ticket</label>
                                            <select name="sub_final_category" id="sub_final_category" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Outage Location</label><br>
                                        <input type="text" name="outage_loc" id="outage_loc" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-close">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                    {{-- </div> --}}
                    <!--panel-widget-->
                </div><!-- modal body-->
                <div class="modal-footer" align="right">
                    <button type="button" class="btn btn-primary" id="btnResetModalClose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div><!-- modal-content-->
    </div><!-- modal dialog-->
</div><!-- modal fade-->
<!-- End Modal Close Selain Request-->

{{-- Modal Close Request --}}
<div class="modal fade" id="myModalCloseRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Close Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalClose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/close-request/'.$tickets[0]->id_ticket) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" id="cid" name="cid" value="{{ $ticket->cid }}">
                <input type="hidden" id="ticket_date" name="ticket_date" value="{{ $ticket->created_date }}">
                <input type="hidden" id="hold_date" name="hold_date" value="{{ $ticket->hold_date }}">
                <input type="hidden" id="unhold_date" name="unhold_date" value="{{ $ticket->unhold_date }}">
                <input type="hidden" id="req_time" name="req_time" value="{{ $ticket->request_at }}">
                <input type="hidden" id="category" name="category" value="{{ $ticket->category }}">
                <input type="hidden" id="sub_category" name="sub_category" value="{{ $ticket->type_incident }}">
                <input type="hidden" id="no_ticket" name="no_ticket" value="{{ $ticket->no_ticket }}">
                <input type="hidden" id="req_by" name="req_by" value="{{ $ticket->request }}">
                <div class="modal-body">
                    {{-- <div class="panel panel-widget top-grids"> --}}
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Reporting Date</label><br>
                                        @if ($ticket->request_at == '')
                                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->created_date)) }}</i>
                                        @else
                                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->request_at)) }}</i>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Resolving Date</label>
                                        <input type="date" id="finish_date" name="finish_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Resolving Time</label>
                                        <input type="time" id="finish_time" name="finish_time" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-close">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                    {{-- </div> --}}
                    <!--panel-widget-->
                </div><!-- modal body-->
                <div class="modal-footer" align="right">
                    <button type="button" class="btn btn-primary" id="btnResetModalClose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div><!-- modal-content-->
    </div><!-- modal dialog-->
</div><!-- modal fade-->
{{-- End Modal Close Request --}}

<!-- Modal Preclosed-->
<div class="modal fade" id="myModalPreclosed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Preclosed Ticket</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalPreclose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/preclosed/'.$tickets[0]->id_ticket) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id_employee }}">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Message (max 255 character)</label><br>
                            <textarea name="message" id="message" cols="20" rows="5" class="form-control message-preclose">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalPreclose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Hold-->


<!-- Modal Edit RFO by Head-->
<div class="modal fade" id="myModalEditRFOHead" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Edit RFO by Head</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalPreclose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/edit-rfo-head/'.$tickets[0]->id_ticket) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id_employee }}">
                <input type="hidden" id="ticket_date" name="ticket_date" value="{{ $ticket->created_date }}">
                <input type="hidden" id="hold_date" name="hold_date" value="{{ $ticket->hold_date }}">
                <input type="hidden" id="unhold_date" name="unhold_date" value="{{ $ticket->unhold_date }}">
                <input type="hidden" id="resolved_date_old" name="resolved_date_old" value="{{ $ticket->resolved_date }}">
                <input type="hidden" id="slr_old" name="slr_old" value="{{ $ticket->status_link_respond }}">
                <input type="hidden" id="sub_slr_old" name="sub_slr_old" value="{{ $ticket->sub_status_link_respond }}">
                <input type="hidden" id="outage_loc_old" name="outage_loc_old" value="{{ $ticket->outage_location }}">
                <input type="hidden" id="closed_message_old" name="closed_message_old" value="{{ $ticket->closed_message }}">
                <input type="hidden" id="outage_reason_old" name="outage_reason_old" value="{{ $ticket->outage_reason }}">
                <input type="hidden" id="outage_sub_old" name="outage_sub_old" value="{{ $ticket->outage_sub }}">
                <input type="hidden" id="action_close_old" name="action_close_old" value="{{ $ticket->action_close }}">
                <input type="hidden" id="req_time" name="req_time" value="{{ $ticket->request_at }}">
                <div class="modal-body">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <ul>
                                        <li>Please only fill the coloumn that you want to edit</li>
                                        <li>Other coloumn that you wont edit please let it be empty</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Reporting Date</label><br>
                                        @if ($ticket->request_at == '')
                                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->created_date)) }}</i>
                                        @else
                                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->request_at)) }}</i>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Resolving Date</label>
                                        <input type="date" id="finish_date" name="finish_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Resolving Time</label>
                                        <input type="time" id="finish_time" name="finish_time" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Status Link Reported</label>
                                        <select name="link_respond" id="link_respond" class="form-control">
                                            <option value="">- Please Select Status Link Respond -</option>
                                            @foreach ($dropdowns['status link respond'] as $slr_data)
                                            <option value="{{ $slr_data->name_value }}">{{ $slr_data->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Status Link Reported</label>
                                        <select name="sub_link_respond" id="sub_link_respond" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Outage Reason 1</label>
                                        <select name="outage_reason1" id="outage_reason1" class="form-control">
                                            <option value="">- Select Outage Reason -</option>
                                            @foreach ($dropdowns['outage reason'] as $outage )
                                            <option value="{{ $outage->name_value }}">{{ $outage->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Outage Reason 1</label>
                                        <select name="outage_sub1" id="outage_sub1" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Action 1</label>
                                        <select name="action_close1" id="action_close1" class="form-control">
                                            <option value="">- Please Select Action -</option>
                                            @foreach ($dropdowns['close'] as $close )
                                            <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="form-group" id="solvedby1">
                                        <label>Solved By 1</label>
                                        <select name="solved_by1" id="solved_by1" class="form-control">
                                            <option value="">- Please Select Solved By -</option>
                                            @foreach ($dropdowns['solved_by'] as $solved )
                                            <option value="{{ $solved->name_value }}">{{ $solved->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}
                                    <div class="form-group">
                                        <a href="#collapse2" class="btn btn-info btn-xs" onclick="showhide(this)">Add Reason</a>
                                    </div>
                                    <div id="collapse2" style="display:none">
                                        <div class="form-group">
                                            <label>Outage Reason 2</label>
                                            <select name="outage_reason2" id="outage_reason2" class="form-control">
                                                <option value="">- Select Outage Reason -</option>
                                                @foreach ($dropdowns['outage reason'] as $outage )
                                                <option value="{{ $outage->name_value }}">{{ $outage->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Outage Reason 2</label>
                                            <select name="outage_sub2" id="outage_sub2" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Action 2</label>
                                            <select name="action_close2" id="action_close2" class="form-control">
                                                <option value="">- Please Select Action -</option>
                                                @foreach ($dropdowns['close'] as $close )
                                                <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="form-group" id="solvedby2">
                                            <label>Solved By 2</label>
                                            <select name="solved_by2" id="solved_by2" class="form-control">
                                                <option value="">- Please Select Solved By -</option>
                                                @foreach ($dropdowns['solved_by'] as $solved )
                                                <option value="{{ $solved->name_value }}">{{ $solved->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Responsible</label>
                                        <select name="responsible" id="responsible" class="form-control">
                                            <option value="">- Please Select Responsible -</option>
                                            @foreach ($dropdowns['responsible'] as $responsible )
                                            <option value="{{ $responsible->name_value }}">{{ $responsible->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Solved By</label>
                                        <select name="solved_by[0]" id="solved_by" class="form-control"></select>
                                        {{-- Test --}}
                                        <div id="add_solved_by"></div>
                                        <button type="button" name="add-btn" id="add-btn" class="btn btn-success btn-xs" style="display: none; margin-top: 1rem;">Add</button>
                                        <button type="button" name="rem-btn" id="rem-btn" class="btn btn-danger btn-xs" style="display: none; margin-top: 1rem;">Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Outage Location</label><br>
                                        <input type="text" name="outage_loc" id="outage_loc" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-close">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalPreclose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Edit RFO by Head-->

<!-- Modal Edit RFO by Staff-->
<div class="modal fade" id="myModalEditRFOStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Edit RFO by Staff</b>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalPreclose" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <form action="{{ url('/tickets/edit-rfo-staff/'.$tickets[0]->id_ticket) }}" method="post" id="myform" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id_employee }}">
                <input type="hidden" id="slr_old" name="slr_old" value="{{ $ticket->status_link_respond }}">
                <input type="hidden" id="sub_slr_old" name="sub_slr_old" value="{{ $ticket->sub_status_link_respond }}">
                <input type="hidden" id="outage_loc_old" name="outage_loc_old" value="{{ $ticket->outage_location }}">
                <input type="hidden" id="closed_message_old" name="closed_message_old" value="{{ $ticket->closed_message }}">
                <input type="hidden" id="outage_reason_old" name="outage_reason_old" value="{{ $ticket->outage_reason }}">
                <input type="hidden" id="outage_sub_old" name="outage_sub_old" value="{{ $ticket->outage_sub }}">
                <input type="hidden" id="action_close_old" name="action_close_old" value="{{ $ticket->action_close }}">
                <div class="modal-body">
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <ul>
                                        <li>Please only fill the coloumn that you want to edit</li>
                                        <li>Other coloumn that you wont edit please let it be empty</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Status Link Reported</label>
                                        <select name="link_respond" id="link_respond" class="form-control">
                                            <option value="">- Please Select Status Link Respond -</option>
                                            @foreach ($dropdowns['status link respond'] as $slr_data)
                                            <option value="{{ $slr_data->name_value }}">{{ $slr_data->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Status Link Reported</label>
                                        <select name="sub_link_respond" id="sub_link_respond" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Outage Reason 1</label>
                                        <select name="outage_reason1" id="outage_reason1" class="form-control">
                                            <option value="">- Select Outage Reason -</option>
                                            @foreach ($dropdowns['outage reason'] as $outage )
                                            <option value="{{ $outage->name_value }}">{{ $outage->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Outage Reason 1</label>
                                        <select name="outage_sub1" id="outage_sub1" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Action 1</label>
                                        <select name="action_close1" id="action_close1" class="form-control">
                                            <option value="">- Please Select Action -</option>
                                            @foreach ($dropdowns['close'] as $close )
                                            <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="form-group" id="solvedby1">
                                        <label>Solved By 1</label>
                                        <select name="solved_by1" id="solved_by1" class="form-control">
                                            <option value="">- Please Select Solved By -</option>
                                            @foreach ($dropdowns['solved_by'] as $solved )
                                            <option value="{{ $solved->name_value }}">{{ $solved->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}
                                    <div class="form-group">
                                        <a href="#collapse3" class="btn btn-info btn-xs" onclick="showhide(this)">Add Reason</a>
                                    </div>
                                    <div id="collapse3" style="display:none">
                                        <div class="form-group">
                                            <label>Outage Reason 2</label>
                                            <select name="outage_reason2" id="outage_reason2" class="form-control">
                                                <option value="">- Select Outage Reason -</option>
                                                @foreach ($dropdowns['outage reason'] as $outage )
                                                <option value="{{ $outage->name_value }}">{{ $outage->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Outage Reason 2</label>
                                            <select name="outage_sub2" id="outage_sub2" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Action 2</label>
                                            <select name="action_close2" id="action_close2" class="form-control">
                                                <option value="">- Please Select Action -</option>
                                                @foreach ($dropdowns['close'] as $close )
                                                <option value="{{ $close->name_value }}">{{ $close->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="form-group" id="solvedby2">
                                            <label>Solved By 2</label>
                                            <select name="solved_by2" id="solved_by2" class="form-control">
                                                <option value="">- Please Select Solved By -</option>
                                                @foreach ($dropdowns['solved_by'] as $solved )
                                                <option value="{{ $solved->name_value }}">{{ $solved->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- row mb40-->

                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Responsible</label>
                                        <select name="responsible" id="responsible" class="form-control">
                                            <option value="">- Please Select Responsible -</option>
                                            @foreach ($dropdowns['responsible'] as $responsible )
                                            <option value="{{ $responsible->name_value }}">{{ $responsible->name_value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Solved By</label>
                                        <select name="solved_by[0]" id="solved_by" class="form-control"></select>
                                        {{-- Test --}}
                                        <div id="add_solved_by"></div>
                                        <button type="button" name="add-btn" id="add-btn" class="btn btn-success btn-xs" style="display: none; margin-top: 1rem;">Add</button>
                                        <button type="button" name="rem-btn" id="rem-btn" class="btn btn-danger btn-xs" style="display: none; margin-top: 1rem;">Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="demo-grid">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label>Outage Location</label><br>
                                        <input type="text" name="outage_loc" id="outage_loc" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Message (max 255 character)</label><br>
                                        <textarea name="message" id="message" cols="20" rows="5" class="form-control message-close">{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- row mb40-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnResetModalPreclose"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Edit RFO by Staff-->
<script>
    function showhide(elem) {
        console.log(elem.getAttribute("href"));
        var div = document.getElementById(elem.getAttribute("href").replace("#", ""));

        if (div.style.display !== "none") {
            div.style.display = "none";
        } else {
            div.style.display = "block";
        }
    }

    $(document).ready(function() {
        var table = $('#tblComment').DataTable({
            "order": [],
            responsive: true
        });

        var tableLogs = $('#tblLogsAssign').DataTable({
            "order": [],
            responsive: true
        });

        $('#btnResetModalHold').off('click').on('click', resetModalHold);
        $('#closeModalHold').off('click').on('click', resetModalHold);

        function resetModalHold() {
            $('#holdtime').val('');
            $('#hold_time').val('');
            $('#category_hold').val('');
            $('.message-hold').val('');
        }

        $('#btnResetModalReturn').off('click').on('click', resetModalReturn);
        $('#closeModalReturn').off('click').on('click', resetModalReturn);

        function resetModalReturn() {
            $('.message-return').val('');
        }

        $('#btnResetModalReassign').off('click').on('click', resetModalReassign);
        $('#closeModalReassign').off('click').on('click', resetModalReassign);

        function resetModalReassign() {
            $('#department').val('');
            $('#message').val('');
        }

        $('#btnResetModalClose').off('click').on('click', resetModalClose);
        $('#closeModalClose').off('click').on('click', resetModalClose);

        function resetModalClose() {
            $('#finish_date').val('');
            $('#finish_time').val('');
            $('#baseonrfo_date').val('');
            $('#baseonrfo_time').val('');
            $('#gen_rfo').val('1');
            $('#outage_reason1').val('');
            $('#outage_sub1').val('');
            $('#outage_reason2').val('');
            $('#outage_sub2').val('');
            $('#solved_by1').val('');
            $('#solved_by2').val('');
            $('#action_close1').val('');
            $('#action_close2').val('');
            $('#outage_loc').val('');
            $('.message-close').val('');
            $('#solved_by').val('');
            $('#responsible').val('');
            $('#add_solved_by').empty();
            $('#add-btn').hide();
            $('#rem-btn').hide();
        }

        $('#btnResetModalPreclose').off('click').on('click', resetModalPreclose);
        $('#closeModalPreclose').off('click').on('click', resetModalPreclose);

        function resetModalPreclose() {
            $('.message-preclose').val('');
        }

        $('#formComment').submit(function() {
            $("#btnComment", this)
                .html("Please Wait...")
                .attr('disabled', 'disabled');
            return true;
        });
    });

    // no issue change solved by 1
    $('#outage_reason1').click(function() {
        var id = $(this).val();
        var text = $('option:selected', this).text(); //to get selected text
        // alert(text)
        if (text == 'No Issue') {
            $('#solvedby1').show();

        } else {
            $('#solvedby1').hide();
        }

    });

    // no issue change solved by 2
    $('#outage_reason2').click(function() {
        var id = $(this).val();
        var text = $('option:selected', this).text(); //to get selected text
        // alert(text)
        if (text == 'No Issue') {
            $('#solvedby2').show();

        } else {
            $('#solvedby2').hide();
        }

    });

    //select staff from select dept
    $('select[name="department"]').on('change', function() {
        var deptID = $(this).val();
        var url = '{{ route("dept", ":id") }}';
        url = url.replace(':id', deptID);
        if (deptID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="pic_staff"]').empty();
                    $('select[name="pic_staff"]').append(
                        '<option value="all">- All Staff in Department -</option>'
                    );
                    $.each(data, function(dept_data, value) {
                        $('select[name="pic_staff"]').append(
                            '<option value="' + value.int_emp_id + '">' + value.int_emp_name + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="pic_staff"]').empty();
        }
    });

    // select solved by from select responsible
    $('select[name="responsible"]').on('change', function() {
        var responsibleID = $(this).val();
        var url = '{{ route("responsible", ":id") }}';
        url = url.replace(':id', responsibleID);
        if (responsibleID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {
                    var index = 0;

                    $('select[name="solved_by[0]"]').empty();
                    $('select[name="solved_by[0]"]').append(
                        '<option value="">- Select Solved By -</option>'
                    );

                    $.each(data, function(responsible, value) {
                        if (responsibleID == 'NAP Info') {
                            $("#add-btn").show();
                            $('select[name="solved_by[0]"]').append(
                                '<option value="' + value.name_value + '">' + value.name_value + '</option>'
                            );
                            
                            $("#add-btn").unbind('click').click(function() {
                                ++index;
                                $("#add_solved_by").append('<select name="solved_by['+index+']" id="solved_by" class="form-control" style="margin-top: 1rem;"></select>');

                                $('select[name="solved_by['+ index +']"]').empty();
                                $('select[name="solved_by['+ index +']"]').append(
                                    '<option value="">- Select Solved By -</option>'
                                );
                                
                                $.each(data, function(responsible, value) {
                                    $('select[name="solved_by['+index+']"]').append(
                                        '<option value="' + value.name_value + '">' + value.name_value + '</option>'
                                    );
                                });

                                $("#rem-btn").show();
                            });

                            $("#rem-btn").unbind('click').click(function() {
                                var drops = index--;

                                if (drops == 1) {
                                    $("#rem-btn").hide();
                                    $('select[name="solved_by['+ drops +']"]').remove();
                                } else {
                                    $('select[name="solved_by['+ drops +']"]').remove();
                                }
                            });
                            
                        }   else {
                            
                            $("#add-btn").hide();
                            $("#rem-btn").hide();

                            ++index;
                            $('select[name="solved_by['+ index +']"]').remove();

                            $('select[name="solved_by[0]"]').append(
                                '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                            );
                        }
                    });
                }
            });
        } else {
            $('select[name="solved_by"]').empty();
        }
    });

    //select sub outage from outage 1
    $('select[name="outage_reason1"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if (outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="outage_sub1"]').empty();
                    $('select[name="outage_sub1"]').append(
                        '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub1"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="outage_sub1"]').empty();
        }
    });

    //select sub outage from outage 2
    $('select[name="outage_reason2"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if (outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="outage_sub2"]').empty();
                    $('select[name="outage_sub2"]').append(
                        '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub2"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
            $('#btnComment').click(function() {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('select[name="outage_sub2"]').empty();
        }
    });

    //select sub SLR from select SLR
    $('select[name="link_respond"]').on('change', function() {
        var slrID = $(this).val();
        var url = '{{ route("slr", ":id") }}';
        url = url.replace(':id', slrID);
        if (slrID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="sub_link_respond"]').empty();
                    $('select[name="sub_link_respond"]').append(
                        '<option value="">- Select Sub Status Link Respond -</option>'
                    );
                    $.each(data, function(slr_data, value) {
                        $('select[name="sub_link_respond"]').append(
                            '<option value="' + value.name_incident + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="sub_link_respond"]').empty();
        }
    });

    //select sub final category from select final category
    $('select[name="final_category"]').on('change', function() {
        var categoryID = $(this).val();
        var url = '{{ route("mappingIncident", ":id") }}';
        url = url.replace(':id', categoryID);
        if (categoryID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function(data) {

                    $('select[name="sub_final_category"]').empty();
                    $('select[name="sub_final_category"]').append(
                        '<option value="">- Select Sub Category -</option>'
                    );
                    $.each(data, function(category, value) {
                        $('select[name="sub_final_category"]').append(
                            '<option value="' + value.id + '">' + value.name_incident + '</option>'
                        );
                    });

                }
            });
        } else {
            $('select[name="sub_final_category"]').empty();
        }
    });

    // add remove input field on solved by

    // checkbox ticket for ca
    $('#department').on('change', function() {
        // alert($(this).val())
        if ($(this).val() == 34) {
            $('#checkbox-container').show();
        } else {
            $('#checkbox-container').hide();
        }
    });
</script>
@endsection