<!DOCTYPE html>
<html>

<head>
	<title>REASON FOR OUTAGE</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<style>
		#footer {
			position: fixed;
			padding: 10px 10px 0px 10px;
			bottom: 0;
			width: 100%;
			/* Height of the footer*/
			height: 40px;
		}

		tr,
		td {
			padding-left: 15px;
		}
	</style>
</head>
@foreach($ticket as $data)

<body style="min-height: 100%">
	<img <img src="{{public_path('admin/images/header_mail.JPG')}}" alt="" width="700" height="140" />
	<br>
	<p class="text-left">
		Number: {{ $data->rfo }}
		<br>
		Date: {{ date('d F Y', strtotime($data->created_date)); }}
		<br>
		<br>
		Dear Our Valued Customer.
		<br>
		<br>
		Thank you for your trust in using our service. Below is the information that we can convey regarding the problems that previously occurred:
	</p>
	<br>
	<center>
		<h5>REASON FOR OUTAGE (RFO)</h5>
	</center>
	<table border='1' style="width: 100%">
		<tbody>
			<tr>
				<td colspan="2"><b>CUSTOMER DETAILS</b></td>
			</tr>
			<tr>
				<td>Ticket Number</td>
				<td>{{ $data->no_ticket }}</td>
			</tr>
			<tr>
				<td>Customer ID (CID)</td>
				<td>{{ $data->cid }}</td>
			</tr>
			<tr>
				<td>Customer Name</td>
				<td>{{ $data->company_name }}</td>
			</tr>
			<tr>
				<td>Service</td>
				<td>{{ $data->service }}</td>
			</tr>
			<tr>
				<td colspan="2"><b>OUTAGE DETAILS</b></td>
			</tr>
			<tr>
				<td>Outage Type</td>
				<td>{{ $data->status_link_respond }}</td>
			</tr>
			<tr>
				<td>Outage Location</td>
				<td>{{ $data->outage_location }}</td>
			</tr>
			<tr>
				<td>Outage Reason</td>
				<td>{{ $data->outage_reason }}</td>
			</tr>
			<tr>
				<td>Sub Outage Reason</td>
				<td>{{ $data->outage_sub }}</td>
			</tr>
			<tr>
				<td>Recovery Action</td>
				<td>{{ $data->action_close." (".$data->closed_message.")" }}</td>
			</tr>
			<tr>
				<td colspan="2"><b>RECOVERY DETAIL </b></td>
			</tr>
			<tr>
				<td>Outage Start Time</td>
				<td>{{ date('l,d F Y H:i:s', strtotime($data->start_date)).' (GMT+7)' }}</td>
			</tr>
			<tr>
				<td>Outage Finish Time</td>
				<td>{{ date('l,d F Y H:i:s', strtotime($data->resolved_date)).' (GMT+7)' }}</td>
			</tr>
			<tr>
				<td>Hold Time (Due to {{ $data->hold_category }})</td>
				<td>
					@for($i=0; $i < count($holdtimes); $i++) {{ date('l,d F Y H:i:s', strtotime($holdtimes[$i]['hold_created_date'])).' (GMT+7)' }} {{ date('l,d F Y H:i:s', strtotime($holdtimes[$i]['unhold_date'])).' (GMT+7)' }} <br>

						@endfor
				</td>
			</tr>
			<tr>
				<td>Total Duration</td>
				<td>{{ $data->duration }}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<p>
		We kindly inform you that the service has been running normally again. We sincerely apologize for any inconvenience caused by this outage and appreciate your understanding in this matter.
	</p>
	<div id="footer">
		<img <img src="{{public_path('admin/images/footer_mail.JPG')}}" alt="" width="700" height="70" />
	</div>
</body>
@endforeach

</html>