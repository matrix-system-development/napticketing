<!doctype html>
<html lang="en">
  <head>
    <title>Napticketing</title>
  </head>
  <body>
    <span>
        Dear {{ $data['assign_to'] }}, <br><br> 

        Please help this new ticket with detail: <br><br>
        <b>No Ticket</b>  : {{ $data['no_ticket'] }}<br>

        @if($data['cust_name'] == null)
            
        @else
            <b>Customer Name</b>  : {{ $data['cust_name'] }}<br>
        @endif

        <b>Category</b>  : {{ $data['category'] }}<br>
        <b>Sub Category</b>    : {{ $data['subcategory'] }}<br>
        <b>Status Complain</b>    : {{ $data['status_complain'] }}<br>
        <b>Status Link Reported</b>    : {{ $data['link_respond'] }}<br>
        <b>Notes</b>    : {{ $data['notes'] }}<br>
        <br>
        <br>
        Thank you,<br>
        Regards <br>
        [{{ $data['created_by'] }}]
        <br>
        <br>
    </span>
  </body>
</html>
