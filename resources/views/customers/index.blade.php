<style>
    #user_apps {
        border: 4px solid gray; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }

    table > thead > tr > th {
        background-color:#151A48; 
        color:white;
    }
    #btnAddCust {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnAddCust:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnAddCust:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-footer > button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnDetail {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnDetail:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnDetail:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnEdit {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnEdit:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnEdit:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <!--alert success -->
        @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{ session('status') }}</strong>
            </div> 
        @endif
        <!--alert success -->
        
        <!--validasi form-->
            @if (count($errors)>0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        <li><strong>Saved Data Failed !</strong></li>
                        @foreach ($errors->all() as $error)
                            <li><strong>{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>   
            @endif
        <!--end validasi form-->

        <div class="buttons-heading">
            <h2>Master Customers</h2>
        </div>

        @if (auth()->user()->role == 'Partnership')
            <div class="bs-component mb20">
                <a href="{{ url('/masters/customers/create-partnership') }}" class="btn btn-primary" id="btnAddCust">Add Customer</a>
            </div>
        @else   
            <div class="bs-component mb20">
                <a href="{{ url('/masters/customers/create') }}" class="btn btn-primary" id="btnAddCust">Add Customer</a>
            </div>
        @endif

        <!-- table-->
            <table id="tblcustomer" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>CID</th>
                        <th>Customer Code</th>
                        <th>Name</th>
                        <th>Service</th>
                        <th>Net.Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $customer->cid }}</td>
                        <td>{{ $customer->customer_code }}</td>
                        <td>{{ $customer->company_name }}</td>
                        <td>{{ $customer->service }}</td>
                        <td>{{ $customer->network_type }}</td>
                        <td><i><b>{{ $customer->status_customer }}</b></i></td>
                        <td>
                            <a href="{{ url('/masters/customers/'.base64_encode($customer->id)) }}"  class="btn btn-xs" id="btnDetail">Detail</a>
                            @if (auth()->user()->role != 'Partnership')
                            <a href="{{ url('/masters/customers/'.base64_encode($customer->id).'/edit') }}"  class="btn btn-xs" id="btnEdit">Edit</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <!-- table-->
    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $('#tblcustomer').DataTable( {
            "order": [],
            responsive: true
        });

        $('#btnResetModalUser').off('click').on('click',resetModalUser);
        $('#closeModalUser').off('click').on('click',resetModalUser);
        
        function resetModalUser() 
        {
            $('#email1').val('');
        }

        $('#btnResetModalRole').off('click').on('click',resetModalRole);
        $('#closeModalRole').off('click').on('click',resetModalRole);
        
        function resetModalRole() 
        {
            $('#email').val('');
            $('#role').val('');
        }

        $('#btnResetModalPic').off('click').on('click',resetModalPic);
        $('#closeModalPic').off('click').on('click',resetModalPic);
        
        function resetModalPic() 
        {
            $('.email-pic').val('');
        }
    } );
</script>
@endsection