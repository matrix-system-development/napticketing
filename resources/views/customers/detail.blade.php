@extends('layout.master')
@section('content')

    <div class="main-grid">
        <div class="agile-grids">
            <div class="buttons-heading">
                <h2>Detail Customer</h2>
            </div>
            <br>
            <!-- grids -->
            <div class="grids">
                <h2>Customer Information</h2>
                <!--validasi form-->
                    @if (count($errors)>0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                <li><strong>Saved Data Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>   
                    @endif
                <!--end validasi form-->
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>CID</label> 
                                            <input type="text" name="cid" class="form-control" id="cid" value="{{ $customer->cid  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>CID New</label> 
                                            <input type="text" name="cid_new" class="form-control" id="cid_new" value="{{ $customer->cid_new  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Cust Code</label> 
                                            <input type="text" name="cust_code" class="form-control" id="cust_code" value="{{ $customer->customer_code  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Company / Customer Name</label> 
                                            <input type="text" name="company_name" class="form-control" id="company_name" value="{{ $customer->company_name  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Customer Type</label> 
                                            <input type="text" name="cust_type" class="form-control" id="cust_type" value="{{ $customer->company_type  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Address</label> 
                                            <input type="text" name="address" class="form-control" id="address" value="{{ $customer->address  }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>PIC Name</label> 
                                            <input type="text" name="pic_name" class="form-control" id="pic_name" value="{{ $customer->pic_name  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>PIC Contact</label> 
                                            <input type="text" name="pic_contact" class="form-control" id="pic_contact" value="{{ $customer->pic_contact  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>PIC Email</label> 
                                            <input type="text" name="pic_email" class="form-control" id="pic_email" value="{{ $customer->pic_email  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>A-End</label> 
                                            <input type="text" name="a_end" class="form-control" id="a_end" value="{{ $customer->a_end  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>B-End</label> 
                                            <input type="text" name="b_end" class="form-control" id="b_end" value="{{ $customer->b_end  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>AM Name</label> 
                                            <input type="text" name="am_name" class="form-control" id="am_name" value="{{ $customer->am_name  }}" readonly> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Broadband</label>
                                            <input type="text" name="broadband" class="form-control" id="broadband" value="{{ $customer->broadband  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Priority</label>
                                            <input type="text" name="priority" class="form-control" id="priority" value="{{ $customer->priority  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Type</label>
                                            <input type="text" name="net_type" class="form-control" id="net_type" value="{{ $customer->network_type  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Owner</label> 
                                            <input type="text" name="net_owner" class="form-control" id="net_owner" value="{{ $customer->network_owner  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>3rd Party Circuit ID</label> 
                                            <input type="text" name="partner_cid" class="form-control" id="partner_cid" value="{{ $customer->partner_cid  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Regional</label>
                                            <input type="text" name="regional" class="form-control" id="regional" value="{{ $customer->regional  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>City</label> 
                                            <input type="text" name="city" class="form-control" id="city" value="{{ $customer->city  }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Capacity Local / IXX (Mbps)</label> 
                                            <input type="text" name="capacity" class="form-control" id="capacity" value="{{ $customer->capacity  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity International / IX (Mbps)</label> 
                                            <input type="text" name="capacity_international" class="form-control" id="capacity_international" value="{{ $customer->capacity_international  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity Mix (Mbps)</label> 
                                            <input type="text" name="capacity_mix" class="form-control" id="capacity_mix" value="{{ $customer->capacity_mix  }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Service</label>
                                            <input type="text" name="service" class="form-control" id="service" value="{{ $customer->service  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Start Date</label> 
                                            <input type="date" name="start_date" class="form-control" id="start_date" value="{{ $customer->start_date_customer  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Finish Date</label> 
                                            <input type="date" name="finish_date" class="form-control" id="finish_date" value="{{ $customer->finish_date  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Username PPoE</label> 
                                            <input type="text" name="username_ppoe" class="form-control" id="username_ppoe" value="{{ $customer->username_ppoe  }}" readonly>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Password PPoE</label> 
                                            <input type="text" name="password_ppoe" class="form-control" id="password_ppoe" value="{{ $customer->password_ppoe  }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Status Customer</label>
                                            <input type="text" name="status_cust" class="form-control" id="status_cust" value="{{ $customer->status_customer  }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Notes</label> 
                                            <textarea name="notes" class="form-control" id="notes" cols="20" rows="5" readonly>{{ $customer->notes }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="bs-component mb20" align="center">
                    <a href="{{ url('/masters/customers/') }}" class="btn btn-primary">Back to Customer List</a>
                    @if (auth()->user()->role != 'Partnership')
                    <a href="{{ url('/masters/customers/'.base64_encode($customer->id).'/edit') }}" class="btn btn-primary">Edit</a>
                    @endif
                </div>
            </div>
            <!-- //grids -->
        </div>
    </div> 

    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            //customer search
            $( "#customer_search" ).autocomplete({
                
                source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url:"{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,
                    search: request.term
                    },
                    success: function( data ) {
                    
                    response( data );
                   
                    }
                });
                },
                select: function (event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.label); // display the selected text
                $('#cid').val(ui.item.value_cid); // save selected id to input
                $('#cust_code').val(ui.item.value_custcode);
                $('#company_name').val(ui.item.value_compname); // save selected id to input
                $('#priority').val(ui.item.value_priority);
                $('#company_type').val(ui.item.value_comptype);
                $('#service').val(ui.item.value_service);
                $('#pic_name').val(ui.item.value_picname);
                $('#pic_contact').val(ui.item.value_piccontact);
                $('#pic_email').val(ui.item.value_picemail);
                $('#capacity').val(ui.item.value_capacity);
                $('#capacity_international').val(ui.item.value_capacityInt);
                $('#a_end').val(ui.item.value_a_end);
                $('#b_end').val(ui.item.value_b_end);
                $('#network_type').val(ui.item.value_nettype);
                $('#network_owner').val(ui.item.value_netowner);
                $('#start_date').val(ui.item.value_startdate);
                $('#finish_date').val(ui.item.value_finishdate);
                $('#partner_cid').val(ui.item.value_partnercid);
                $('#username_ppoe').val(ui.item.value_usernameppoe);
                $('#password_ppoe').val(ui.item.value_passwordppoe);
                $('#regional').val(ui.item.value_regional);
                $('#city').val(ui.item.value_city);
                return false;
                },
                
            });

            //select network owner from network type
            $('select[name="net_type"]').on('change', function() {
                var categoryID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', categoryID);
                if(categoryID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="net_owner"]').empty();
                            $('select[name="net_owner"]').append(
                                    '<option value="">- Select Network Owner -</option>'
                            );
                            $.each(data, function(category, value) {
                                $('select[name="net_owner"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="net_owner"]').empty();
                }
            });

            //select city from select regional
            $('select[name="regional"]').on('change', function() {
                var cityID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', cityID);
                if(cityID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="city"]').empty();
                            $('select[name="city"]').append(
                                    '<option value="">- Select City -</option>'
                            );
                            $.each(data, function(slr_data, value) {
                                $('select[name="city"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="city"]').empty();
                }
            });

            //reset search customer
            $('#reset').click(function(){
                $(':input','#myform')
                .not(':button, :submit, :reset, :hidden')
                $('#customer_search').val('');
                $('#cid').val('');
                $('#company_name').val('');
                $('#priority').val('');
                $('#company_type').val('');
                $('#service').val('');
                $('#pic_name').val('');
                $('#pic_contact').val('');
                $('#capacity').val('');
                $('#capacity_international').val('');
                $('#a_end').val('');
                $('#b_end').val('');
                $('#network_type').val('');
                $('#network_owner').val('');
                $('#start_date').val('');
                $('#finish_date').val('');
                $('#partner_cid').val('');
                $('#username_ppoe').val('');
                $('#password_ppoe').val('');
                $('#regional').val('');
                $('#city').val('');
            });

            $('#myform').submit(function(){
                $("#btnSubmitExternal", this).html("Please Wait...").attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@endsection