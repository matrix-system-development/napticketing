@extends('layout.master')
@section('content')

    <div class="main-grid">
        <div class="agile-grids">
            <div class="buttons-heading">
                <h2>Add Customer</h2>
            </div>
            <br>
            <!-- grids -->
            <div class="grids">
                <h2>Customer Information</h2>
                <!--validasi form-->
                    @if (count($errors)>0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                <li><strong>Saved Data Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>   
                    @endif
                <!--end validasi form-->
                <form action="{{ url('/masters/customers/store-partnership') }}" method="post" id="myform" enctype="multipart/form-data">
                    @csrf
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Company / Customer Name*</label> 
                                            <input type="text" name="company_name" class="form-control" id="company_name" value="{{ old('company_name') }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>A-End*</label> 
                                            <input type="text" name="a_end" class="form-control" id="a_end" value="{{ old('a_end') }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>B-End</label> 
                                            <input type="text" name="b_end" class="form-control" id="b_end" value="{{ old('b_end') }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>AM Name*</label> 
                                            <select name="am_name" id="am_name" class="form-control">
                                                <option value="">- Please Select AM Name -</option>
                                                @foreach ($dataSales as $sales)
                                                    <option value="{{ $sales->name }}" {{ old('am_name') == $sales->name ? 'selected' : '' }}>{{ $sales->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Service*</label> 
                                            <select name="service" id="service" class="form-control">
                                                <option value="">- Please Select Service -</option>
                                                @foreach ($dropdowns['service'] as $category)
                                                    <option value="{{ $category->name_value }}">{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Network Type*</label> 
                                            <select name="net_type" id="net_type" class="form-control">
                                                <option value="">- Please Select Network Type -</option>
                                                @foreach ($dropdowns['network type'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ old('net_type') == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Owner*</label> 
                                            <select name="net_owner" id="net_owner" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity Local / IXX (Mbps)</label> 
                                            <input type="text" name="capacity" class="form-control" id="capacity" value="{{ old('capacity') }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity International / IX (Mbps)</label> 
                                            <input type="text" name="capacity_international" class="form-control" id="capacity_international" value="{{ old('capacity_international') }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity Mix (Mbps)</label> 
                                            <input type="text" name="capacity_mix" class="form-control" id="capacity_mix" value="{{ old('capacity_mix') }}"> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="bs-component mb20" align="center">
                    <button type="submit" id="btnSubmitExternal" class="btn btn-primary">Submit</button>
                </div>

                </form>
            </div>
            <!-- //grids -->
        </div>
    </div> 

    <script>
        $(document).ready(function() {
            // Initialize Select2
            $('#am_name').select2({
                placeholder: '- Please Select AM Name -',
                // theme: '.js-example-basic-single'
            });
        });
    </script>

    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            //customer search
            $( "#cid" ).autocomplete({
                
                source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url:"{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,
                    search: request.term
                    },
                    success: function( data ) {
                    
                    response( data );
                   
                    }
                });
                },
                select: function (event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.label); // display the selected text
                $('#cid').val(ui.item.value_cid); // save selected id to input
                return false;
                },
                
            });

            //select network owner from network type
            $('select[name="net_type"]').on('change', function() {
                var categoryID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', categoryID);
                if(categoryID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="net_owner"]').empty();
                            $('select[name="net_owner"]').append(
                                    '<option value="">- Select Network Owner -</option>'
                            );
                            $.each(data, function(category, value) {
                                $('select[name="net_owner"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="net_owner"]').empty();
                }
            });

            //select city from select regional
            $('select[name="regional"]').on('change', function() {
                var cityID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', cityID);
                if(cityID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="city"]').empty();
                            $('select[name="city"]').append(
                                    '<option value="">- Select City -</option>'
                            );
                            $.each(data, function(slr_data, value) {
                                $('select[name="city"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="city"]').empty();
                }
            });

            //reset search customer
            $('#reset').click(function(){
                $(':input','#myform')
                .not(':button, :submit, :reset, :hidden')
                $('#customer_search').val('');
                $('#cid').val('');
                $('#company_name').val('');
                $('#priority').val('');
                $('#cust_type').val('');
                $('#service').val('');
                $('#pic_name').val('');
                $('#pic_contact').val('');
                $('#capacity').val('');
                $('#capacity_international').val('');
                $('#a_end').val('');
                $('#b_end').val('');
                $('#network_type').val('');
                $('#network_owner').val('');
                $('#start_date').val('');
                $('#finish_date').val('');
                $('#partner_cid').val('');
                $('#username_ppoe').val('');
                $('#password_ppoe').val('');
                $('#regional').val('');
                $('#city').val('');
            });

            $('#myform').submit(function(){
                $("#btnSubmitExternal", this).html("Please Wait...").attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@endsection