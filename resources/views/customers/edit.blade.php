@extends('layout.master')
@section('content')

    <div class="main-grid">
        <div class="agile-grids">
            <div class="buttons-heading">
                <h2>Edit Customer</h2>
            </div>
            <br>
            <!-- grids -->
            <div class="grids">
                <h2>Customer Information</h2>
                <!--validasi form-->
                    @if (count($errors)>0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                <li><strong>Saved Data Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>   
                    @endif
                <!--end validasi form-->
                <form action="{{ url('/masters/customers/'.$customer->id) }}" method="post" id="myform" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>CID*</label> 
                                            <input type="text" name="cid" class="form-control" id="cid" value="{{ $customer->cid }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>CID New</label> 
                                            <input type="text" name="cid_new" class="form-control" id="cid_new" value="{{ $customer->cid_new }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Cust Code*</label> 
                                            <input type="text" name="cust_code" class="form-control" id="cust_code" value="{{ $customer->customer_code }}" readonly> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Company / Customer Name*</label> 
                                            <input type="text" name="company_name" class="form-control" id="company_name" value="{{ $customer->company_name }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Customer Type</label> 
                                            <select name="cust_type" id="cust_type" class="form-control">
                                            <option value="">- Please Select Customer Type -</option>
                                                @foreach ($dropdowns['Customer Type'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->company_type == $category->name_value ? 'selected' : '' }}>{{ $category->name_value}}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" name="cust_type" class="form-control" id="cust_type" value="{{ $customer->company_type }}"> --}}
                                        </div>
                                        <div class="form-group"> 
                                            <label>Address*</label> 
                                            <input type="text" name="address" class="form-control" id="address" value="{{ $customer->address }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>PIC Name*</label> 
                                            <input type="text" name="pic_name" class="form-control" id="pic_name" value="{{ $customer->pic_name }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>PIC Contact*</label> 
                                            <input type="text" name="pic_contact" class="form-control" id="pic_contact" value="{{ $customer->pic_contact }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>PIC Email*</label> 
                                            <input type="text" name="pic_email" class="form-control" id="pic_email" value="{{ $customer->pic_email }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>A-End*</label> 
                                            <input type="text" name="a_end" class="form-control" id="a_end" value="{{ $customer->a_end }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>B-End</label> 
                                            <input type="text" name="b_end" class="form-control" id="b_end" value="{{ $customer->b_end }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>AM Name*</label>
                                            <select name="am_name" id="am_name" class="form-control">
                                                {{-- <option value="">- Please Select AM Name -</option> --}}
                                                @foreach ($dataSales as $sales)
                                                    <option value="{{ $sales->name }}" {{ $customer->am_name == $sales->name ? 'selected' : '' }}>{{ $sales->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Broadband*</label> 
                                            <select name="broadband" id="broadband" class="form-control">
                                                <option value="">- Please Select Broadband -</option>
                                                @foreach ($dropdowns['broadband'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->broadband == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Priority*</label> 
                                            <select name="priority" id="priority" class="form-control">
                                                <option value="">- Please Select Priority -</option>
                                                @foreach ($dropdowns['priority'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->priority == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Type*</label> 
                                            <select name="net_type" id="net_type" class="form-control">
                                                <option value="">- Please Select Network Type -</option>
                                                @foreach ($dropdowns['network type'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->network_type == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Network Owner*</label> 
                                            <select name="net_owner" id="net_owner" class="form-control">
                                                <option value="{{ $customer->network_owner }}">{{ $customer->network_owner }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>3rd Party Circuit ID</label> 
                                            <input type="text" name="partner_cid" class="form-control" id="partner_cid" value="{{ $customer->partner_cid }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Regional</label> 
                                            <select name="regional" id="regional" class="form-control">
                                                <option value="">- Please Select Regional -</option>
                                                @foreach ($dropdowns['regional'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->regional == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>City</label> 
                                            <select name="city" id="city" class="form-control">
                                                <option value="{{ $customer->city }}">{{ $customer->city }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Capacity Local / IXX (Mbps)</label> 
                                            <input type="text" name="capacity" class="form-control" id="capacity" value="{{ $customer->capacity }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity International / IX (Mbps)</label> 
                                            <input type="text" name="capacity_international" class="form-control" id="capacity_international" value="{{ $customer->capacity_international }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Capacity Mix (Mbps)</label> 
                                            <input type="text" name="capacity_mix" class="form-control" id="capacity_mix" value="{{ $customer->capacity_mix }}"> 
                                        </div>
                                        <div class="form-group"> 
                                            <label>Service*</label> 
                                            <select name="service" id="service" class="form-control">
                                                <option value="">- Please Select Service -</option>
                                                @foreach ($dropdowns['service'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->service == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Start Date*</label> 
                                            <input type="date" name="start_date" class="form-control" id="start_date" value="{{ $customer->start_date_customer }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Finish Date*</label> 
                                            <input type="date" name="finish_date" class="form-control" id="finish_date" value="{{ $customer->finish_date }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Username PPoE</label> 
                                            <input type="text" name="username_ppoe" class="form-control" id="username_ppoe" value="{{ $customer->username_ppoe }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Password PPoE</label> 
                                            <input type="text" name="password_ppoe" class="form-control" id="password_ppoe" value="{{ $customer->password_ppoe }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Status Customer*</label>
                                            <select name="status_cust" id="status_cust" class="form-control">
                                                <option value="">- Please Select Status -</option>
                                                @foreach ($dropdowns['status customer'] as $category)
                                                    <option value="{{ $category->name_value }}" {{ $customer->status_customer == $category->name_value ? 'selected' : '' }}>{{ $category->name_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Notes</label> 
                                            <textarea name="notes" class="form-control" id="notes" cols="20" rows="5">{{ $customer->notes }}</textarea>
                                        </div>
                                        <div class="form-group"> 
                                            <label><span style="color: red">NOTES: input fields with the symbol * are mandatory</span> </label> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="bs-component mb20" align="center">
                    <button type="submit" id="btnSubmitExternal" class="btn btn-primary">Update</button>
                </div>

                </form>
            </div>
            <!-- //grids -->
        </div>
    </div> 

    <script>
        $(document).ready(function() {
            // Initialize Select2
            $('#am_name').select2({
                // placeholder: '- Please Select AM Name -',
                // theme: '.js-example-basic-single'
            });
        });
    </script> 

    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            //customer search
            $( "#customer_search" ).autocomplete({
                
                source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url:"{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,
                    search: request.term
                    },
                    success: function( data ) {
                    
                    response( data );
                   
                    }
                });
                },
                select: function (event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.label); // display the selected text
                $('#cid').val(ui.item.value_cid); // save selected id to input
                $('#cust_code').val(ui.item.value_custcode);
                $('#company_name').val(ui.item.value_compname); // save selected id to input
                $('#priority').val(ui.item.value_priority);
                $('#company_type').val(ui.item.value_comptype);
                $('#service').val(ui.item.value_service);
                $('#pic_name').val(ui.item.value_picname);
                $('#pic_contact').val(ui.item.value_piccontact);
                $('#pic_email').val(ui.item.value_picemail);
                $('#capacity').val(ui.item.value_capacity);
                $('#capacity_international').val(ui.item.value_capacityInt);
                $('#a_end').val(ui.item.value_a_end);
                $('#b_end').val(ui.item.value_b_end);
                $('#network_type').val(ui.item.value_nettype);
                $('#network_owner').val(ui.item.value_netowner);
                $('#start_date').val(ui.item.value_startdate);
                $('#finish_date').val(ui.item.value_finishdate);
                $('#partner_cid').val(ui.item.value_partnercid);
                $('#username_ppoe').val(ui.item.value_usernameppoe);
                $('#password_ppoe').val(ui.item.value_passwordppoe);
                $('#regional').val(ui.item.value_regional);
                $('#city').val(ui.item.value_city);
                return false;
                },
                
            });

            //select network owner from network type
            $('select[name="net_type"]').on('change', function() {
                var categoryID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', categoryID);
                if(categoryID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="net_owner"]').empty();
                            $('select[name="net_owner"]').append(
                                    '<option value="">- Select Network Owner -</option>'
                            );
                            $.each(data, function(category, value) {
                                $('select[name="net_owner"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    //alert('hai');
                    $('select[name="net_owner"]').empty();
                }
            });

            //select city from select regional
            $('select[name="regional"]').on('change', function() {
                var cityID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', cityID);
                if(cityID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="city"]').empty();
                            $('select[name="city"]').append(
                                    '<option value="">- Select City -</option>'
                            );
                            $.each(data, function(slr_data, value) {
                                $('select[name="city"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="city"]').empty();
                }
            });

            //reset search customer
            $('#reset').click(function(){
                $(':input','#myform')
                .not(':button, :submit, :reset, :hidden')
                $('#customer_search').val('');
                $('#cid').val('');
                $('#company_name').val('');
                $('#priority').val('');
                $('#company_type').val('');
                $('#service').val('');
                $('#pic_name').val('');
                $('#pic_contact').val('');
                $('#capacity').val('');
                $('#capacity_international').val('');
                $('#a_end').val('');
                $('#b_end').val('');
                $('#network_type').val('');
                $('#network_owner').val('');
                $('#start_date').val('');
                $('#finish_date').val('');
                $('#partner_cid').val('');
                $('#username_ppoe').val('');
                $('#password_ppoe').val('');
                $('#regional').val('');
                $('#city').val('');
            });

            $('#myform').submit(function(){
                $("#btnSubmitExternal", this).html("Please Wait...").attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@endsection