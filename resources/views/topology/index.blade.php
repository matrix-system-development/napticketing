<style>
    #topology {
        border: 4px solid gray; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }

    table > thead > tr > th {
        background-color:#151A48; 
        color:white;
    }
    #btnTopology {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnTopology:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnTopology:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-footer > button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <!--alert success -->
        @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{ session('status') }}</strong>
            </div> 
        @endif
        <!--alert success -->
        
        <!--validasi form-->
            @if (count($errors)>0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        <li><strong>Saved Data Failed !</strong></li>
                        @foreach ($errors->all() as $error)
                            <li><strong>{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>   
            @endif
        <!--end validasi form-->

        <div class="buttons-heading">
            <h2>Master Topology</h2>
        </div>
        <!-- Button trigger modal Category -->
        <div class="bs-component mb20">
            <button type="button" class="btn btn-primary" data-toggle="modal" id="btnTopology" data-target="#myModalTopology">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span class="nav-text">
                    Add Topology
                </span>
            </button>
        </div>
        <!-- Button trigger modal Category-->

        <!-- table-->
            <table id="topology" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Customers Info</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                    <tr>
                        <td>
                            <a href="{{url('/masters/topology/'.$customer->id)}}" target="_blank"><b>{{ $customer->company_name }}</b><br></a>
                            {{ $customer->cid }}
                        </td>
                        <td>
                            @php
                                $file1=base64_encode($customer->topology_file1);
                                $file2=base64_encode($customer->topology_file2);
                                $file3=base64_encode($customer->topology_file3);
                                
                                $cust=base64_encode($customer->company_name);
                            @endphp
                            
                            @if ($file1<>'')
                                <a href="{{ url('/masters/topology/view/'.$file1.'/'.$cust) }}" class="btn btn-info btn-xs" target="_blank">
                                    Topology 1
                                </a>
                            @endif

                            @if ($file2<>'')
                                <a href="{{ url('/masters/topology/view/'.$file2.'/'.$cust) }}" class="btn btn-info btn-xs" target="_blank">
                                    Topology 2
                                </a>
                            @endif
                            
                            @if ($file3<>'')
                                <a href="{{ url('/masters/topology/view/'.$file3.'/'.$cust) }}" class="btn btn-info btn-xs" target="_blank">
                                    Topology 3
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <!-- table-->
    </div>
</div>

<!-- Modal Category-->
<div class="modal fade" id="myModalTopology" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Add Topology</b>
                <button type="button" class="close" data-dismiss="modal" id="closeModalTopology" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <form action="{{url('/masters/topology')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <label>CID</label>
                        <input type="text" name="topology_cid" id="topology_cid" class="form-control" value="{{ old('topology_cid') }}">
                    </div>
                    <div class="form-group">
                        <label>Choose Topology File</label>
                        <input type="file" name="topology_file1" id="topology_file1" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Choose Topology File</label>
                        <input type="file" name="topology_file2" id="topology_file2" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Choose Topology File</label>
                        <input type="file" name="topology_file3" id="topology_file3" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnResetModalTopology"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- Modal -->

<script>
    $(document).ready(function() {
        var table = $('#topology').DataTable( {
            "order": [],
            responsive: true
        });

        $('#btnResetModalTopology').off('click').on('click',resetModalTopology);
        $('#closeModalTopology').off('click').on('click',resetModalTopology);
        
        function resetModalTopology() 
        {
            $('#topology_cid').val('');
            $('#topology_file1').val('');
            $('#topology_file2').val('');
            $('#topology_file3').val('');
        }
    } );
</script>
@endsection