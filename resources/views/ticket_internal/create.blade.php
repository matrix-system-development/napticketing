@extends('layout.master')
@section('content')
<style type="text/css">
    .datacust {
        display: none;
    }
</style>
<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>Create Ticket Internal</h2>
        </div>
        <br>
        <form action="{{ url('/tickets_internal') }}" method="post" id="myform" enctype="multipart/form-data">
            @csrf
            <!--validasi form-->
            @if (count($errors)>0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    <li><strong>Saved Data Failed !</strong></li>
                    @foreach ($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                    @endforeach
                </ul>
            </div>
            @endif
            <!--end validasi form-->
            <!-- grids -->
            <div class="grids">
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="checkbox" align="center">
                                        <input type="checkbox" id="needcust" name="colorCheckbox" value="Cust"> Need Customer Data
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Cust datacust">
                    <h2>Customer Information</h2>
                    <div class="panel panel-widget top-grids">
                        <div class="chute chute-center">
                            <div class="row mb40">
                                <div class="col-md-12">
                                    <div class="demo-grid">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Multiple CID</label>
                                                <select name="multiple" id="multiple" class="form-control">
                                                    <option value="no">No</option>
                                                    <option value="yes">Yes</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Search by Name / CID / CID New</label>
                                                <input type="text" class="form-control" name='customer_search' id='customer_search' value="{{ old('customer_search') }}">
                                                <div class="form-group-append">
                                                    <input class="btn btn-primary" type="button" id="reset" value="Reset Search">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb40">
                                <div class="col-md-6">
                                    <div class="demo-grid">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>CID</label>
                                                    <input type="text" name="cid" class="form-control" id="cid" value="{{ old('cid') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Company Name</label>
                                                    <input type="text" name="company_name" class="form-control" id="company_name" value="{{ old('company_name') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Priority</label>
                                                    <input type="text" name="priority" class="form-control" id="priority" value="{{ old('priority') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Company Type</label>
                                                    <input type="text" name="company_type" class="form-control" id="company_type" value="{{ old('company_type') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Service</label>
                                                    <input type="text" name="service" class="form-control" id="service" value="{{ old('service') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Network Owner</label>
                                                    <input type="text" name="network_owner" class="form-control" id="network_owner" value="{{ old('network_owner') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>AM Name</label>
                                                    <input type="text" name="am_name" class="form-control" id="am_name" value="{{ old('am_name') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <input type="text" name="start_date" class="form-control" id="start_date" value="{{ old('start_date') }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="demo-grid">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>PIC Name</label>
                                                    <input type="text" name="pic_name" class="form-control" id="pic_name" value="{{ old('pic_name') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>PIC Contact</label>
                                                    <input type="text" name="pic_contact" class="form-control" id="pic_contact" value="{{ old('pic_contact') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Capacity Local / IXX (Mbps)</label>
                                                    <input type="text" name="capacity" class="form-control" id="capacity" value="{{ old('capacity') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Capacity International / IX (Mbps)</label>
                                                    <input type="text" name="capacity_international" class="form-control" id="capacity_international" value="{{ old('capacity_international') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Capacity Mix</label>
                                                    <input type="text" name="capacity_mix" class="form-control" id="capacity_mix" value="{{ old('capacity_mix') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>A-End</label>
                                                    <input type="text" name="a_end" class="form-control" id="a_end" value="{{ old('a_end') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>B-End</label>
                                                    <input type="text" name="b_end" class="form-control" id="b_end" value="{{ old('b_end') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Network Type</label>
                                                    <input type="text" name="network_type" class="form-control" id="network_type" value="{{ old('network_type') }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Finish Date</label>
                                                    <input type="text" name="finish_date" class="form-control" id="finish_date" value="{{ old('finish_date') }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h2>Detail Ticket</h2>
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label>Status Complain</label>
                                            <select name="status_complain" id="status_complain" class="form-control">
                                                <option value="Low">Low</option>
                                                <option value="Medium">Medium</option>
                                                <option value="High">High</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Category Ticket Internal</label>
                                            <select name="sub_category" id="sub_category" class="form-control">
                                                <option value="">- Please Select Sub Category -</option>
                                                @foreach ($mappings as $mapping)
                                                <option value="{{ $mapping->id }}" {{ old('sub_category') == $mapping->name_incident ? 'selected' : '' }}>{{ $mapping->name_incident }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Notes (max 255 character)</label>
                                            <textarea name="message" id="message" cols="20" rows="5" class="form-control">{{ old('message') }}</textarea>
                                        </div>

                                        <!-- Button trigger modal -->
                                        <div class="bs-component mb20" align="center">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-upload" aria-hidden="true"></i> Upload Document
                                            </button>
                                        </div>

                                        <!-- Modal Upload-->
                                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel">Upload Document</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-body">
                                                            <span style="color: red">* file must : pdf, xls, xlsx, png, jpg, jpeg, doc, docx, gif | max : 10 MB</span>
                                                            <div class="form-group">
                                                                <input type="file" name="doc1" id="doc1" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="file" name="doc2" id="doc2" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="file" name="doc3" id="doc3" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="file" name="doc4" id="doc4" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="file" name="doc5" id="doc5" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Modal Upload-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h2>Assign To</h2>
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label>Department NAP</label>
                                            <select name="department" id="department" class="form-control">
                                                <option value="">- Please Select Department -</option>
                                                @foreach ($dropdowns['dept'] as $dept_data )
                                                <option value="{{ $dept_data->department_id }}">{{ $dept_data->department_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div id="checkbox-container" style="display:none;">
                                                <input type="checkbox" id="myCheckbox_ca" name="for_ca" class="form-check-input form-control-lg" value="1">
                                                <label for="myCheckbox_ca" class="font-boldfont-weight-bold text-primary ">Ticket For Customer Assistant</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bs-component mb20" align="center">
                    <button type="submit" id="btnSubmitInternal" class="btn btn-primary">Submit</button>
                </div>

        </form>
    </div>
    <!-- //grids -->
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#needcust').click(function() {
            var inputValue = $(this).attr("value");
            $("." + inputValue).toggle();
            $(':input', '#myform')
                .not(':button, :submit, :reset, :hidden')
            $('#customer_search').val('');
            $('#cid').val('');
            $('#company_name').val('');
            $('#priority').val('');
            $('#company_type').val('');
            $('#service').val('');
            $('#pic_name').val('');
            $('#pic_contact').val('');
            $('#capacity').val('');
            $('#capacity_international').val('');
            $('#capacity_mix').val('');
            $('#am_name').val('');
            $('#a_end').val('');
            $('#b_end').val('');
            $('#network_type').val('');
            $('#network_owner').val('');
            $('#start_date').val('');
            $('#finish_date').val('');
        });
    });


    // CSRF Token
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function() {
        //customer search
        $("#customer_search").autocomplete({

            source: function(request, response) {
                // Fetch data
                $.ajax({
                    url: "{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                        _token: CSRF_TOKEN,
                        search: request.term
                    },
                    success: function(data) {

                        response(data);
                    }
                });
            },
            select: function(event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.label); // display the selected text
                $('#cid').val(ui.item.value_cid); // save selected id to input
                $('#company_name').val(ui.item.value_compname); // save selected id to input
                $('#priority').val(ui.item.value_priority);
                $('#company_type').val(ui.item.value_comptype);
                $('#service').val(ui.item.value_service);
                $('#pic_name').val(ui.item.value_picname);
                $('#pic_contact').val(ui.item.value_piccontact);
                $('#capacity').val(ui.item.value_capacity);
                $('#capacity_international').val(ui.item.value_capacityInt);
                $('#capacity_mix').val(ui.item.value_capacitymix);
                $('#am_name').val(ui.item.value_am);
                $('#a_end').val(ui.item.value_a_end);
                $('#b_end').val(ui.item.value_b_end);
                $('#network_type').val(ui.item.value_nettype);
                $('#network_owner').val(ui.item.value_netowner);
                $('#start_date').val(ui.item.value_startdate);
                $('#finish_date').val(ui.item.value_finishdate);
                return false;
            },

        });

        //reset search customer
        $('#reset').click(function() {
            $(':input', '#myform')
                .not(':button, :submit, :reset, :hidden')
            $('#customer_search').val('');
            $('#cid').val('');
            $('#company_name').val('');
            $('#priority').val('');
            $('#company_type').val('');
            $('#service').val('');
            $('#pic_name').val('');
            $('#pic_contact').val('');
            $('#capacity').val('');
            $('#capacity_international').val('');
            $('#a_end').val('');
            $('#b_end').val('');
            $('#network_type').val('');
            $('#network_owner').val('');
            $('#start_date').val('');
            $('#finish_date').val('');
        });

        $('#myform').submit(function() {
            $("#btnSubmitInternal", this).html("Please Wait...").attr('disabled', 'disabled');
            return true;
        });

        // checkbox ticket for ca
        $('#department').on('change', function() {
            // alert($(this).val())
            if ($(this).val() == 34) {
                $('#checkbox-container').show();
            } else {
                $('#checkbox-container').hide();
            }
        });
    });
</script>
@endsection