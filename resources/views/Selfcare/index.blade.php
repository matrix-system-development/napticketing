<style>
    #tracking {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    thead > tr > th {
        background-color: #151A48;
        color: white;
    }

    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }

    #btnSubmit {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnSubmit:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnSubmit:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnCancel {
        background-color: #f10b0b;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnCancel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnCancel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnNotes {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnNotes:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnNotes:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    
    #btnExportExcel {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnExportExcel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnExportExcel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnFilterPeriod:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnDetail {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnFH {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnDetail:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnFilterPeriod:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>SelfCare Report List</h2>
        </div>
        <!--validasi form-->
        @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                <li><strong>Processing Data Failed !</strong></li>
                @foreach ($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>   
        @endif
        <!--end validasi form-->

        <!--alert success -->
        @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{ session('status') }}</strong>
            </div> 
        @endif
        <!--alert success -->
        <!-- Button trigger modal-->
        <center>
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnExportExcel" data-toggle="modal" data-target="#myModalExport" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Export Customer Report to Excel
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            <!-- Button trigger modal-->
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnFilterPeriod" data-toggle="modal" data-target="#myModalPeriod" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Filter Customer Report Period
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            Customer Report Period From <b><i>{{ date('d-M-Y', strtotime($date_start2)) }}</i></b> Until <b><i>{{ date('d-M-Y', strtotime($date_now)) }}</i></b>
        </center>
        
        <table id="tracking" class="display" style="width: 100%">
            <thead>
                <tr>
                    <th>No Report</th>
                    <th>Cust Info</th>
                    <th>Report Info</th>
                    <th>Attachment</th>
                    <th>Notes</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($selfcares as $data)
                    <tr>
                    <td>
                        <b>No Report: </b><i>{{ $data->no_request }}</i> <br>

                        @if ($data->status_request=='0')
                            <span class="badge" style="background-color: green"><b>OPEN</b></span>
                        @elseif($data->status_request=='1')
                            <b>No Ticket: </b><i>{{ $data->no_ticket }}</i><br>
                            <span class="badge" style="background-color: rgb(39, 104, 245)"><b>IN PROGRESS</b></span>
                        @elseif($data->status_request=='2')
                            <span class="badge" style="background-color: rgb(245, 39, 39)"><b>CANCEL</b></span>
                        @else
                            <b>No Ticket: </b><i>{{ $data->no_ticket }}</i><br>
                            <span class="badge" style="background-color: rgb(39, 180, 245)"><b>CLOSED</b></span>
                        @endif
                    </td>
                    <td>
                        <b>{{ $data->cust_code }}</b><br>
                        <i>{{ $data->company_name }}</i>
                    </td>
                    <td>
                        <b>{{ $data->report_type }}</b><br>
                        <i>{{ $data->report_category }}</i><br>
                        {{ date('d-M-Y H:i:s', strtotime($data->created_at)) }}
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" id="btnNotes" data-target="#myModalAttach{{ $data->id }}">
                            See Attachment
                        </button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" id="btnNotes" data-target="#myModalNotes{{ $data->id }}">
                            Detail
                        </button>
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" id="btnFH" data-target="#myModalFH{{ $data->id }}">
                            First Handling
                        </button>
                    </td>
                    <td>
                        @if ($data->status_request=='0')
                            <button type="button" class="btn btn-xs" data-toggle="modal" id="btnSubmit" data-target="#myModalSubmit{{ $data->id }}">
                                Submit
                            </button>
                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" id="btnCancel" data-target="#myModalCancel{{ $data->id }}">
                                Cancel
                            </button>
                        @else
                            
                        @endif
                    </td>
                    </tr>
                    
                    <!-- Modal Attachment-->
                    <div class="modal fade" id="myModalAttach{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Detail Notes</b>
                                    <button type="button" class="close" data-dismiss="modal" id="closeModalUser" aria-label="Close" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <div class="modal-body d-block text-center">
                                <img src="{{ $data->attachment }}" class="img-responsive" alt="Responsive image">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-md" data-dismiss="modal" id="closeModalNotes">
                                    Close
                                </button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->

                    <!-- Modal Notes-->
                    <div class="modal fade" id="myModalNotes{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Detail Notes</b>
                                    <button type="button" class="close" data-dismiss="modal" id="closeModalUser" aria-label="Close" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <div class="modal-body">
                                <span>
                                    @if ($data->report_type=='billing' || $data->report_type=='Billing')
                                        <h4><b>Billing Period: </b>{{ $data->billing_period }}</h4><br>
                                    @endif
                                  
                                    
                                    @php
                                        $rc=json_decode($data->report_content,true);
                                    @endphp
                                    
                                    <b>Notes: </b>{{ $data->notes }} <br><br>
                                   
                                    @if (is_array($rc) || is_object($rc))
                                        @foreach($rc as $key => $value)
                                            <b>{{ ucwords($key) }}: </b>{{ $value }} <br>
                                        @endforeach
                                    @endif
                                </span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-md" data-dismiss="modal" id="closeModalNotes">
                                    Close
                                </button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->

                    <!-- Modal First Handling-->
                    <div class="modal fade" id="myModalFH{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>First Handling</b>
                                    <button type="button" class="close" data-dismiss="modal" id="closeModalUser" aria-label="Close" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <div class="modal-body">
                                <span>
                                    @if ($data->report_type=='billing' || $data->report_type=='Billing')
                                        <h4><b>Billing Period: </b>{{ $data->billing_period }}</h4><br>
                                    @endif
                                  
                                    
                                    @php
                                        $fh=json_decode($data->first_handling,true);
                                    @endphp
                                   
                                    @if (is_array($fh) || is_object($fh))
                                        @foreach($fh as $key => $value)
                                            <b>{{ ucwords($key) }}: </b>  
                                            @if ($value=='true')
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            @else
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            @endif
                                            <br>
                                        @endforeach
                                    @endif
                                </span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-md" data-dismiss="modal" id="closeModalNotes">
                                    Close
                                </button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->

                    <!-- Modal Submit-->
                    <div class="modal fade" id="myModalSubmit{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Create Ticket</b>
                                    <button type="button" class="close" data-dismiss="modal" id="closeModalUser" aria-label="Close" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ url('/selfcare/create-ticket/') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <span>
                                        <input type="hidden" name="req_id" id="req_id" value="{{ $data->id }}">
                                        Are you sure create ticket from this report <b>({{ $data->no_request }})</b> ?    
                                    </span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-md" data-dismiss="modal" id="closeModalNotes">
                                    <i class="fa fa-times"></i> No
                                </button>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Yes</button>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->

                    <!-- Modal Cancel-->
                    <div class="modal fade" id="myModalCancel{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><b>Cancel Report</b>
                                    <button type="button" class="close" data-dismiss="modal" id="closeModalUser" aria-label="Close" style=" margin-top : 1px;">
                                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                                    </button>
                                </h4>
                            </div>
                            <form action="{{ url('/selfcare/cancel/'. $data->id) }}" method="post">
                            @method('patch')
                            @csrf
                            <div class="modal-body">
                                <input type="hidden" name="req_id" id="req_id" value="{{ $data->id }}">
                                <div class="form-group">
                                    <label>Please input Cancel Notes for <b>{{ $data->no_request }}</b></label>
                                    <textarea name="cancel_notes" class="from-group" id="cancel_notes" cols="10" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-md" data-dismiss="modal" id="closeModalNotes">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                            </form>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal Export-->
<div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Export to Excel (Filter by Created Date)</b>
                <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <div class="modal-body">
            <form action="{{url('/selfcare/export')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">
                    <div class="form-group">
                        <label>From</label>
                        <input type="date" id="date_start" name="date_start" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Until</label>
                        <input type="date" id="date_finish" name="date_finish" class="form-control">
                    </div>
                </div>
        </div>
            <div class="modal-footer">
                <button type="button" id="btnResetModalExport" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Export Now</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- Modal Export-->

<!-- Modal Period-->
<div class="modal fade" id="myModalPeriod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Ticket Period (Filter by Ticket Created Date)</b>
                <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <div class="modal-body">
            <form action="{{url('/selfcare/filter')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">
                    <div class="form-group">
                        <label>From</label>
                        <input type="date" id="date_start" name="date_start" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Until</label>
                        <input type="date" id="date_finish" name="date_finish" class="form-control">
                    </div>
                </div>
        </div>
                <div class="modal-footer">
                    <a href="{{url('/selfcare/')}}" class="btn btn-primary">Reset Filter</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Submit Period</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Period-->

<script>
    $(document).ready(function() {
        var table = $('#tracking').DataTable( {
            // order dipakai untuk mengurutkan data table
            "order": [],
            responsive: true,
        } );

        $('#btnResetModalExport').off('click').on('click',resetExport);

        function resetExport() 
        {
            $('#type').val('all');
            $('#date_start').val('');
            $('#date_finish').val('');
            $('#report_type').val('');
        }
        // $('#btnResetModalExport').off('click').on('click',resetExport);
    } );

    $('#btnResetModalExport').off('click').on('click',resetExport);

    function resetExport() 
    {
        $('#type').val('all');
        $('#date_start').val('');
        $('#date_finish').val('');
        $('#report_type').val('');
    }

    console.log(resetExport);
</script>
@endsection