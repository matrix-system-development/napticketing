<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>NapCMS</title>
    
	
  </head>
  <body>
    <span>
        Dear Customer Care,
        <br>
        <br>
        Mohon bantuannya untuk proses ticket dengan :
        <br>
        <br>
        <pre><b>No Request</b>  : {{ $details['no_request'] }}</pre>
        <br>
        <pre><b>Kode Pelanggan</b>  : {{ $details['cust_code'] }}</pre>
        <br>
        <pre><b>Nama</b>    : {{ $details['cust_name'] }}</pre>
        <br>
        <pre><b>Tipe Keluhan</b>    : {{ $details['report_type'] }}</pre>
        <br>
        <pre><b>Kategori Keluhan</b>    : {{ $details['report_category'] }}</pre>
        <br>
        <br>
        Terima kasih.
        <br>
        <br>
    </span>
  </body>
</html>
