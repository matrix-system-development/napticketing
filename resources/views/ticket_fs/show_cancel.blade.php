@extends('layout.master')
@section('content')

    <div class="main-grid">
        <div class="agile-grids">
            <div class="buttons-heading">
                <h2>Cancel Ticket FiberStar</h2>
            </div>
            <br>
            <!-- grids -->
            <div class="grids">
                <!--validasi form-->
                    @if (count($errors)>0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                <li><strong>Saved Data Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>   
                    @endif
                <!--end validasi form-->
                <form action="{{ url('/tickets_fs/cancel') }}" method="post" id="myform" enctype="multipart/form-data">
                    @csrf
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>No Ticket: </label> {{ $fstickets->no_ticket }}
                                        </div>
                                        <div class="form-group"> 
                                            <label>No Request: </label> {{ $fstickets->req_no }}
                                            <input type="hidden" name="req_no" class="form-control" id="req_no" value="{{ $fstickets->req_no }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Reason</label> 
                                            <input type="text" name="reason" class="form-control" id="reason" value="{{ old('reason') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                
                <div class="bs-component mb20" align="center">
                    <button type="submit" id="btnSubmitExternal" class="btn btn-primary">Submit</button>
                </div>

                </form>
            </div>
            <!-- //grids -->
        </div>
    </div> 

    <script type="text/javascript">
            $('#myform').submit(function(){
                $("#btnSubmitExternal", this).html("Please Wait...").attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@endsection