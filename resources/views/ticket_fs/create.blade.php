@extends('layout.master')
@section('content')

    <div class="main-grid">
        <div class="agile-grids">
            <div class="buttons-heading">
                <h2>Create Ticket FiberStar</h2>
            </div>
            <br>
            <!-- grids -->
            <div class="grids">
                <h2>Customer Information</h2>
                <!--validasi form-->
                    @if (count($errors)>0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                <li><strong>Saved Data Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>   
                    @endif
                <!--end validasi form-->
                <form action="{{ url('/tickets_fs') }}" method="post" id="myform" enctype="multipart/form-data">
                    @csrf
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="form-group"> 
                                                <label>CID</label> 
                                                <input type="text" name="cid" class="form-control" id="cid" value="{{ $customer_detail->cid }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>Cust Code*</label> 
                                                <input type="text" name="cust_code" class="form-control" id="cust_code" value="{{ $customer_detail->customer_code }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>Company / Customer Name</label> 
                                                <input type="text" name="company_name" class="form-control" id="company_name" value="{{ $customer_detail->company_name }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>Priority</label> 
                                                <input type="text" name="priority" class="form-control" id="priority" value="{{ $customer_detail->priority }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Company Type</label> 
                                                <input type="text" name="company_type" class="form-control" id="company_type" value="{{ $customer_detail->company_type }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Service</label> 
                                                <input type="text" name="service" class="form-control" id="service" value="{{ $customer_detail->service }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Network Owner</label> 
                                                <input type="text" name="network_owner" class="form-control" id="network_owner" value="{{ $customer_detail->network_owner }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Start Date</label> 
                                                <input type="text" name="start_date" class="form-control" id="start_date" value="{{ $customer_detail->start_date_customer }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>3rd Party Circuit ID*</label> 
                                                <input type="text" name="partner_cid" class="form-control" id="partner_cid" value="{{ $customer_detail->partner_cid }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Username PPoE</label> 
                                                <input type="text" name="username_ppoe" class="form-control" id="username_ppoe" value="{{ $customer_detail->username_ppoe }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Regional</label> 
                                                <input type="text" name="regional" class="form-control" id="regional" value="{{ $customer_detail->regional }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="form-group"> 
                                                <label>PIC Name</label> 
                                                <input type="text" name="pic_name" class="form-control" id="pic_name" value="{{ $customer_detail->pic_name }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>PIC Contact*</label> 
                                                <input type="text" name="pic_contact" class="form-control" id="pic_contact" value="{{ $customer_detail->pic_contact }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>PIC Email*</label> 
                                                <input type="text" name="pic_email" class="form-control" id="pic_email" value="{{ $customer_detail->pic_email }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>Capacity Local / IXX (Mbps)</label> 
                                                <input type="text" name="capacity" class="form-control" id="capacity" value="{{ $customer_detail->capacity }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>Capacity International / IX (Mbps)</label> 
                                                <input type="text" name="capacity_international" class="form-control" id="capacity_international" value="{{ $customer_detail->capacity_international }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>A-End</label> 
                                                <input type="text" name="a_end" class="form-control" id="a_end" value="{{ $customer_detail->a_end }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>B-End</label> 
                                                <input type="text" name="b_end" class="form-control" id="b_end" value="{{ $customer_detail->b_end }}" readonly> 
                                            </div>
                                            <div class="form-group"> 
                                                <label>Finish Date</label> 
                                                <input type="text" name="finish_date" class="form-control" id="finish_date" value="{{ $customer_detail->finish_date }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Network Type</label> 
                                                <input type="text" name="network_type" class="form-control" id="network_type" value="{{ $customer_detail->network_type }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>Password PPoE</label> 
                                                <input type="text" name="password_ppoe" class="form-control" id="password_ppoe" value="{{ $customer_detail->password_ppoe }}" readonly>
                                            </div>
                                            <div class="form-group"> 
                                                <label>City</label> 
                                                <input type="text" name="city" class="form-control" id="city" value="{{ $customer_detail->city }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                
                <h2>Detail Ticket</h2>
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-12">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group"> 
                                            <label>Category*</label> 
                                            <select name="category" id="category" class="form-control">
                                                <option value="">-Choose Category Ticket-</option>
                                                @foreach ($dropdowns['ticket type FS'] as $type_fs)
                                                    <option value="{{ $type_fs->name_value }}">{{ $type_fs->name_value." - ".$type_fs->code_format }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group"> 
                                            <label>Subject*</label> 
                                            <input type="text" name="subject" class="form-control" id="subject" value="{{ old('subject') }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Reference Ticket</label> 
                                            <input type="text" name="ref_ticket" placeholder="Fill in the Processed Request Number" class="form-control" id="ref_ticket" value="{{ old('ref_ticket') }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Notes (max 255 character)</label> 
                                            <textarea name="message" id="message" cols="20" rows="4" class="form-control">{{ old('message') }}</textarea>
                                        </div>
                                        <!-- Button trigger modal -->
                                        <div class="bs-component mb20" align="center">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-upload" aria-hidden="true"></i> Upload Document
                                            </button>
                                        </div>
                                        <div class="form-group"> 
                                            <label><span style="color: red">NOTES: input fields with the symbol * are mandatory</span> </label> 
                                        </div>

                                        <!-- Modal Upload-->
                                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Upload Document</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-body">
                                                        <span style="color: red">* file must : pdf, xls, xlsx, png, jpg, jpeg, doc, docx | max : 2 MB</span>
                                                        <div class="form-group">
                                                            <input type="file" name="doc" id="doc" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Modal Upload-->
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                
                <div class="bs-component mb20" align="center">
                    <button type="submit" id="btnSubmitExternal" class="btn btn-primary">Submit</button>
                </div>

                </form>
            </div>
            <!-- //grids -->
        </div>
    </div> 

    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            //customer search
            $( "#customer_search" ).autocomplete({
                
                source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url:"{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,
                    search: request.term
                    },
                    success: function( data ) {
                    
                    response( data );
                   
                    }
                });
                },
                select: function (event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.label); // display the selected text
                $('#cid').val(ui.item.value_cid); // save selected id to input
                $('#cust_code').val(ui.item.value_custcode);
                $('#company_name').val(ui.item.value_compname); // save selected id to input
                $('#priority').val(ui.item.value_priority);
                $('#company_type').val(ui.item.value_comptype);
                $('#service').val(ui.item.value_service);
                $('#pic_name').val(ui.item.value_picname);
                $('#pic_contact').val(ui.item.value_piccontact);
                $('#pic_email').val(ui.item.value_picemail);
                $('#capacity').val(ui.item.value_capacity);
                $('#capacity_international').val(ui.item.value_capacityInt);
                $('#a_end').val(ui.item.value_a_end);
                $('#b_end').val(ui.item.value_b_end);
                $('#network_type').val(ui.item.value_nettype);
                $('#network_owner').val(ui.item.value_netowner);
                $('#start_date').val(ui.item.value_startdate);
                $('#finish_date').val(ui.item.value_finishdate);
                $('#partner_cid').val(ui.item.value_partnercid);
                $('#username_ppoe').val(ui.item.value_usernameppoe);
                $('#password_ppoe').val(ui.item.value_passwordppoe);
                $('#regional').val(ui.item.value_regional);
                $('#city').val(ui.item.value_city);
                return false;
                },
                
            });

            //select sub category from select category
            $('select[name="category"]').on('change', function() {
                var categoryID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', categoryID);
                if(categoryID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="sub_category"]').empty();
                            $('select[name="sub_category"]').append(
                                    '<option value="">- Select Sub Category -</option>'
                            );
                            $.each(data, function(category, value) {
                                $('select[name="sub_category"]').append(
                                    '<option value="'+ value.id +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="sub_category"]').empty();
                }
            });

            //select sub SLR from select SLR
            $('select[name="link_respond"]').on('change', function() {
                var slrID = $(this).val();
                var url = '{{ route("slr", ":id") }}';
                url = url.replace(':id', slrID);
                if(slrID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="sub_link_respond"]').empty();
                            $('select[name="sub_link_respond"]').append(
                                    '<option value="">- Select Sub Status Link Respond -</option>'
                            );
                            $.each(data, function(slr_data, value) {
                                $('select[name="sub_link_respond"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="sub_link_respond"]').empty();
                }
            });

            //reset search customer
            $('#reset').click(function(){
                $(':input','#myform')
                .not(':button, :submit, :reset, :hidden')
                $('#customer_search').val('');
                $('#cid').val('');
                $('#cust_code').val('');
                $('#company_name').val('');
                $('#priority').val('');
                $('#company_type').val('');
                $('#service').val('');
                $('#pic_name').val('');
                $('#pic_contact').val('');
                $('#pic_email').val('');
                $('#capacity').val('');
                $('#capacity_international').val('');
                $('#a_end').val('');
                $('#b_end').val('');
                $('#network_type').val('');
                $('#network_owner').val('');
                $('#start_date').val('');
                $('#finish_date').val('');
                $('#partner_cid').val('');
                $('#username_ppoe').val('');
                $('#password_ppoe').val('');
                $('#regional').val('');
                $('#city').val('');
            });

            $('#myform').submit(function(){
                $("#btnSubmitExternal", this).html("Please Wait...").attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@endsection