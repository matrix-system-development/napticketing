<style>
    .btnBack {
        margin-top: 30px;
        margin-bottom: 50px;
    }
    #btnHold {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnHold:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnUnhold {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnUnhold > a {
        color: white;
    }

    #btnUnhold:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnUnhold:hover > a {
        color: #151A48;
    }
    #btnClose {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnClose:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnPreclose {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnPreclose:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnReassign {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReassign:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnReturn {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturn:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnReturnCustCare {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnReturnCustCare:hover{
        border: 2px solid #151A48;
        color: #151A48;
        font-weight: 600;
        background-color: white;
    }
    #btnEditRFOHead {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnEditRFOHead:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnEditRFOStaff {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnEditRFOStaff:hover{
        border: 2px solid #151A48;
        color: #151A48;
        background-color: white;
    }
    #btnOther > a{
        margin-top: 5px;
    }

    #panelIncident > span {
        word-break: break-all;
    }
    #lineShadow {
        width: 98%;
        height: 10px;
        border: 0;
        box-shadow: 0 10px 10px -10px #8c8c8c inset;

    }

    #btnComment {
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    #btnComment:hover{
        background-color: white;
        color: #151A48;
        border: 2px solid #151A48;
    }

    textarea {
        resize: none; 
        border: 4px solid gray;
    }

    textarea:focus {
        border: 4px solid #151A48;
    }

    #attc1 {
        height: 100px;
        border: 4px solid gray;
    }

    #attc1:hover {
        border: 4px solid #151A48;
    }

    #panelHeader {
        background-color: #151A48;
        overflow: hidden;
    }

    #panelBasic {
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #borderPanel{
        border: 3px solid #151A48;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblTicketHistory {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblWoHistory {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    #tblComment {
        border: 4px solid gray; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }
    table > thead > tr > th {
        background-color:#151A48; 
        color:white;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-footer > button:focus {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
        outline: none;
    }
    .modal-footer > button:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnTopo_1 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }
    #btnTopo_1:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }
    #btnTopo_2 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }
    #btnTopo_2:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }
    #btnTopo_3 {
        background-color: #151A48;
        color: white;
        border: 2px solid white;
        font-weight: 700;
    }
    #btnTopo_3:hover {
        background-color: white;
        color: 151A48;
        border: 2px solid #151A48;
        font-weight: 700;
    }

    #btnExportLogExternal {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }
    #btnExportLogExternal:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnrfo {
        background-color: #151A48;
        color: white;
        font-weight: 700;
        border: 2px solid white;
    }
    #btnrfo:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
    }

    #btnAttachment > a{
        background-color: #151A48;
        color: white;
        font-weight: 700;
    }

    .form-body > ul > li {
        color: red;
    }

</style>

@extends('layout.master')
@section('content')

<!--alert success -->
@if (session('status'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>{{ session('status') }}</strong>
</div> 
@endif
<!--alert success -->
<!--validasi form-->
@if (count($errors)>0)
<div class="alert alert-danger alert-dismissible" role="alert">
    <ul>
        <li><strong>Submit Failed !</strong></li>
        @foreach ($errors->all() as $error)
            <li><strong>{{ $error }}</strong></li>
        @endforeach
    </ul>
</div>   
@endif
<!--end validasi form-->

<div class="btnBack">
    <div class="container-fluid">
        <div class="bs-component mb20" align="right">
            <a href="{{url('/tickets_fs')}}" class="btn btn-danger btn-md">
                <i class="fa fa-angle-double-left"></i>
                <span class="nav-text" style="font-weight: 700">
                    Back To Ticket List
                </span>
            </a>
        </div>
    </div>
</div>


{{-- <div class="panelIncident"> --}}
    <div class="container-fluid">
        <div class="panel panel-primary" id="borderPanel">
            <div class="panel-heading" id="panelHeader">
                <h3 style="font-weight: 700">Detail Incident</h3>
            </div>
            <div class="panel-body" id="panelIncident">
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>No Ticket Matrix</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $fstickets['no_ticket'] }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Ticket Type</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $fstickets['ticket_type'] }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>No Ticket FS</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $ticketNumber }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Category</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $fstickets['category']." - ".$fstickets['sub_category'] }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>Request Number</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $fstickets['req_no'] }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>WO Number</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $workOrderNumber }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>Created By</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $fstickets['created_by'] }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>WO Status</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $workOrderStatus }}</span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>Remarks</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>{{ $fstickets['created_at'] }}</span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Current Latest Status</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span><b>{{ $fstickets['status'] }}</b></span>
                    </div>
                </div>
                <div class="row mb20" align="left">
                    <div class="col-md-2">
                        <span><b>Attachment</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span>
                            @if ($fstickets['doc_name'] != '')
                            <a href="{{ url('/tickets_fs/'.$fstickets['doc_name'].'/download') }}" class="btn btn-primary btn-xs">
                                <span>
                                    <i class="fa fa-download" aria-hidden="true"></i> Download
                                </span>
                            </a>
                        @endif
                        </span>
                    </div>
                    <div class="col-md-2">
                        <span><b>Notes</b></span>
                    </div>
                    <div class="col-md-1">
                        <span>:</span>
                    </div>
                    <div class="col-md-3">
                        <span><i>{{ $fstickets['notes'] }}</i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-primary" id="borderPanel">
            <div class="panel-heading" id="panelHeader">
                <h3 style="font-weight: 700">Ticket History</h3>
            </div>
            <div class="panel-body" id="panelIncident">
                <div class="w3l-table-info">
                    <table id="tblTicketHistory" class="display">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>PIC Department</th>
                                <th>PIC</th>
                                <th>Assignee</th>
                                <th>Note</th>
                                <th>Processed At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (is_array($ticketHistory) || is_object($ticketHistory))
                                @foreach ($ticketHistory as $data)
                                <tr>
                                    <td>{{ $data['status'] }}</td>
                                    <td>{{ $data['picDept'] }}</td>
                                    <td>{{ $data['picPerson'] }}</td>
                                    <td>{{ $data['assignee'] }}</td>
                                    <td>{{ $data['note'] }}</td>
                                    <td>{{ date('Y-m-d H:i:s',strtotime($data['processedAt']))  }}</td>
                                </tr>
                                @endforeach   
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-primary" id="borderPanel">
            <div class="panel-heading" id="panelHeader">
                <h3 style="font-weight: 700">WO History</h3>
            </div>
            <div class="panel-body" id="panelIncident">
                <div class="w3l-table-info">
                    <table id="tblWoHistory" class="display">
                        <thead>
                            <tr>
                                <th>WO Info</th>
                                <th>Cust. Info</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Technician Info</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (is_array($woHistory) || is_object($woHistory))
                                @foreach ($woHistory as $datawo)
                                <tr>
                                    <td>
                                        <b>WO Code: </b>{{ $datawo['wo_code'] }}<br>
                                        <b>WO Name: </b>{{ $datawo['wo_name'] }}
                                    </td>
                                    <td>
                                        <b>Cust ID: </b>{{ $datawo['customer_id'] }}<br>
                                        <b>Loc ID: </b>{{ $datawo['location_id'] }}<br>
                                        <b>Cust Name: </b>{{ $datawo['customer_name'] }}
                                    </td>
                                    <td>{{ $datawo['status'] }}</td>
                                    <td>{{ date('Y-m-d H:i:s', strtotime($datawo['date'])) }}</td>
                                    <td>
                                        <b>Tech Code: </b>{{ $datawo['technician_code'] }}<br>
                                        <b>Tech Name: </b>{{ $datawo['technician_name'] }}
                                    </td>
                                </tr>
                                @endforeach  
                            @endif 
                        </tbody>
                    </table>
                </div>
                    
            </div>
        </div>
    </div>
{{-- </div> --}}
    
    <br>

<script>
    function showhide(elem) 
    {  
        console.log(elem.getAttribute("href"));
        var div = document.getElementById(elem.getAttribute("href").replace("#", ""));
        
        if (div.style.display !== "none") {  
            div.style.display = "none";  
        }  
        else {  
            div.style.display = "block";  
        }  
    }  

    $(document).ready(function() {
        var table = $('#tblComment').DataTable( {
            "order": [],
            responsive: true
        } );

        var tableLogs = $('#tblTicketHistory').DataTable( {
            "order": [],
            responsive: true
        } );

        var tableLogs = $('#tblWoHistory').DataTable( {
            "order": [],
            responsive: true
        } );

        $('#btnResetModalHold').off('click').on('click',resetModalHold);
        $('#closeModalHold').off('click').on('click',resetModalHold);
        
        function resetModalHold() 
        {
            $('#holdtime').val('');
            $('#hold_time').val('');
            $('#category_hold').val('');
            $('.message-hold').val('');
        }

        $('#btnResetModalReturn').off('click').on('click',resetModalReturn);
        $('#closeModalReturn').off('click').on('click',resetModalReturn);
        
        function resetModalReturn() 
        {
            $('.message-return').val('');
        }

        $('#btnResetModalReassign').off('click').on('click',resetModalReassign);
        $('#closeModalReassign').off('click').on('click',resetModalReassign);
        
        function resetModalReassign() 
        {
            $('#department').val('');
            $('#message').val('');
        }

        $('#btnResetModalClose').off('click').on('click',resetModalClose);
        $('#closeModalClose').off('click').on('click',resetModalClose);

        function resetModalClose() 
        {
            $('#finish_date').val('');
            $('#finish_time').val('');
            $('#gen_rfo').val('1');
            $('#outage_reason1').val('');
            $('#outage_sub1').val('');
            $('#outage_reason2').val('');
            $('#outage_sub2').val('');
            $('#action_close1').val('');
            $('#action_close2').val('');
            $('#outage_loc').val('');
            $('.message-close').val('');
        }

        $('#btnResetModalPreclose').off('click').on('click',resetModalPreclose);
        $('#closeModalPreclose').off('click').on('click',resetModalPreclose);

        function resetModalPreclose() 
        {
            $('.message-preclose').val('');
        }

        $('#formComment').submit(function(){
            $("#btnComment", this)
            .html("Please Wait...")
            .attr('disabled', 'disabled');
            return true;
        });
    } );

    //select staff from select dept
    $('select[name="department"]').on('change', function() {
        var deptID = $(this).val();
        var url = '{{ route("dept", ":id") }}';
        url = url.replace(':id', deptID);
        if(deptID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success:function(data) {

                    $('select[name="pic_staff"]').empty();
                    $('select[name="pic_staff"]').append(
                            '<option value="all">- All Staff in Department -</option>'
                    );
                    $.each(data, function(dept_data, value) {
                        $('select[name="pic_staff"]').append(
                            '<option value="'+ value.int_emp_id +'">'+ value.int_emp_name +'</option>'
                        );
                    });

                }
            });
        }else{
            $('select[name="pic_staff"]').empty();
        }
    });

    //select sub outage from outage 1
    $('select[name="outage_reason1"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if(outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success:function(data) {

                    $('select[name="outage_sub1"]').empty();
                    $('select[name="outage_sub1"]').append(
                            '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub1"]').append(
                            '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                        );
                    });

                }
            });
        }else{
            $('select[name="outage_sub1"]').empty();
        }
    });

    //select sub outage from outage 2
    $('select[name="outage_reason2"]').on('change', function() {
        var outageID = $(this).val();
        var url = '{{ route("outage", ":id") }}';
        url = url.replace(':id', outageID);
        if(outageID) {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success:function(data) {

                    $('select[name="outage_sub2"]').empty();
                    $('select[name="outage_sub2"]').append(
                            '<option value="">- Sub Outage -</option>'
                    );
                    $.each(data, function(outage, value) {
                        $('select[name="outage_sub2"]').append(
                            '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                        );
                    });

                }
            });
            $('#btnComment').click(function() { $(this).attr('disabled','disabled'); });
        }else{
            $('select[name="outage_sub2"]').empty();
        }
    });

    //select sub SLR from select SLR
    $('select[name="link_respond"]').on('change', function() {
                var slrID = $(this).val();
                var url = '{{ route("slr", ":id") }}';
                url = url.replace(':id', slrID);
                if(slrID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="sub_link_respond"]').empty();
                            $('select[name="sub_link_respond"]').append(
                                    '<option value="">- Select Sub Status Link Respond -</option>'
                            );
                            $.each(data, function(slr_data, value) {
                                $('select[name="sub_link_respond"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="sub_link_respond"]').empty();
                }
            });
</script>
@endsection