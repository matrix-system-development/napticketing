<style>
    #tracking {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    thead > tr > th {
        background-color: #151A48;
        color: white;
    }

    table > tbody > tr:hover > td {
        background-color: lightblue;
        color: black;
    }

    #btnCancel {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnCancel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnCancel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    
    #btnExportExcel {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    #btnExportExcel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnExportExcel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnFilterPeriod:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnDetail {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnDetail:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    #btnFilterPeriod:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }
    .modal-header {
        border-bottom:1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }
    .modal-footer > button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }
    .modal-footer > button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
    }
    .form-group > .form-control:focus {
        border: 2px solid #151A48;
    }
    
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>Tickets Tracking FiberStar</h2>
        </div>
        <!--validasi form-->
        @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                <li><strong>Export Data Failed !</strong></li>
                @foreach ($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>   
        @endif
        <!--end validasi form-->

        <!--alert success -->
        @if (session('status'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{ session('status') }}</strong>
            </div> 
        @endif
        <!--alert success -->
        <!-- Button trigger modal-->
        <center>
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnExportExcel" data-toggle="modal" data-target="#myModalExport" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Export Tickets to Excel
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            <!-- Button trigger modal-->
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnFilterPeriod" data-toggle="modal" data-target="#myModalPeriod" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Filter Ticket Period
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            Ticket Period From <b><i>{{ date('d F Y', strtotime($date_start2)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_now)) }}</i></b>
        </center>
        
        <table id="tracking" class="display" style="width: 100%">
            <thead>
                <tr>
                    <th>No Ticket</th>
                    <th>Cust Info</th>
                    <th>Category</th>
                    <th>Time Info</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($fstickets as $item)
                    <tr>
                        <td>
                            <b>Matrix: </b>{{ $item->no_ticket }} <br>
                            <b>FS: </b>{{ $item->req_no }} <br>
                            @if ($item->status == 'Canceled')
                                <span class="badge" style="background-color: red"><i>{{ $item->status }}</i></span>
                            @elseif ($item->status == 'Closed')
                                <span class="badge" style="background-color: green"><i>{{ $item->status }}</i></span>
                            @else
                                <span class="badge" style="background-color: blue"><i>{{ $item->status }}</i></span>
                            @endif
                        </td>
                        <td>
                            <b>{{ $item->company_name }}</b> <br>
                            <i>{{ $item->cid }}</i>
                        </td>
                        <td>
                            <b>{{ $item->category }}</b> <br>
                            {{ $item->sub_category }}
                        </td>
                        <td>
                            <b>Created: </b><i>{{ date('d-M-Y H:i:s', strtotime($item->created_at)) }}</i><br>
                            @if ($item->status == 'Canceled'||$item->status == 'Opened')
                                
                            @else
                                <b>Updated: </b><i>{{ date('d-M-Y H:i:s', strtotime($item->fs_last_update)) }}</i><br>
                            @endif
                            <b>Duedate: </b><i>{{ date('d-M-Y H:i:s', strtotime($item->duedate)) }}</i><br>
                            @php
                                $created_date=strtotime($item->created_at);
                                $aging=time()-$created_date;
                                $aging_info=round($aging / 86400).' Days';
                            @endphp
                            @if ($item->status == 'Canceled'||$item->status == 'Closed')
                                
                            @else
                                <b>Aging: </b><i>{{ $aging_info }}</i>
                            @endif
                        </td>
                        <td>
                            @if ($item->status == 'Canceled' )
                                
                            @elseif ($item->status == 'Closed' || $item->status != 'Opened')
                                <a href="{{url('/tickets_fs/detail/'.$item->req_no)}}" class="btn btn-primary btn-sm" id='btnDetail'>Detail</a>
                            @else
                                <a href="{{url('/tickets_fs/show-cancel/'.$item->req_no)}}" class="btn btn-info btn-sm" id='btnCancel'>Cancel</a>
                                <a href="{{url('/tickets_fs/detail/'.$item->req_no)}}" class="btn btn-primary btn-sm" id='btnDetail'>Detail</a>
                            @endif
                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal Export-->
<div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Export to Excel (Filter by Created Date)</b>
                <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <div class="modal-body">
            <form action="{{url('/tickets_fs/export')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">
                    <div class="form-group">
                        <label>From</label>
                        <input type="date" id="date_start" name="date_start" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Until</label>
                        <input type="date" id="date_finish" name="date_finish" class="form-control">
                    </div>
                </div>
        </div>
            <div class="modal-footer">
                <button type="button" id="btnResetModalExport" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Export Now</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- Modal Export-->

<!-- Modal Period-->
<div class="modal fade" id="myModalPeriod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><b>Ticket Period (Filter by Ticket Created Date)</b>
                <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                    <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                </button>
            </h4>
        </div>
        <div class="modal-body">
            <form action="{{url('/tickets_fs/filter')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">
                    <div class="form-group">
                        <label>From</label>
                        <input type="date" id="date_start" name="date_start" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Until</label>
                        <input type="date" id="date_finish" name="date_finish" class="form-control">
                    </div>
                </div>
        </div>
                <div class="modal-footer">
                    <a href="{{url('/tickets_fs/')}}" class="btn btn-primary">Last 2 Month</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Submit Period</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Period-->

<script>
    $(document).ready(function() {
        var table = $('#tracking').DataTable( {
            // order dipakai untuk mengurutkan data table
            "order": [],
            responsive: true,
        } );

        $('#btnResetModalExport').off('click').on('click',resetExport);

        function resetExport() 
        {
            $('#type').val('all');
            $('#date_start').val('');
            $('#date_finish').val('');
            $('#report_type').val('');
        }
        // $('#btnResetModalExport').off('click').on('click',resetExport);
    } );

    $('#btnResetModalExport').off('click').on('click',resetExport);

    function resetExport() 
    {
        $('#type').val('all');
        $('#date_start').val('');
        $('#date_finish').val('');
        $('#report_type').val('');
    }

    console.log(resetExport);
</script>
@endsection