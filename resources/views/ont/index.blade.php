@extends('layout.master')
@section('content')

    <div class="main-grid">
        <div class="agile-grids">
            <div class="buttons-heading">
                <h2>Check ONT FiberStar</h2>
            </div>
            <br>
            <!-- grids -->
            <div class="grids">
                <!--validasi form-->
                    @if (count($errors)>0)
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <ul>
                                <li><strong>Saved Data Failed !</strong></li>
                                @foreach ($errors->all() as $error)
                                    <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>   
                    @endif
                <!--end validasi form-->
                <!--alert success -->
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{{ session('status') }}</strong>
                    </div> 
                @endif
                <!--alert success -->
                <!--alert success -->
                @if (session('info'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{{ session('info') }}</strong>
                    </div> 
                @endif
                <!--alert success -->
                <form action="{{ url('/ont-fs/check') }}" method="post" id="myform" enctype="multipart/form-data">
                @csrf
                <div class="panel panel-widget top-grids">
                    <div class="chute chute-center">
                        <div class="row mb40">
                            <div class="col-md-4">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label>Check by CID</label> 
                                            <input type="text" class="form-control" name='cid' id='cid' value="{{ session()->get('circuit_id') }}">
                                        </div>
                                        <div class="form-group"> 
                                            <label>Command</label> 
                                            <select name="command" id="command" class="form-control">
                                                <option value="">- Please Select Command -</option>
                                                @foreach ($dropdowns['command name'] as $command_data)
                                                    <option value="{{ $command_data->name_value }}">{{ $command_data->name_value }}</option>
                                                @endforeach
                                            </select> 
                                        </div>
                                        <div class="bs-component" align="center">
                                            <button type="submit" class="btn btn-primary" id="btnSubmit">
                                                Check
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="demo-grid">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <center><label>RESULT</label></center>
                                            @if (session()->get('status'))
                                            <p>
                                                @if (session()->get('command_name') <> 'ONTReboot')
                                                    <b>{{ "Circuit ID: "}}</b>{{ session()->get('circuit_id') }}<br>
                                                    <b>{{ "Command: "}}</b>{{ session()->get('command') }}<br>
                                                    <b>{{ "Command Result"}}</b><br><pre>{{ session()->get('command_return') }}</pre><br>
                                                    <b>{{ "Using: "}}</b>{{ session()->get('using') }}<br>
                                                @else
                                                    <center><i>{{ session()->get('status')." ONT Reboot" }}</i></center>
                                                @endif
                                            </p>
                                            <a href="{{url('/tickets_fs/create/'.base64_encode(session()->get('circuit_id')))}}" class="btn btn btn-primary">Create Ticket</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <!-- //grids -->
        </div>
    </div>

    <script type="text/javascript">
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function(){
            //customer search
            $( "#cid" ).autocomplete({
                
                source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url:"{{route('autocomplete')}}",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,
                    search: request.term
                    },
                    success: function( data ) {
                    
                    response( data );
                   
                    }
                });
                },
                select: function (event, ui) {
                // Set selection
                $('#customer_search').val(ui.item.value_partnercid); // display the selected text
                $('#cid').val(ui.item.value_partnercid); // save selected id to input
                $('#cust_code').val(ui.item.value_custcode);
                $('#company_name').val(ui.item.value_compname); // save selected id to input
                $('#priority').val(ui.item.value_priority);
                $('#company_type').val(ui.item.value_comptype);
                $('#service').val(ui.item.value_service);
                $('#pic_name').val(ui.item.value_picname);
                $('#pic_contact').val(ui.item.value_piccontact);
                $('#pic_email').val(ui.item.value_picemail);
                $('#capacity').val(ui.item.value_capacity);
                $('#capacity_international').val(ui.item.value_capacityInt);
                $('#a_end').val(ui.item.value_a_end);
                $('#b_end').val(ui.item.value_b_end);
                $('#network_type').val(ui.item.value_nettype);
                $('#network_owner').val(ui.item.value_netowner);
                $('#start_date').val(ui.item.value_startdate);
                $('#finish_date').val(ui.item.value_finishdate);
                $('#partner_cid').val(ui.item.value_partnercid);
                $('#username_ppoe').val(ui.item.value_usernameppoe);
                $('#password_ppoe').val(ui.item.value_passwordppoe);
                $('#regional').val(ui.item.value_regional);
                $('#city').val(ui.item.value_city);
                return false;
                },
                
            });

            //select sub category from select category
            $('select[name="category"]').on('change', function() {
                var categoryID = $(this).val();
                var url = '{{ route("mappingIncident", ":id") }}';
                url = url.replace(':id', categoryID);
                if(categoryID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="sub_category"]').empty();
                            $('select[name="sub_category"]').append(
                                    '<option value="">- Select Sub Category -</option>'
                            );
                            $.each(data, function(category, value) {
                                $('select[name="sub_category"]').append(
                                    '<option value="'+ value.id +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="sub_category"]').empty();
                }
            });

            //select sub SLR from select SLR
            $('select[name="link_respond"]').on('change', function() {
                var slrID = $(this).val();
                var url = '{{ route("slr", ":id") }}';
                url = url.replace(':id', slrID);
                if(slrID) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {

                            $('select[name="sub_link_respond"]').empty();
                            $('select[name="sub_link_respond"]').append(
                                    '<option value="">- Select Sub Status Link Respond -</option>'
                            );
                            $.each(data, function(slr_data, value) {
                                $('select[name="sub_link_respond"]').append(
                                    '<option value="'+ value.name_incident +'">'+ value.name_incident +'</option>'
                                );
                            });

                        }
                    });
                }else{
                    $('select[name="sub_link_respond"]').empty();
                }
            });

            //reset search customer
            $('#reset').click(function(){
                $(':input','#myform')
                .not(':button, :submit, :reset, :hidden')
                $('#customer_search').val('');
                $('#cid').val('');
                $('#company_name').val('');
                $('#priority').val('');
                $('#company_type').val('');
                $('#service').val('');
                $('#pic_name').val('');
                $('#pic_contact').val('');
                $('#capacity').val('');
                $('#capacity_international').val('');
                $('#a_end').val('');
                $('#b_end').val('');
                $('#network_type').val('');
                $('#network_owner').val('');
                $('#start_date').val('');
                $('#finish_date').val('');
                $('#partner_cid').val('');
                $('#username_ppoe').val('');
                $('#password_ppoe').val('');
                $('#regional').val('');
                $('#city').val('');
            });

            $('#myform').submit(function(){
                $("#btnSubmit", this).html("Please Wait...").attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@endsection