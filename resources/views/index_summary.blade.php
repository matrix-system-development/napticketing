<style>
    #tracking {
        border: 4px solid gray;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    thead>tr>th {
        background-color: #151A48;
        color: white;
    }

    table>tbody>tr:hover>td {
        background-color: lightblue;
        color: black;
    }

    #btnExportExcel {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnExportExcel:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnExportExcel:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    #btnFilterPeriod:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    #btnFilterPeriod:focus:hover {
        background-color: white;
        color: #151A48;
        font-weight: 700;
        border: 2px solid #151A48;
        outline: none;
    }

    .modal-header {
        border-bottom: 1px solid #eee;
        background-color: #151A48;
        color: white;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        margin-left: -1px;
        margin-top: -5px;
        width: auto;
    }

    .modal-footer>button {
        background-color: #151A48;
        color: white;
        font-weight: 600;
        border: 2px solid white;
    }

    .modal-footer>button:hover {
        background-color: white;
        color: #151A48;
        font-weight: 600;
        border: 2px solid #151A48;
    }

    .form-group>.form-control:focus {
        border: 2px solid #151A48;
    }
</style>
@extends('layout.master')

@section('content')
<div class="main-grid">
    <div class="agile-grids">
        <div class="buttons-heading">
            <h2>Tickets Tracking Matrix</h2>
        </div>
        <!--validasi form-->
        @if (count($errors)>0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                <li><strong>Export Data Failed !</strong></li>
                @foreach ($errors->all() as $error)
                <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>
        @endif
        <!--end validasi form-->

        <!--alert success -->
        @if (session('status'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session('status') }}</strong>
        </div>
        @endif
        <!--alert success -->
        <!-- Button trigger modal-->
        <center>
            <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnExportExcel" data-toggle="modal" data-target="#myModalExport" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Export Tickets to Excel
                    </span>
                </button>
            </div>
            <!-- Button trigger modal-->
            <!-- Button trigger modal-->
            {{-- <div class="bs-component mb20">
                <button type="button" class="btn btn-primary" id="btnFilterPeriod" data-toggle="modal" data-target="#myModalPeriod" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-table" aria-hidden="true"></i>
                    <span class="nav-text">
                        Filter Ticket Period
                    </span>
                </button>
            </div> --}}
            <!-- Button trigger modal-->
            Ticket Period From <b><i>{{ date('d F Y', strtotime($date_start2)) }}</i></b> Until <b><i>{{ date('d F Y', strtotime($date_now)) }}</i></b>
        </center>

        <div class="btnBack">
            <div class="container-fluid">
                <div class="bs-component mb20 mt30" align="left">
                    <a href="{{ url('/summary/matrix') }}" class="btn btn-danger btn-md">
                        <i class="fa fa-angle-double-left"></i>
                        <span class="nav-text" style="font-weight: 700">
                            Back To Ticket Summary
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <table id="tracking" class="display" style="width: 100%">
            <thead>
                <tr>
                    <th>Info</th>
                    <th>Status Ticket</th>
                    <th>Status Complain</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Created At</th>
                    <th>Aging</th>
                    <th>Last Assign To</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tickets as $ticket)
                <tr>
                    <td>
                        @php
                        $created_date=strtotime($ticket->created_date);
                        $aging=time()-$created_date;

                        if(date('d-M-Y H:i:s')>date('d-M-Y H:i:s', strtotime($ticket->due_date))){
                        $labeldue="red";
                        }
                        else{
                        $labeldue="green";
                        }
                        @endphp

                        @if ($ticket->ticket_type=='External')
                        <a href="{{url('/tickets/'.$ticket->id)}}">{{ $ticket->company_name }}</a><br>
                        @elseif($ticket->ticket_type=='Internal')
                        <a href="{{url('/tickets_'.$ticket->ticket_type."/".$ticket->id)}}">{{ $ticket->dept_created }}</a><br>
                        @elseif($ticket->ticket_type=='Maintenance')
                        <a href="{{url('/tickets_'.$ticket->ticket_type."/".$ticket->id)}}">{{ $ticket->department_name }}</a><br>
                        @endif

                        @if ($ticket->cid <> '' && $ticket->ticket_type <> 'External')
                                {{ $ticket->company_name }}<br>
                                @endif

                                @if($ticket->ticket_type=='IntMaintenance')
                                <b>No Ticket: </b><a href="{{url('/tickets_'.$ticket->ticket_type."/".$ticket->id)}}"><i>{{ $ticket->no_ticket }}</i></a>
                                @else
                                <b>No Ticket: </b><i>{{ $ticket->no_ticket }}</i><br>
                                @if ($ticket->no_request <> '')
                                    <b>No Selfcare: </b><i>{{ $ticket->no_request }}</i><br>
                                    @endif
                                    @endif




                                    @if (($ticket->ticket_type=='External') && $ticket->status<>'4' )
                                        <b>Duedate: </b><i>
                                            <font color='{{ $labeldue }}'>{{ date('d-M-Y H:i:s', strtotime($ticket->due_date)); }}</font>
                                        </i><br>
                                        @elseif(($ticket->ticket_type=='External'||$ticket->ticket_type=='Internal'||$ticket->ticket_type=='Maintenance') && $ticket->status=='4' )
                                        <b>Closed Date: </b><i>{{ date('d-M-Y H:i:s', strtotime($ticket->closed_date)); }}</i><br>
                                        @endif

                                        @if (($ticket->status=='4')&&($ticket->rfo<>'NON RFO') && ($ticket->ticket_type=='External'))
                                            <a href="{{url('/tickets/rfo/'.$ticket->id)}}" class="btn btn-primary btn-xs">Download RFO</a>

                                            @endif
                    </td>
                    <td>
                        @if (($ticket->status=='1') && date('d-M-Y', strtotime($ticket->created_date))==date('d-M-Y'))
                        <span class="badge" style="background-color: green"><b>New !</b></span>
                        @elseif (($ticket->status=='1') && date('d-M-Y', strtotime($ticket->created_date))<>date('d-M-Y'))
                            <span class="badge" style="background-color: green"><b>In Progress</b></span>
                            @elseif ($ticket->status=='2')
                            <span class="badge" style="background-color: blue"><b>Unassign</b></span>
                            @elseif ($ticket->status=='3')
                            <span class="badge" style="background-color: orange"><b>Hold</b></span><br>
                            <i>{{ date('d-M-Y H:i:s', strtotime($ticket->hold_date)) }}</i>
                            @else
                            <span class="badge" style="background-color: red"><b>Closed</b></span>
                            @endif
                    </td>
                    <td>
                        @if ($ticket->status_complain=='Low')
                        <span class="badge" style="background-color: green"><b>{{ $ticket->status_complain }}</b></span>
                        @elseif ($ticket->status_complain=='Medium')
                        <span class="badge" style="background-color: orange"><b>{{ $ticket->status_complain }}</b></span>
                        @else
                        <span class="badge" style="background-color: red"><b>{{ $ticket->status_complain }}</b></span>
                        @endif
                    </td>
                    <td>
                        <i>
                            @if ($ticket->final_category=='')
                            {{ $ticket->category }}
                            @else
                            {{ $ticket->final_category }}
                            @endif
                        </i><br>
                        @if (($ticket->category == 'Maintenance')&&($ticket->status_mtc == '1'))
                        <span class="badge" style="background-color: green"><b>Open</b></span>
                        @elseif(($ticket->category == 'Maintenance')&&($ticket->status_mtc == '2'))
                        <span class="badge" style="background-color: green"><b>Reschedule</b></span>
                        @elseif(($ticket->category == 'Maintenance')&&($ticket->status_mtc == '3'))
                        <span class="badge" style="background-color: orange"><b>Cancel</b></span>
                        @elseif(($ticket->category == 'Maintenance')&&($ticket->status_mtc == '4'))
                        <span class="badge" style="background-color: red"><b>Closed</b></span>
                        @else
                        <span></span>
                        @endif
                        @if (($ticket->category == 'IntMaintenance')&&($ticket->status_mtc == '1'))
                        <span class="badge" style="background-color: green"><b>Open</b></span>
                        @elseif(($ticket->category == 'IntMaintenance')&&($ticket->status_mtc == '2'))
                        <span class="badge" style="background-color: green"><b>Reschedule</b></span>
                        @elseif(($ticket->category == 'IntMaintenance')&&($ticket->status_mtc == '3'))
                        <span class="badge" style="background-color: orange"><b>Cancel</b></span>
                        @elseif(($ticket->category == 'IntMaintenance')&&($ticket->status_mtc == '4'))
                        <span class="badge" style="background-color: red"><b>Closed</b></span>
                        @else
                        <span></span>
                        @endif
                    </td>
                    <td>
                        @if ($ticket->final_category=='')
                        <i>{{ $ticket->name_incident }}</i>
                        @else
                        <i>{{ $ticket->final_name_incident }}</i>
                        @endif
                    </td>
                    <td>
                        <i>{{ date('d-M-Y H:i:s', strtotime($ticket->created_date)) }}</i>
                    </td>
                    <td>
                        @php
                        if($ticket->status=='3') {
                        $aging_info='Hold';
                        $labelaging="green";
                        }
                        elseif ($ticket->status=='4') {
                        if($ticket->ticket_type=='External'){
                        if($ticket->closed_date > $ticket->due_date){
                        $aging_info='Over SLA';
                        $labelaging="red";
                        }
                        else{
                        $aging_info='Closed';
                        $labelaging="green";
                        }
                        }
                        else{
                        $aging_info='Closed';
                        $labelaging="green";
                        }
                        }
                        else{
                        $aging_info=round($aging / 86400).' Days';
                        $labelaging="green";
                        }
                        @endphp

                        <font color='{{ $labelaging }}'><i>{{ $aging_info }}</i></font>
                    </td>
                    <td>
                        @if (empty($ticket->dept_assign))
                        <span class="badge" style="background-color: red"><i>UNASSIGN</i></span>
                        @else
                        <i>{{ $ticket->dept_assign }}</i><br>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal Export-->
<div class="modal fade" id="myModalExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Export to Excel (Filter by Created Date)</b>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
            <form action="{{url('/tickets/export/'.$summary.'/'.$category.'/'.$from.'/'.$until)}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-body">
                    <div class="form-group">
                        <label>Report Type</label>
                        <select name="report_type" id="report_type" class="form-control">
                            <option value="" selected>-- Choose Report Type --</option>
                            <option value="summary_type">Tickets</option>
                            <option value="logAssign_type">Logs Ticket</option>
                            <option value="logComment_type">Logs Ticket Journey</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnResetModalExport" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Export Now</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Export-->

<!-- Modal Period-->
<div class="modal fade" id="myModalPeriod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Ticket Period (Filter by Ticket Created Date)</b>
                    <button type="button" class="close" data-dismiss="modal" onclick="resetExport()" aria-label="Close" style=" margin-top : 1px;">
                        <span aria-hidden="true" style="color:white"><i class="fa fa-times"></i></span>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/tickets/filter')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>From</label>
                            <input type="date" id="date_start" name="date_start" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Until</label>
                            <input type="date" id="date_finish" name="date_finish" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <a href="{{url('/tickets')}}" class="btn btn-primary">Last 2 Month</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file-text"></i> Submit Period</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Period-->

<script>
    $(document).ready(function() {
        var table = $('#tracking').DataTable({
            // order dipakai untuk mengurutkan data table
            "order": [],
            responsive: true,
        });

        $('#btnResetModalExport').off('click').on('click', resetExport);

        function resetExport() {
            $('#type').val('all');
            $('#date_start').val('');
            $('#date_finish').val('');
            $('#report_type').val('');
        }
        // $('#btnResetModalExport').off('click').on('click',resetExport);
    });

    $('#btnResetModalExport').off('click').on('click', resetExport);

    function resetExport() {
        $('#type').val('all');
        $('#date_start').val('');
        $('#date_finish').val('');
        $('#report_type').val('');
    }

    console.log(resetExport);
</script>
@endsection