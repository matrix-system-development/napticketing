<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
   return $request->user();
});

Route::post("login", [AuthController::class, 'doLogin']);

Route::post('/ticket-request', 'App\Http\Controllers\API\ApiController@storeRequest');
Route::get('/ticket-status/{no_req}', 'App\Http\Controllers\API\ApiController@checkRequest')->where('no_req', '(.*)'); //ditambah where supaya param bisa baca slash
Route::get('/ticket-all/{cust_code}', 'App\Http\Controllers\API\ApiController@checkByCustCode');
Route::get('/ticket-cc-all/{cust_code}', 'App\Http\Controllers\API\ApiController@AllTicketByCustCode');
Route::get('/ticket-cc-history/{id}', 'App\Http\Controllers\API\ApiController@HistoryTicketByid');
Route::get('/ticket-cc-histAssign/{id}', 'App\Http\Controllers\API\ApiController@HistoryAssignByid');
Route::get('/ticket-cc-search/{no_ticket}', 'App\Http\Controllers\API\ApiController@SearchTicket')->where('no_ticket', '.*');
Route::get('/ticket-req-terminate', 'App\Http\Controllers\API\ApiController@RequestTerminate')->where('no_ticket', '.*');

//API Ticket Internal
Route::post('/ticket-internal', 'App\Http\Controllers\API\ApiTicketIntController@storeTicketInt');
Route::post('/ticket-internal/closed', 'App\Http\Controllers\API\ApiTicketIntController@closedTicketInt');