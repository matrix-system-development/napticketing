<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Notification;
use Illuminate\Routing\Route as RoutingRoute;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These 
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//start Login
Route::get('/', 'App\Http\Controllers\AuthController@login')->name('login');
Route::get('/redirect-ticket/{q}', 'App\Http\Controllers\AuthController@postloginSSO'); //kalau pakai SSO
Route::post('/postlogin', 'App\Http\Controllers\AuthController@postlogin');
Route::get('/logout', 'App\Http\Controllers\AuthController@logout');
//end Login

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'App\Http\Controllers\HomeController@index')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Guest,Partnership']);
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@handleChart')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Guest']);

    //SUMMARY
    Route::get('/summary/matrix', 'App\Http\Controllers\SummaryController@sumMatrix')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/summary/matrix/filter', 'App\Http\Controllers\SummaryController@sumMatrix')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/summary/matrix/{summary}/{category}/{from}/{until}', 'App\Http\Controllers\TicketController@indexSum')->middleware(['checkRole:Super Admin,Customer Care']);

    //Ticket External
    Route::resource('/tickets', 'App\Http\Controllers\TicketController')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Partnership']);
    Route::post('/tickets/filter', 'App\Http\Controllers\TicketController@index')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets/addComment', 'App\Http\Controllers\TicketController@addComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets/{id}', 'App\Http\Controllers\TicketController@show')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets/{id}/download', 'App\Http\Controllers\TicketController@download')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets/{id}/downloadFileComment', 'App\Http\Controllers\TicketController@downloadFileComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets/reassign', 'App\Http\Controllers\TicketController@reassign')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets/return/{id}', 'App\Http\Controllers\TicketController@returnTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets/hold/{id}', 'App\Http\Controllers\TicketController@holdTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::get('tickets/unhold/{id}/{idHold}', 'App\Http\Controllers\TicketController@unholdTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets/close/{id}', 'App\Http\Controllers\TicketController@close')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets/close-request/{id}', 'App\Http\Controllers\TicketController@closeRequest')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets/edit-rfo-head/{id}', 'App\Http\Controllers\TicketController@EditRFOHead')->middleware(['checkRole:Super Admin,User,Sales Admin,Cus    tomer Care']);
    Route::patch('tickets/edit-rfo-staff/{id}', 'App\Http\Controllers\TicketController@EditRFOStaff')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets/preclosed/{id}', 'App\Http\Controllers\TicketController@preclosed')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::get('/masters/topology/view/{topology_file}/{cust}', 'App\Http\Controllers\TicketController@ViewFile')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/tickets/selfcare', 'App\Http\Controllers\TicketController@storeSelfcare')->middleware(['checkRole:Super Admin,Customer Care']);
    //End Ticket External

    //Ticket Internal
    Route::resource('/tickets_internal', 'App\Http\Controllers\TicketIntController')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Partnership']);
    Route::post('tickets_Internal/addComment', 'App\Http\Controllers\TicketIntController@addComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Partnership']);
    Route::get('tickets_Internal/{id}', 'App\Http\Controllers\TicketIntController@show')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Partnership']);
    Route::get('tickets_Internal/{id}/download', 'App\Http\Controllers\TicketIntController@download')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Partnership']);
    Route::get('tickets_Internal/{id}/downloadFileComment', 'App\Http\Controllers\TicketIntController@downloadFileComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD,Partnership']);
    Route::post('tickets_Internal/reassign', 'App\Http\Controllers\TicketIntController@reassign')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::patch('tickets_Internal/return/{id}', 'App\Http\Controllers\TicketIntController@returnTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::patch('tickets_Internal/hold/{id}', 'App\Http\Controllers\TicketIntController@holdTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('tickets_Internal/unhold/{id}', 'App\Http\Controllers\TicketIntController@unholdTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::patch('tickets_Internal/close/{id}', 'App\Http\Controllers\TicketIntController@close')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::patch('tickets_Internal/preclosed/{id}', 'App\Http\Controllers\TicketIntController@preclosed')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('/masters/topology/view/{topology_file}/{cust}', 'App\Http\Controllers\TicketIntController@ViewFile')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    //End Ticket Internal

    //Ticket Maintenance
    Route::resource('/tickets_maintenance', 'App\Http\Controllers\TicketMaintenanceController')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets_Maintenance/{id}', 'App\Http\Controllers\TicketMaintenanceController@show')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets_Maintenance/{id}/download', 'App\Http\Controllers\TicketMaintenanceController@download')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('/masters/topology/view/{topology_file}/{cust}', 'App\Http\Controllers\TicketMaintenanceController@ViewFile')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets_Maintenance/addComment', 'App\Http\Controllers\TicketMaintenanceController@addComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets_Maintenance/{id}/downloadFileComment', 'App\Http\Controllers\TicketMaintenanceController@downloadFileComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets_Maintenance/reschedule', 'App\Http\Controllers\TicketMaintenanceController@reschedule')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets_Maintenance/cancel', 'App\Http\Controllers\TicketMaintenanceController@cancel')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets_Maintenance/closeticket', 'App\Http\Controllers\TicketMaintenanceController@closeticket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    //End Ticket Maintenance

    //Ticket Internal Maintenance
    Route::resource('/tickets_internal_maintenance', 'App\Http\Controllers\TicketInternalMaintenanceController')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('tickets_IntMaintenance/{id}', 'App\Http\Controllers\TicketInternalMaintenanceController@show')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::patch('tickets_IntMaintenance/return/{id}', 'App\Http\Controllers\TicketInternalMaintenanceController@returnTicket')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::patch('tickets_IntMaintenance/reassign', 'App\Http\Controllers\TicketInternalMaintenanceController@reassign')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets_IntMaintenance/preclosed/{id}', 'App\Http\Controllers\TicketInternalMaintenanceController@preclosed')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::patch('tickets_IntMaintenance/close/{id}', 'App\Http\Controllers\TicketInternalMaintenanceController@close')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care']);
    Route::post('tickets_IntMaintenance/addComment', 'App\Http\Controllers\TicketInternalMaintenanceController@addComment')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets_IntMaintenance/reschedule', 'App\Http\Controllers\TicketInternalMaintenanceController@reschedule')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('tickets_IntMaintenance/cancel', 'App\Http\Controllers\TicketInternalMaintenanceController@cancel')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    //End Ticket Internal Maintenance

    //Ticket FS
    Route::resource('/tickets_fs', 'App\Http\Controllers\FsTicketController')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/tickets_fs/create/{cid}', 'App\Http\Controllers\FsTicketController@create')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/tickets_fs/filter', 'App\Http\Controllers\FsTicketController@index')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/tickets_fs/show-cancel/{reqno}', 'App\Http\Controllers\FsTicketController@ShowCancel')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('/tickets_fs/detail/{reqno}', 'App\Http\Controllers\FsTicketController@DetailTicket')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/tickets_fs/cancel', 'App\Http\Controllers\FsTicketController@CancelTicket')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::get('tickets_fs/{docname}/download', 'App\Http\Controllers\FsTicketController@download')->middleware(['checkRole:Super Admin,Customer Care']);
    //End Ticket FS

    //Check ONT
    Route::get('/ont-fs', 'App\Http\Controllers\CheckONTController@index')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/ont-fs/check', 'App\Http\Controllers\CheckONTController@checkONT')->middleware(['checkRole:Super Admin,Customer Care']);
    //End Check ONT

    //Selfcare
    Route::resource('/selfcare', 'App\Http\Controllers\SelfcareController')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/selfcare/filter', 'App\Http\Controllers\SelfcareController@index')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::patch('/selfcare/cancel/{id}', 'App\Http\Controllers\SelfcareController@cancelRequest')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/selfcare/create-ticket/', 'App\Http\Controllers\SelfcareController@createTicket')->middleware(['checkRole:Super Admin,Customer Care']);
    //End Selfcare

    //ticket partnership
    Route::get('/tickets_partnership', 'App\Http\Controllers\TicketPartnerController@index')->middleware(['checkRole:Partnership,Customer Care']);
    Route::post('/tickets_partnership', 'App\Http\Controllers\TicketPartnerController@index')->middleware(['checkRole:Partnership,Customer Care']);
    Route::get('/tickets_partnership/create', 'App\Http\Controllers\TicketPartnerController@create')->middleware(['checkRole:Partnership,Customer Care']);
    Route::post('/tickets_partnership/store', 'App\Http\Controllers\TicketPartnerController@store')->middleware(['checkRole:Partnership,Customer Care']);
    Route::get('/tickets_partnership/{id_ticket}', 'App\Http\Controllers\TicketPartnerController@show')->middleware(['checkRole:Partnership,Customer Care']);
    Route::get('/tickets_partnership/{filename}/download', 'App\Http\Controllers\TicketPartnerController@download')->middleware(['checkRole:Partnership,Customer Care']);
    Route::get('/tickets_partnership/{filename}/download', 'App\Http\Controllers\TicketPartnerController@downloadpo')->middleware(['checkRole:Partnership,Customer Care']);
    Route::post('tickets_partnership/addComment', 'App\Http\Controllers\TicketPartnerController@addComment')->middleware(['checkRole:Partnership,Customer Care']);
    Route::get('tickets_partnership/{filename}/downloadFileComment', 'App\Http\Controllers\TicketPartnerController@downloadFileComment')->middleware(['checkRole:Partnership,Customer Care']);
    Route::patch('tickets_partnership/close/{id_ticket}', 'App\Http\Controllers\TicketPartnerController@closeTicket')->middleware(['checkRole:Partnership,Customer Care']);
    Route::patch('tickets_partnership/update-rfs/{id_ticket}', 'App\Http\Controllers\TicketPartnerController@updateRFS')->middleware(['checkRole:Partnership,Customer Care']);
    //ticket partnership

    // Old Ticket
    Route::resource('/old_tickets', 'App\Http\Controllers\OldTicketController')->middleware(['checkRole:Super Admin,Customer Care']);

    //Export
    Route::get('/tickets/rfo/{id}', 'App\Http\Controllers\ExportController@rfo')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::get('/tickets/rfo_maintenance/{id}', 'App\Http\Controllers\ExportController@rfoMaintenance')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/tickets/exportSummary', 'App\Http\Controllers\ExportController@exportSummary')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/tickets/export/{summary}/{category}/{from}/{until}', 'App\Http\Controllers\ExportController@export')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/tickets/exportLogAssign', 'App\Http\Controllers\ExportController@exportLogAssign')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/tickets_internal/exportLogAssign', 'App\Http\Controllers\ExportController@exportLogAssign')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/tickets/export-bulk-Assign', 'App\Http\Controllers\ExportController@exportBulkAssign')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,BOD']);
    Route::post('/Log/audit-log/export', 'App\Http\Controllers\ExportController@exportAuditLog')->middleware(['checkRole:Super Admin']);
    Route::post('/tickets_fs/export', 'App\Http\Controllers\ExportController@exportFS')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/selfcare/export', 'App\Http\Controllers\ExportController@exportSelfcare')->middleware(['checkRole:Super Admin,Customer Care']);
    Route::post('/tickets_partnership/export', 'App\Http\Controllers\ExportController@exportPartnership')->middleware(['checkRole:Partnership,Customer Care']);
    //End export

    //Ajax
    Route::get('/tickets/ajax/{dept_id}', 'App\Http\Controllers\AssigndeptController@selectDeptAjax')->name('dept')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('/tickets/ajax/mappingIncident/{category_id}', 'App\Http\Controllers\MappingIncAjaxController@selectIncidentAjax')->name('mappingIncident')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('/tickets/ajax/outage/{outage_id}', 'App\Http\Controllers\MappingOutageAjaxController@selectOutageAjax')->name('outage')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('/tickets/ajax/mapping/{category_id}', 'App\Http\Controllers\MappingAjaxController@selectMappingAjax')->name('mapping')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('/tickets/ajax/mappingslr/{slr_id}', 'App\Http\Controllers\MappingSLRController@selectSLRAjax')->name('slr')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    Route::get('/tickets/ajax/responsible/{responsibleID}', 'App\Http\Controllers\MappingResponsibleAjaxController@selectResponsibleAjax')->name('responsible')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
    //End Ajax

    // Notification
    Route::get('/holdnotif', 'App\Http\Controllers\NotifController@CreateHoldNotif')->middleware(['checkRole:Super Admin,Customer Care']);
    // End notification

    //Master
    Route::get('/masters/customers/create-partnership', 'App\Http\Controllers\CustomerController@createbyPartnership')->middleware(['checkRole:Super Admin,Sales Admin,Partnership']);
    Route::post('/masters/customers/store-partnership', 'App\Http\Controllers\CustomerController@storebyPartnership')->middleware(['checkRole:Super Admin,Sales Admin,Partnership']);
    Route::resource('/masters/dropdowns', 'App\Http\Controllers\DropdownController')->middleware(['checkRole:Super Admin']);
    Route::resource('/masters/categories', 'App\Http\Controllers\CategoryController')->middleware(['checkRole:Super Admin']);
    Route::resource('/masters/mapping_incident', 'App\Http\Controllers\MappingIncidentController')->middleware(['checkRole:Super Admin']);
    Route::resource('/masters/topology', 'App\Http\Controllers\TopologyController')->middleware(['checkRole:Super Admin,User']);
    Route::get('/masters/topology/view/{topology_file}/{cust}', 'App\Http\Controllers\TopologyController@ViewFile')->middleware(['checkRole:Super Admin,User']);
    Route::resource('/masters/users', 'App\Http\Controllers\UserController')->middleware(['checkRole:Super Admin']);
    Route::get('/masters/topology/view/{topology_file}/{cust}', 'App\Http\Controllers\TopologyController@ViewFile')->middleware(['checkRole:Super Admin']);
    Route::get('/masters/users/activate/{id}', 'App\Http\Controllers\UserController@ActiveUser')->middleware(['checkRole:Super Admin']);
    Route::get('/masters/users/revoke/{id}', 'App\Http\Controllers\UserController@RevokeUser')->middleware(['checkRole:Super Admin']);
    Route::get('/masters/users/activate_ca/{id}', 'App\Http\Controllers\UserController@ActiveUserCA')->middleware(['checkRole:Super Admin']);
    Route::get('/masters/users/revoke_ca/{id}', 'App\Http\Controllers\UserController@RevokeUserCA')->middleware(['checkRole:Super Admin']);
    Route::post('/masters/users/role', 'App\Http\Controllers\UserController@UpdateRole')->middleware(['checkRole:Super Admin']);
    Route::post('/masters/users/pic', 'App\Http\Controllers\UserController@UpdatePIC')->middleware(['checkRole:Super Admin']);
    Route::resource('/masters/rule', 'App\Http\Controllers\RuleController')->middleware(['checkRole:Super Admin']);
    Route::resource('/masters/customers', 'App\Http\Controllers\CustomerController')->middleware(['checkRole:Super Admin,Sales Admin,Partnership']);
    //Route::resource('/masters/customers/create', 'App\Http\Controllers\CustomerController')->middleware(['checkRole:Super Admin']);
    //End Master

    //Logs
    Route::get('/Log/audit-log/', 'App\Http\Controllers\AuditLogController@index')->middleware(['checkRole:Super Admin']);
    Route::get('/Log/api-log/', 'App\Http\Controllers\ApiLogsController@index')->middleware(['checkRole:Super Admin']);
    //Edn Logs

    Route::post('autocomplete', 'App\Http\Controllers\SearchcustController@searchcustomer')->name('autocomplete')->middleware(['checkRole:Super Admin,User,Sales Admin,Customer Care,Partnership']);
});