<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketPartnershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_partnerships', function (Blueprint $table) {
            $table->id();
            $table->string('no_ticket')->nullable();
            $table->string('id_cust')->nullable();
            $table->string('building_type')->nullable();
            $table->text('coordinate')->nullable();
            $table->decimal('otc',13,2)->nullable();
            $table->string('term_of_contract')->nullable();
            $table->string('spesific_requirement')->nullable();
            $table->string('network_type')->nullable();
            $table->string('network_owner')->nullable();
            $table->dateTime('sales_inquiry')->nullable();
            $table->text('notes')->nullable();
            $table->text('quotation_file')->nullable();
            $table->string('status')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_partnerships');
    }
}
