<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsPartnershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_partnerships', function (Blueprint $table) {
            $table->id();
			$table->bigInteger('id_ticket');
			$table->string('create_by');
			$table->string('description');
			$table->string('message');
			$table->string('attachment_1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_partnerships');
    }
}
