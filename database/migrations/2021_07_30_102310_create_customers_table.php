<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('cid');
			$table->string('company_name');
			$table->string('priority');
			$table->string('company_type');
			$table->string('service');
			$table->string('pic_name');
			$table->string('pic_contact');
			$table->string('capacity');
			$table->string('a_end');
			$table->string('b_end');
			$table->string('network_type');
			$table->string('topology_file1', 225);
			$table->string('topology_file2', 225);
			$table->string('topology_file3', 225);
			$table->timestamps(6);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
