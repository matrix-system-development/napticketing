<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFsTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fs_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('ticket_type');
            $table->string('no_ticket');
            $table->string('no_urut');
            $table->string('cid');
            $table->string('category');
            $table->string('sub_category');
            $table->string('status');
            $table->dateTime('duedate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fs_tickets');
    }
}
