<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSelfcaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selfcares', function (Blueprint $table) {
            $table->id();
            $table->string('no_request');
            $table->string('no_ticket');
            $table->string('cust_code');
            $table->string('report_type');
            $table->string('report_category');
            $table->text('attachment');
            $table->text('notes');
            $table->string('billing_period');
            $table->string('status_request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selfcares');
    }
}
