<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logs_tickets', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('id_ticket');
			$table->bigInteger('id_log');
			$table->string('assign_by');
			$table->integer('assign_to_dept');
			$table->dateTime('assign_date');
			$table->integer('assign_status');
			$table->string('preclosed_status');
			$table->dateTime('preclosed_date');
			$table->string('preclosed_message');
			$table->timestamps(6);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logs_tickets');
	}

}
