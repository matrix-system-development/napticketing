<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogHoldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_holds', function (Blueprint $table) {
            $table->id();
            $table->string('id_ticket');
            $table->string('hold_by');
            $table->dateTime('hold_created_date');
            $table->dateTime('hold_date_until');
            $table->string('hold_category');
            $table->text('hold_message');
            $table->string('unhold_by');
            $table->dateTime('unhold_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_holds');
    }
}
