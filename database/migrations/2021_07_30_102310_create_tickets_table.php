<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('ticket_type');
			$table->string('no_ticket');
			$table->integer('no_urut');
			$table->string('cid')->nullable();
			$table->string('multiple_cid', 10);
			$table->string('priority', 50)->nullable();
			$table->string('status_complain');
			$table->string('request');
			$table->string('status_link_respond');
			$table->string('sub_status_link_respond');
			$table->string('category');
			$table->string('type_incident');
			$table->string('file_1');
			$table->string('file_2');
			$table->string('file_3');
			$table->string('file_4');
			$table->string('file_5');
			$table->string('remarks');
			$table->string('created_by');
			$table->dateTime('created_date');
			$table->dateTime('due_date');
			$table->string('closed_by');
			$table->dateTime('closed_date');
			$table->dateTime('start_date');
			$table->dateTime('resolved_date');
			$table->string('outage_reason', 500);
			$table->string('outage_sub', 500);
			$table->string('outage_location', 500);
			$table->string('action_close', 200);
			$table->text('closed_message');
			$table->string('rfo', 225);
			$table->string('duration');
			$table->string('hold_by');
			$table->dateTime('hold_created_date');
			$table->dateTime('hold_date');
			$table->string('hold_category');
			$table->text('hold_message');
			$table->string('unhold_by', 20);
			$table->dateTime('unhold_date');
			$table->string('status');
			$table->timestamps(6);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
