<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOldTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('request_id');
            $table->string('requester');
            $table->string('custom');
            $table->string('subject');
            $table->string('inquiry');
            $table->string('status_link_reported');
            $table->string('service');
            $table->string('responsible');
            $table->string('RFO');
            $table->string('duration');
            $table->string('resolution');
            $table->string('category');
            $table->dateTime('created_time');
            $table->dateTime('completed_time');
            $table->dateTime('resolved_time');
            $table->string('cc_open_ticket');
            $table->string('cc_close_ticket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_tickets');
    }
}
