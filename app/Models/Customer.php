<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $fillable =[
        'cid',
        'cid_new',
        'customer_code',
        'company_name',
        'address',
        'priority',
        'company_type',
        'service',
        'broadband',
        'pic_name',
        'pic_contact',
        'pic_email',
        'capacity',
        'capacity_international',
        'a_end',
        'b_end',
        'am_name',
        'capacity_mix',
        'network_type',
        'network_owner',
        'status_customer',
        'topology_file1',
        'topology_file2',
        'topology_file3',
        'start_date_customer',
        'finish_date',
        'partner_cid',
        'username_ppoe',
        'password_ppoe',
        'regional',
        'city',
        'notes'   
    ];
}