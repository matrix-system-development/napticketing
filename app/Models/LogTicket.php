<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogTicket extends Model
{
    use HasFactory;

    protected $table = 'logs_tickets';
    protected $fillable = ['id_ticket', 'id_log', 'assign_by', 'assign_to_dept', 'assign_date', 'assign_status', 'preclosed_status', 'preclosed_date', 'preclosed_message', 'for_ca'];
}
