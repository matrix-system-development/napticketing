<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MappingIncident extends Model
{
    use HasFactory;
    protected $table='mapping_incident';
    protected $fillable =['id_category','id_dropdown','name_incident'];
}
