<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OldTicket extends Model
{
    use HasFactory;
    protected $fillable = [
        'request_id',
        'requester',
        'custom',
        'subject',
        'inquiry',
        'status_link_reported',
        'service',
        'responsible',
        'RFO',
        'duration',
        'resolution',
        'category',
        'created_time',
        'completed_time',
        'resolved_time',
        'cc_open_ticket',
        'cc_close_ticket',
        // 17 coloumn
    ];
}
