<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable=[
        'ticket_type',
        'no_ticket',
        'no_urut',
        'cid',
        'multiple_cid',
        'priority',
        'status_complain',
        'request',
        'status_link_respond',
        'sub_status_link_respond',
        'category',
        'final_category',
        'type_incident',
        'final_category',
        'final_type_incident',
        'file_1',
        'file_2',
        'file_3',
        'file_4',
        'file_5',
        'created_by',
        'created_date',
        'due_date',
        'rfo',
        'duration',
        'closed_by',
        'closed_date',
        'start_date',
        'resolved_date',
        'outage_reason',
        'outage_sub',
        'outage_location',
        'action_close',
        'closed_message',
        'hold_by',
        'hold_created_date',
        'hold_date',
        'hold_category',
        'hold_message',
        'unhold_by',
        'unhold_date',
        'status',
        'remarks',
        'request_at',
        'start_date_mtc',
        'finish_date_mtc',
        'status_mtc',
        'third_party_assign',
        'responsible',
        'solved_by'
    ];
    
    public function logtickets(){
        return $this->belongsToMany(LogTickets::class);
    }
}