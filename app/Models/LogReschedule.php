<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogReschedule extends Model
{
    use HasFactory;
    protected $fillable=[
        'id_ticket',
        'start_date_mtc',
        'finish_date_mtc',
        'notes',
    ];
}
