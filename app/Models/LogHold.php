<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogHold extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_ticket',
        'hold_by',
        'hold_created_date',
        'hold_date_until',
        'hold_category',
        'hold_message',
        'unhold_by',
        'unhold_date',
    ];
}
