<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogToken extends Model
{
    use HasFactory;
    protected $fillable=[
        'access_token',
        'partner',
    ];
}
