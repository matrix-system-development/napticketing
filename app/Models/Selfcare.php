<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Selfcare extends Model
{
    use HasFactory;
    protected $fillable = [
        'no_urut',
        'no_request', 
        'no_ticket', 
        'cust_code',
        'cust_email', 
        'report_type', 
        'report_category',
        'report_content',
        'first_handling',
        'attachment', 
        'notes', 
        'billing_period', 
        'status_request',
        'cancel_notes',
        'canceled_at',
    ];
}
