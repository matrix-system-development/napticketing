<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketPartnership extends Model
{
    use HasFactory;
    protected $guarded=[
        'id'
    ];
}
