<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;
    protected $fillable=[
        'id_ticket',
        'create_by',
        'description',
        'message',
        'attachment_1'
    ];

    public function ticket(){
        return $this->belongsToMany(Ticket::class);
    }
}
