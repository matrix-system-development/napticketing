<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fsTicket extends Model
{
    use HasFactory;
    protected $fillable=[
        'ticket_type',
        'no_ticket',
        'req_no',
        'ref_no',
        'no_urut',
        'cid',
        'category',
        'sub_category',
        'status',
        'reason_cancel',
        'duedate',
        'created_by',
        'doc_name',
        'notes',
        'fs_last_update'
    ];
}
