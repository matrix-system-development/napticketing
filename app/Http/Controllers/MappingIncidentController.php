<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\MappingIncident;
use App\Models\Category;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class MappingIncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        $mappings=MappingIncident::select('categories.category_name','mapping_incident.id','dropdowns.name_value','mapping_incident.name_incident')
        ->leftJoin('dropdowns','dropdowns.id','=','mapping_incident.id_dropdown')
        ->leftJoin('categories','categories.id','=','mapping_incident.id_category')
        ->orderBy('mapping_incident.id','desc')
        ->get();

        $dropdowns['group incident']=DB::table('dropdowns')
        ->where('category','=','Group Incident')
        ->orderBy('name_value','asc')
        ->get();

        $dropdowns['category']=DB::table('categories')
        ->orderBy('category_name','asc')
        ->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Master Mapping';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('mapping_incident.index',compact('mappings','dropdowns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            'category' => 'required',
            'group' => 'required',
            'name_incident' => 'required'
        ]);

        $category=Category::where('category_name',$request->category)->get();

        $create_mapping=MappingIncident::create([
            'id_category' => $category[0]->id,
            'id_dropdown' => $request->group,
            'name_incident' => $request->name_incident
        ]);

        if($create_mapping){
            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Create Mapping';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/masters/mapping_incident')->with('status','Success Mapping !');
        }
        else{
            return redirect('/masters/mapping_incident')->with('status','Failed Mapping !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mappings=MappingIncident::select('mapping_incident.id','dropdowns.name_value','mapping_incident.name_incident')
        ->where('mapping_incident.id','=',$id)
        ->leftJoin('dropdowns','dropdowns.id','=','mapping_incident.id_dropdown')
        ->orderBy('mapping_incident.id','desc')
        ->get();

        $dropdowns=DB::table('dropdowns')
        ->where('category','=','Group Incident')
        ->orderBy('name_value','asc')
        ->get();
        
        //dd($mappings);

        return view('mapping_incident.edit',compact('mappings','dropdowns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $request->validate([
            'group_incident' => 'required',
            'name_incident' => 'required'
        ]);

        
        MappingIncident::where('id', $id)
        ->update([
            'id_dropdown' => $request->group_incident,
            'name_incident' => $request->name_incident
        ]);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Update Mapping';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/masters/mapping_incident')->with('status', 'Data Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //dd($id);
        MappingIncident::destroy($id);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Delete Mapping';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/masters/mapping_incident')->with('status','Data Deleted!');
    }
}
