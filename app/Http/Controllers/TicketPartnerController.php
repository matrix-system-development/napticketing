<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Rule;
use App\Models\TicketPartnership;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\Traits\AuditLogsTrait;
use App\Models\LogsPartnership;
use Browser;
use Illuminate\Database\Events\TransactionBeginning;
use PhpParser\Node\Stmt\TryCatch;

class TicketPartnerController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        //dd('hai');
        if ($request->date_start == '' || $request->date_finish == '') {
            $now = Carbon::now();
            $date_now = Carbon::now()->lastOfMonth();
            $date_start2 = Carbon::now()->startOfMonth()->subMonth(2);
        } else {
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);


            $date_now = $request->date_finish;
            $date_start2 = $request->date_start;
        }

        $tickets = TicketPartnership::select('ticket_partnerships.*', 'customers.company_name', 'employee.int_emp_name')
            ->leftJoin('customers', 'ticket_partnerships.id_cust', 'customers.id')
            ->leftJoin('employee', 'ticket_partnerships.created_by', 'employee.int_emp_id')
            ->whereBetween(DB::raw("(STR_TO_DATE(ticket_partnerships.created_at,'%Y-%m-%d'))"), [$date_start2, $date_now])
            ->orderBy('ticket_partnerships.id', 'desc')
            ->get();
        // dd($tickets);

        return view('ticket_partnership.index', compact('tickets', 'date_start2', 'date_now'));
    }

    public function create()
    {
        // dd('create ticket partner');
        $dropdowns = array();

        $dropdowns['building type'] = DB::table('dropdowns')
            ->where('category', '=', 'Building Type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['toc'] = DB::table('dropdowns')
            ->where('category', '=', 'Term of Contract')
            ->orderBy('id', 'asc')
            ->get();

        $dropdowns['network type'] = DB::table('dropdowns')
            ->where('category', '=', 'network type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['currency'] = DB::table('dropdowns')
            ->where('category', '=', 'Currency')
            ->orderBy('name_value', 'asc')
            ->get();
        
        //ambil nama sales dari api master data
        $ruleAuthMstData = Rule::where('rule_name', 'API Get Token Master Data')->first();
        $url_AuthMstData = $ruleAuthMstData->rule_value;

        $ruleEmailMstData = Rule::where('rule_name', 'Username API Master Data')->first();
        $emailMstData = $ruleEmailMstData->rule_value;

        $rulePasswordMstData = Rule::where('rule_name', 'Password API Master Data')->first();
        $passwordMstData = $rulePasswordMstData->rule_value;

        $ruleSalesName = Rule::where('rule_name', 'API AM Name Master Data')->first();
        $url_SalesName = $ruleSalesName->rule_value;

        //get token dulu yh
        $response = Http::post($url_AuthMstData, [
            'email' => $emailMstData,
            'password' => $passwordMstData,
        ]);
        $result = json_decode($response);
        $token = $result->data->token;

        //ambil data nama sales
        $responseSales = Http::withToken($token)->get($url_SalesName);
        $resultSales = json_decode($responseSales);
        $dataSales = $resultSales->data;

        return view('ticket_partnership.create', compact('dropdowns', 'dataSales'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'id_cust' => 'required',
            'building_type' => 'required',
            'coordinate' => 'required',
            'currency' => 'required',
            'otc' => 'required',
            'mrc' => 'required',
            'toc' => 'required',
            'requirement' => 'required',
            'net_owner' => 'required',
            'net_type' => 'required',
            'requestor' => 'required',
            'inquiry_date' => 'required',
            'quotation_file' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048'
        ]);

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $ticket_status = '0';
        $otc = str_replace(',', '', $request->otc);
        $mrc = str_replace(',', '', $request->mrc);
        // dd($otc,$mrc);

        if ($request->toc == 'Other') {
            $toc = $request->value_toc . " " . $request->unit_toc;
        } else {
            $toc = $request->toc;
        }

        if ($request->net_type == 'OFFNET') {
            $request->validate([
                'cid_third_party' => 'required'
            ]);
            $cid_third_party = $request->cid_third_party;
        } else {
            $cid_third_party = '';
        }

        DB::beginTransaction();
        try {
            //format tanggal
            $period_ticket = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = TicketPartnership::whereDay('created_at', $created_date)
                ->whereMonth('created_at', $created_date)
                ->whereYear('created_at', $created_date)
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noticket = "P/" . $period_ticket . "/" . $lastno;

            //insert into table tickets
            $ticket_create = TicketPartnership::create([
                'no_ticket' => $noticket,
                'no_urut'   => $no_urut,
                'id_cust' => $request->id_cust,
                'building_type' => $request->building_type,
                'coordinate' => $request->coordinate,
                'currency' => $request->currency,
                'otc' => $otc,
                'mrc' => $mrc,
                'term_of_contract' => $toc,
                'spesific_requirement' => $request->requirement,
                'network_type' => $request->net_type, //'kolom db' => $request->nama input name dari form
                'network_owner' => $request->net_owner,
                'cid_third_party' => $cid_third_party,
                'requestor' => $request->requestor,
                'sales_inquiry' => $request->inquiry_date,
                'notes' => $request->notes,
                'quotation_file' => '',
                'status' => $ticket_status,
                'created_by' => $created_by,
            ]);
            //end insert into table tickets

            //upload file
            $id = $ticket_create->id;
            if ($request->file('quotation_file')) {
                $extension = $request->file('quotation_file')->getClientOriginalExtension();
                $doc_name = $id . '_a.' . $extension;
                $store = $request->file('quotation_file')->storeAs('attachment_ticketpartnership', $doc_name);
            } else {
                $doc_name = "";
            }

            $insert_filename = TicketPartnership::where('id', $id)
                ->update([
                    'quotation_file' => $doc_name
                ]);

            DB::commit();
            // all good

            return redirect('/tickets_partnership')->with('status', 'Success Create Ticket Partnership');
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong

            return redirect('/tickets_partnership')->with('failed', 'Failed Create Ticket Partnership');
        }
    }

    public function show($id_ticket)
    {
        $id_ticket = decrypt($id_ticket);
        // dd($id_ticket);

        $dropdownsSubStatus = DB::table('dropdowns')
            ->where('category', '=', 'Sub Status Partnership')
            ->orderBy('id', 'asc')
            ->get();

        $ticket = TicketPartnership::where('id', $id_ticket)->first();

        $customer = Customer::where('id', $ticket->id_cust)->first();

        $comments  = LogsPartnership::where('id_ticket', $id_ticket)
            ->select('logs_partnerships.*', 'employee.int_emp_name', 'department.department_name')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_partnerships.create_by')
            ->leftJoin('department', 'department.department_id', '=', 'employee.int_emp_department')
            ->orderBy('logs_partnerships.created_at', 'desc')
            ->get();

        // dd($comments);

        return view('ticket_partnership.show', compact('ticket', 'dropdownsSubStatus', 'customer', 'comments'));
    }

    public function download($filename)
    {
        //dd($ticket);
        $pathToFile = storage_path('app/attachment_ticketpartnership/' .  $filename);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File Ticket Partnership';

        //dd($location);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);
        return response()->download($pathToFile);
    }

    public function downloadFileComment(Request $request,  $filename)
    {
        $pathToFileComment_1 = storage_path('app/attachment_comment_partnership/' . $filename);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File Comment Partnership';

        //dd($location);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);
        return response()->download($pathToFileComment_1);
    }

    public function addComment(Request $request)
    {
        // 'nama object'
        $request->validate([
            'id_ticket' => 'required',
            'message_comment'  => 'required',
            'attc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        $description_comment =  'Success Create Comment';

        $comment_creator = auth()->user()->id_employee;

        // ' nama coloumn table'
        $comment_create =  LogsPartnership::create([
            'id_ticket' => $request->id_ticket,
            'create_by' => $comment_creator,
            'message' => $request->message_comment,
            // 'attachment_1' => $request->attc1,
            'description' => $description_comment
        ]);

        // dd($comment_create);

        if ($comment_create) {
            // generate id comment
            $id_comment = $comment_create->id;

            //upload file
            if ($request->file('attc1')) {
                $extension1 = $request->file('attc1')->getClientOriginalExtension();
                $doc_name1 = $id_comment . '_comment.' . $extension1;
                $store1 = $request->file('attc1')->storeAs('attachment_comment_partnership', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            // 'nama coloumn table' <- insert attachment ke table logs
            $insert_log = LogsPartnership::where('id', $id_comment)
                ->update([
                    'attachment_1' => $doc_name1
                ]);

            // $date_submit=$comment_create->created_at;

            // dd($date_submit - $created_date);

            if ($comment_create && $insert_log != NULL || $comment_create && $insert_log == NULL) {
                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Comment Partnership';

                //dd($location);
                $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

                return redirect('/tickets_partnership/'  . encrypt($comment_create->id_ticket))->with('status',  'Success Create Comment !');
            } else {
                return redirect('/tickets_partnership/'  . encrypt($comment_create->id_ticket))->with('status',  'Fail Create Comment !');
            }
        } else {
            return redirect('/tickets_partnership/' . encrypt($comment_create->id_ticket))->with('status', 'Fail Create Comment !');
        }
    }

    public function closeTicket(Request $request, $id_ticket)
    {
        // dd($request->all());
        // dd($id_ticket);
        //status 1=follow up, 2=cover, 3=purchase order, 4=not cover, 5=no response, 6=Inquiry, 7=Feedback, 8=Req 3rd Party, 9=Purchase request

        $closed_date = Carbon::now();
        $username = auth()->user()->email;

        if ($request->ticket_status != '3' && $request->ticket_status != '7' && $request->ticket_status != '8' && $request->ticket_status != '9') {
            // done
            // dd ('halo');
            $request->validate([
                'ticket_status' => 'required',
                'notes' => 'required',
                'last_status_date' => 'required',
            ]);

            $status = $request->ticket_status;
            $last_status_date = $request->last_status_date;
            $close_notes = $request->notes;
            
            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_third_party_time = [];
            array_push(
                $array_third_party_time,
                $ticket->best_price_otc,
                $ticket->best_price_mrc,
                $ticket->rfs,
                $ticket->contract_period,
                $ticket->no_po,
                $ticket->no_pr,
                $ticket->request_third_party_time,
                $ticket->partnership_response_time,
                $ticket->feedback_third_party_time,
                $ticket->third_party_response_time
            );
            $best_price_otc = $array_third_party_time[0];
            $best_price_mrc = $array_third_party_time[1];
            $rfs = $array_third_party_time[2];
            $contract_period = $array_third_party_time[3];
            $no_po = $array_third_party_time[4];
            $no_pr = $array_third_party_time[5];
            $request_third_party_time = $array_third_party_time[6];
            $partnership_response_time = $array_third_party_time[7];
            $feedback_third_party_time = $array_third_party_time[8];
            $third_party_response_time = $array_third_party_time[9];
            
        } elseif ($request->ticket_status == '3') {
            // done
            // dd("9 ni");
            //validasi form purchase order
            $request->validate([
                'ticket_status' => 'required',
                'notes' => 'required',
                'last_status_date' => 'required',
                'best_price_otc' => 'required',
                'rfs' => 'required',
                'contract_period' => 'required',

            ]);

            $status = $request->ticket_status;
            $last_status_date = $request->last_status_date;
            $close_notes = $request->notes;
            $best_price_otc = str_replace(',', '', $request->best_price_otc);
            $best_price_mrc = str_replace(',', '', $request->best_price_mrc);
            $rfs = $request->rfs;
            $contract_period = $request->contract_period;
            $no_po = $request->no_po;
            $sub_status = '';

            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_third_party_time = [];
            array_push(
                $array_third_party_time,
                $ticket->no_pr,
                $ticket->request_third_party_time,
                $ticket->partnership_response_time,
                $ticket->feedback_third_party_time,
                $ticket->third_party_response_time
            );
            $no_pr = $array_third_party_time[0];
            $request_third_party_time = $array_third_party_time[1];
            $partnership_response_time = $array_third_party_time[2];
            $feedback_third_party_time = $array_third_party_time[3];
            $third_party_response_time = $array_third_party_time[4];

            //upload file
            if ($request->file('file_po')) {
                $extension = $request->file('file_po')->getClientOriginalExtension();
                $doc_name = $id_ticket . '_b.' . $extension;
                $store = $request->file('file_po')->storeAs('attachment_ticketpartnership', $doc_name);
            } else {
                $doc_name = "";
            }

            $insert_filename = TicketPartnership::where('id', $id_ticket)
                ->update([
                    'file_po' => $doc_name
                ]);

            // dd($request->all());


        } elseif ($request->ticket_status == '7') {

            //done 
            $request->validate([
                'ticket_status' => 'required', //'input name dari form' => 'required'
                'notes' => 'required',
                'last_status_date' => 'required'
            ]);

            $thirdparty = TicketPartnership::where('id', $id_ticket)->first();
            $feedback_third_party_time = $request->last_status_date;

            //duration
            if ($thirdparty->request_third_party_time != '') {
                $start = strtotime($thirdparty->request_third_party_time);
            } else {
                return redirect('/tickets_partnership/' . encrypt($id_ticket))->with('statusError', 'Failed Update Ticket, Request to 3rd Party Not Defined');
            }

            $end = strtotime($feedback_third_party_time);
            $duration = $end - $start;
            // dd($end);

            $total_second = $duration; // 177300 second
            $total_minutes = ($duration / 60); // 2955 minute
            $temp_minutes = ($total_second % 3600 / (60));  // 177300 sec % 3600 = 900 sec / 60 = 15 menit
            $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
            $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
            $minutes = ceil(((($temp_minutes * 60) - $second) / 60)); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
            //end duration

            // dd($hours . ' hours ' . $minutes . ' minutes');

            $status = $request->ticket_status;
            $last_status_date = $request->last_status_date;
            $close_notes = $request->notes;

            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_third_party_time = [];
            array_push(
                $array_third_party_time,
                $ticket->best_price_otc,
                $ticket->best_price_mrc,
                $ticket->rfs,
                $ticket->contract_period,
                $ticket->no_po,
                $ticket->no_pr,
                $ticket->request_third_party_time,
                $ticket->partnership_response_time
            );
            $best_price_otc = $array_third_party_time[0];
            $best_price_mrc = $array_third_party_time[1];
            $rfs = $array_third_party_time[2];
            $contract_period = $array_third_party_time[3];
            $no_po = $array_third_party_time[4];
            $no_pr = $array_third_party_time[5];
            $request_third_party_time = $array_third_party_time[6];
            $partnership_response_time = $array_third_party_time[7];
            $feedback_third_party_time = $request->last_status_date;

            $third_party_response_time = $hours . ' hours ' . $minutes . ' minutes';

            // dd($request->all());

        } elseif ($request->ticket_status == '8') {
            // done
            $request->validate([
                'ticket_status' => 'required', //'input name dari form' => 'required'
                'notes' => 'required',
                'last_status_date' => 'required',
            ]);

            $sales_inquiry = TicketPartnership::where('id', $id_ticket)->first();
            $last_status_date = $request->last_status_date;
            // dd($sales_inquiry->sales_inquiry);

            //duration
            $start = strtotime($sales_inquiry->sales_inquiry);
            $end = strtotime($last_status_date);
            $duration = $end - $start;
            // dd($start);

            $total_second = $duration; // 177300 second
            $total_minutes = ($duration / 60); // 2955 minute
            $temp_minutes = ($total_second % 3600 / (60));  // 177300 sec % 3600 = 900 sec / 60 = 15 menit
            $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
            $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
            $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
            //end duration

            // dd('jam '.$hours);

            $status = $request->ticket_status;
            $last_status_date = $request->last_status_date;
            $close_notes = $request->notes;
            $request_third_party_time = $request->last_status_date;
            $partnership_response_time = $hours . ' hours ' . $minutes . ' minutes';
            $feedback_third_party_time = null;
            $third_party_response_time = null;

            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_third_party_time = [];
            array_push(
                $array_third_party_time,
                $ticket->best_price_otc,
                $ticket->best_price_mrc,
                $ticket->rfs,
                $ticket->contract_period,
                $ticket->no_po,
                $ticket->no_pr
            );
            $best_price_otc = $array_third_party_time[0];
            $best_price_mrc = $array_third_party_time[1];
            $rfs = $array_third_party_time[2];
            $contract_period = $array_third_party_time[3];
            $no_po = $array_third_party_time[4];
            $no_pr = $array_third_party_time[5];
            
        } elseif ($request->ticket_status == '9') {
            // done
            // dd("9 ni");
            //validasi form purchase order
            $request->validate([
                'ticket_status' => 'required', //'input name dari form' => 'required'
                'notes' => 'required',
                'best_price_otc' => 'required',
                'best_price_mrc' => 'required',
                'rfs' => 'required',
                'contract_period' => 'required',
                'no_pr' => 'required',
                'last_status_date' => 'required',
            ]);

            // dd($request->all());

            $status = $request->ticket_status;
            $last_status_date = $request->last_status_date;
            $close_notes = $request->notes;
            $best_price_otc = str_replace(',', '', $request->best_price_otc);
            $best_price_mrc = str_replace(',', '', $request->best_price_mrc);
            $rfs = $request->rfs;
            $contract_period = $request->contract_period;
            $no_pr = $request->no_pr;

            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_third_party_time = [];
            array_push(
                $array_third_party_time,
                $ticket->no_po,
                $ticket->request_third_party_time,
                $ticket->partnership_response_time,
                $ticket->feedback_third_party_time,
                $ticket->third_party_response_time
            );
            $no_po = $array_third_party_time[0];
            $request_third_party_time = $array_third_party_time[1];
            $partnership_response_time = $array_third_party_time[2];
            $feedback_third_party_time = $array_third_party_time[3];
            $third_party_response_time = $array_third_party_time[4];
            
        }
        DB::beginTransaction();

        $updateStatus = TicketPartnership::where('id', $id_ticket)
            ->update([
                'status' => $status,
                'last_status_date' => $last_status_date,
                'close_notes' => $close_notes,
                'best_price_otc' => $best_price_otc,
                'best_price_mrc' => $best_price_mrc,
                'rfs' => $rfs,
                'contract_period' => $contract_period,
                'no_po' => $no_po,
                'no_pr' => $no_pr,
                'closed_by' => $username,
                'closed_at' => $closed_date,
                'sub_status' => '',
                'request_third_party_time' => $request_third_party_time,
                'partnership_response_time' => $partnership_response_time,
                'feedback_third_party_time' => $feedback_third_party_time,
                'third_party_response_time' => $third_party_response_time
            ]);

        DB::commit();
        // all good

        // dd($request->all());

        return redirect('/tickets_partnership/' . encrypt($id_ticket))->with('status', 'Success Close Ticket');
    }

    public function downloadpo($filename)
    {
        //dd($ticket);
        $pathToFile = storage_path('app/attachment_ticketpartnership/' .  $filename);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File Purchase Order';

        //dd($location);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);
        return response()->download($pathToFile);
    }

    public function updateRFS(Request $request, $id_ticket)
    {
        // dd('hai');

        // $checkRFS = TicketPartnership::where('id', $id_ticket)->first();
        // $checkRFS->rfs = $request->rfs;
        $ticket = TicketPartnership::where('id', $id_ticket)->first();

        if ($ticket->status == '3') {

            $request->validate([
                'sub_status' => 'required',
            ]);

            $rfs = $request->rfs;
            $sub_status = $request->sub_status;
            $no_po = $request->no_po;
            
            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_purchase = [];
            array_push(
                $array_purchase,
                $ticket->no_pr
            );
            $no_pr = $array_purchase[0];
            
        } elseif ($ticket->status == '9') {

            $request->validate([
                'sub_status' => 'required',
            ]);

            $rfs = $request->rfs;
            $sub_status = $request->sub_status;
            $no_pr = $request->no_pr;
            
            $ticket = TicketPartnership::where('id', $id_ticket)->first();
            $array_purchase = [];
            array_push(
                $array_purchase,
                $ticket->no_po
            );
            $no_po = $array_purchase[0];

        }
            DB::beginTransaction();

            $updateRFS = TicketPartnership::where('id', $id_ticket)
                ->update([
                    'rfs' => $rfs,
                    'sub_status' => $sub_status,
                    'no_po' => $no_po,
                    'no_pr' => $no_pr
                ]);

            DB::commit();

            // dd($request->all());

            return redirect('/tickets_partnership/' . encrypt($id_ticket))->with('status', 'Success Update Info');    
    }
        
}