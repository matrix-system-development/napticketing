<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dropdown;

class MappingIncAjaxController extends Controller
{
    public function selectIncidentAjax($category_id){

        $dropdown=Dropdown::where('name_value',$category_id)->get();

        $query=DB::table("mapping_incident")
        ->where("id_dropdown","=",$dropdown[0]->id)
        ->orderBy('name_incident','asc')
        ->get();
        //dd($query);
        return json_encode($query);
    }
}
