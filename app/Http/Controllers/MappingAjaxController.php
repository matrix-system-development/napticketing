<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dropdown;

class MappingAjaxController extends Controller
{
    public function selectMappingAjax($category_id){

        $query=DB::table("dropdowns")
        ->where("category","=",$category_id)
        ->orderBy('name_value','asc')
        ->get();
        //dd($query);
        return json_encode($query);
    }
}
