<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Illuminate\Support\Facades\Http;
use Stevebauman\Location\Facades\Location;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index()
    {
        $customers = Customer::orderBy('id', 'desc')->get();

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Customers List Data';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dropdowns = array();

        $dropdowns['network type'] = DB::table('dropdowns')
            ->where('category', '=', 'network type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['service'] = DB::table('dropdowns')
            ->where('category', '=', 'service')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['regional'] = DB::table('dropdowns')
            ->where('category', '=', 'regional')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['status customer'] = DB::table('dropdowns')
            ->where('category', '=', 'status customer')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['priority'] = DB::table('dropdowns')
            ->where('category', '=', 'priority')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['broadband'] = DB::table('dropdowns')
            ->where('category', '=', 'broadband')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['Customer Type'] = DB::table('dropdowns')
            ->where('category', '=', 'Customer Type')
            ->orderBy('name_value', 'asc')
            ->get();


        //ambil nama sales dari api master data
        $ruleAuthMstData = Rule::where('rule_name', 'API Get Token Master Data')->first();
        $url_AuthMstData = $ruleAuthMstData->rule_value;

        $ruleEmailMstData = Rule::where('rule_name', 'Username API Master Data')->first();
        $emailMstData = $ruleEmailMstData->rule_value;

        $rulePasswordMstData = Rule::where('rule_name', 'Password API Master Data')->first();
        $passwordMstData = $rulePasswordMstData->rule_value;

        $ruleSalesName = Rule::where('rule_name', 'API AM Name Master Data')->first();
        $url_SalesName = $ruleSalesName->rule_value;

        //get token dulu yh
        $response = Http::post($url_AuthMstData, [
            'email' => $emailMstData,
            'password' => $passwordMstData,
        ]);
        $result = json_decode($response);
        $token = $result->data->token;
        // dd($token);

        //ambil data nama sales
        $responseSales = Http::withToken($token)->get($url_SalesName);
        $resultSales = json_decode($responseSales);
        $dataSales = $resultSales->data;
        
        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Create Customers Menu';

        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('customers.create', compact('dropdowns', 'dataSales'));
    }

    public function createbyPartnership(){
        // dd('hi partnership');

        $dropdowns = array();

        $dropdowns['network type'] = DB::table('dropdowns')
            ->where('category', '=', 'network type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['service'] = DB::table('dropdowns')
            ->where('category', '=', 'service')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['regional'] = DB::table('dropdowns')
            ->where('category', '=', 'regional')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['status customer'] = DB::table('dropdowns')
            ->where('category', '=', 'status customer')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['priority'] = DB::table('dropdowns')
            ->where('category', '=', 'priority')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['broadband'] = DB::table('dropdowns')
            ->where('category', '=', 'broadband')
            ->orderBy('name_value', 'asc')
            ->get();
            
        //ambil nama sales dari api sales
        $ruleAuthMstData = Rule::where('rule_name', 'API Get Token Master Data')->first();
        $url_AuthMstData = $ruleAuthMstData->rule_value;

        $ruleEmailMstData = Rule::where('rule_name', 'Username API Master Data')->first();
        $emailMstData = $ruleEmailMstData->rule_value;

        $rulePasswordMstData = Rule::where('rule_name', 'Password API Master Data')->first();
        $passwordMstData = $rulePasswordMstData->rule_value;

        //get token dulu yh
        $response = Http::post($url_AuthMstData,[
            'email'     => $emailMstData,
            'password'  => $passwordMstData,
        ]);
        $result = json_decode($response);
        $token = $result->data->token;
        // dd($token);

        //ambil data nama sales
        $ruleSalesName = Rule::where('rule_name', 'API AM Name Master Data')->first();
        $url_SalesName = $ruleSalesName->rule_value;

        $responseSales = Http::withToken($token)->get($url_SalesName);
        $resultSales = json_decode($responseSales);
        $dataSales = $resultSales->data;
        // dd($dataSales);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Create Customers Menu';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('customers.create_partnership', compact('dropdowns', 'dataSales'));
    }

    public function store(Request $request)
    {
        // dd ('eak');
        // dd($request->all());
        $request->validate([
            'cid' => 'required',
            'cust_code' => 'required',
            'company_name' => 'required',
            'address' => 'required',
            'pic_name' => 'required',
            'pic_contact' => 'required',
            'pic_email' => 'required',
            'a_end' => 'required',
            'broadband' => 'required',
            'priority' => 'required',
            'net_type' => 'required',
            'net_owner' => 'required',
            'service' => 'required',
            'am_name' => 'required',
            'start_date' => 'required|date',
            'finish_date' => 'required|date|after:start_date',
            'status_cust' => 'required',
            'regional' => 'required_if:broadband,==,YES',
            'city' => 'required_if:broadband,==,YES',
        ]);

        //cek customer dulu cid sudah ada atau belum
        $cekCID = Customer::where('cid', $request->cid)->count();
        if ($cekCID > '0') {
            return redirect('/masters/customers')->with('status', 'Failed Add Customer, Customer Already Exist');
        }

        //simpan ke table customer
        $storeCust = Customer::create([
            'cid' => $request->cid,
            'cid_new' => $request->cid_new,
            'customer_code' => $request->cust_code,
            'company_name' => $request->company_name,
            'address' => $request->address,
            'priority' => $request->priority,
            'company_type' => $request->cust_type,
            'service' => $request->service,
            'broadband' => $request->broadband,
            'pic_name' => $request->pic_name,
            'pic_contact' => $request->pic_contact,
            'pic_email' => $request->pic_email,
            'capacity' => $request->capacity,
            'capacity_international' => $request->capacity_international,
            'a_end' => $request->a_end,
            'b_end' => $request->b_end,
            'network_type' => $request->net_type,
            'network_owner' => $request->net_owner,
            'status_customer' => $request->status_cust,
            'start_date_customer' => $request->start_date,
            'finish_date' => $request->finish_date,
            'partner_cid' => $request->partner_cid,
            'username_ppoe' => $request->username_ppoe,
            'password_ppoe' => $request->password_ppoe,
            'regional' => $request->regional,
            'city' => $request->city,
            'notes' => $request->notes,
            'capacity_mix' => $request->capacity_mix,
            'am_name' => $request->am_name,
        ]);

        // dd($storeCust);

        if ($storeCust) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Success Add Customer';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/masters/customers')->with('status', 'Success Add Customer');
        } else {
            return redirect('/masters/customers')->with('status', 'Failed Add Customer');
        }
    }

    public function storebyPartnership(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'company_name' => 'required',
            'a_end' => 'required',
            'net_type' => 'required',
            'net_owner' => 'required',
            'am_name' => 'required',
            'service' => 'required'
        ]);

        //simpan ke table customer
        $storeCust = Customer::create([
            'company_name' => $request->company_name,
            'a_end' => $request->a_end,
            'b_end' => $request->b_end,
            'service' => $request->service,
            'network_type' => $request->net_type,
            'network_owner' => $request->net_owner,
            'capacity' => $request->capacity,
            'capacity_international' => $request->capacity_international,
            'capacity_mix' => $request->capacity_mix,
            'am_name' => $request->am_name,
        ]);

        if ($storeCust) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Success Add Customer';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/masters/customers')->with('status', 'Success Add Customer');
        } else {
            return redirect('/masters/customers')->with('status', 'Failed Add Customer');
        }
    }

    public function show($id)
    {
        //dd($id);
        $id = base64_decode($id);
        //search customer by id
        $customer = Customer::where('id', $id)->first();

        if ($customer) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'View Detail Customer Data';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);
            return view('customers.detail', compact('customer'));
        } else {
            return redirect('/masters/customers')->with('status', 'Customer Not Found');
        }
    }

    public function edit(Request $request, $id)
    {
        //dd('hai edit');
        $dropdowns = array();

        $dropdowns['network type'] = DB::table('dropdowns')
            ->where('category', '=', 'network type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['service'] = DB::table('dropdowns')
            ->where('category', '=', 'service')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['regional'] = DB::table('dropdowns')
            ->where('category', '=', 'regional')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['status customer'] = DB::table('dropdowns')
            ->where('category', '=', 'status customer')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['priority'] = DB::table('dropdowns')
            ->where('category', '=', 'priority')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['broadband'] = DB::table('dropdowns')
            ->where('category', '=', 'broadband')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['Customer Type'] = DB::table('dropdowns')
            ->where('category', '=', 'Customer Type')
            ->orderBy('name_value', 'asc')
            ->get();

        //ambil nama sales dari api sales
        $ruleAuthMstData = Rule::where('rule_name', 'API Get Token Master Data')->first();
        $url_AuthMstData = $ruleAuthMstData->rule_value;

        $ruleEmailMstData = Rule::where('rule_name', 'Username API Master Data')->first();
        $emailMstData = $ruleEmailMstData->rule_value;

        $rulePasswordMstData = Rule::where('rule_name', 'Password API Master Data')->first();
        $passwordMstData = $rulePasswordMstData->rule_value;

        $response = Http::post($url_AuthMstData,[
            'email'     => $emailMstData,
            'password'  => $passwordMstData,
        ]);
        $result = json_decode($response);
        $token = $result->data->token;
        // dd($token);

        //ambil data nama sales
        $ruleSalesName = Rule::where('rule_name', 'API AM Name Master Data')->first();
        $url_SalesName = $ruleSalesName->rule_value;

        $responseSales = Http::withToken($token)->get($url_SalesName);
        $resultSales = json_decode($responseSales);
        $dataSales = $resultSales->data;
        // dd($dataSales);
        
        $id = base64_decode($id);

        //search customer by id
        $customer = Customer::where('id', $id)->first();

        if ($customer) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'View Edit Customer Data';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return view('customers.edit', compact('customer', 'dropdowns', 'dataSales'));
        } else {
            return redirect('/masters/customers')->with('status', 'Customer Not Found');
        }
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $request->validate([
            'cid' => 'required',
            'cust_code' => 'required',
            'company_name' => 'required',
            'address' => 'required',
            'pic_name' => 'required',
            'pic_contact' => 'required',
            'pic_email' => 'required',
            'a_end' => 'required',
            'broadband' => 'required',
            'priority' => 'required',
            'net_type' => 'required',
            'net_owner' => 'required',
            'service' => 'required',
            'start_date' => 'required|date',
            'finish_date' => 'required|date|after:start_date',
            'status_cust' => 'required'
        ]);

        //update ke table customer
        $updateCust = Customer::where('id', $id)
            ->update([
                'cid' => $request->cid,
                'cid_new' => $request->cid_new,
                'customer_code' => $request->cust_code,
                'company_name' => $request->company_name,
                'address' => $request->address,
                'priority' => $request->priority,
                'company_type' => $request->cust_type,
                'service' => $request->service,
                'broadband' => $request->broadband,
                'pic_name' => $request->pic_name,
                'pic_contact' => $request->pic_contact,
                'pic_email' => $request->pic_email,
                'capacity' => $request->capacity,
                'capacity_international' => $request->capacity_international,
                'capacity_mix' => $request->capacity_mix,
                'a_end' => $request->a_end,
                'b_end' => $request->b_end,
                'am_name' => $request->am_name,
                'network_type' => $request->net_type,
                'network_owner' => $request->net_owner,
                'status_customer' => $request->status_cust,
                'start_date_customer' => $request->start_date,
                'finish_date' => $request->finish_date,
                'partner_cid' => $request->partner_cid,
                'username_ppoe' => $request->username_ppoe,
                'password_ppoe' => $request->password_ppoe,
                'regional' => $request->regional,
                'city' => $request->city,
                'notes' => $request->notes
            ]);

        if ($updateCust) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Success Update Customer Data';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/masters/customers')->with('status', 'Success Update Customer');
        } else {
            return redirect('/masters/customers')->with('status', 'Failed Update Customer');
        }
    }

    public function destroy($id)
    {
        //
    }
}