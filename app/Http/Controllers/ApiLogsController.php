<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LogApi;
use Illuminate\Support\Carbon;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class ApiLogsController extends Controller
{
    use AuditLogsTrait;
    public function index(Request $request)
    {
        $now=Carbon::now();
        $apilogs=LogApi::whereMonth('created_at',$now)
        ->whereYear('created_at',$now)
        ->orderBy('id','desc')
        ->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View API Logs';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('api_log.index',compact('apilogs'));
    }
}
