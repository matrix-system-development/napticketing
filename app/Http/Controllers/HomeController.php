<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Models\Selfcare;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    public function index()
    {
        $now=Carbon::now();

        $total=Ticket::all();
        $count_total=count($total);

        $new=Ticket::whereDate('created_date', Carbon::today())
        ->where('status','<>','4')
        ->get();
        $count_new=count($new);

        $progress=Ticket::where('status', '1')->get();
        $count_progress=count($progress);

        $hold=Ticket::where('status', '3')->get();
        $count_hold=count($hold);

        $over_sla=Ticket::where('status', '4')
        ->where('ticket_type', 'External')
        ->whereRaw('closed_date > due_date') //kalau membandingkan 2 kolom table pakai whereRaw
        ->get();
        $count_over=count($over_sla);

        $closed=Ticket::where('status', '4')->get();
        $count_closed=count($closed);

        $selfcareOpen=Selfcare::where('status_request', '0')->count();

        return view('home',compact('selfcareOpen','count_total','count_new','count_progress','count_hold','count_closed','count_over'));
    }
}
