<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\Dropdown;
use App\Models\Log;
use App\Models\LogTicket;
use App\Models\Rule;
use App\Models\User;
use App\Models\Department;
use App\Models\MappingIncident;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Mail;
use App\Mail\InternalMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TicketExport;
use App\Models\Employee;

class TicketIntController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dropdowns = array();

        $dropdowns['dept'] = DB::table("department")
            ->orderBy('department_name', 'asc')
            ->get();

        $mappings = MappingIncident::where("id_dropdown", "67")
            ->orderBy('name_incident', 'asc')
            ->get();

        //dd($dropdowns);

        return view('ticket_internal.create', compact('mappings', 'dropdowns'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'sub_category' => 'required',
            'message' => 'required',
            'department' => 'required',
            'doc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc2' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc3' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc4' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc5' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        //checkbox for_ca
        $for_ca = $request->input('for_ca') == '1' ? 1 : 0;

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $ticket_status = '1';
        $hold_status = '0';

        //mendapatkan duedate
        // $rule_query=Rule::where('rule_name', 'due date')->get();
        // $due_hours=$rule_query[0]->rule_value;
        // $duedate=Carbon::parse($created_date)->addHours($due_hours);
        //end mendapatkan duedate

        //insert into table tickets
        $ticket_create = Ticket::create([
            'ticket_type' => 'Internal',
            'cid' => $request->cid,
            'multiple_cid' => $request->multiple,
            'priority' => $request->priority,
            'status_complain' => $request->status_complain,
            'category' => 'Internal',
            'type_incident' => $request->sub_category,
            'created_by' => $created_by,
            'created_date' => $created_date,
            'status' => $ticket_status,
            'remarks' => $request->message,
            'due_date' => '',
            'hold_status' => $hold_status,
        ]);
        //end insert into table tickets

        if ($ticket_create) {
            $id = $ticket_create->id;

            //upload file
            if ($request->file('doc1')) {
                $extension1 = $request->file('doc1')->getClientOriginalExtension();
                $doc_name1 = $id . '_a.' . $extension1;
                $store1 = $request->file('doc1')->storeAs('attachment_tickets', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            if ($request->file('doc2')) {
                $extension2 = $request->file('doc2')->getClientOriginalExtension();
                $doc_name2 = $id . '_b.' . $extension2;
                $store2 = $request->file('doc2')->storeAs('attachment_tickets', $doc_name2);
            } else {
                $doc_name2 = "";
            }

            if ($request->file('doc3')) {
                $extension3 = $request->file('doc3')->getClientOriginalExtension();
                $doc_name3 = $id . '_c.' . $extension3;
                $store3 = $request->file('doc3')->storeAs('attachment_tickets', $doc_name3);
            } else {
                $doc_name3 = "";
            }

            if ($request->file('doc4')) {
                $extension4 = $request->file('doc4')->getClientOriginalExtension();
                $doc_name4 = $id . '_d.' . $extension4;
                $store4 = $request->file('doc4')->storeAs('attachment_tickets', $doc_name4);
            } else {
                $doc_name4 = "";
            }

            if ($request->file('doc5')) {
                $extension5 = $request->file('doc5')->getClientOriginalExtension();
                $doc_name5 = $id . '_e.' . $extension5;
                $store5 = $request->file('doc5')->storeAs('attachment_tickets', $doc_name5);
            } else {
                $doc_name5 = "";
            }

            $insert_filename = Ticket::where('id', $id)
                ->update([
                    'file_1' => $doc_name1,
                    'file_2' => $doc_name2,
                    'file_3' => $doc_name3,
                    'file_4' => $doc_name4,
                    'file_5' => $doc_name5
                ]);
            //end upload file

            //nomor otomatis

            //format tanggal
            $period_ticket = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = Ticket::whereDay('created_date', $created_date)
                ->whereMonth('created_date', $created_date)
                ->whereYear('created_date', $created_date)
                ->where('ticket_type', 'Internal')
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noticket = "I/" . $period_ticket . "/" . $lastno;

            $create_noticket = Ticket::where('id', $id)
                ->update([
                    'no_urut' => $no_urut,
                    'no_ticket' => $noticket
                ]);
            //end nomor otomatis

            //insert log success create ticket
            $desc_success_create = 'Success Created Ticket';

            $log_history = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_create,
                'message'   => $request->message
            ]);
            //insert log success create ticket

            //insert log success assign ticket
            $desc_success_assign = 'Success Assign Ticket';
            $assign_dept = Department::where('department_id', $request->department)
                ->get();
            $message_assign = 'Assign To: ' . $assign_dept[0]->department_name;

            // dd($assign_dept[0]->department_name);
            $log_assign = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $id,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $request->department,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status,
                'for_ca' => $for_ca
            ]);

            //end insert logs_ticket (log assign)

            if ($create_noticket && $log_history && $log_ticket &&  $log_assign) {

                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Ticket Internal';

                //dd($location);
                $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

                //initial InternalMail
                $subcategory = Ticket::select('name_incident')
                ->where('type_incident', $request->sub_category)
                ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
                ->first()->name_incident;

                $created = User::select('department_name','department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();
                
                $assign = Department::select('department_name','email') // tinggal ganti kalo udah ada email department
                ->where('department_id', $request->department)
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();

                if (env('APP_ENV') != 'production') {
                    $createdMail = 'hardy.prabowo@napinfo.co.id';
                    $createddept = $created->department_name;
                    $assignMail = 'enquity.ekayekti@napinfo.co.id';
                    $assigndept = $assign->department_name;
                }
                else {
                    $createdMail = $created->deptmail;
                    $createddept = $created->department_name;
                    $assignMail = $assign->email;
                    $assigndept = $assign->department_name;
                }

                $data = [
                    'status_ticket' => 'NEW TICKET',
                    'greeting'  => 'Please help this new ticket with detail:', 
                    'no_ticket' => $noticket,
                    'cust_name' => $request->company_name,
                    'category' => 'Internal',
                    'subcategory' => $subcategory,
                    'status_complain' => $request->status_complain,
                    'notes' => $request->message,
                    'created_by' => $createddept,
                    'assign_to' => $assigndept,
                    'receipt' => [
                        'createdMail' => $createdMail,
                        'assignMail' => $assignMail,
                    ]
                ];
                // dd($data);
                Mail::to($assignMail)->send(new InternalMail($data));

                return redirect('/tickets')->with('status', 'Success Create Ticket !');
            } else {
                return redirect('/tickets')->with('status', 'Error Create Log !');
            }
        } else {
            return redirect('/tickets')->with('status', 'Error Create Ticket !');
        }

        // pake /tickets karena page ticket external dan internal sama dalam 1 page /tickets
    }

    public function show(Request $request, $id)
    {
        // untuk reassign
        // dd($id);

        $dropdowns['dept'] = DB::table("department")
            ->orderBy('department_name', 'asc')
            ->get();

        // modal, category hold
        $dropdowns['hold'] = DB::table('dropdowns')
            ->where('category', '=', 'Hold')
            ->orderBy('name_value', 'asc')
            ->get();

        // gatau
        $dropdowns['close'] = DB::table('dropdowns')
            ->where('category', '=', 'Close')
            ->orderBy('name_value', 'asc')
            ->get();

        // dropdown outage reason di modal close ticket
        $dropdowns['outage reason'] = DB::table('dropdowns')
            ->where('category', '=', 'Outage Type')
            ->orderBy('name_value', 'asc')
            ->get();

        $tickets_internal = Ticket::where('tickets.id', '=', $id)
            ->where('tickets.status', '!=', '0')
            ->select('*', 'incident.id as id_mapping', 'tickets.id as id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
            ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
            ->get();

        // dd($tickets_internal[0]->cid);
        // dd($tickets_internal);

        $assign_history = LogTicket::where('logs_tickets.id_ticket', '=', $id)
            ->leftJoin('logs', 'logs_tickets.id_log', '=', 'logs.id')
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
            ->orderBy('logs_tickets.assign_date', 'desc')
            ->get(['employee.int_emp_name', 'department.department_name', 'logs.message', 'logs_tickets.assign_date', 'logs_tickets.preclosed_status', 'logs_tickets.preclosed_date']);

        $comment_history = Log::where('id_ticket', $id)
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs.create_by')
            ->leftJoin('department', 'department.department_id', '=', 'employee.int_emp_department')
            ->groupBy('logs.id')
            ->orderBy('logs.created_at', 'desc')
            ->get(['logs.created_at', 'logs.message', 'logs.attachment_1', 'employee.int_emp_name', 'department.department_name', 'logs.description']);
        // dd($comment_history[0]->attachment_1);
        $return = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '1')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $reassign = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '1')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $preclosed = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '1')
            ->where('preclosed_status', '0')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $close_ticket = LogTicket::where('id_ticket', $id)
            ->where('preclosed_status', '0')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $id_login = auth()->user()->id_employee; // ini id login
        $role_user = auth()->user()->role; // ini role user
        $creator_ticket = $tickets_internal[0]->created_by;
        $department_creator_ticket = $tickets_internal[0]->int_emp_department;
        $ticket_status = $tickets_internal[0]->status;
        $id_login_department = auth()->user()->id_department;
        $pic_status = auth()->user()->pic; // ini id login

        if ($ticket_status == '1' && $id_login_department == '1') {
            $buttonHold = '0';
        } else if ($ticket_status == '3') {
            $buttonHold = '0';
        } else {
            $buttonHold = '0';
        }

        // kalo ticket di hold dan yang akses divisi custcare
        if ($ticket_status == '3' && $id_login_department == '1') {
            $buttonUnhold = '1';
        } else {
            $buttonUnhold = '0';
        }
        // dd($buttonUnhold,$ticket_status);

        $check_row_preclose = count($close_ticket);

        // seluruh pic department
        // id_login_department == department_creator_ticket && pic_status == '1'
        // if($creator_ticket == $id_login && $check_row_preclose == '0' && $ticket_status != '4' && $ticket_status != '3') {
        if ($id_login_department == $department_creator_ticket && $check_row_preclose == '0' && $ticket_status != '4' && $ticket_status != '3') {
            $buttonClosed = '1';
        } else {
            $buttonClosed = '0';
        }

        $id_login_department = auth()->user()->id_department;
        $role_user_login = auth()->user()->role;
        $id_login = auth()->user()->id_employee; // ini id login
        $pic_status = auth()->user()->pic; // ini id login
        $check_count_return = count($return);
        // dd($check_count_return);

        if ($check_count_return < 1) {
            $cek_department_return = '0';
        } else {
            $cek_department_return = $return[0]->int_emp_department;
        }

        // dd($id_login_department,$cek_department_assign,$pic_status);
        // if kalo bukan department custcare dan pic bisa return ticket

        if (($id_login_department == $cek_department_return) && ($pic_status == '1' && $return[0]->preclosed_status == '0')) { // kalo di department lain dia cuman orang yang diassign
            $buttonReturn = 1;
        }
        // fungsi else if untuk department custcare dan bukan pic bisa return with case baru 2 divisi misal custcare ke sysdev
        else if (($id_login_department == '1' && $id_login_department == $cek_department_return && $return[0]->preclosed_status == '0')) { // case kalo department = custcare dia tampilin ke seluruh castcare, apakah custcare perlu ada pic staff nya
            $buttonReturn = 2;
        } else {
            $buttonReturn = 0;
        }

        // dd($buttonReturn);

        $check_count_reassign = count($reassign);
        // dd($check_count_reassign);

        if ($check_count_reassign < 1) {
            $cek_department_reassign = '0';
            $cek_preclosed_status = 'NULL';
        } else {
            $cek_department_reassign = $reassign[0]->assign_to_dept;
            $cek_preclosed_status = $reassign[0]->preclosed_status;
        }

        // kalo department yang diassign cocok dengan department user login, orang dari department yang diassign harus pic, dan tombol reassign muncul setelah dipreclosed / job selesai
        // case normal apabila reassign tapi row masih > 1
        if ($id_login_department == $cek_department_reassign && $pic_status == '1' && $cek_preclosed_status == '1' && $ticket_status != '4') {
            $buttonReassign = 1;
        }
        // case kalo row gaada
        else if ($cek_department_reassign == '0' && $cek_preclosed_status == 'NULL') {
            $buttonReassign = 2;
        }
        // case kalo unassigned gimana, buat sekarang belom bisa karena error pas return ticket
        else {
            $buttonReassign = 0;
        }

        $check_count_preclosed = count($preclosed);

        // kalo gaada row 
        if ($check_count_preclosed < 1) {
            $cek_department_preclosed = 'NULL';
        }
        // ada row
        else {
            $cek_department_preclosed = $preclosed[0]->assign_to_dept;
        }

        if ($id_login_department ==  $cek_department_preclosed && $pic_status == '1') {
            $buttonPreclose = 1;
        }
        // kalo misalkan case creator ticket udah return ticket tetapi belum reassign, tombol preclose hilang
        else {
            $buttonPreclose = 0;
        }

        if ($tickets_internal[0]->cid != NULL) {
            $panelCustomer = 1;
        } else {
            $panelCustomer = 0;
        }

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Ticket Internal Detail';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('ticket_internal.show', compact('tickets_internal', 'comment_history', 'dropdowns', 'return', 'preclosed', 'buttonHold', 'buttonUnhold', 'buttonClosed', 'buttonReturn', 'buttonReassign', 'buttonPreclose', 'assign_history', 'panelCustomer'));
    }

    public function reassign(Request $request)
    {
        // dd($request);
        $request->validate([
            'id_ticket' => 'required',
            'department'  => 'required',
            'message'   => 'required'
        ]);

        //checkbox for_ca
        $for_ca = $request->input('for_ca') == '1' ? 1 : 0;

        //cek dulu apakah dept dan staff yg dipilih sama
        $cek_ticket = Ticket::where('tickets.id', $request->id_ticket)
            ->leftJoin('logs_tickets', function ($join) {
                $join->on('logs_tickets.id_ticket', '=', 'tickets.id');
                $join->where('logs_tickets.assign_status', '=', '1');
            })
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->get();
        //dd($cek_ticket);

        if ($request->department == $cek_ticket[0]->assign_to_dept) {
            return redirect('/tickets_Internal/' . $request->id_ticket)->with('status', 'Failed Re-Assign Ticket, Ticket has been assigned to this Department');
        } else {
            $created_by = auth()->user()->id_employee;
            $created_date = Carbon::now();

            //update ticket status
            //cek status ticket dulu kalau posisinya hold maka tidak mengubah status
            if ($request->status_ticket == '3') {
                $ticket_status = '3';
            } else {
                $ticket_status = '1';
            }

            $update_ticket = Ticket::where('id', $request->id_ticket)
                ->update([
                    'status'    => $ticket_status
                ]);

            //insert log success assign ticket
            $desc_success_assign = 'Success Re-Assign Ticket';
            $assign_dept = Department::where('department_id', $request->department)
                ->get();
            $message_assign = 'Assign To: ' . $assign_dept[0]->department_name . ' (' . $request->message . ')';

            $log_assign = log::create([
                'id_ticket' => $request->id_ticket,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //update assign status semua jadi nol berdasarkan id_ticket
            $assign_status = '0';
            LogTicket::where('id_ticket', $request->id_ticket)
                ->update([
                    'assign_status' => $assign_status
                ]);
            //end update assign status nol

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $request->id_ticket,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $request->department,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status,
                'for_ca' => $for_ca
            ]);
            //end insert logs_ticket (log assign)

            if ($log_assign && $log_ticket && $update_ticket) {
                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Reassign Ticket Internal';

                //dd($location);
                $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

                //initial InternalMail
                $dataticket = Ticket::select('name_incident','no_ticket','category','status_complain','customers.company_name as cust_name','assign_by','department.department_name','department_emails.email as deptmail')
                ->where('tickets.id', $request->id_ticket)
                ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
                ->leftJoin('logs_tickets', 'tickets.id', '=', 'logs_tickets.id_ticket')
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('users', 'logs_tickets.assign_by', '=', 'users.id_employee')
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();

                $assign_to = LogTicket::where('id_ticket',$request->id_ticket)
                    ->select('logs_tickets.*','dept_assignby.department_name as department_name_assign_by','dept_assignto.department_name as department_name_assign_to','department_emails.email as deptmail')
                    ->leftJoin('users', 'logs_tickets.assign_by', '=', 'users.id_employee')
                    ->leftJoin('department as dept_assignby', 'users.id_department', '=', 'dept_assignby.department_id')
                    ->leftJoin('department as dept_assignto', 'logs_tickets.assign_to_dept', '=', 'dept_assignto.department_id')
                    ->leftJoin('department_emails', 'department_emails.id_department', '=', 'dept_assignto.department_id')
                    ->orderBy('id','desc')->first();
                
                $created = User::select('department_name','department_emails.email as deptmail')
                    ->where('users.email', $username)
                    ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                    ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                    ->first();
                
                if (env('APP_ENV') != 'production') {
                    $createdMail = 'hardy.prabowo@napinfo.co.id';
                    $createddept = $created->department_name;
                    $assignMail = 'enquity.ekayekti@napinfo.co.id';
                    $assigndept = $dataticket->department_name;
                }
                else {
                    $createdMail = $created->deptmail;
                    $createddept = $created->department_name;
                    $assignMail = $dataticket->email;
                    $assigndept = $dataticket->department_name;
                }

                $data = [
                    'status_ticket' => 'REASSIGN TICKET',
                    'greeting'  => 'Please help this new ticket with detail:', 
                    'no_ticket' => $dataticket->no_ticket,
                    'cust_name' => $dataticket->cust_name,
                    'category' => $dataticket->category,
                    'subcategory' => $dataticket->name_incident,
                    'status_complain' => $dataticket->status_complain,
                    'notes' => $request->message,
                    'created_by' => $createddept,
                    'assign_to' => $assigndept,
                    'receipt' => [
                        'createdMail' => $createdMail,
                        'assignMail' => $assignMail,
                    ]
                ];
                // dd($data);
                Mail::to($assignMail)->send(new InternalMail($data));

                return redirect('/tickets_Internal/' . $request->id_ticket)->with('status', 'Success Re-Assign Ticket !');
            } else {
                return redirect('/tickets_Internal/' . $request->id_ticket)->with('status', 'Failed Re-Assign Ticket !');
            }
        }
    }

    public function returnTicket(Request $request, $id)
    {
        //dd($id);
        $request->validate([
            'message' => 'required',
        ]);

        // dd($id);
        $delete_logticket = LogTicket::destroy('id', $id);


        //di cek dulu kalau yg retur adalah creator seharusnya table logs_ticket bersih
        $cek_begin_assign = LogTicket::where('id_ticket', $request->id_ticket)->get();
        $count_row = count($cek_begin_assign);
        //dd($count_row); 

        if ($count_row >= '1') {
            //cari row terakhir
            $cek_lastlogs_ticket = LogTicket::where('id_ticket', $request->id_ticket)
                ->orderby('id', 'desc')
                ->take(1)
                ->get();

            //dd($cek_lastlogs_ticket);

            $id_last = $cek_lastlogs_ticket[0]->id;

            //update assign_status jadi 1
            $update_status_assign = LogTicket::where('id', $id_last)
                ->update([
                    'assign_status' => '1',
                    'preclosed_status' => '0'
                ]);
        }

        //insert log success return ticket
        $desc_success_create = 'Success Return Ticket';
        $created_by = auth()->user()->id_employee;

        $log_history = log::create([
            'id_ticket' => $request->id_ticket,
            'create_by' => $created_by,
            'description' => $desc_success_create,
            'message'   => $request->message
        ]);
        //insert log success create ticket

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Return Ticket Internal';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return redirect('/tickets_Internal/' . $request->id_ticket)->with('status', 'Success Return Ticket !');
    }

    public function holdTicket(Request $request, $id)
    {
        // dd($request,$id);
        $request->validate([
            'hold_date'  => 'required|date|after_or_equal:today',
            'hold_time' => 'required',
            'message'   => 'required'
        ]);

        // dd($request,$id);

        $created_by = auth()->user()->id_employee;
        $created_date = Carbon::now();
        $hold_date = $request->hold_date . " " . $request->hold_time;
        //dd($hold_date);

        //update ticket status & hold status
        $ticket_status = '3';
        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'hold_by' => $created_by,
                'hold_created_date' => $created_date,
                'hold_date' => $hold_date,
                'hold_category' => $request->category_hold,
                'hold_message' => $request->message,
                'unhold_by' => '',
                'unhold_date' => 'null'
            ]);

        //insert log success hold ticket
        $desc_success_hold = 'Success Hold Ticket';

        $log_hold = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_hold,
            'message'   => $request->message
        ]);
        //insert log success hold ticket

        if ($update_ticket && $log_hold) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Hold Ticket Internal';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/tickets_Internal/' . $id)->with('status', 'Success Hold Ticket !');
        } else {
            return redirect('/tickets_Internal/' . $id)->with('status', 'Failed Hold Ticket !');
        }
    }

    public function unholdTicket(Request $request, $id)
    {
        $created_by = auth()->user()->id_employee;
        $created_date = Carbon::now();
        //dd($hold_date);

        //update ticket status & unhold status
        $ticket_status = '1';
        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'unhold_by' => $created_by,
                'unhold_date' => $created_date
            ]);

        //insert log success unhold ticket
        $desc_success_unhold = 'Success Unhold Ticket';
        $message_unhold = 'Unhold By: ' . auth()->user()->prefix_name;
        $log_unhold = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_unhold,
            'message' => $message_unhold,
        ]);
        //insert log success unhold ticket

        if ($update_ticket && $log_unhold) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Unhold Ticket Internal';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/tickets_Internal/' . $id)->with('status', 'Success unhold Ticket !');
        } else {
            return redirect('/tickets_Internal/' . $id)->with('status', 'Failed unhold Ticket !');
        }
    }

    public function preclosed(Request $request, $id)
    {
        // dd($id,$request);
        $request->validate([
            'message' => 'required',
        ]);

        $employee = Employee::where('int_emp_id', $request->user)->get();

        $dept = $employee[0]->int_emp_department;

        $log_ticket = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '=', '1')
            ->where('assign_to_dept', $dept)
            ->get();

        // dd($log_ticket);

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $created_name = auth()->user()->name;

        $update_log_ticket = LogTicket::where('id', $log_ticket[0]->id)
            ->update([
                'preclosed_status' => '1',
                'preclosed_date' => $created_date,
                'preclosed_message' => $request->message,
            ]);

        $log_close = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => 'Preclosed by: ' . $created_name,
            'message'   => $request->message
        ]);

        // dd($update_log_ticket);
        if ($update_log_ticket) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Preclose Ticket Internal';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            //initial InternalMail
            $dataticket = Ticket::select('name_incident','no_ticket','category','status_complain','customers.company_name as cust_name','assign_by','department.department_name','department_emails.email as deptmail')
            ->where('tickets.id', $id)
            ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
            ->leftJoin('logs_tickets', 'tickets.id', '=', 'logs_tickets.id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('users', 'logs_tickets.assign_by', '=', 'users.id_employee')
            ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
            ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            ->first();
            
            $created = User::select('department_name','department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();

            if (env('APP_ENV') != 'production') {
                $createdMail = 'hardy.prabowo@napinfo.co.id';
                $createddept = $created->department_name;
                $assignMail = 'enquity.ekayekti@napinfo.co.id';
                $assigndept = $dataticket->department_name;
            }
            else {
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;
                $assignMail = $dataticket->email;
                $assigndept = $dataticket->department_name;
            }

            $data = [
                'status_ticket' => 'PRECLOSED TICKET',
                'greeting'  => 'We have already preclosed this ticket with detail:',
                'no_ticket' => $dataticket->no_ticket,
                'cust_name' => $dataticket->cust_name,
                'category' => $dataticket->category,
                'subcategory' => $dataticket->name_incident,
                'status_complain' => $dataticket->status_complain,
                'notes' => $request->message,
                'created_by' => $createddept,
                'assign_to' => $assigndept,
                'receipt' => [
                    'createdMail' => $createdMail,
                    'assignMail' => $assignMail,
                ]
            ];
            // dd($data);
            Mail::to($assignMail)->send(new InternalMail($data));


            return redirect('/tickets_Internal/' . $id)->with('status', 'Success Pre Closed Ticket !');
        } else {
            return redirect('/tickets_Internal/' . $id)->with('status', 'Failed Pre Closed Ticket !');
        }
    }

    public function close(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            //'cid'   => 'required',
            'ticket_date'   => 'required',
            'finish_date' => 'required|date|before_or_equal:today',
            'finish_time' => 'required',
            'message'   => 'required'
        ]);

        //dd($outage_reason);
        $created_by = auth()->user()->id_employee;
        $created_date = $request->ticket_date;
        $closed_date = Carbon::now();
        $resolved_date = $request->finish_date . " " . $request->finish_time;

        //dd($start_date);

        //update ticket status & hold status
        $ticket_status = '4';

        //duration
        $start = strtotime($created_date);
        $end = strtotime($resolved_date);
        $hold = strtotime($request->hold_date);
        $unhold = strtotime($request->unhold_date);
        $duration = ($end - $start) - ($unhold - $hold);

        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        //end duration

        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'closed_by' => $created_by,
                'closed_date' => $closed_date,
                'start_date' => $created_date,
                'resolved_date' => $resolved_date,
                'closed_message' => $request->message,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
            ]);

        //insert log success hold ticket
        $desc_success_close = 'Success Close Ticket';

        $log_close = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_close,
            'message'   => $request->message
        ]);
        //insert log success hold ticket

        if ($update_ticket && $log_close) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Close Ticket Internal';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            //initial InternalMail
            $dataticket = Ticket::select('name_incident','no_ticket','category','status_complain','customers.company_name as cust_name','assign_to_dept','department.department_name','department_emails.email as deptmail')
            ->where('tickets.id', $id)
            ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
            ->leftJoin('logs_tickets', 'tickets.id', '=', 'logs_tickets.id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            ->first();
            
            $created = User::select('department_name','department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();
                
            if (env('APP_ENV') != 'production') {
                $createdMail = 'hardy.prabowo@napinfo.co.id';
                $createddept = $created->department_name;
                $assignMail = 'enquity.ekayekti@napinfo.co.id';
                $assigndept = $dataticket->department_name;
            }
            else {
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;
                $assignMail = $dataticket->email;
                $assigndept = $dataticket->department_name;
            }

            //ambil cust name
            if($dataticket->cid == ''){
                $custname = '';
            }
            else{
                $custname = $dataticket->cust_name;
            }

            $data = [
                'status_ticket' => 'CLOSED TICKET',
                'greeting'  => 'Ticket with this detail has been closed', 
                'no_ticket' => $dataticket->no_ticket,
                'cust_name' => $custname,
                'category' => $dataticket->category,
                'subcategory' => $dataticket->name_incident,
                'status_complain' => $dataticket->status_complain,
                'notes' => $request->message,
                'created_by' => $createddept,
                'assign_to' => $assigndept,
                'receipt' => [
                    'createdMail' => $createdMail,
                    'assignMail' => $assignMail,
                ]
            ];
            // dd($data);
            Mail::to($assignMail)->send(new InternalMail($data));

            return redirect('/tickets_Internal/' . $id)->with('status', 'Success Closed Ticket !');
        } else {
            return redirect('/tickets_Internal/' . $id)->with('status', 'Failed Closed Ticket !');
        }
    }

    public function addComment(Request $request)
    {
        $request->validate([
            'id_ticket' => 'required',
            'message_comment'  => 'required',
            'attc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        $description_comment = 'Success Create Comment';

        $comment_creator = auth()->user()->id_employee;

        // ' nama coloumn table'
        $comment_create = Log::create([
            'id_ticket' => $request->id_ticket,
            'create_by' => $comment_creator,
            'message' => $request->message_comment,
            'description' => $description_comment
        ]);

        // dd($comment_create);

        if ($comment_create) {
            // generate id comment
            $id_comment = $comment_create->id;
            // dd($id_comment);

            //upload file


            //upload file
            if ($request->file('attc1')) {
                $extension1 = $request->file('attc1')->getClientOriginalExtension();
                $doc_name1 = $id_comment . '_comment.' . $extension1;
                $store1 = $request->file('attc1')->storeAs('attachment_comment', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            // 'nama coloumn table' <- insert attachment ke table logs
            $insert_log = Log::where('id', $id_comment)
                ->update([
                    'attachment_1' => $doc_name1
                ]);

            // $date_submit=$comment_create->created_at;
            // $created_date=Carbon::now();

            // dd($date_submit - $created_date);

            if ($comment_create && $insert_log != NULL || $comment_create && $insert_log == NULL) {
                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Comment';

                //dd($location);
                $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

                return redirect('/tickets_Internal/' . $comment_create->id_ticket)->with('status', 'Success Create Comment !');
            } else {
                return redirect('/tickets_Internal/' . $comment_create->id_ticket)->with('status', 'Fail Create Comment !');
            }
        } else {
            return redirect('/tickets_Internal/' . $comment_create->id_ticket)->with('status', 'Fail Create Comment !');
        }
    }

    public function download(Request $request, $id)
    {
        $pathToFile_1 = storage_path('app/attachment_tickets/' . $id);
        // $pathToFile_2 = storage_path('app/attachment_tickets/' .$ticket->file_2);
        // $pathToFile_3 = storage_path('app/attachment_tickets/' .$ticket->file_3);
        // $pathToFile_4 = storage_path('app/attachment_tickets/' .$ticket->file_4);
        // $pathToFile_5 = storage_path('app/attachment_tickets/' .$ticket->file_5);
        // return response()->download([$pathToFile_1, $pathToFile_2, $pathToFile_3, $pathToFile_4, $pathToFile_5]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return response()->download($pathToFile_1);
    }

    public function downloadFileComment(Request $request, $id)
    {
        $pathToFileComment_1 = storage_path('app/attachment_comment/' . $id);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File Comment';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return response()->download($pathToFileComment_1);
    }

    public function ViewFile(Request $request, $topology_file, $cust)
    {
        $file = base64_decode($topology_file);
        $customer = base64_decode($cust);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View topology File';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('topology.view', compact('file', 'customer'));
    }
}
