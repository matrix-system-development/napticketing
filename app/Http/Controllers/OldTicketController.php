<?php

namespace App\Http\Controllers;

use App\Models\OldTicket;
use Illuminate\Http\Request;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class OldTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        $old_tickets=OldTicket::all();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Ticket History';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('OldTickets.index',compact('old_tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OldTicket  $oldTicket
     * @return \Illuminate\Http\Response
     */
    public function show(OldTicket $oldTicket)
    {
        return view('OldTickets.show',compact('oldTicket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OldTicket  $oldTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(OldTicket $oldTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OldTicket  $oldTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OldTicket $oldTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OldTicket  $oldTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(OldTicket $oldTicket)
    {
        //
    }
}
