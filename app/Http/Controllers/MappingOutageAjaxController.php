<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dropdown;

class MappingOutageAjaxController extends Controller
{
    public function selectOutageAjax($slr_id){

        $query=DB::table("mapping_incident")
        ->leftJoin('dropdowns','dropdowns.id','=','mapping_incident.id_dropdown')
        ->where("dropdowns.name_value","=",$slr_id)
        ->orderBy('name_incident','asc')
        ->get();
        //dd($query);
        return json_encode($query);
    }
}
