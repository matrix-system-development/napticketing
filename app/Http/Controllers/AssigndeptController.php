<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssigndeptController extends Controller
{
    public function selectDeptAjax($dept_id){
        $emp=DB::table("employee")
        ->where("int_emp_department","=",$dept_id)
        ->orderBy('int_emp_name','asc')
        ->get();
        return json_encode($emp);
    }
}
