<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        $users = User::leftjoin('department', 'users.id_department', '=', 'department.department_id')
            ->get();

        $dropdowns = array();
        $dropdowns['role'] = DB::table('dropdowns')
            ->where('category', '=', 'Role')
            ->orderBy('name_value', 'asc')
            ->get();

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Master User';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('user.index', compact('users', 'dropdowns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email1' => 'required|unique:users,email'
        ]);

        $employee = Employee::where('int_emp_email_nap', $request->email1)
            ->first();
        //dd($employee->int_emp_department);
        if ($employee->int_emp_department == "1") {
            $pic = "1";
        } else {
            $pic = "0";
        }
        //dd($pic);
        $users = User::create([
            'id_employee' => $employee->int_emp_id,
            'id_department' => $employee->int_emp_department,
            'functional' => $employee->int_emp_position,
            'name' => $employee->int_emp_name,
            'prefix_name' => $employee->int_emp_pref_name,
            'email' => $request->email1,
            'password' => Hash::make('-'),
            'active_status' => '1',
            'pic' => $pic,
        ]);

        if ($users) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Create Master User';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/masters/users')->with('status', 'Success Create User');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ActiveUser(Request $request, $id)
    {
        $update_user = User::where('id', $id)
            ->update([
                'active_status' => '1',
            ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Activate Master User';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return redirect('/masters/users')->with('status', 'User Has Been Activated');
    }

    public function ActiveUserCA(Request $request, $id)
    {
        // dd('hai');
        $update_user = User::where('id', $id)
            ->update([
                'is_ca' => '1',
            ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Activate Access Custommer Assistant';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return redirect('/masters/users')->with('status', 'Custommer Assistant Has Been Activated');
    }

    public function RevokeUser(Request $request, $id)
    {
        $update_user = User::where('id', $id)
            ->update([
                'active_status' => '0',
                'revoked_at' => now(),
            ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Revoke Master User';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return redirect('/masters/users')->with('status', 'User Has Been Revoked');
    }

    public function RevokeUserCA(Request $request, $id)
    {
        $update_user = User::where('id', $id)
            ->update([
                'is_ca' => '0',
            ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Revoke Access Custommer Assistant';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return redirect('/masters/users')->with('status', 'Custommer Assistant Has Been Revoked');
    }

    public function UpdateRole(Request $request)
    {
        $request->validate([
            'email' => 'exists:users,email',
            'role' => 'required'
        ]);

        $update_role = User::where('email', $request->email)
            ->update([
                'role' => $request->role
            ]);

        //dd($update_role);

        if ($update_role) {

            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Update Role User';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/masters/users')->with('status', 'Role Has Been Given');
        } else {
            return redirect('/masters/users')->with('status', 'Failed Create Role');
        }
    }
    public function UpdatePIC(Request $request)
    {
        $request->validate([
            'pic_status' => 'required'
        ]);

        $update_user = User::where('email', $request->email)
            ->update([
                'pic' => $request->pic_status,
            ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Update PIC User';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return redirect('/masters/users')->with('status', 'Success Set PIC');
    }
}
