<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Rule;
use App\Models\LogApi;
use App\Models\LogToken;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Client\ConnectionException;

class CheckONTController extends Controller
{
    use AuditLogsTrait;
    public function index()
    {
        $dropdowns['command name']=DB::table('dropdowns')
        ->where('category','=','Command Name')
        ->orderBy('name_value','asc')
        ->get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Check ONT';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('ont.index',compact('dropdowns'));
    }

    public function checkONT(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'cid'=>'required',
            'command' => 'required',
        ]);

        //validation customers table
        $checkCust=Customer::where('partner_cid',$request->cid)->count();
        if($checkCust=='0'){
            $status='Customer Not Found at Internal Customers Data';
            return redirect('/ont-fs')->with('info',$status);
        }

        //login fs
        //call url,username,password for api fs
        $rule=Rule::where('rule_name','FS-ONT Login')->first();
        $url_fsont_login=$rule->rule_value;

        $rule_username=Rule::where('rule_name','username fs ONT')->first();
        $username_fs=$rule_username->rule_value;

        $rule_password=Rule::where('rule_name','password fs ONT')->first();
        $password_fs=$rule_password->rule_value;

        //login post to FS
        try {
            $response = Http::withBasicAuth($username_fs, $password_fs)->post($url_fsont_login);
            $error=false;
        } catch (ConnectionException $e) {
            $error=true;
        }

        if ($error == 'true') {
            return redirect('/ont-fs')->with('info',"Failed to Connect, Please Check Your VPN Connection");
        }

        $api_name='FS ONT Login';
        $token=$response['token'];
        $description_log='Success Login';
        //dd($token);

        //insert log token
        $log_token=LogToken::create([
            'access_token' => $token,
            'partner' => 'FiberStar-ONT',
        ]);

        //insert log api
        $log_api=LogApi::create([
            'api_name' => $api_name,
            'description' => $description_log,
            'conn_status' => '1',
        ]);
        //end login fs

        //command fs
        $rule=Rule::where('rule_name','FS-ONT Command')->first();
        $url_fsont_command=$rule->rule_value;

        $response_check = Http::withToken($token)
        ->post($url_fsont_command,[
            'customerID' => $request->cid,
            'commandName' => $request->command,
        ]);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Submit Check ONT';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        //dd($response_check['result']);
        if(isset($response_check['result']))
        {
            if($request->command <> 'ONTReboot'){
                foreach ($response_check['result'] as $data_response) {
                    $circuit_id=$data_response['circuit_id'];
                    $command=$data_response['command'];
                    $command_return=$data_response['command_return'];
                    $status=$data_response['status'];
                    $using=$data_response['using'];
                }
    
                //dd($status);
    
                //insert log api
                $log_api=LogApi::create([
                    'api_name' => 'FS Check ONT',
                    'description' => $status,
                    'conn_status' => '1',
                ]);
    
                return redirect('/ont-fs')
                //->with('status',$status)
                ->with([
                    'command_name' => $request->command,
                    'circuit_id' => $circuit_id,
                    'command' => $command,
                    'command_return' => $command_return,
                    'status' => $status,
                    'using' => $using,
                ]);
            }
            else
            {
                //dd($response_check['result']);
                foreach ($response_check['result'] as $data_response) {
                    $status=$data_response['status'];
                }
    
                //dd($status);
    
                //insert log api
                $log_api=LogApi::create([
                    'api_name' => 'FS Check ONT',
                    'description' => $status,
                    'conn_status' => '1',
                ]);
    
                return redirect('/ont-fs')->with([
                    'command_name' => $request->command,
                    'status' => $status
                ]);
            }
        }
        else{
            $status=$response_check['status'];
            //dd($status);
            //insert log api
            $log_api=LogApi::create([
                'api_name' => 'FS Check ONT',
                'description' => $status,
                'conn_status' => '1',
            ]);

            return redirect('/ont-fs')->with('info',$status);
        }
    }
}
