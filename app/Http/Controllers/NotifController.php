<?php

namespace App\Http\Controllers;

use CreateHoldNotif;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class NotifController extends Controller
{
    public function CreateHoldNotif()
    {
        // dd('test');

        Notification::route(MicrosoftTeamsChannel::class, null)
            ->notify(new CreateHoldNotif());
        return redirect('/tickets')->with('status', 'Success Send Notification !');
    }
}
