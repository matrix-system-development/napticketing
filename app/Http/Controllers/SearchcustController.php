<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class SearchcustController extends Controller
{
    public function searchcustomer(Request $request)
    {
        $search=$request->search;
        if($search==''){
            $customers = Customer::orderby('company_name','asc')
            ->select(
                'id',
                'cid',
                'cid_new',
                'customer_code',
                'company_name',
                'priority',
                'company_type',
                'service',
                'pic_name',
                'pic_contact',
                'pic_email',
                'capacity',
                'capacity_international',
                'a_end',
                'b_end',
                'network_type',
                'network_owner',
                'start_date_customer',
                'finish_date',
                'partner_cid',
                'username_ppoe',
                'password_ppoe',
                'regional',
                'city',
                'notes',
                'capacity_mix',
                'am_name')
            ->limit(10)
            ->get();
        }
        else{
            $customers = Customer::orderby('company_name','asc')
            ->select(
                'id',
                'cid',
                'cid_new',
                'customer_code',
                'company_name',
                'priority',
                'company_type',
                'service',
                'pic_name',
                'pic_contact',
                'pic_email',
                'capacity',
                'capacity_international',
                'a_end',
                'b_end',
                'network_type',
                'network_owner',
                'start_date_customer',
                'finish_date',
                'partner_cid',
                'username_ppoe',
                'password_ppoe',
                'regional',
                'city',
                'notes',
                'capacity_mix',
                'am_name')
            ->where('cid', 'like', '%' .$search . '%')
            ->orwhere('company_name', 'like', '%' .$search . '%')
            ->orwhere('cid_new', 'like', '%' .$search . '%')
            ->limit(10)->get();
        }

        // dd($customers);

        $response = array();
        foreach($customers as $customer){
            $response[] = array(
                "value_id"=>$customer->id,
                "value_cid"=>$customer->cid,
                "value_custcode"=>$customer->customer_code,
                "value_compname"=>$customer->company_name,
                "value_priority"=>$customer->priority,
                "value_comptype"=>$customer->company_type,
                "value_service"=>$customer->service,
                "value_picname"=>$customer->pic_name,
                "value_piccontact"=>$customer->pic_contact,
                "value_picemail"=>$customer->pic_email,
                "value_capacity"=>$customer->capacity,
                "value_capacityInt"=>$customer->capacity_international,
                "value_a_end"=>$customer->a_end,
                "value_b_end"=>$customer->b_end,
                "value_nettype"=>$customer->network_type,
                "value_netowner"=>$customer->network_owner,
                "value_startdate"=>$customer->start_date_customer,
                "value_finishdate"=>$customer->finish_date,
                "value_partnercid"=>$customer->partner_cid,
                "value_usernameppoe"=>$customer->username_ppoe,
                "value_passwordppoe"=>$customer->password_ppoe,
                "value_regional"=>$customer->regional,
                "value_city"=>$customer->city,
                "value_cid_new"=>$customer->cid_new,
                "value_notes"=>$customer->notes,
                "value_capacitymix"=>$customer->capacity_mix,
                "value_am"=>$customer->am_name,
                "label"=> 'Cust Name: ' . $customer->company_name . ' | CID: ' . $customer->cid . ' | Service Name: ' . $customer->service . ' | Net Owner: ' . $customer->network_owner . ' | Capacity: '.$customer->capacity . ' | CID New: ' . $customer->cid_new
            );
        }
        return response()->json($response);
    }
}