<?php

namespace App\Http\Controllers;

use Browser;
use App\Models\Log;
use App\Models\Rule;
use App\Models\User;
use App\Models\Ticket;
use App\Models\LogHold;
use App\Models\Dropdown;
use App\Models\Employee;
use App\Models\LogTicket;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Exports\TicketExport;
use App\Traits\AuditLogsTrait;
use Illuminate\Support\Carbon;
use App\Exports\AuditLogExport;
use App\Exports\SelfcareExport;

use App\Exports\TicketFSExport;
use App\Models\MappingIncident;
use App\Exports\LogAssignExport;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Exports\TicketExportSummary;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LogsAssignBulkExport;
use Stevebauman\Location\Facades\Location;
use App\Exports\LogsAssignBulkExportSummary;
use App\Exports\LogsJourneyBulkExport;
use App\Exports\LogsJourneyBulkExportSummary;
use App\Exports\TicketPartnershipExport;

class ExportController extends Controller
{
    use AuditLogsTrait;
    public function rfo(Request $request, $id)
    {
        $ticket = Ticket::where('tickets.id', $id)
            ->leftjoin('customers', 'tickets.cid', '=', 'customers.cid')
            // ->leftjoin('log_holds', 'tickets.id', '=', 'log_holds.id_ticket')
            ->get();
        // dd($ticket);

        $holdtimes = LogHold::where('id_ticket', $id)
            ->select('hold_created_date', 'unhold_date')
            ->orderBy('id_ticket')
            ->get()
            ->toArray();

        if ($ticket[0]->hold_by == '') {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Download RFO';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            $pdf = PDF::loadview(
                'ticket_external.rfo_download',
                [
                    'ticket' => $ticket
                ]
            );
            return $pdf->download($ticket[0]->rfo . '.pdf');
            //return view('ticket_external.rfo_download');
        } else {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Download RFO';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            $pdf = PDF::loadview('ticket_external.rfo_download_hold', [
                'ticket' => $ticket,
                'holdtimes' => $holdtimes,

            ]);
            return $pdf->download($ticket[0]->rfo . '.pdf');
        }
    }

    public function rfoMaintenance(Request $request, $id)
    {
        $ticket = Ticket::where('tickets.id', $id)
            ->leftjoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftjoin('mapping_incident', 'tickets.type_incident', '=', 'mapping_incident.id')
            ->get();

        if ($ticket[0]->hold_by == '') {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Download RFO Maintenance';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            $pdf = PDF::loadview('ticket_maintenance.rfoMaintenance_download', ['ticket' => $ticket]);
            return $pdf->download($ticket[0]->rfo . '.pdf');
            //return view('ticket_external.rfo_download');
        } else {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Download RFO Maintenance';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            $pdf = PDF::loadview('ticket_external.rfo_download_hold', ['ticket' => $ticket]);
            return $pdf->download($ticket[0]->rfo . '.pdf');
        }
    }

    public function exportSummary(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'date_start' => 'required|date',
            'date_finish' => 'required|date|after_or_equal:date_start',
            'report_type' => 'required',
        ]);

        $dept_login = auth()->user()->id_department;

        if ($request->report_type == "summary_type") {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Export Summary Report';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return Excel::download(new TicketExportSummary($request->date_start, $request->date_finish, $request->type,$dept_login), 'Tickets Summary Report.xlsx');
        } 
        elseif ($request->report_type == "logAssign_type") {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Export Log Assign Bulk';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return Excel::download(new LogsAssignBulkExportSummary($request->date_start, $request->date_finish, $request->type,$dept_login), 'Logs Assign Bulk Report.xlsx');
        }
        elseif ($request->report_type == "logComment_type") {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Export Log Journey Bulk';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return Excel::download(new LogsJourneyBulkExportSummary($request->date_start, $request->date_finish, $request->type), 'Logs Journey Bulk Report.xlsx');
        }
    }

    public function export(Request $request, $summary, $category, $from, $until)
    {
        // dd('hai');
        $request->validate([
            'report_type' => 'required',
        ]);

        //untuk dapatin jenis summary
        if ($summary == 'progress') {
            $summary = '1';
        } elseif ($summary == 'unassign') {
            $summary = '2';
        } elseif ($summary == 'hold') {
            $summary = '3';
        } elseif ($summary == 'closed') {
            $summary = '4';
        } elseif ($summary == 'total') {
            $summary = 'all';
        } else {
            $summary = 'all';
        }

        // dd($summary, $category, $from, $until);

        if ($request->report_type == "summary_type") {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Export Summary Report';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return Excel::download(new TicketExport($summary, $from, $until, $category), 'Tickets Summary Report.xlsx');
        } 
        elseif ($request->report_type == "logAssign_type") {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Export Log Assign Bulk';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return Excel::download(new LogsAssignBulkExport($summary, $from, $until, $category), 'Logs Assign Bulk Report.xlsx');
        }
        elseif ($request->report_type == "logComment_type") {
            // dd('hai');
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Export Log Journey Bulk';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return Excel::download(new LogsJourneyBulkExport($summary, $from, $until, $category), 'Logs Journey Bulk Report.xlsx');
        }
    }

    public function exportLogAssign(Request $request)
    {
        // dd($request);
        $request->validate([
            'id_ticket' => 'required',
        ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Export Log Ticket';

        //dd($activity);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return Excel::download(new LogAssignExport($request->id_ticket), 'Tickets Log Report.xlsx');
    }

    public function exportAuditLog(Request $request)
    {
        $request->validate([
            'date_start' => 'required|date',
            'date_finish' => 'required|date|after_or_equal:date_start'
        ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Export Audit Log';

        //dd($activity);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return Excel::download(new AuditLogExport($request->date_start, $request->date_finish), 'Audit Logs.xlsx');
    }
    public function exportFS(Request $request)
    {
        //dd('hai');
        $request->validate([
            'date_start' => 'required|date',
            'date_finish' => 'required|date|after_or_equal:date_start'
        ]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Export Ticket FS';

        //dd($activity);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return Excel::download(new TicketFSExport($request->date_start, $request->date_finish), 'Ticket FS.xlsx');
    }
    public function exportSelfcare(Request $request)
    {
        //dd('hai');
        $request->validate([
            'date_start' => 'required|date',
            'date_finish' => 'required|date|after_or_equal:date_start'
        ]);

        return Excel::download(new SelfcareExport($request->date_start, $request->date_finish), 'Selfcare.xlsx');
    }

    public function exportPartnership(Request $request)
    {
        $request->validate([
            'date_start' => 'required|date',
            'date_finish' => 'required|date|after_or_equal:date_start'
        ]);

        // //Audit Log
        // $username = auth()->user()->email;
        // $ipAddress = $_SERVER['REMOTE_ADDR'];
        // $location = '0';
        // $access_from = Browser::browserName();
        // $activity = 'Export Ticket Partnership';

        // //dd($activity);
        // $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return Excel::download(new TicketPartnershipExport($request->date_start, $request->date_finish), 'Partnership.xlsx');
    }
}
