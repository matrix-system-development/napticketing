<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dropdown;

class MappingResponsibleAjaxController extends Controller
{
    public function selectResponsibleAjax($responsibleID){

        if ($responsibleID == '3rd Party') {
                if(env('APP_ENV')=='production'){
                    $query=DB::table("mapping_incident")
                    ->leftJoin('dropdowns','dropdowns.id','=','mapping_incident.id_dropdown')
                    ->where("id_dropdown","=",102)
                    ->orderBy('name_incident','asc')
                    ->get();
                }
                else{
                    $query=DB::table("mapping_incident")
                    ->leftJoin('dropdowns','dropdowns.id','=','mapping_incident.id_dropdown')
                    ->where("id_dropdown","=",98)
                    ->orderBy('name_incident','asc')
                    ->get();
                }
        }   else {
                $query=DB::table("dropdowns")
                ->where('category','Solved By')
                ->orderBy('name_value','asc')
                ->get();
        }
        
        //dd($query);
        return json_encode($query);
    }
}