<?php

namespace App\Http\Controllers\API;

use App\Models\Ticket;
use App\Models\Log;
use App\Models\LogTicket;
use App\Models\User;
use App\Models\Department;
use App\Models\MappingIncident;
use App\Traits\AuditLogsTrait;
use Browser;
use Illuminate\Support\Facades\Mail;
use App\Mail\InternalMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\ApiBaseController as ApiBaseController;

class ApiTicketIntController extends ApiBaseController
{
    use AuditLogsTrait;
    public function storeTicketInt(Request $request){
        //dd('hai');
        $validator = Validator::make($request->all(), [
            'user_email' => 'email|required',
            'status_complain' => 'required',
            'message' => 'required',
            'doc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc2' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc3' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc4' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc5' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }

        $created_date = Carbon::now();
        $ticket_status = '1';
        $hold_status = '0';

        //flagging for_ca
        $for_ca = '0';

        //cari created By
        $email_created = $request->user_email;
        $q_user = User::where('email',$email_created);
        $user_count = $q_user->count();
        if($user_count == 0){
            $message = "User Not Found in Ticketing Dashboard";
            return $this->sendError('Validation Error', $message);
        }
        else{
            $user = $q_user->first();
            $created_by = $user->id_employee;
        }

        //sub_category
        $q_sub_cat = MappingIncident::where('name_incident',$request->sub_category);
        $sub_count = $q_sub_cat->count(); 
        if($sub_count == 0){
            $message = "Sub Category Not Found";
            return $this->sendError('Validation Error', $message);
        }
        else{
            $sub_cat = $q_sub_cat->first();
            $sub_category = $sub_cat->id;
        }
        //end sub_category

        //Department
        if ($request->sub_category == "Request End Trial") { //kalau Request End Trial di hardcoded ke NOC
            if (env('APP_ENV') != 'production') {
                $department = '19'; //dept Network Operation
            } else {
                $department = '19'; //dept Network Operation
            }
        }
        else{ //selain Request End Trial sesuai request key department
            $department = $request->department;
        }

        DB::beginTransaction();
        try {
            //insert into table tickets
            $ticket_create = Ticket::create([
                'ticket_type' => 'Internal',
                'cid' => '',
                'multiple_cid' => 'no',
                'priority' => '',
                'status_complain' => $request->status_complain,
                'category' => 'Internal',
                'type_incident' => $sub_category,
                'created_by' => $created_by,
                'created_date' => $created_date,
                'status' => $ticket_status,
                'remarks' => $request->message,
                'due_date' => '',
                'hold_status' => $hold_status,
            ]);
            //end insert into table tickets

            $id = $ticket_create->id;

            //upload file
            if ($request->file('doc1')) {
                $extension1 = $request->file('doc1')->getClientOriginalExtension();
                $doc_name1 = $id . '_a.' . $extension1;
                $store1 = $request->file('doc1')->storeAs('attachment_tickets', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            if ($request->file('doc2')) {
                $extension2 = $request->file('doc2')->getClientOriginalExtension();
                $doc_name2 = $id . '_b.' . $extension2;
                $store2 = $request->file('doc2')->storeAs('attachment_tickets', $doc_name2);
            } else {
                $doc_name2 = "";
            }

            if ($request->file('doc3')) {
                $extension3 = $request->file('doc3')->getClientOriginalExtension();
                $doc_name3 = $id . '_c.' . $extension3;
                $store3 = $request->file('doc3')->storeAs('attachment_tickets', $doc_name3);
            } else {
                $doc_name3 = "";
            }

            if ($request->file('doc4')) {
                $extension4 = $request->file('doc4')->getClientOriginalExtension();
                $doc_name4 = $id . '_d.' . $extension4;
                $store4 = $request->file('doc4')->storeAs('attachment_tickets', $doc_name4);
            } else {
                $doc_name4 = "";
            }

            if ($request->file('doc5')) {
                $extension5 = $request->file('doc5')->getClientOriginalExtension();
                $doc_name5 = $id . '_e.' . $extension5;
                $store5 = $request->file('doc5')->storeAs('attachment_tickets', $doc_name5);
            } else {
                $doc_name5 = "";
            }

            $insert_filename = Ticket::where('id', $id)
                ->update([
                    'file_1' => $doc_name1,
                    'file_2' => $doc_name2,
                    'file_3' => $doc_name3,
                    'file_4' => $doc_name4,
                    'file_5' => $doc_name5
                ]);
            //end upload file

            //nomor otomatis
            //format tanggal
            $period_ticket = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = Ticket::whereDay('created_date', $created_date)
                ->whereMonth('created_date', $created_date)
                ->whereYear('created_date', $created_date)
                ->where('ticket_type', 'Internal')
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noticket = "I/" . $period_ticket . "/" . $lastno;

            $create_noticket = Ticket::where('id', $id)
                ->update([
                    'no_urut' => $no_urut,
                    'no_ticket' => $noticket
                ]);
            //end nomor otomatis

            //insert log success create ticket
            $desc_success_create = 'Success Created Ticket';

            $log_history = Log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_create,
                'message'   => $request->message
            ]);
            //insert log success create ticket

            //insert log success assign ticket
            $desc_success_assign = 'Success Assign Ticket';
            $assign_dept = Department::where('department_id', $department)
                ->get();
            $message_assign = 'Assign To: ' . $assign_dept[0]->department_name;

            $log_assign = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $id,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $department,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status,
                'for_ca' => $for_ca
            ]);
            //end insert logs_ticket (log assign)

            //Audit Log
            $username = $request->user_email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Create Ticket Internal';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            //initial InternalMail
            $subcategory = Ticket::select('name_incident')
            ->where('type_incident', $sub_category)
            ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
            ->first()->name_incident;

            $created = User::select('department_name','department_emails.email as deptmail')
            ->where('users.email', $request->user_email)
            ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
            ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            ->first();
            
            $assign = Department::select('department_name','email') // tinggal ganti kalo udah ada email department
            ->where('department_id', $department)
            ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            ->first();
            
            if (env('APP_ENV') != 'production') {
                $createdMail = 'hardy.prabowo@napinfo.co.id';
                $createddept = $created->department_name;
                $assignMail = 'enquity.ekayekti@napinfo.co.id';
                $assigndept = $assign->department_name;
            }
            else {
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;
                $assignMail = $assign->email;
                $assigndept = $assign->department_name;
            }

            $data = [
                'status_ticket' => 'NEW TICKET',
                'greeting'  => 'Please help this new ticket with detail:', 
                'no_ticket' => $noticket,
                'cust_name' => $request->company_name,
                'category' => 'Internal',
                'subcategory' => $subcategory,
                'status_complain' => $request->status_complain,
                'notes' => $request->message,
                'created_by' => $createddept,
                'assign_to' => $assigndept,
                'receipt' => [
                    'createdMail' => $createdMail,
                    'assignMail' => $assignMail,
                ]
            ];

            Mail::to($assignMail)->send(new InternalMail($data));

            DB::commit();

            //response
            $response = [
                'success' => true,
                'message' => "Success create ticket internal",
                'idTicket' => $id,
                'noTicket' => $noticket,
                'createdDate' => $created_date,
            ];

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            //response
            $response = [
                'success' => false,
                'message' => "Failed create ticket internal",
            ];

            return response()->json($response, 200);
        }
    }

    public function closedTicketInt(Request $request){

        $validator = Validator::make($request->all(), [
            'user_email' => 'email|required',
            'id_ticket' => 'required',
            'ticket_date'   => 'required',
            'message'   => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }

        $id = $request->id_ticket;

        //cek id_ticket valid for ticket internal
        $ticket = Ticket::where('id', $id)
        ->where('category', 'Internal')
        ->first();
        
        if (!$ticket) {
            $response = [
                'success' => false,
                'message' => "Invalid ticket ID for internal ticket",
            ];
            return response()->json($response, 202);
        }

        //cek created date sesuai atau tidak
        if($ticket->created_date != $request->ticket_date){
            $response = [
                'success' => false,
                'message' => "Invalid ticket created date",
            ];
            return response()->json($response, 202);
        }

        DB::beginTransaction();
        try {
            //code...
            $created_by = $request->user_email;
            $created_date = $request->ticket_date;
            $closed_date = Carbon::now();
            $resolved_date = Carbon::now();

            //dd($start_date);

            //update ticket status & hold status
            $ticket_status = '4';

            //duration
            $start = strtotime($created_date);
            $end = strtotime($resolved_date);
            $hold = strtotime($request->hold_date);
            $unhold = strtotime($request->unhold_date);
            $duration = ($end - $start) - ($unhold - $hold);

            $total_second = $duration; // 177300 second
            $total_minutes = ($duration / 60); // 2955 minute
            $temp_minutes = ($total_second % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
            $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
            $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
            $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
            //end duration

            $update_ticket = Ticket::where('id', $id)
                ->update([
                    'status' => $ticket_status,
                    'closed_by' => $created_by,
                    'closed_date' => $closed_date,
                    'start_date' => $created_date,
                    'resolved_date' => $resolved_date,
                    'closed_message' => $request->message,
                    'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                ]);

            //insert log success hold ticket
            $desc_success_close = 'Success Close Ticket';

            $log_close = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_close,
                'message'   => $request->message
            ]);

            //insert log success hold ticket
            //Audit Log
            $username = $request->user_email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Close Ticket Internal';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            //initial InternalMail
            $dataticket = Ticket::select('name_incident','tickets.cid','no_ticket','category','status_complain','customers.company_name as cust_name','assign_to_dept','department.department_name','department_emails.email as deptmail')
            ->where('tickets.id', $id)
            ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
            ->leftJoin('logs_tickets', 'tickets.id', '=', 'logs_tickets.id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            ->first();
            
            $created = User::select('department_name','department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();
            
            if (env('APP_ENV') != 'production') {
                $createdMail = 'hardy.prabowo@napinfo.co.id';
                $createddept = $created->department_name;
                $assignMail = 'enquity.ekayekti@napinfo.co.id';
                $assigndept = $dataticket->department_name;
            }
            else {
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;
                $assignMail = $dataticket->email;
                $assigndept = $dataticket->department_name;
            }
            
            //ambil cust name
            if($dataticket->cid == ''){
                $custname = '';
            }
            else{
                $custname = $dataticket->cust_name;
            }

            $data = [
                'status_ticket' => 'CLOSED TICKET',
                'greeting'  => 'Ticket with this detail has been closed', 
                'no_ticket' => $dataticket->no_ticket,
                'cust_name' => $custname,
                'category' => $dataticket->category,
                'subcategory' => $dataticket->name_incident,
                'status_complain' => $dataticket->status_complain,
                'notes' => $request->message,
                'created_by' => $createddept,
                'assign_to' => $assigndept,
                'receipt' => [
                    'createdMail' => $createdMail,
                    'assignMail' => $assignMail,
                ]
            ];

            Mail::to($assignMail)->send(new InternalMail($data));

            DB::commit();

            //response
            $response = [
                'success' => true,
                'message' => "Success closed ticket internal",
                'idTicket' => $id,
                'noTicket' => $dataticket->no_ticket,
            ];

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            $response = [
                'success' => false,
                'message' => "Failed closed ticket internal",
            ];

            return response()->json($response, 200);
        }
    }
}
