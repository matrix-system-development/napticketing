<?php

namespace App\Http\Controllers\API;

use App\Models\Log;
use App\Models\Rule;
use App\Models\Ticket;
use App\Models\Customer;
use App\Models\Selfcare;
use App\Models\LogTicket;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Mail\NotifSelfcareMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\ApiBaseController as ApiBaseController;

class ApiController extends ApiBaseController
{
    public function storeRequest(Request $request)
    {
        if ($request->report_type == 'billing' || $request->report_type == 'Billing') {
            $validator = Validator::make($request->all(), [
                'cust_code' => 'required',
                'cust_email' => 'required|email',
                'report_type' => 'required',
                'report_category' => 'required',
                'report_content' => 'required',
                'first_handling' => 'required',
                'notes' => 'required',
                'billing_period' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'cust_code' => 'required',
                'cust_email' => 'required|email',
                'report_type' => 'required',
                'report_category' => 'required',
                'report_content' => 'required',
                'first_handling' => 'required',
                'notes' => 'required'
            ]);
        }

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }

        //cek customer exist
        $cekCustExist = Customer::where('customer_code', $request->cust_code)->count();

        if ($cekCustExist > 0) {
            //dd('hai');
            $created_date = Carbon::now();

            //nomor otomatis

            //format tanggal
            $period_request = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = Selfcare::whereDay('created_at', $created_date)
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noRequest = "RQ/" . $period_request . "/" . $lastno;

            //end nomor otomatis

            $storeRequest = Selfcare::create([
                'no_urut' => $no_urut,
                'no_request' => $noRequest,
                'no_ticket' => '',
                'cust_code' => $request->cust_code,
                'cust_email' => $request->cust_email,
                'report_type' => $request->report_type,
                'report_category' => $request->report_category,
                'report_content' => $request->report_content,
                'first_handling' => $request->first_handling,
                'attachment' => '',
                'notes' => $request->notes,
                'billing_period' => $request->billing_period,
                'attachment' => $request->attachment,
                'status_request' => '0',
            ]);

            if ($storeRequest) {
                $message = 'Success submit request';
                $request_no = $storeRequest->no_request;
                $request_status = 'OPEN';

                //ambil recipient
                $rule = Rule::where('rule_name', 'Email Notif Selfcare')->first();
                $recipient = $rule->rule_value;

                //Nama Customer
                $customer = Customer::where('customer_code', $request->cust_code)->first();
                $customer_name = $customer->company_name;

                $details = [
                    'no_request' => $storeRequest->no_request,
                    'cust_code' => $request->cust_code,
                    'cust_name' => $customer_name,
                    'report_type' => $request->report_type,
                    'report_category' => $request->report_category,
                    'report_content' => $request->report_content,
                    'first_handling' => $request->first_handling,
                ];

                $sent = Mail::to($recipient)
                    ->send(new NotifSelfcareMail($details));
            } else {
                $message = 'Internal error, failed submit request';
                $request_no = '';
                $request_status = '';
            }

            $response = [
                'success' => true,
                'message' => $message,
                'request number' => $request_no,
                'status' => $request_status,
            ];

            return response()->json($response, 200);
        } else {

            $message = "Data Customer Not Found";

            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
        }
    }

    public function checkRequest($no_req)
    {
        //return $no_req;

        $countRequest = Selfcare::where('no_request', $no_req)->count();

        $checkRequest = DB::table('selfcares')
            ->select(
                'selfcares.no_request',
                'selfcares.no_ticket',
                'selfcares.cust_code',
                'selfcares.cust_email',
                'selfcares.report_type',
                'selfcares.report_category',
                'selfcares.report_content',
                'selfcares.first_handling',
                'selfcares.notes',
                'selfcares.billing_period',
                DB::raw(
                    '(CASE
                    WHEN status_request = "0" THEN "OPEN" 
                    WHEN status_request = "1" THEN "IN PROGRESS" 
                    WHEN status_request = "2" THEN "CANCEL"
                    WHEN status_request = "3" THEN "CLOSED"  
                    ELSE "ERROR STATUS" 
                    END
                ) as status'
                ),
                'selfcares.cancel_notes',
                'selfcares.canceled_at as cancel_date',
                'selfcares.created_at as reporting_date',
                'b.created_at as processing_date',
                'b.resolved_date as resolving_date',
                'b.closed_date as closing_date',
                'selfcares.attachment',
            )
            ->where('selfcares.no_request', $no_req)
            ->leftJoin('tickets as b', 'selfcares.no_ticket', '=', 'b.no_ticket')
            ->get();

        if ($countRequest > 0) {
            $message = 'Request Number ' . $no_req . ' Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data' => $checkRequest,
            ];
        } else {
            $message = 'Request Number ' . $no_req . ' Not Found';
            $response = [
                'success' => true,
                'message' => $message,
            ];
        }

        return response()->json($response, 200);
    }

    //Ticket Selfcare
    public function checkByCustCode($cust_code)
    {
        //return $no_req;

        $countRequest = Selfcare::where('cust_code', $cust_code)->count();

        $checkRequest = DB::table('selfcares')
            ->select(
                'selfcares.no_request',
                'selfcares.no_ticket',
                'selfcares.cust_code',
                'selfcares.cust_email',
                'selfcares.report_type',
                'selfcares.report_category',
                'selfcares.report_content',
                'selfcares.first_handling',
                'selfcares.notes',
                'selfcares.billing_period',
                DB::raw(
                    '(CASE
                    WHEN status_request = "0" THEN "OPEN" 
                    WHEN status_request = "1" THEN "IN PROGRESS" 
                    WHEN status_request = "2" THEN "CANCEL"
                    WHEN status_request = "3" THEN "CLOSED"  
                    ELSE "ERROR STATUS" 
                    END
                ) as status'
                ),
                'selfcares.cancel_notes',
                'selfcares.canceled_at as cancel_date',
                'selfcares.created_at as reporting_date',
                'b.created_at as processing_date',
                'b.resolved_date as resolving_date',
                'b.closed_date as closing_date',
                'selfcares.attachment',
            )
            ->where('selfcares.cust_code', $cust_code)
            ->leftJoin('tickets as b', 'selfcares.no_ticket', '=', 'b.no_ticket')
            ->get();

        if ($countRequest > 0) {
            $message = 'Customer Code ' . $cust_code . ' Found';
            $response = [
                'success' => true,
                'message' => $message,
                'data_count' => $countRequest,
                'data' => $checkRequest,
            ];
        } else {
            $message = 'Customer Code ' . $cust_code . ' Not Found';
            $response = [
                'success' => true,
                'message' => $message,
            ];
        }

        return response()->json($response, 200);
    }

    //Ticket yg dibuat oleh CC (bukan Selfcare)
    public function AllTicketByCustCode($cust_code)
    {
        //find cid
        $queryCid = Customer::where('customer_code', $cust_code)
            ->first();

        $countCID = Customer::where('customer_code', $cust_code)->count();

        if ($countCID > 0) {
            $cid = $queryCid->cid;

            $countTicket = Ticket::where('cid', $cid)->count();
            //return $cid;

            $listTickets = DB::table('tickets')
                ->select(
                    //'tickets.*',
                    'tickets.id',
                    'tickets.no_ticket',
                    'e.no_request as no_request',
                    'customers.company_name',
                    'tickets.category',
                    'tickets.final_category',
                    'c.name_incident as name_incident',
                    'd.name_incident as final_name_incident',
                    'tickets.request',
                    'tickets.remarks',
                    'a.department_name as dept_assign',
                    'b.department_name as dept_created',
                    DB::raw(
                        '(CASE
                            WHEN tickets.status = "1" THEN "IN PROGRESS" 
                            WHEN tickets.status = "2" THEN "UNASSIGN" 
                            WHEN tickets.status = "3" THEN "HOLD"
                            WHEN tickets.status = "4" THEN "CLOSED"  
                            ELSE "ERROR STATUS" 
                            END
                        ) as ticket_status'
                    ),
                    DB::raw(
                        '(CASE
                            WHEN e.status_request = "0" THEN "OPEN" 
                            WHEN e.status_request = "1" THEN "IN PROGRESS" 
                            WHEN e.status_request = "2" THEN "CANCEL"
                            WHEN e.status_request = "3" THEN "CLOSED"  
                            ELSE "" 
                            END
                        ) as selfcare_status'
                    ),
                    'e.cancel_notes as selfcare_notes',
                    'e.canceled_at as selfcare_cancel_date',
                    'e.created_at as selfcare_reporting_date',
                    'tickets.created_at as processing_date',
                    'tickets.resolved_date as resolving_date',
                    'tickets.closed_date as closing_date',
                )
                ->where('tickets.cid', $cid)
                ->leftJoin('logs_tickets', function ($join) {
                    $join->on('logs_tickets.id_ticket', '=', 'tickets.id');
                    $join->where('logs_tickets.assign_status', '=', '1');
                })
                ->leftjoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'logs_tickets.assign_to_dept', '=', 'a.department_id')
                ->leftJoin('department as b', 'employee.int_emp_department', '=', 'b.department_id')
                ->leftJoin('logs', 'tickets.id', '=', 'logs.id_ticket')
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('selfcares as e', 'tickets.no_ticket', '=', 'e.no_ticket')
                ->groupBy('tickets.id')
                ->orderBy('tickets.status', 'asc')
                ->orderBy('tickets.created_date', 'desc')
                ->orderBy('tickets.id', 'desc')
                ->orderBy('tickets.status', 'asc')
                ->get();

            if ($countTicket > 0) {
                $message = 'Tickets With Customer Code ' . $cust_code . ' Found';
                $response = [
                    'success' => true,
                    'message' => $message,
                    'data count' => $countTicket,
                    'ticket info' => $listTickets
                ];
            } else {
                $message = 'No Tickets For Customer Code ' . $cust_code;
                $response = [
                    'success' => true,
                    'message' => $message,
                    'data count' => $countTicket,
                ];
            }

            return response()->json($response, 200);
        } else {
            $message = 'Invalid Data For Customer Code ' . $cust_code;
            $response = [
                'success' => false,
                'message' => $message,
            ];

            return response()->json($response, 200);
        }
    }

    //History Ticket
    public function HistoryTicketByid($id)
    {
        //return $id;

        $countHistory = Log::where('id_ticket', $id)->count();
        //return $countHistory;

        $history = Log::where('id_ticket', $id)
            ->select(
                'logs.id',
                'logs.id_ticket',
                'tickets.no_ticket',
                'employee.int_emp_name',
                'logs.description',
                'logs.message',
                DB::raw("IF(logs.attachment_1 IS NOT NULL AND logs.attachment_1 != '',CONCAT('" . env('APP_URL') . "/tickets_Internal/', logs.attachment_1,'/downloadFileComment'),'') as attachment_1"),
                'logs.created_at'
            )
            ->leftjoin('employee', 'logs.create_by', '=', 'employee.int_emp_id')
            ->leftjoin('tickets', 'tickets.id', '=', 'logs.id_ticket')
            ->orderBy('id', 'desc')
            ->orderBy('id', 'desc')
            ->get();

        if ($countHistory > 0) {
            $message = 'History Found For ID Ticket ' . $id;
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countHistory,
                'history' => $history
            ];
            return response()->json($response, 200);
        } else {
            $message = 'No History For ID Ticket ' . $id;
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countHistory,
            ];
            return response()->json($response, 200);
        }
    }

    public function HistoryAssignByid($id)
    {
        //return $id;

        $countHistAssign = LogTicket::where('id_ticket', $id)->count();
        //return $countHistAssign;

        $historyAssign = LogTicket::where('id_ticket', $id)
            ->select(
                'logs_tickets.id',
                'logs_tickets.id_ticket',
                'tickets.no_ticket',
                'a.int_emp_name as assign_by',
                'b.department_name as assign_to dept',
                'logs_tickets.preclosed_status',
                'logs_tickets.preclosed_message',
                'logs_tickets.preclosed_date',
                'logs_tickets.created_at'
            )
            ->leftjoin('employee as a', 'logs_tickets.assign_by', '=', 'a.int_emp_id')
            ->leftjoin('department as b', 'logs_tickets.assign_to_dept', '=', 'b.department_id')
            ->leftjoin('tickets', 'tickets.id', '=', 'logs_tickets.id_ticket')
            ->orderBy('id', 'desc')
            ->orderBy('id', 'desc')
            ->get();

        if ($countHistAssign > 0) {
            $message = 'History Assign Found For ID Ticket ' . $id;
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countHistAssign,
                'history' => $historyAssign
            ];
            return response()->json($response, 200);
        } else {
            $message = 'No History Assign For ID Ticket ' . $id;
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countHistAssign,
            ];
            return response()->json($response, 200);
        }
    }

    public function SearchTicket($no_ticket)
    {
        $countTicketSearch = Ticket::where('no_ticket', $no_ticket)->count();
        //return $countTicketSearch;

        $TicketSearch = Ticket::where('no_ticket', $no_ticket)
            ->select(
                'no_ticket',
                'category',
                'c.name_incident as name_incident',
                'remarks',
                'a.int_emp_id as created_by',
                // 'd.name_incident as final_name_incident',
                'request',
                'company_name',
                'tickets.cid',
                'a_end',
                'b_end',
                'network_type',
                'network_owner',
                'status_link_respond',
                'sub_status_link_respond',
                'service',
                'rfo',
                'duration',
                'outage_reason',
                'outage_sub',
                'action_close',
                'a.int_emp_name as created_by',
                'created_date',
                // 'start_date',
                DB::raw('(CASE 
        WHEN category = "Maintenance" THEN "0000-00-00 00:00:00"
        ELSE start_date
        END) AS start_date'),
                'b.int_emp_name as closed_by',
                'closed_date',
                'closed_message',
                'resolved_date',
                'regional',
                'city',
                DB::raw('(CASE 
        WHEN status = "1" THEN "In Progress" 
        WHEN status = "3" THEN "Hold"
        WHEN status = "4" THEN "Closed"  
        ELSE "Unassign" 
        END) AS status'),
            )
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
            ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
            ->leftJoin('employee as a', 'tickets.created_by', '=', 'a.int_emp_id')
            ->leftJoin('employee as b', 'tickets.closed_by', '=', 'b.int_emp_id')
            ->groupby('tickets.no_ticket')
            ->get();

        if ($countTicketSearch > 0) {
            $message = 'Data found for no ticket ' . $no_ticket;
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countTicketSearch,
                'data_ticket' => $TicketSearch
            ];
            return response()->json($response, 200);
        } else {
            $message = 'Data not found for no ticket ' . $no_ticket;
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $countTicketSearch,
            ];
            return response()->json($response, 200);
        }
    }

    public function RequestTerminate()
    {
        $TicketCount = DB::table('napticketing.tickets')
            ->leftJoin('mapping_incident', 'tickets.type_incident', '=', 'mapping_incident.id')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('users', 'tickets.created_by', '=', 'users.id_employee')
            ->whereIn('tickets.ticket_type', ['External', 'Internal'])
            ->whereIn('tickets.category', ['Request', 'Internal'])
            ->whereIn('mapping_incident.name_incident', ['Terminate Off Net', 'Terminate On Net'])
            ->where(function ($query) {
                $query->whereNull('customers.service')
                    ->orWhere('customers.service', 'like', '%home%');
            })
            ->orderBy('tickets.created_at', 'desc')
            ->count();

        $TicketReqTerminate = DB::table('napticketing.tickets')
            ->select(
                'tickets.no_ticket',
                'tickets.category',
                'tickets.ticket_type',
                'mapping_incident.name_incident',
                DB::raw('(CASE 
                WHEN tickets.status = "1" THEN "In Progress" 
                WHEN tickets.status = "3" THEN "Hold"
                WHEN tickets.status = "4" THEN "Closed"  
                ELSE "Unassign" 
                END) AS status'),
                'tickets.remarks as note',
                'tickets.created_at',
                'tickets.request_at as request_terminate_date',
                'users.name as created_by',
                'users.email as created_email',
                'customers.customer_code',
                'customers.company_name',
                'customers.address',
                'customers.service',
                'customers.capacity',
                'customers.broadband',
                'customers.network_type',
                'customers.network_owner',
                'customers.a_end',
                'customers.b_end',
                'customers.regional',
                'customers.city',
            )
            ->leftJoin('mapping_incident', 'tickets.type_incident', '=', 'mapping_incident.id')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('users', 'tickets.created_by', '=', 'users.id_employee')
            ->whereIn('tickets.ticket_type', ['External', 'Internal'])
            ->whereIn('tickets.category', ['Request', 'Internal'])
            ->whereIn('mapping_incident.name_incident', ['Terminate Off Net', 'Terminate On Net'])
            ->where(function ($query) {
                $query->whereNull('customers.service')
                    ->orWhere('customers.service', 'like', '%home%');
            })
            ->orderBy('tickets.created_at', 'desc')
            ->get();


        if ($TicketCount > 0) {
            $message = 'Data found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $TicketCount,
                'data_ticket' => $TicketReqTerminate
            ];
            return response()->json($response, 200);
        } else {
            $message = 'Data not found';
            $response = [
                'success' => true,
                'message' => $message,
                'data count' => $TicketCount,
            ];
            return response()->json($response, 200);
        }
    }
}
