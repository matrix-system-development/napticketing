<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;
use App\Models\Rule;

class AuthController extends Controller
{
    use AuditLogsTrait;
    public function login()
    {
        //cek rule sso
        $cekRule=Rule::where('rule_name','sso')->first();

        //cek rule sso login
        $cekRuleLogin=Rule::where('rule_name','SSO Login')->first();
        $sso_login=$cekRuleLogin->rule_value;

        if($cekRule->rule_value=='1'){
            return redirect($sso_login); //-> kalau pakai SSO
        }
        else{
            return view('auth.login');
        }
    }

    //postlogin tanpa sso
    public function postlogin(Request $request)
    {
        //kalau pakai SSO
        // $email =base64_decode($q);
        // $password='-';
        $email=$request->email;
        $password=$request->password;
        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        $cekuser_status=User::where('email',$email)->first();

        $dologin=Auth::attempt($credentials);
        if($dologin){
            if($cekuser_status->active_status=='1' && $cekuser_status->role<>''){

                //update last login
                $update_lastlogin=User:: where('email',$email)
                ->update([
                    'last_login' => now(),
                    'login_counter' => $cekuser_status->login_counter+1,
                ]);

                if($update_lastlogin){
                    
                    //Audit Log
                    $username= auth()->user()->email; 
                    $ipAddress=$_SERVER['REMOTE_ADDR'];
                    $location='0';
                    $access_from=Browser::browserName();
                    $activity='Login';

                    //dd($access_from);
                    $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                    return redirect('/home');
                }
            }
            else{
                return redirect('/')->with('statusLogin','Give Access First to User');
                //return redirect('http://localhost:8000/signout'); //kalau pakai sso
            }
        }
        else{
            return redirect('/')->with('statusLogin','Wrong Email or Password');
            //return redirect('http://localhost:8000/signout'); //kalau pakai sso
        }

    }

    //post login dengan sso
    public function postloginSSO(Request $request,$q)
    {
        //cek rule sso logout
        $cekRuleLogout=Rule::where('rule_name','SSO Logout')->first();
        $sso_logout=$cekRuleLogout->rule_value;

        //kalau pakai SSO
        $email =base64_decode($q);
        $password='-';
        // $email=$request->email;
        // $password=$request->password;
        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        $cekuser_status=User::where('email',$email)->first();

        $dologin=Auth::attempt($credentials);
        if($dologin){
            if($cekuser_status->active_status=='1' && $cekuser_status->role<>''){

                //update last login
                $update_lastlogin=User:: where('email',$email)
                ->update([
                    'last_login' => now(),
                    'login_counter' => $cekuser_status->login_counter+1,
                ]);

                if($update_lastlogin){
                    
                    //Audit Log
                    $username= auth()->user()->email; 
                    $ipAddress=$_SERVER['REMOTE_ADDR'];
                    $location='0';
                    $access_from=Browser::browserName();
                    $activity='Login';

                    //dd($location);
                    $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

                    return redirect('/home');
                }
            }
            else{
                //return redirect('/')->with('statusLogin','Give Access First to User');
                return redirect($sso_logout); //kalau pakai sso
            }
        }
        else{
            //return redirect('/')->with('statusLogin','Wrong Email or Password');
            return redirect($sso_logout); //kalau pakai sso
        }

    }

    public function logout(Request $request)
    {
        //cek rule sso
        $cekRule=Rule::where('rule_name','sso')->first();

        //cek rule sso portal
        $cekRulePortal=Rule::where('rule_name','SSO Portal')->first();
        $sso_portal=$cekRulePortal->rule_value;

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Logout';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        if($cekRule->rule_value=='1'){
            $email_user=auth()->user()->email;
            return redirect($sso_portal.$email_user);
        }
        else{
            Auth::logout();
            return redirect('/')->with('statusLogout','Success Logout');
        }
    }
}

