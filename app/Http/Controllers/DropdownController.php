<?php

namespace App\Http\Controllers;
use App\Models\Dropdown;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class DropdownController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        //$view_data = Dropdown::all();
        //$view_data = DB::table('dropdowns')->paginate(5);

        $categories=DB::table('categories')
        ->orderBy('category_name','asc')
        ->get();
        //dd($categories);

        $dropdowns=DB::table('dropdowns')
        ->orderBy('id','desc')
        ->get();
        //dd($categories);
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Master Dropdown';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('dropdown.index',compact('categories','dropdowns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        //Validasi
        $request->validate([
            'group_incident' => 'required',
            'item_name' => 'required'
        ]);

        Dropdown::create([
            'category' => $request->group_incident,
            'name_value' => $request->item_name,
            'code_format' => $request->code_format
        ]);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Create Dropdown';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/masters/dropdowns')->with('status','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dropdown $dropdown)
    {
        //dd($dropdown);
        $categories=DB::table('categories')
        ->orderBy('category_name','asc')
        ->get();

        return view('dropdown.edit',compact('categories','dropdown'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dropdown $dropdown)
    {
        //Validasi
        $request->validate([
            'category' => 'required',
            'item_name' => 'required',
            'code' => 'required'
        ]);

        Dropdown::where('id', $dropdown->id)
            ->update([
                'category' => $request->category,
                'name_value' => $request->item_name,
                'code_format' => $request->code
            ]);
        
            //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Update Dropdown';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/masters/dropdowns')->with('status', 'Data Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Dropdown $dropdown)
    {
        Dropdown::destroy($dropdown->id);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Delete Dropdown';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/masters/dropdowns')->with('status','Data Deleted!');
    }
}
