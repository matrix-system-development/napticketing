<?php

namespace App\Http\Controllers;

use App\Models\Selfcare;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;

class SelfcareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->date_start."-".$request->date_finish);
        if($request->date_start=='' || $request->date_finish==''){
            $now=Carbon::now();
            $date_now=Carbon::now()->lastOfMonth();
            $date_start2=Carbon::now()->startOfMonth()->subMonth(24);
        }
        else{
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now=Carbon::now();
            $date_now=$request->date_finish;
            $date_start2=$request->date_start;
        }
        
        $selfcares=DB::table('selfcares')
        ->select('selfcares.*','customers.company_name')
        ->whereIn('selfcares.status_request',[0,1,2,3])
        ->whereBetween(DB::raw("(STR_TO_DATE(selfcares.created_at,'%Y-%m-%d'))"), [$date_start2, $date_now])
        ->leftjoin('customers','selfcares.cust_code','=','customers.customer_code')
        ->orderBy('selfcares.status_request','asc')
        ->orderBy('selfcares.id','desc')
        ->get();

        //$resp=json_decode($selfcares[0]->report_content);
        // dd($resp->judul);
        return view('Selfcare.index',compact('date_start2','date_now','selfcares'));
    }

    public function cancelRequest(Request $request,$id)
    {
        $request->validate([
            'cancel_notes' => 'required'
        ]);

        $now=Carbon::now();

        $cancelSelfcare=Selfcare::where('id',$id)
        ->update([
            'cancel_notes' => $request->cancel_notes,
            'canceled_at' => $now,
            'status_request' => '2',
        ]);

        if($cancelSelfcare){
            return redirect('/selfcare')->with('status','Success Cancel Request Customer');
        }
        else{
            return redirect('/selfcare')->with('status','Failed Cancel Request Customer');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTicket(Request $request)
    {
        $dropdowns = array();

        $dropdowns['status link respond']=DB::table('dropdowns')
        ->where('category','=','status link respond')
        ->orderBy('name_value','asc')
        ->get();

        $dropdowns['request']=DB::table('dropdowns')
        ->where('category','=','request')
        ->orderBy('name_value','asc')
        ->get();
        
        $dropdowns['category ticket']=DB::table('dropdowns')
        ->where('category','Classification Ticket')
        ->where('name_value','<>','Maintenance')
        ->orderBy('name_value','asc')
        ->get();

        $dropdowns['dept']=DB::table("department")
        ->orderBy('department_name','asc')
        ->get();

        $selfcare=Selfcare::where('id',$request->req_id)->first();

        $cust_code=$selfcare->cust_code;
        $reporting_time=$selfcare->created_at;
        $report_info=$selfcare->report_type."-".$selfcare->report_category;
        $req_id=$selfcare->id;

        $customer_detail=Customer::where('customer_code',$cust_code)->first();

        return view('Selfcare.createTicket',compact('dropdowns','reporting_time','customer_detail','req_id','report_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Selfcare  $selfcare
     * @return \Illuminate\Http\Response
     */
    public function show(Selfcare $selfcare)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Selfcare  $selfcare
     * @return \Illuminate\Http\Response
     */
    public function edit(Selfcare $selfcare)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Selfcare  $selfcare
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Selfcare $selfcare)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Selfcare  $selfcare
     * @return \Illuminate\Http\Response
     */
    public function destroy(Selfcare $selfcare)
    {
        //
    }
}
