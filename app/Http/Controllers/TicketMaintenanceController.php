<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\Dropdown;
use App\Models\Log;
use App\Models\LogTicket;
use App\Models\Rule;
use App\Models\User;
use App\Models\Department;
use App\Models\MappingIncident;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TicketExport;
use App\Models\Employee;
use App\Models\LogReschedule;

class TicketMaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use AuditLogsTrait;
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dropdowns = array();

        $dropdowns['dept'] = DB::table("department")
            ->orderBy('department_name', 'asc')
            ->get();

        $mappings = MappingIncident::where("id_dropdown", "41")
            ->orderBy('name_incident', 'asc')
            ->get();

        //dd($dropdowns);

        return view('ticket_maintenance.create', compact('mappings', 'dropdowns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'date_start' => 'required',
            'time_start' => 'required',
            'date_finish' => 'required',
            'time_finish' => 'required',
            'location' => 'required',
            'sub_category' => 'required',
            'message' => 'required',
            'doc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc2' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc3' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc4' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc5' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $ticket_status = '1';
        $hold_status = '0';
        $mtc_status = '1'; //OPEN
        $start_date_mtc = $request->date_start . " " . $request->time_start;
        $finish_date_mtc = $request->date_finish . " " . $request->time_finish;

        //mendapatkan duedate
        // $rule_query=Rule::where('rule_name', 'due date')->get();
        // $due_hours=$rule_query[0]->rule_value;
        // $duedate=Carbon::parse($created_date)->addHours($due_hours);
        //end mendapatkan duedate

        //duration blom bisa karena hold dan unhold // but di ticket maintenance gapeka hold onhold lgsg dr start_date_mtc and finish_date_mtc
        $start = strtotime($request->start_date_mtc);
        $end = strtotime($request->finish_date_mtc);
        $duration = $end - $start;
        // dd($end, $start);
        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($duration, $hours, $second, $minutes);
        //end duration


        //insert into table tickets
        $third_party = 'party';
        $ticket_create = Ticket::create([
            'ticket_type' => 'Maintenance',
            'cid' => $request->cid,
            'multiple_cid' => $request->multiple,
            'priority' => $request->priority,
            'status_complain' => $request->status_complain,
            'category' => 'Maintenance',
            'type_incident' => $request->sub_category,
            'created_by' => $created_by,
            'created_date' => $created_date,
            'status' => $ticket_status,
            'remarks' => $request->message,
            'due_date' => '',
            'hold_status' => $hold_status,
            'start_date_mtc' => $start_date_mtc,
            'finish_date_mtc' => $finish_date_mtc,
            'status_mtc' => $mtc_status,
            'third_party_assign' => $third_party,
            'outage_location' => $request->location,

            //INI TIDAK DIPAKAI KARNA ADA TICKET JOURNEY
            // 'closed_by' => $created_by,
            // 'closed_date' => $created_date,
            // 'start_date' => $created_date,
            // 'resolved_date' => $resolved_date,
            // 'closed_message' => $request->message,
            // 'duration' => $hours.' hours '.$minutes.' minutes ',
        ]);
        //end insert into table tickets

        if ($ticket_create) {
            $id = $ticket_create->id;

            //upload file
            if ($request->file('doc1')) {
                $extension1 = $request->file('doc1')->getClientOriginalExtension();
                $doc_name1 = $id . '_a.' . $extension1;
                $store1 = $request->file('doc1')->storeAs('attachment_tickets', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            if ($request->file('doc2')) {
                $extension2 = $request->file('doc2')->getClientOriginalExtension();
                $doc_name2 = $id . '_b.' . $extension2;
                $store2 = $request->file('doc2')->storeAs('attachment_tickets', $doc_name2);
            } else {
                $doc_name2 = "";
            }

            if ($request->file('doc3')) {
                $extension3 = $request->file('doc3')->getClientOriginalExtension();
                $doc_name3 = $id . '_c.' . $extension3;
                $store3 = $request->file('doc3')->storeAs('attachment_tickets', $doc_name3);
            } else {
                $doc_name3 = "";
            }

            if ($request->file('doc4')) {
                $extension4 = $request->file('doc4')->getClientOriginalExtension();
                $doc_name4 = $id . '_d.' . $extension4;
                $store4 = $request->file('doc4')->storeAs('attachment_tickets', $doc_name4);
            } else {
                $doc_name4 = "";
            }

            if ($request->file('doc5')) {
                $extension5 = $request->file('doc5')->getClientOriginalExtension();
                $doc_name5 = $id . '_e.' . $extension5;
                $store5 = $request->file('doc5')->storeAs('attachment_tickets', $doc_name5);
            } else {
                $doc_name5 = "";
            }

            $insert_filename = Ticket::where('id', $id)
                ->update([
                    'file_1' => $doc_name1,
                    'file_2' => $doc_name2,
                    'file_3' => $doc_name3,
                    'file_4' => $doc_name4,
                    'file_5' => $doc_name5
                ]);
            //end upload file

            //nomor otomatis

            //format tanggal
            $period_ticket = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = Ticket::whereDay('created_date', $created_date)
                ->whereMonth('created_date', $created_date)
                ->whereYear('created_date', $created_date)
                ->wherein('ticket_type', ['Maintenance', 'IntMaintenance'])
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noticket = "M/" . $period_ticket . "/" . $lastno;


            $create_noticket = Ticket::where('id', $id)
                ->update([
                    'no_urut' => $no_urut,
                    'no_ticket' => $noticket
                ]);
            //end nomor otomatis

            //insert log success create ticket
            $desc_success_create = 'Success Created Ticket';

            $log_history = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_create,
                'message'   => $request->message
            ]);
            //insert log success create ticket

            //insert log success assign ticket
            if ($third_party == 'party') {
                $assign_to = '1'; //ke dept CC karna kalau ke third party assign nya ke CC 
                $desc_success_assign = 'Success Assign Ticket';
                $message_assign = 'Assign To: 3rd Party';
            }
            //  else {
            //     $assign_to = $request->department;
            //     $desc_success_assign = 'Success Assign Ticket';
            //     $assign_dept = Department::where('department_id', $assign_to)
            //         ->get();
            //     $message_assign = 'Assign To: ' . $assign_dept[0]->department_name;
            // }

            //dd();
            $log_assign = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $id,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $assign_to,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status,
                // 'preclosed_date' => $created_date,
                // 'preclosed_message' =>$request-> message,
            ]);
            //end insert logs_ticket (log assign)

            if ($create_noticket && $log_history && $log_ticket &&  $log_assign) {

                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Ticket Maintenance';

                //dd($location);
                $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

                return redirect('/summary/matrix')->with('status', 'Success Create Ticket !');
            } else {
                return redirect('/summary/matrix')->with('status', 'Error Create Log !');
            }
        } else {
            return redirect('/summary/matrix')->with('status', 'Error Create Ticket !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $tickets_maintenance = Ticket::where('tickets.id', '=', $id)
            ->where('tickets.status', '!=', '0')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
            ->leftJoin('logs_tickets', 'tickets.id', '=', 'logs_tickets.id_ticket')
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->leftJoin('mapping_incident', 'tickets.type_incident', '=', 'mapping_incident.id')
            ->get();

        // dd($tickets_internal[0]->temp_id);
        // dd( $tickets_maintenance);

        $assign_history = LogTicket::where('logs_tickets.id_ticket', '=', $id)
            ->leftJoin('logs', 'logs_tickets.id_log', '=', 'logs.id')
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
            ->orderBy('logs_tickets.assign_date', 'desc')
            ->get(['employee.int_emp_name', 'department.department_name', 'logs.message', 'logs_tickets.assign_date', 'logs_tickets.preclosed_status', 'logs_tickets.preclosed_date']);

        $reschedules = LogReschedule::where('log_reschedules.id_ticket', '=', $id)
            ->leftJoin('logs', 'log_reschedules.id_ticket', '=', 'logs.id_ticket')
            ->leftJoin('tickets', 'log_reschedules.id_ticket', '=', 'tickets.id')
            ->orderBy('log_reschedules.id', 'desc')
            ->groupBy('log_reschedules.id')
            ->get(['log_reschedules.id_ticket', 'log_reschedules.start_date_mtc', 'log_reschedules.finish_date_mtc', 'log_reschedules.notes', 'logs.create_by', 'tickets.status_mtc', 'tickets.id']);

        // dd($rescheduls);

        $comment_history = Log::where('id_ticket', $id)
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs.create_by')
            ->leftJoin('department', 'department.department_id', '=', 'employee.int_emp_department')
            ->groupBy('logs.id')
            ->orderBy('logs.created_at', 'desc')
            ->get(['logs.created_at', 'logs.message', 'logs.attachment_1', 'employee.int_emp_name', 'department.department_name', 'logs.description']);

        $return = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '1')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $reassign = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '1')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $preclosed = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '1')
            ->where('preclosed_status', '0')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $close_ticket = LogTicket::where('id_ticket', $id)
            ->where('preclosed_status', '0')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $dropdowns['close'] = DB::table('dropdowns')
            ->where('category', '=', 'Close')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['outage reason'] = DB::table('dropdowns')
            ->where('category', '=', 'Outage Type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['status link respond'] = DB::table('dropdowns')
            ->where('category', '=', 'status link respond')
            ->orderBy('name_value', 'asc')
            ->get();

        $mappings = MappingIncident::where("id_dropdown", "41")
            ->orderBy('name_incident', 'asc')
            ->get();

        $ticket = Ticket::where('tickets.id', $id)
            ->select('*', 'incident.id as id_mapping', 'tickets.id as id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
            ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
            ->first();

        $id_login = auth()->user()->id_employee; // ini id login
        $role_user = auth()->user()->role; // ini role user
        $id_login_department = auth()->user()->id_department;
        $ticket_status = $tickets_maintenance[0]->status;
        // dd($ticket_status);

        // cek ada data customer atau tidak waktu create ticket, kalo ada panel ditampilkan
        if ($tickets_maintenance[0]->cid != NULL) {
            $panelCustomer = 1;
        } else { // panel customer tidak ditampilkan
            $panelCustomer = 0;
        }

        if ($tickets_maintenance[0]->status_mtc == '1' || $tickets_maintenance[0]->status_mtc == '2') {
            $buttonOn = 1;
        } else {
            $buttonOn = 0;
        }

        // dd($close_ticket[0]->department_name);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Ticket Maintenance Detail';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('ticket_maintenance.show', compact('mappings', 'dropdowns', 'ticket', 'tickets_maintenance', 'comment_history', 'return', 'assign_history', 'panelCustomer', 'id', 'reschedules', 'buttonOn'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download(Request $request, $id)
    {
        //dd($id);
        $tickets_maitenance = Ticket::where('id', $id)->firstOrFail();
        //dd($ticket);
        $pathToFile_1 = storage_path('app/attachment_tickets/' . $tickets_maitenance->file_1);
        // $pathToFile_2 = storage_path('app/attachment_tickets/' .$ticket->file_2);
        // $pathToFile_3 = storage_path('app/attachment_tickets/' .$ticket->file_3);
        // $pathToFile_4 = storage_path('app/attachment_tickets/' .$ticket->file_4);
        // $pathToFile_5 = storage_path('app/attachment_tickets/' .$ticket->file_5);
        // return response()->download([$pathToFile_1, $pathToFile_2, $pathToFile_3, $pathToFile_4, $pathToFile_5]);
        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return response()->download($pathToFile_1);
    }

    public function ViewFile(Request $request, $topology_file, $cust)
    {
        $file = base64_decode($topology_file);
        $customer = base64_decode($cust);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Topology File';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('topology.view', compact('file', 'customer'));
    }

    public function EditRFOHead(Request $request, $id)
    {
        // dd($request);

        if ($request->start_date == '' && $request->start_time == '') {
            $start_date_mtc = $request->start_date_old;
        } else {
            $start_date_mtc = $request->start_date . " " . $request->start_time;
        }

        if ($request->finish_date == '' && $request->finish_time == '') {
            $finish_date_mtc = $request->resolved_date_old;
        } else {
            $finish_date_mtc = $request->finish_date . " " . $request->finish_time;
        }

        if ($request->sub_category == '') {
            $sub_category = $request->type_incident_old;
        } else {
            $sub_category = $request->sub_category;
        }

        if ($request->outage_loc == '') {
            $outage_loc = $request->outage_loc_old;
        } else {
            $outage_loc = $request->outage_loc;
        }

        if ($request->message == '') {
            $message = $request->closed_message_old;
        } else {
            $message = $request->message;
        }


        $created_by = auth()->user()->id_employee;
        $created_date = $request->ticket_date;
        $click_date = Carbon::now();
        //$resolved_date=$request->finish_date." ".$request->finish_time;

        //ini duration ticket
        $start = strtotime($created_date);
        $end = strtotime($click_date);
        $duration = $end - $start;
        // dd($end, $start);
        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($hours, $minutes);
        //end duration


        //ini duration mtc
        $start_mtc = strtotime($start_date_mtc);
        $end_mtc = strtotime($finish_date_mtc);
        $duration_mtc = $end_mtc - $start_mtc;
        // dd($end, $start);
        $total_second_mtc = $duration_mtc; // 177300 second
        $total_minutes_mtc = ($duration_mtc / 60); // 2955 minute
        $temp_minutes_mtc = ($total_second_mtc % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours_mtc = (($total_minutes_mtc - $temp_minutes_mtc) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second_mtc = (($temp_minutes_mtc * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes_mtc = ((($temp_minutes_mtc * 60) - $second_mtc) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($hours_mtc, $minutes_mtc);
        //end duration

        $update_ticket = Ticket::where('id', $id)
            ->update([
                'start_date_mtc' => $start_date_mtc,
                'finish_date_mtc' => $finish_date_mtc,
                'outage_location' => $outage_loc,
                'closed_message' => $message,
                // 'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                'duration_mtc' => $hours_mtc . ' hours ' . $minutes_mtc . ' minutes ',
                'type_incident' => $sub_category,
                'remarks' => $message,
            ]);

        //insert log success hold ticket
        $desc_success_edit = 'Success Edit RFO by Head Dept';

        //insert log success hold ticket

        if ($update_ticket) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Edit RFO';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/tickets_Maintenance/' . $id)->with('status', 'Success Edit RFO !');
        } else {
            return redirect('/tickets_Maintenance/' . $id)->with('status', 'Failed Edit RFO !');
        }
    }

    public function EditRFOStaff(Request $request, $id)
    {
        // dd($request);

        if ($request->sub_category == '') {
            $sub_category = $request->type_incident_old;
        } else {
            $sub_category = $request->sub_category;
        }

        if ($request->outage_loc == '') {
            $outage_loc = $request->outage_loc_old;
        } else {
            $outage_loc = $request->outage_loc;
        }

        if ($request->message == '') {
            $message = $request->closed_message_old;
        } else {
            $message = $request->message;
        }


        $created_by = auth()->user()->id_employee;

        $update_ticket = Ticket::where('id', $id)
            ->update([
                'outage_location' => $outage_loc,
                'closed_message' => $message,
                'type_incident' => $sub_category,
                'remarks' => $message,
            ]);

        //insert log success hold ticket
        $desc_success_edit = 'Success Edit RFO by Staff   ';


        if ($update_ticket) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Edit RFO';

            //dd($location);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/tickets_Maintenance/' . $id)->with('status', 'Success Edit RFO !');
        } else {
            return redirect('/tickets_Maintenance/' . $id)->with('status', 'Failed Edit RFO !');
        }
    }

    public function reschedule(Request $request)
    {
        // dd($request);

        // validate update ticket start_date_mtc  finish_date_mtc ststus_mtc
        $request->validate([
            'start_date' => 'required',
            'time_start' => 'required',
            'finish_date' => 'required',
            'time_finish' => 'required',
            'message' => 'required'
        ]);

        $status_mtc = "2";
        $start_date_mtc = $request->start_date . " " . $request->time_start;
        $finish_date_mtc = $request->finish_date . " " . $request->time_finish;
        // dd($start_date_mtc);
        // 'nama coloumn table' <- update tiket reschedule

        $update_ticket_reschedule = Ticket::where('id', $request->id_ticket)
            ->update([
                'status_mtc' => $status_mtc,
                'start_date_mtc' => $start_date_mtc,
                'finish_date_mtc' => $finish_date_mtc,
            ]);

        if ($update_ticket_reschedule) {
            $description_comment = 'Success Create Comment';
            $reschedule_creator = auth()->user()->id_employee;

            // ' nama coloumn table insert log'
            $log_create = Log::create([
                'id_ticket' => $request->id_ticket,
                'create_by' => $reschedule_creator,
                'message' => $request->message,
                'description' => $description_comment
            ]);

            // ' nama coloumn table insert log_reschedule'
            $log_reschedule_create = LogReschedule::create([
                'id_ticket' => $request->id_ticket,
                'start_date_mtc' => $start_date_mtc,
                'finish_date_mtc' => $finish_date_mtc,
                'notes' => $request->message,
            ]);

            $id = $request->id_ticket;
            return redirect('/tickets_Maintenance/' . $id)->with('status', 'Success Reschedule!');
        }
    }

    public function cancel(Request $request)
    {

        // dd($request);
        $request->validate([
            'message' => 'required'
        ]);

        $status_mtc = "3";
        $status_ticket = "4";
        $closed_by = auth()->user()->id_employee;
        $click_date = Carbon::now();

        //ini duration ticket
        $start = strtotime($request->created_date);
        $end = strtotime($click_date);
        $duration = $end - $start;
        // dd($end, $start);
        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($hours, $minutes);
        //end duration


        //duration blom bisa karena hold dan unhold // but di ticket maintenance gapeka hold onhold lgsg dr start_date_mtc and finish_date_mtc
        $start_mtc = strtotime($request->start_date_mtc);
        $end_mtc = strtotime($request->finish_date_mtc);
        $duration_mtc = $end_mtc - $start_mtc;
        // dd($end, $start);
        $total_second_mtc = $duration_mtc; // 177300 second
        $total_minutes_mtc = ($duration_mtc / 60); // 2955 minute
        $temp_minutes_mtc = ($total_second_mtc % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours_mtc = (($total_minutes_mtc - $temp_minutes_mtc) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second_mtc = (($temp_minutes_mtc * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes_mtc = ((($temp_minutes_mtc * 60) - $second_mtc) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($hours_mtc, $minutes_mtc);
        //end duration

        Ticket::where('id', $request->id_ticket)
            ->update([
                'status_mtc' => $status_mtc,
                'status' => $status_ticket,
                'closed_by' => $closed_by,
                'closed_date' => $click_date,
                'start_date' => $click_date,
                'resolved_date' => $click_date,
                'closed_message' => $request->cancel_message,
                'duration_mtc' => $hours_mtc . ' hours ' . $minutes_mtc . ' minutes ',
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
            ]);

        $preclosed_status = "1";
        LogTicket::where('id_ticket', $request->id_ticket)
            ->update([
                'preclosed_status' => $preclosed_status,
                'preclosed_date' => $click_date,
                'preclosed_message' => $request->cancel_message
            ]);

        $id = $request->id_ticket;
        return redirect('/tickets_Maintenance/' . $id)->with('status', 'Success Cancel Ticket!');
    }

    public function closeticket(Request $request)
    {

        // dd($request);
        $request->validate([
            'message' => 'required'
        ]);


        $status_mtc = "4";
        $status_ticket = "4";
        $closed_by = auth()->user()->id_employee;
        $click_date = Carbon::now();

        //ini duration ticket
        $start = strtotime($request->created_date);
        $end = strtotime($click_date);
        $duration = $end - $start;
        // dd($end, $start);
        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($hours, $minutes);
        //end duration


        //duration blom bisa karena hold dan unhold // but di ticket maintenance gapeka hold onhold lgsg dr start_date_mtc and finish_date_mtc
        $start_mtc = strtotime($request->start_date_mtc);
        $end_mtc = strtotime($request->finish_date_mtc);
        $duration_mtc = $end_mtc - $start_mtc;
        // dd($end, $start);
        $total_second_mtc = $duration_mtc; // 177300 second
        $total_minutes_mtc = ($duration_mtc / 60); // 2955 minute
        $temp_minutes_mtc = ($total_second_mtc % 3600 / (60)); // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours_mtc = (($total_minutes_mtc - $temp_minutes_mtc) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second_mtc = (($temp_minutes_mtc * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes_mtc = ((($temp_minutes_mtc * 60) - $second_mtc) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // dd($hours_mtc, $minutes_mtc);
        //end duration

        Ticket::where('id', $request->id_ticket)
            ->update([
                'status_mtc' => $status_mtc,
                'status' => $status_ticket,
                'closed_by' => $closed_by,
                'closed_date' => $click_date,
                'start_date' => $click_date,
                'resolved_date' => $click_date,
                'closed_message' => $request->close_message,
                'duration_mtc' => $hours_mtc . ' hours ' . $minutes_mtc . ' minutes ',
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
            ]);

        $preclosed_status = "1";
        LogTicket::where('id_ticket', $request->id_ticket)
            ->update([
                'preclosed_status' => $preclosed_status,
                'preclosed_date' => $click_date
            ]);

        $id = $request->id_ticket;
        return redirect('/tickets_Maintenance/' . $id)->with('status', 'Success Close Ticket!');
    }

    public function addComment(Request $request)
    {
        // 'nama object'
        $request->validate([
            'id_ticket' => 'required',
            'message_comment'  => 'required',
            'attc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        $description_comment = 'Success Create Comment';

        $comment_creator = auth()->user()->id_employee;

        // ' nama coloumn table'
        $comment_create = Log::create([
            'id_ticket' => $request->id_ticket,
            'create_by' => $comment_creator,
            'message' => $request->message_comment,
            // 'attachment_1' => $request->attc1,
            'description' => $description_comment
        ]);

        // dd($comment_create);

        if ($comment_create) {
            // generate id comment
            $id_comment = $comment_create->id;

            //upload file
            if ($request->file('attc1')) {
                $extension1 = $request->file('attc1')->getClientOriginalExtension();
                $doc_name1 = $id_comment . '_comment.' . $extension1;
                $store1 = $request->file('attc1')->storeAs('attachment_comment', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            // 'nama coloumn table' <- insert attachment ke table logs
            $insert_log = Log::where('id', $id_comment)
                ->update([
                    'attachment_1' => $doc_name1
                ]);

            // $date_submit=$comment_create->created_at;
            // $created_date=Carbon::now();

            // dd($date_submit - $created_date);

            if ($comment_create && $insert_log != NULL || $comment_create && $insert_log == NULL) {
                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Comment';

                //dd($location);
                $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

                return redirect('/tickets_Maintenance/' . $comment_create->id_ticket)->with('status', 'Success Create Comment !');
            } else {
                return redirect('/tickets_Maintenance/' . $comment_create->id_ticket)->with('status', 'Fail Create Comment !');
            }
        } else {
            return redirect('/tickets_Maintenance/' . $comment_create->id_ticket)->with('status', 'Fail Create Comment !');
        }
    }

    public function downloadFileComment(Request $request, $id)
    {
        $pathToFileComment_1 = storage_path('app/attachment_comment/' . $id);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File Comment';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);
        return response()->download($pathToFileComment_1);
    }
}
