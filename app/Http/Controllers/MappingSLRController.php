<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dropdown;

class MappingSLRController extends Controller
{
    public function selectSLRAjax($slr_id){

        $dropdown=Dropdown::where('name_value',$slr_id)->get();

        $query=DB::table("mapping_incident")
        ->where("id_dropdown","=",$dropdown[0]->id)
        ->orderBy('name_incident','asc')
        ->get();
        //dd($query);
        return json_encode($query);
    }
}
