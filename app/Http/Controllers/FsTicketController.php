<?php

namespace App\Http\Controllers;

use App\Models\fsTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Rule;
use Illuminate\Support\Facades\Http;
use App\Models\LogToken;
use App\Models\LogApi;
use App\Traits\AuditLogsTrait;
use App\Models\Customer;
use Browser;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Storage;

class FsTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        if($request->date_start=='' || $request->date_finish==''){
            $now=Carbon::now();
            $date_now=Carbon::now()->lastOfMonth();
            $date_start2=Carbon::now()->startOfMonth()->subMonth(2);
        }
        else{
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now=Carbon::now();
            $date_now=$request->date_finish;
            $date_start2=$request->date_start;
        }

        $fstickets = DB::table('fs_tickets')
        ->select('fs_tickets.*','customers.company_name')
        ->whereBetween(DB::raw("(STR_TO_DATE(fs_tickets.created_at,'%Y-%m-%d'))"), [$date_start2, $date_now])
        ->leftJoin('customers','fs_tickets.cid','=','customers.partner_cid')
        ->groupBy('fs_tickets.id')
        ->orderBy('fs_tickets.created_at','desc')
        ->orderBy('fs_tickets.id','desc')
        ->get();

        //dd($status_ticket);
        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Ticket Tracking FS';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('ticket_fs.index',compact('date_start2','date_now','fstickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cid)
    {
        $customer_id=base64_decode($cid);
        //dd($customer_id);
        
        $customer_detail=Customer::where('partner_cid',$customer_id)->first();

        $dropdowns['ticket type FS']=DB::table('dropdowns')
        ->where('category','Ticket Type FS')
        ->orderBy('name_value','asc')
        ->get();

        //login fs
        //call url,username,password for api fs
        $rule=Rule::where('rule_name','FS Login')->first();
        $url_fslogin=$rule->rule_value;

        $rule_username=Rule::where('rule_name','username fs')->first();
        $username_fs=$rule_username->rule_value;

        $rule_password=Rule::where('rule_name','password fs')->first();
        $password_fs=$rule_password->rule_value;

        //login post to FS
        $response = Http::post($url_fslogin, [
            'username' => $username_fs,
            'password' => $password_fs,
        ]);

        $api_name='FS Login';

        if($response->failed()){
            $description_log= $response['title']." - ".$response['traceId'];

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '0',
            ]);
        }
        else{
            $token=$response['token'];
            $description_log='Success Login';

            //insert log token
            $log_token=LogToken::create([
                'access_token' => $token,
                'partner' => 'FiberStar',
            ]);

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '1',
            ]);
        }
        //end login FS

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Create Ticket FS';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('ticket_fs.create',compact('dropdowns','customer_detail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'category' => 'required',
            'pic_contact'  => 'required',
            'pic_email' => 'required',
            'cust_code' => 'required',
            'partner_cid' => 'required',
            'subject' => 'required',
            'file' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx|max:2000',
        ]);

        $created_date=Carbon::now();
        
        $created_by=auth()->user()->name;
        $ticket_status='Opened';

        //nomor otomatis

        //format tanggal
        $period_ticket = $created_date->format('ymd');

        //no urut akhir
        $searchlastno = fsTicket::whereDay('created_at', $created_date)
            ->max('no_urut');
        //dd($searchlastno);
        
        $no = 1;
        if($searchlastno){
            $lastno=sprintf("%04s", abs($searchlastno + 1));
            $no_urut=abs($searchlastno + 1);
        }
        else{
            $lastno=sprintf("%04s", abs($no));
            $no_urut=1;
        }

        //dd($no_urut);

        // $create_noticket=fsTicket::where('id',$id)
        //     ->update([
        //     'no_urut' => $no_urut,
        //     'no_ticket' => $noticket
        // ]);
        //end nomor otomatis

        //mandatory FS
        $noticket="FS/".$period_ticket."/".$lastno;
        $category=$request->category;
        $contact_person=auth()->user()->email;
        $pic_contact=$request->pic_contact;
        $pic_email=$request->pic_email;
        $cust_code=$request->cust_code;
        $partner_cid=$request->partner_cid;
        $subject=$request->subject;
        $ref_ticket=$request->ref_ticket;
        $notes=$request->message;

        //dd($noticket);
        //hit API FS
        //url
        $rule=Rule::where('rule_name','FS Receiving Ticket')->first();
        $url_fscreate=$rule->rule_value;
        
        //last token
        $log_token=LogToken::where('partner','FiberStar')
        ->orderBy('id','desc')
        ->first();
        $token=$log_token->access_token;

        //dd($request->file('doc'));
        //$path=Storage::path('app\attachment_ticketsfs');
        $doc=$request->file('doc');
        //dd($filename);
        
        if($doc){
            $filename = strtolower($doc->getClientOriginalName());
            $response = Http::withToken($token)
            ->asMultipart()
            ->attach('files', file_get_contents($doc),$filename)
            //->asForm()
            ->post($url_fscreate,[
                'ticketId' => $noticket,
                'category' => $category,
                'contactPerson' => 'customer.care@napinfo.co.id',
                'phoneNumber' => substr($pic_contact,0,20),
                'email' => $pic_email,
                'customerId' => $cust_code,
                'circuitId' => $partner_cid,
                'subject' => $subject,
                'referenceTicket' => $ref_ticket,
                'note' => $notes
            ]);
        }else{
            $response = Http::withToken($token)
            ->asMultipart()
            ->post($url_fscreate,[
                'ticketId' => $noticket,
                'category' => $category,
                'contactPerson' => 'customer.care@napinfo.co.id',
                'phoneNumber' => substr($pic_contact,0,20),
                'email' => $pic_email,
                'customerId' => $cust_code,
                'circuitId' => $partner_cid,
                'subject' => $subject,
                'referenceTicket' => $ref_ticket,
                'note' => $notes
            ]);
        }
        //dd($response->json());
        $api_name='FS Receiving Ticket';

        if($response->failed()){
            $message=$response['message'];

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $message." - ".$noticket,
                'conn_status' => '1',
            ]);
        }
        else{
            $message=$response['message'];
            $req_no=$response['requestTicketNumber'];

            if($category=='CT0001'){
                $category_name='Down';
                $sub_category='Los Merah';
            }
            elseif($category=='CT0002'){
                $category_name='Down';
                $sub_category='Lampu Indikator Normal';
            }
            elseif($category=='CT0003'){
                $category_name='Intermiten';
                $sub_category='Redaman';
            }
            else{
                $category_name='Undefined';
                $sub_category='Undefined';
            }
    
            //duedate
            $rule_duedate=Rule::where('rule_name','due date fs')->first();
            $due_hours=$rule_duedate->rule_value;
            $duedate=Carbon::parse($created_date)->addHours($due_hours);
    
            //inser table fs_tickets
            $fs_ticket=fsTicket::create([
                'ticket_type' => 'Complaint',
                'no_ticket' => $noticket,
                'req_no' => $req_no,
                'ref_no' => $ref_ticket,
                'no_urut' => $no_urut,
                'cid' => $partner_cid,
                'category' => $category_name,
                'sub_category' => $sub_category,
                'status' => $ticket_status,
                'duedate' => $duedate,
                'created_by' => $created_by,
                'notes' => $notes,
            ]);
    
            //upload file
            $id=$fs_ticket->id;
            if($request->file('doc')){
                $extension = $request->file('doc')->getClientOriginalExtension();
                $doc_name = $id.'_a.'.$extension;
                $store = $request->file('doc')->storeAs('attachment_ticketsfs', $doc_name);
            }
            else{
                $doc_name="";
            }

            $update_fsticket=fsTicket::where('id',$id)
            ->update([
                'doc_name' => $doc_name,
            ]);

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $message." - ".$noticket,
                'conn_status' => '1',
            ]);
        }
        //dd($response->json());

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Submit Create Ticket FS';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        $result=$message;
        return redirect('/tickets_fs')->with('status',$result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fsTicket  $fsTicket
     * @return \Illuminate\Http\Response
     */
    public function show($id,fsTicket $fsTicket)
    {
        return view('ticket_fs.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fsTicket  $fsTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(fsTicket $fsTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\fsTicket  $fsTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fsTicket $fsTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fsTicket  $fsTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(fsTicket $fsTicket)
    {
        //
    }

    public function ShowCancel($reqno)
    {
        $fstickets=fsTicket::where('req_no',$reqno)->first();
        return view('ticket_fs.show_cancel',compact('fstickets'));
    }

    public function CancelTicket(Request $request)
    {
        $request->validate([
            'reason' => 'required',
        ]);

        $canceled_at=Carbon::now();
        $canceled_by=auth()->user()->name;
        $reqno=$request->req_no;

        //login fs
        //call url,username,password for api fs
        $rule=Rule::where('rule_name','FS Login')->first();
        $url_fslogin=$rule->rule_value;

        $rule_username=Rule::where('rule_name','username fs')->first();
        $username_fs=$rule_username->rule_value;

        $rule_password=Rule::where('rule_name','password fs')->first();
        $password_fs=$rule_password->rule_value;

        //login post to FS
        $response = Http::post($url_fslogin, [
            'username' => $username_fs,
            'password' => $password_fs,
        ]);

        $api_name='FS Login';

        if($response->failed()){
            $description_log= $response['title']." - ".$response['traceId'];

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '0',
            ]);
        }
        else{
            $token=$response['token'];
            $description_log='Success Login';

            //insert log token
            $log_token=LogToken::create([
                'access_token' => $token,
                'partner' => 'FiberStar',
            ]);

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '1',
            ]);
        }
        //end login FS

        $rule_check=Rule::where('rule_name','FS Check by Request Number')->first();
        $check_fs=$rule_check->rule_value;

        $response_check=Http::withToken($response['token'])
        ->get($check_fs.$reqno);

        if(isset($response_check['ticketNumber'])){ ///jika ticket sudah di proses oleh FS
            //dd($response_check['currentLatestStatus']);
            $description_log="Ticket ".$reqno." can't be canceled,This Ticket Has Been Processed";
            $update_fsticket=fsTicket::where('req_no',$reqno)
            ->update([
                'status' => $response_check['currentLatestStatus'],
            ]);

            //insert log api
            $log_api=LogApi::create([
                'api_name' => 'FS Cancel',
                'description' => $description_log,
                'conn_status' => '0',
            ]);
        }
        else{
            //cancel Ticket FS
            $rule=Rule::where('rule_name','FS Cancel Ticket')->first();
            $url_fscancel=$rule->rule_value;

            $response_cancel = Http::withToken($response['token'])
            ->post($url_fscancel, [
                'requestTicketNumber' => $reqno,
                'reason' => 'test',
            ]);

            //dd($response_cancel->json());

            if($response->failed()){
                $description_log= $response_cancel['message']." - ".$reqno;

                //insert log api
                $log_api=LogApi::create([
                    'api_name' => 'FS Cancel',
                    'description' => $description_log,
                    'conn_status' => '0',
                ]);
            }
            elseif($response->clientError()){
                $description_log= $response_cancel['message']." - ".$reqno;

                //insert log api
                $log_api=LogApi::create([
                    'api_name' => 'FS Cancel',
                    'description' => $description_log,
                    'conn_status' => '0',
                ]);
            }
            else{
                $token=$response['token'];
                $description_log=$response_cancel['message'];
                //dd($description_log);
                if($description_log!="Invalid ticket number, or can't be canceled"){
                    $update_fsticket=fsTicket::where('req_no',$reqno)
                    ->update([
                        'status' => 'Canceled',
                        'reason_cancel' => $request->reason,
                        'canceled_at' => $canceled_at,
                        'canceled_by' => $canceled_by,
                    ]);

                    //insert log api
                    $log_api=LogApi::create([
                        'api_name' => 'FS Cancel',
                        'description' => $description_log,
                        'conn_status' => '1',
                    ]);
                }
                else{
                    $description_log= $response_cancel['message']." - ".$reqno;

                    //insert log api
                    $log_api=LogApi::create([
                        'api_name' => 'FS Cancel',
                        'description' => $description_log,
                        'conn_status' => '0',
                    ]);
                }
            }
        }

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Cancel Ticket FS';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return redirect('/tickets_fs')->with('status',$description_log);
    }

    public function Detailticket($reqno)
    {
        //dd($reqno);
        
        //login fs
        //call url,username,password for api fs
        $rule=Rule::where('rule_name','FS Login')->first();
        $url_fslogin=$rule->rule_value;

        $rule_username=Rule::where('rule_name','username fs')->first();
        $username_fs=$rule_username->rule_value;

        $rule_password=Rule::where('rule_name','password fs')->first();
        $password_fs=$rule_password->rule_value;

        //login post to FS
        $response = Http::post($url_fslogin, [
            'username' => $username_fs,
            'password' => $password_fs,
        ]);

        $api_name='FS Login';

        if($response->failed()){
            $description_log= $response['title']." - ".$response['traceId'];

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '0',
            ]);
        }
        else{
            $token=$response['token'];
            $description_log='Success Login';

            //insert log token
            $log_token=LogToken::create([
                'access_token' => $token,
                'partner' => 'FiberStar',
            ]);

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '1',
            ]);
        }
        //end login FS

        //get detail ticket
        $rule_check=Rule::where('rule_name','FS Check by Request Number')->first();
        $check_fs=$rule_check->rule_value;
        //dd($check_fs.$reqno);

        $response_check=Http::withToken($response['token'])
        ->get($check_fs.$reqno);

        //dd($response_check->json());

        if(isset($response_check['ticketNumber'])){ ///jika ticket sudah di proses oleh FS
            //dd($response_check->json());

            //update status FS
            $description_log="Success Check Detail"." ".$reqno;
            $update_fsticket=fsTicket::where('req_no',$reqno)
            ->update([
                'status' => $response_check['currentLatestStatus'],
            ]);

            //update fs_last_update
            foreach ($response_check['ticketHistory'] as $history) {
                if ($response_check['currentLatestStatus']==$history['status']) {
                    $update_fsticket=fsTicket::where('req_no',$reqno)
                    ->update([
                        'fs_last_update' => date('Y-m-d H:i:s',strtotime($history['processedAt'])),
                    ]);
                }
            }

            //insert log api
            $log_api=LogApi::create([
                'api_name' => 'FS Check by Request Number',
                'description' => $description_log,
                'conn_status' => '1',
            ]);

            $fstickets=fsTicket::where('req_no',$reqno)->first();
            //dd($fstickets);
            //dd($response_check->json());
            $ticketNumber=$response_check['ticketNumber'];
            $workOrderNumber=$response_check['workOrderNumber'];
            $workOrderStatus=$response_check['workOrderStatus'];

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Detail Ticket FS';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return view(
                'ticket_fs.show',
                [
                    'ticketHistory'=>$response_check['ticketHistory'],
                    'woHistory'=>$response_check['woHistory']
                ],
                compact('fstickets','ticketNumber','workOrderNumber','workOrderStatus')
            );
        }
        else{
            //dd($response_check->json());
            $description_log=$reqno." ".$response_check['message'];

            //insert log api
            $log_api=LogApi::create([
                'api_name' => 'FS Check by Request Number',
                'description' => $description_log,
                'conn_status' => '1',
            ]);

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='View Detail Ticket FS';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/tickets_fs')->with('status',$description_log);
        }
    }

    public function download($docname)
    {
        //dd($docname);
        $pathToFile = storage_path('app/attachment_ticketsfs/' .$docname);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='Download File FS';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
        return response()->download($pathToFile);
    }
}
