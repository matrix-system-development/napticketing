<?php

namespace App\Http\Controllers;

use Browser;
use App\Models\Log;
use App\Models\Rule;
use App\Models\User;
use App\Models\Ticket;
use App\Models\LogHold;
use App\Models\Customer;
use App\Models\Dropdown;
use App\Models\Employee;
use App\Models\Selfcare;
use App\Models\LogTicket;
use App\Models\Department;
use App\Models\DepartmentEmail;
use Illuminate\Http\Request;

use App\Exports\TicketExport;
use App\Traits\AuditLogsTrait;
use Illuminate\Support\Carbon;
use App\Models\MappingIncident;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;
use App\Notifications\CreateHoldNotif;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Notification;
use CreateHoldNotif as GlobalCreateHoldNotif;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;
use Illuminate\Support\Facades\Mail;
use App\Mail\ExternalMail;
use App\Mail\AssignMail;
use App\Mail\PreclosedMail;
use App\Mail\ClosedMail;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use AuditLogsTrait;

    //index tracking
    public function index(Request $request)
    {
        // dd('test');

        if ($request->date_start == '' || $request->date_finish == '') {
            $now = Carbon::now();
            $date_now = Carbon::now()->lastOfMonth()->format('Y-m-d 23:59:59');
            $date_start2 = Carbon::now()
                ->startOfMonth()
                //->subMonth(1)
                ->format('Y-m-d H:i:s');
        } else {
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);


            $date_now = $request->date_finish;
            $date_start2 = $request->date_start;
        }

        //dd($date_now,$date_start2);
        $dept_login = auth()->user()->id_department;
        $user_ca = auth()->user()->is_ca;

        if ($dept_login == '1' || $dept_login == '16') {
            $tickets = DB::table('tickets')
                ->select(
                    'tickets.*',
                    'customers.company_name',
                    'c.name_incident as name_incident',
                    'd.name_incident as final_name_incident',
                    'a.department_name',
                    'a.department_name as dept_assign',
                    'b.department_name as dept_created',
                    'e.no_request as no_request'
                )
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$date_start2, $date_now])
                ->leftJoin('logs_tickets', function ($join) {
                    $join->on('logs_tickets.id_ticket', '=', 'tickets.id');
                    $join->where('logs_tickets.assign_status', '=', '1');
                })
                ->leftjoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'logs_tickets.assign_to_dept', '=', 'a.department_id')
                ->leftJoin('department as b', 'employee.int_emp_department', '=', 'b.department_id')
                ->leftJoin('logs', 'tickets.id', '=', 'logs.id_ticket')
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('selfcares as e', 'tickets.no_ticket', '=', 'e.no_ticket')
                ->groupBy('tickets.id')
                ->orderBy('tickets.status', 'asc')
                ->orderBy('tickets.created_date', 'desc')
                ->orderBy('tickets.id', 'desc')
                ->orderBy('tickets.status', 'asc')
                ->get();
        } elseif ($dept_login == '34' && $user_ca == '1') {
            $tickets_subquery = DB::table('tickets')
                ->select(
                    'tickets.*',
                    'customers.company_name',
                    'c.name_incident as name_incident',
                    'd.name_incident as final_name_incident',
                    'a.department_name',
                    'a.department_name as dept_assign',
                    'b.department_name as dept_created',
                    'selfcares.no_request as no_request'
                )
                ->where('a.department_id',  $dept_login)  //dept yang di assign
                ->orwhere('b.department_id',  $dept_login)  //dept yg created
                //->whereBetween('tickets.created_at',[$date_start2,$date_now])
                ->leftJoin('logs_tickets', function ($join) {
                    $join->on('tickets.id', '=', 'logs_tickets.id_ticket');
                    $join->where('logs_tickets.assign_status', '=', '1');
                })
                ->leftjoin('employee',  'tickets.created_by',  '=',  'employee.int_emp_id')
                ->leftJoin('department as a',  'logs_tickets.assign_to_dept',  '=',  'a.department_id')
                ->leftJoin('department as b',  'employee.int_emp_department',  '=',  'b.department_id')
                ->leftJoin('logs',  'tickets.id',  '=',  'logs.id_ticket')
                ->leftJoin('customers',  'tickets.cid',  '=',  'customers.cid')
                ->leftJoin('mapping_incident as c',  'tickets.type_incident',  '=',  'c.id')
                ->leftJoin('mapping_incident as d',  'tickets.final_type_incident',  '=',  'd.id')
                ->leftJoin('selfcares',  'tickets.no_ticket',  '=',  'selfcares.no_ticket')
                ->groupBy('tickets.id')
                ->orderBy('tickets.status',  'asc')
                ->orderBy('tickets.created_date',  'desc')
                ->orderBy('tickets.id',  'desc')
                ->orderBy('tickets.status',  'asc');  //gak perlu di get, get nya di query utama

            //masukkan sub query ke query utama
            $tickets = DB::table(DB::raw("({$tickets_subquery->toSql()}) as sub"))
                ->mergeBindings($tickets_subquery) // you need to get underlying Query Builder
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$date_start2, $date_now])
                ->get();
        } elseif ($dept_login == '34' && $user_ca != '1') {
            $tickets_subquery = DB::table('tickets')
                ->select(
                    'tickets.*',
                    'customers.company_name',
                    'c.name_incident as name_incident',
                    'd.name_incident as final_name_incident',
                    'a.department_name',
                    'a.department_name as dept_assign',
                    'b.department_name as dept_created',
                    'selfcares.no_request as no_request'
                )
                ->where('logs_tickets.for_ca',  0)  //ticket for_ca == 0
                ->where('a.department_id',  $dept_login)  //dept yang di assign
                ->orwhere('b.department_id',  $dept_login)  //dept yg created
                //->whereBetween('tickets.created_at',[$date_start2,$date_now])
                ->leftJoin('logs_tickets', function ($join) {
                    $join->on('tickets.id', '=', 'logs_tickets.id_ticket');
                    $join->where('logs_tickets.assign_status', '=', '1');
                })
                ->leftjoin('employee',  'tickets.created_by',  '=',  'employee.int_emp_id')
                ->leftJoin('department as a',  'logs_tickets.assign_to_dept',  '=',  'a.department_id')
                ->leftJoin('department as b',  'employee.int_emp_department',  '=',  'b.department_id')
                ->leftJoin('logs',  'tickets.id',  '=',  'logs.id_ticket')
                ->leftJoin('customers',  'tickets.cid',  '=',  'customers.cid')
                ->leftJoin('mapping_incident as c',  'tickets.type_incident',  '=',  'c.id')
                ->leftJoin('mapping_incident as d',  'tickets.final_type_incident',  '=',  'd.id')
                ->leftJoin('selfcares',  'tickets.no_ticket',  '=',  'selfcares.no_ticket')
                ->groupBy('tickets.id')
                ->orderBy('tickets.status',  'asc')
                ->orderBy('tickets.created_date',  'desc')
                ->orderBy('tickets.id',  'desc')
                ->orderBy('tickets.status',  'asc');  //gak perlu di get, get nya di query utama

            //masukkan sub query ke query utama
            $tickets = DB::table(DB::raw("({$tickets_subquery->toSql()}) as sub"))
                ->mergeBindings($tickets_subquery) // you need to get underlying Query Builder
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$date_start2, $date_now])
                ->get();
        } else {
            // dd('hai');
            $tickets1 = DB::table('tickets')
                ->select('tickets.*', 'customers.company_name', 'c.name_incident as name_incident', 'd.name_incident as final_name_incident', 'a.department_name', 'a.department_name as dept_assign', 'b.department_name as dept_created', 'selfcares.no_request as no_request')
                ->leftJoin('logs_tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'logs_tickets.assign_to_dept', '=', 'a.department_id')
                ->leftJoin('department as b', 'employee.int_emp_department', '=', 'b.department_id')
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('selfcares', 'tickets.no_ticket', '=', 'selfcares.no_ticket')
                ->where(function ($query) use ($dept_login) {
                    $query->where('a.department_id', $dept_login) //dept yang di assign
                        ->orWhere('b.department_id', $dept_login); //dept yg created
                })
                //->where('logs_tickets.assign_status', 1)
                ->whereBetween('tickets.created_date', [$date_start2, $date_now])
                ->groupBy('tickets.id')
                ->orderBy('tickets.status', 'asc')
                ->orderBy('tickets.id', 'desc')
                ->get();


            $tickets2 = DB::table('tickets')
                ->select('tickets.*', 'customers.company_name', 'c.name_incident as name_incident', 'd.name_incident as final_name_incident', 'a.department_name', 'a.department_name as dept_assign', 'b.department_name as dept_created', 'selfcares.no_request as no_request')
                ->leftJoin('logs_tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'logs_tickets.assign_to_dept', '=', 'a.department_id')
                ->leftJoin('department as b', 'employee.int_emp_department', '=', 'b.department_id')
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('selfcares', 'tickets.no_ticket', '=', 'selfcares.no_ticket')
                ->where('tickets.ticket_type', '=', 'External')
                ->whereBetween('tickets.created_date', [$date_start2, $date_now])
                ->groupBy('tickets.id')
                ->orderBy('tickets.status', 'asc')
                ->orderBy('tickets.id', 'desc')
                ->get();

            $tickets = $tickets1->union($tickets2);
        }



        $dropdowns['ticket_type'] = DB::table('tickets')
            ->select('ticket_type')
            ->groupBy('ticket_type')
            ->orderBy('ticket_type', 'asc')
            ->get();


        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Ticket Tracking';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('index', compact('tickets', 'dropdowns', 'date_start2', 'date_now'));
    }

    //index summary
    public function indexSum(Request $request, $summary, $category, $from, $until)
    {
        //dd($summary,$category,$from,$until);

        $now = Carbon::now();
        $date_now = $until;
        $date_start2 = $from;

        //dd($date_last2);
        $dept_login = auth()->user()->id_department;

        $query = DB::table('tickets')
            ->select(
                'tickets.*',
                'customers.company_name',
                'c.name_incident as name_incident',
                'd.name_incident as final_name_incident',
                'a.department_name',
                'a.department_name as dept_assign',
                'b.department_name as dept_created',
                'e.no_request as no_request'
            )
            ->where('tickets.category', $category)
            ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$date_start2, $date_now])
            ->leftJoin('logs_tickets', function ($join) {
                $join->on('logs_tickets.id_ticket', '=', 'tickets.id');
                $join->where('logs_tickets.assign_status', '=', '1');
            })
            ->leftjoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
            ->leftJoin('department as a', 'logs_tickets.assign_to_dept', '=', 'a.department_id')
            ->leftJoin('department as b', 'employee.int_emp_department', '=', 'b.department_id')
            ->leftJoin('logs', 'tickets.id', '=', 'logs.id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
            ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
            ->leftJoin('selfcares as e', 'tickets.no_ticket', '=', 'e.no_ticket')
            ->groupBy('tickets.id')
            ->orderBy('tickets.status', 'asc')
            ->orderBy('tickets.created_date', 'desc')
            ->orderBy('tickets.id', 'desc')
            ->orderBy('tickets.status', 'asc');


        //untuk dapatin jenis summary
        if ($summary == 'progress') {
            $query = $query->where('tickets.status', '1');
        }

        if ($summary == 'hold') {
            $query = $query->where('tickets.status', '3');
        }

        if ($summary == 'unassign') {
            $query = $query->where('tickets.status', '2');
        }

        if ($summary == 'closed') {
            $query = $query->where('tickets.status', '4');
        }

        $tickets = $query->get();

        // $dropdowns = array();
        $dropdowns['ticket_type'] = DB::table('tickets')
            ->select('ticket_type')
            ->groupBy('ticket_type')
            ->orderBy('ticket_type', 'asc')
            ->get();


        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Ticket Tracking';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('index_summary', compact('tickets', 'dropdowns', 'date_start2', 'date_now', 'summary', 'category', 'from', 'until'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dropdowns = array();

        $dropdowns['status link respond'] = DB::table('dropdowns')
            ->where('category', '=', 'status link respond')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['request'] = DB::table('dropdowns')
            ->where('category', '=', 'request')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['category ticket'] = DB::table('dropdowns')
            ->where('category', 'Classification Ticket')
            ->where('name_value', '<>', 'Maintenance')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['dept'] = DB::table("department")
            ->orderBy('department_name', 'asc')
            ->get();

        //dd($dropdowns);

        return view('ticket_external.create', compact('dropdowns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'cid' => 'required',
            'request_by'  => 'required',
            'link_respond' => 'required',
            'sub_link_respond' => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'department' => 'required',
            'message' => 'required',
            'doc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc2' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc3' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc4' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'doc5' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
            'req_date' => 'required|date|before_or_equal:today',
            'req_time' => 'required'
        ]);

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $ticket_status = '1';
        $hold_status = '0';
        $req_time = $request->req_date . " " . $request->req_time;

        //mendapatkan duedate
        $rule_query = Rule::where('rule_name', 'due date')->get();
        $due_hours = $rule_query[0]->rule_value;
        $duedate = Carbon::parse($created_date)->addHours($due_hours);
        //end mendapatkan duedate

        //checkbox for_ca
        $for_ca = $request->input('for_ca') == '1' ? 1 : 0;

        //insert into table tickets
        $ticket_create = Ticket::create([
            'ticket_type' => 'External',
            'cid' => $request->cid,
            'multiple_cid' => $request->multiple,
            'priority' => $request->priority,
            'status_complain' => $request->status_complain,
            'request' => $request->request_by,
            'status_link_respond' => $request->link_respond,
            'sub_status_link_respond' => $request->sub_link_respond,
            'category' => $request->category,
            'type_incident' => $request->sub_category,
            'created_by' => $created_by,
            'created_date' => $created_date,
            'status' => $ticket_status,
            'remarks' => $request->message,
            'due_date' => $duedate,
            'hold_status' => $hold_status,
            'request_at' => $req_time
        ]);
        //end insert into table tickets

        if ($ticket_create) {
            $id = $ticket_create->id;

            //upload file
            if ($request->file('doc1')) {
                $extension1 = $request->file('doc1')->getClientOriginalExtension();
                $doc_name1 = $id . '_a.' . $extension1;
                $store1 = $request->file('doc1')->storeAs('attachment_tickets', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            if ($request->file('doc2')) {
                $extension2 = $request->file('doc2')->getClientOriginalExtension();
                $doc_name2 = $id . '_b.' . $extension2;
                $store2 = $request->file('doc2')->storeAs('attachment_tickets', $doc_name2);
            } else {
                $doc_name2 = "";
            }

            if ($request->file('doc3')) {
                $extension3 = $request->file('doc3')->getClientOriginalExtension();
                $doc_name3 = $id . '_c.' . $extension3;
                $store3 = $request->file('doc3')->storeAs('attachment_tickets', $doc_name3);
            } else {
                $doc_name3 = "";
            }

            if ($request->file('doc4')) {
                $extension4 = $request->file('doc4')->getClientOriginalExtension();
                $doc_name4 = $id . '_d.' . $extension4;
                $store4 = $request->file('doc4')->storeAs('attachment_tickets', $doc_name4);
            } else {
                $doc_name4 = "";
            }

            if ($request->file('doc5')) {
                $extension5 = $request->file('doc5')->getClientOriginalExtension();
                $doc_name5 = $id . '_e.' . $extension5;
                $store5 = $request->file('doc5')->storeAs('attachment_tickets', $doc_name5);
            } else {
                $doc_name5 = "";
            }

            $insert_filename = Ticket::where('id', $id)
                ->update([
                    'file_1' => $doc_name1,
                    'file_2' => $doc_name2,
                    'file_3' => $doc_name3,
                    'file_4' => $doc_name4,
                    'file_5' => $doc_name5
                ]);
            //end upload file

            //nomor otomatis

            //format tanggal
            $period_ticket = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = Ticket::whereDay('created_date', $created_date)
                ->whereMonth('created_date', $created_date)
                ->whereYear('created_date', $created_date)
                ->where('ticket_type', 'External')
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noticket = "E/" . $period_ticket . "/" . $lastno;

            $create_noticket = Ticket::where('id', $id)
                ->update([
                    'no_urut' => $no_urut,
                    'no_ticket' => $noticket
                ]);
            //end nomor otomatis

            //insert log success create ticket
            $desc_success_create = 'Success Created Ticket';

            $log_history = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_create,
                'message'   => $request->message
            ]);
            //insert log success create ticket

            //insert log success assign ticket
            $desc_success_assign = 'Success Assign Ticket';
            $assign_dept = Department::where('department_id', $request->department)
                ->get();
            $message_assign = 'Assign To: ' . $assign_dept[0]->department_name;

            //dd();
            $log_assign = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $id,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $request->department,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status,
                'for_ca' => $for_ca
            ]);
            //end insert logs_ticket (log assign)

            if ($create_noticket && $log_history && $log_ticket &&  $log_assign) {

                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Ticket';

                //dd($location);
                $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

                //initial InternalMail
                $subcategory = Ticket::select('name_incident')
                    ->where('type_incident', $request->sub_category)
                    ->leftJoin('mapping_incident as incident', 'tickets.type_incident', '=', 'incident.id')
                    ->first()->name_incident;

                $created = User::select('department_name', 'department_emails.email as deptmail')
                    ->where('users.email', $username)
                    ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                    ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                    ->first();

                // jangan lupa ini aktifin pas mau production guys
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;

                $assigndept = Department::select('department_name', 'email')
                    ->where('department_id', $request->department)
                    ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                    ->first()->department_name;

                // jangan lupa ini aktifin pas mau production guys (kondisi checbox for_ca)
                $mailDept = $for_ca ? 9999 : $request->department;

                $assignMail = DepartmentEmail::select('email')
                ->where('id_department', $mailDept)
                ->first()->email;

                // nah kalo yang example ini matiin ya pas mau production
                // $createdMail = 'hardy.prabowo@napinfo.co.id'; // example
                // $assignMail = 'enquity.ekayekti@napinfo.co.id';

                $data = [
                    'no_ticket' => $noticket,
                    'cust_name' => $request->company_name,
                    'category' => 'External',
                    'subcategory' => $subcategory,
                    'status_complain' => $request->status_complain,
                    'link_respond' => $request->link_respond,
                    'notes' => $request->message,
                    'created_by' => $createddept,
                    'assign_to' => $assigndept,
                    'receipt' => [
                        'createdMail' => $createdMail,
                        'assignMail' => $assignMail,
                    ]
                ];
                // dd($data);
                Mail::to($assignMail)->send(new ExternalMail($data));

                return redirect('/summary/matrix')->with('status',  'Success Create Ticket !');
            } else {
                return redirect('/summary/matrix')->with('status', 'Error Create Log !');
            }
        } else {
            return redirect('/summary/matrix')->with('status', 'Error Create Ticket !');
        }
    }

    public function storeSelfcare(Request $request)
    {
        //dd($request->cid);
        $request->validate([
            'cid' => 'required',
            'link_respond' => 'required',
            'sub_link_respond' => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'department' => 'required',
            'message' => 'required',
        ]);

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $ticket_status = '1';
        $hold_status = '0';
        $req_time = $request->req_date . " " . $request->req_time;

        //mendapatkan duedate
        $rule_query = Rule::where('rule_name', 'due date')->get();
        $due_hours = $rule_query[0]->rule_value;
        $duedate = Carbon::parse($created_date)->addHours($due_hours);
        //end mendapatkan duedate

        //insert into table tickets
        $ticket_create = Ticket::create([
            'ticket_type' => 'External',
            'cid' => $request->cid,
            'multiple_cid' => $request->multiple,
            'priority' => $request->priority,
            'status_complain' => $request->status_complain,
            'request' => 'Selfcare',
            'status_link_respond' => $request->link_respond,
            'sub_status_link_respond' => $request->sub_link_respond,
            'category' => $request->category,
            'type_incident' => $request->sub_category,
            'created_by' => $created_by,
            'created_date' => $created_date,
            'status' => $ticket_status,
            'remarks' => $request->message,
            'due_date' => $duedate,
            'hold_status' => $hold_status,
            'request_at' => $request->reporting_time
        ]);
        //end insert into table tickets

        if ($ticket_create) {
            $id = $ticket_create->id;

            //nomor otomatis

            //format tanggal
            $period_ticket = $created_date->format('ymd');

            //no urut akhir
            $searchlastno = Ticket::whereDay('created_date', $created_date)
                ->where('ticket_type', 'External')
                ->max('no_urut');

            $no = 1;
            if ($searchlastno) {
                $lastno = sprintf("%04s", abs($searchlastno + 1));
                $no_urut = abs($searchlastno + 1);
            } else {
                $lastno = sprintf("%04s", abs($no));
                $no_urut = 1;
            }

            $noticket = "E/" . $period_ticket . "/" . $lastno;

            $create_noticket = Ticket::where('id', $id)
                ->update([
                    'no_urut' => $no_urut,
                    'no_ticket' => $noticket
                ]);
            //end nomor otomatis

            //update status Selfcare Table
            $updateSelfcare = Selfcare::where('id', $request->req_id)
                ->update([
                    'no_ticket' => $noticket,
                    'status_request' => '1',
                ]);
            //end update status Selfcare Table

            //insert log success create ticket
            $desc_success_create = 'Success Created Ticket';

            $log_history = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_create,
                'message'   => $request->message
            ]);
            //insert log success create ticket

            //insert log success assign ticket
            $desc_success_assign = 'Success Assign Ticket';
            $assign_dept = Department::where('department_id', $request->department)
                ->get();
            $message_assign = 'Assign To: ' . $assign_dept[0]->department_name;

            //dd();
            $log_assign = log::create([
                'id_ticket' => $id,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $id,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $request->department,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status
            ]);
            //end insert logs_ticket (log assign)

            if ($create_noticket && $log_history && $log_ticket &&  $log_assign) {

                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Ticket';

                //dd($location);
                $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

                return redirect('/summary/matrix')->with('status',  'Success Create Ticket !');
            } else {
                return redirect('/summary/matrix')->with('status', 'Error Create Log !');
            }
        } else {
            return redirect('/summary/matrix')->with('status', 'Error Create Ticket !');
        }
    }

    public function showtest($id)
    {
        echo "hai";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket, Request $request)
    {
        // dd($ticket->no_ticket);
        // $dropdowns['solved_by'] = DB::table('dropdowns')
        //     ->where('category', '=', 'Solved By')
        //     ->orderBy('name_value', 'asc')
        //     ->get();

        $dropdowns['responsible'] = DB::table('dropdowns')
            ->where('category', '=', 'Responsible')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['dept'] = DB::table("department")
            ->orderBy('department_name', 'asc')
            ->get();

        $dropdowns['hold'] = DB::table('dropdowns')
            ->where('category', '=', 'Hold')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['close'] = DB::table('dropdowns')
            ->where('category', '=', 'Close')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['outage reason'] = DB::table('dropdowns')
            ->where('category', '=', 'Outage Type')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['status link respond'] = DB::table('dropdowns')
            ->where('category', '=', 'status link respond')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['final category'] = DB::table('dropdowns')
            ->where('category', '=', 'Final Category')
            ->where('name_value', '<>', 'Maintenance')
            ->orderBy('name_value', 'asc')
            ->get();

        $dropdowns['category ticket'] = DB::table('dropdowns')
            ->where('category', '=', 'Classification Ticket')
            ->where('name_value', '<>', 'Maintenance')
            ->orderBy('name_value', 'asc')
            ->get();

        $tickets = Ticket::where('tickets.id', $ticket->id)
            ->select('*', 'incident.id as id_mapping', 'tickets.id as id_ticket')
            ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
            ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
            ->leftJoin('mapping_incident as incident', 'tickets.final_type_incident', '=', 'incident.id')
            ->get();

        $log_holds = LogHold::where('log_holds.id_ticket', $ticket->id)
            ->select('*')
            ->orderBy('log_holds.id', 'desc')
            ->get();
        // dd($log_holds);
        $assign_history = LogTicket::where('logs_tickets.id_ticket', '=', $ticket->id)
            ->leftJoin('logs', 'logs_tickets.id_log', '=', 'logs.id')
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
            ->orderBy('logs_tickets.assign_date', 'desc')
            ->get(['employee.int_emp_name', 'department.department_name', 'logs.message', 'logs_tickets.assign_date', 'logs_tickets.preclosed_status', 'logs_tickets.preclosed_date', 'logs_tickets.duration']);

        $comment_history = Log::where('id_ticket', $ticket->id)
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs.create_by')
            ->leftJoin('department', 'department.department_id', '=', 'employee.int_emp_department')
            ->groupBy('logs.id')
            ->orderBy('logs.created_at', 'desc')
            ->get(['logs.created_at', 'logs.message', 'logs.attachment_1', 'employee.int_emp_name', 'department.department_name', 'logs.description']);

        $return = LogTicket::where('id_ticket', $ticket->id)
            ->where('assign_status', '1')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $reassign = LogTicket::where('id_ticket', $ticket->id)
            ->where('assign_status', '1')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $preclosed = LogTicket::where('id_ticket', $ticket->id)
            ->where('assign_status', '1')
            ->where('preclosed_status', '0')
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'logs_tickets.assign_by')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $close_ticket = LogTicket::where('id_ticket', $ticket->id)
            ->where('preclosed_status', '0')
            ->leftJoin('department', 'department.department_id', '=', 'logs_tickets.assign_to_dept')
            ->get();

        $edit_rfo_head = DB::table('users')
            ->where('id_employee', '=', auth()->user()->id_employee)
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'users.id_employee')
            ->leftJoin('department', 'department.department_id', '=', 'users.id_department')
            ->get();

        $edit_rfo_staff = DB::table('users')
            ->where('id_employee', '=', auth()->user()->id_employee)
            ->leftJoin('employee', 'employee.int_emp_id', '=', 'users.id_employee')
            ->leftJoin('department', 'department.department_id', '=', 'users.id_department')
            ->get();

        //call url for api Retention call
        $rule = Rule::where('rule_name', 'API Retention Call Response')->first();
        $url = $rule->rule_value;
        $url = $url . $ticket->no_ticket;


        // Get Response API Retention call
        $response = Http::get($url);
        $success = $response['success'];

        // If Response True
        if ($success == true) {
            // Get List Outstanding
            $datacall = $response['detail call'];
            $datacall = $datacall[0];
        } else {
            $datacall = '0';
        }

        $id_login = auth()->user()->id_employee; // ini id login
        $role_user = auth()->user()->role; // ini role user


        $creator_ticket = $tickets[0]->created_by;
        $ticket_status = $tickets[0]->status;
        $id_login_department = auth()->user()->id_department;

        if ($ticket_status == '1' && $id_login_department == '1') {  // id create_by ticket == id login
            $buttonHold = '1';
        } else if ($ticket_status == '3') { // tidak peduli siapa yang login kalo status 3 gamuncul
            $buttonHold = '0';
        } else {
            $buttonHold = '0';
        }

        // kalo ticket di hold dan yang akses divisi custcare
        if ($ticket_status == '3' && $id_login_department == '1') {
            $buttonUnhold = '1';
        } else {
            $buttonUnhold = '0';
        }
        // dd($buttonUnhold,$ticket_status);

        $check_row_preclose = count($close_ticket);

        if ($role_user == 'Customer Care' && $check_row_preclose == '0' && $ticket_status != '4' && $ticket_status != '3') {
            $buttonClosed = '1';
        } else {
            $buttonClosed = '0';
        }

        $id_login_department = auth()->user()->id_department;
        $role_user_login = auth()->user()->role;
        $id_login = auth()->user()->id_employee; // ini id login
        $pic_status = auth()->user()->pic; // ini id login
        $check_count_return = count($return);

        if ($check_count_return < 1) {
            $cek_department_return = '0';
        } else {
            $cek_department_return = $return[0]->int_emp_department;
        }

        // dd($id_login_department,$cek_department_assign,$pic_status);
        // if kalo bukan department custcare dan pic bisa return ticket

        if (($id_login_department == $cek_department_return) && ($pic_status == '1' && $return[0]->preclosed_status == '0')) { // kalo di department lain dia cuman orang yang diassign
            $buttonReturn = 1;
        }
        // fungsi else if untuk department custcare dan bukan pic bisa return with case baru 2 divisi misal custcare ke sysdev
        else if (($id_login_department == '1' && $id_login_department == $cek_department_return && $return[0]->preclosed_status == '0')) { // case kalo department = custcare dia tampilin ke seluruh castcare, apakah custcare perlu ada pic staff nya
            $buttonReturn = 2;
        } else {
            $buttonReturn = 0;
        }

        // dd($buttonReturn);

        $check_count_reassign = count($reassign);
        // dd($check_count_reassign);

        if ($check_count_reassign < 1) {
            $cek_department_reassign = '0';
            $cek_preclosed_status = 'NULL';
        } else {
            $cek_department_reassign = $reassign[0]->assign_to_dept;
            $cek_preclosed_status = $reassign[0]->preclosed_status;
        }

        // kalo department yang diassign cocok dengan department user login, orang dari department yang diassign harus pic, dan tombol reassign muncul setelah dipreclosed / job selesai
        // case normal apabila reassign tapi row masih > 1
        if ($id_login_department == $cek_department_reassign && $pic_status == '1' && $cek_preclosed_status == '1' && $ticket_status != '4') {
            $buttonReassign = 1;
        }
        // case kalo row gaada
        else if ($cek_department_reassign == '0' && $cek_preclosed_status == 'NULL' && $ticket_status != '4') {
            $buttonReassign = 2;
        }
        // case kalo unassigned gimana, buat sekarang belom bisa karena error pas return ticket
        else {
            $buttonReassign = 0;
        }

        // dd($buttonReassign,$id_login_department,$cek_department_reassign,$pic_status,$cek_preclosed_status,$ticket_status);

        $check_count_preclosed = count($preclosed);

        // kalo gaada row
        if ($check_count_preclosed < 1) {
            $cek_department_preclosed = 'NULL';
        }
        // ada row
        else {
            $cek_department_preclosed = $preclosed[0]->assign_to_dept;
        }

        if ($id_login_department ==  $cek_department_preclosed && $pic_status == '1') {
            $buttonPreclose = 1;
        }
        // kalo misalkan case creator ticket udah return ticket tetapi belum reassign, tombol preclose hilang
        else {
            $buttonPreclose = 0;
        }

        if ($id_login_department == '1' && $edit_rfo_head[0]->int_emp_position == 'Customer Care Dept Head | Dept Head Property & Partnership (Pjs)' && $ticket_status == '4') {
            $buttonEditRfoHead = 1;
        } else {
            $buttonEditRfoHead = 0;
        }

        if ($id_login_department == '1' && $edit_rfo_staff[0]->int_emp_position != 'Customer Care Dept Head | Dept Head Property & Partnership (Pjs)' && $ticket_status == '4') {
            $buttonEditRfoStaff = 1;
        } else {
            $buttonEditRfoStaff = 0;
        }

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Detail Ticket';

        //dd($location);
        $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

        return view('ticket_external.show', compact('ticket', 'tickets', 'comment_history', 'dropdowns', 'return', 'buttonHold', 'buttonUnhold', 'buttonClosed', 'buttonReturn', 'buttonReassign', 'buttonPreclose', 'assign_history', 'buttonEditRfoHead', 'buttonEditRfoStaff', 'log_holds', 'datacall'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }

    public function addComment(Request $request)
    {
        // 'nama object'
        $request->validate([
            'id_ticket' => 'required',
            'message_comment'  => 'required',
            'attc1' => 'mimes:pdf,xls,xlsx,png,jpg,jpeg,doc,docx,gif|max:10048',
        ]);

        $description_comment =  'Success Create Comment';

        $comment_creator = auth()->user()->id_employee;

        // ' nama coloumn table'
        $comment_create =  Log::create([
            'id_ticket' => $request->id_ticket,
            'create_by' => $comment_creator,
            'message' => $request->message_comment,
            // 'attachment_1' => $request->attc1,
            'description' => $description_comment
        ]);

        // dd($comment_create);

        if ($comment_create) {
            // generate id comment
            $id_comment = $comment_create->id;

            //upload file
            if ($request->file('attc1')) {
                $extension1 = $request->file('attc1')->getClientOriginalExtension();
                $doc_name1 = $id_comment . '_comment.' . $extension1;
                $store1 = $request->file('attc1')->storeAs('attachment_comment', $doc_name1);
            } else {
                $doc_name1 = "";
            }

            // 'nama coloumn table' <- insert attachment ke table logs
            $insert_log = Log::where('id', $id_comment)
                ->update([
                    'attachment_1' => $doc_name1
                ]);

            // $date_submit=$comment_create->created_at;
            // $created_date=Carbon::now();

            // dd($date_submit - $created_date);

            if ($comment_create && $insert_log != NULL || $comment_create && $insert_log == NULL) {
                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Create Comment';

                //dd($location);
                $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

                return redirect('/tickets/'  . $comment_create->id_ticket)->with('status',  'Success Create Comment !');
            } else {
                return redirect('/tickets/'  . $comment_create->id_ticket)->with('status',  'Fail Create Comment !');
            }
        } else {
            return redirect('/tickets/' . $comment_create->id_ticket)->with('status', 'Fail Create Comment !');
        }
    }

    public function download($id, Request $request)
    {
        //dd($id);
        // $ticket = Ticket::where('id', $id)->firstOrFail();
        //dd($ticket);
        $pathToFile_1 = storage_path('app/attachment_tickets/' .  $id);
        // $pathToFile_2 = storage_path('app/attachment_tickets/' .$ticket->file_2);
        // $pathToFile_3 = storage_path('app/attachment_tickets/' .$ticket->file_3);
        // $pathToFile_4 = storage_path('app/attachment_tickets/' .$ticket->file_4);
        // $pathToFile_5 = storage_path('app/attachment_tickets/' .$ticket->file_5);
        // return response()->download([$pathToFile_1, $pathToFile_2, $pathToFile_3, $pathToFile_4, $pathToFile_5]);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File';

        //dd($location);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);
        return response()->download($pathToFile_1);
    }

    public function downloadFileComment(Request $request,  $id)
    {
        $pathToFileComment_1 = storage_path('app/attachment_comment/' . $id);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Download File Comment';

        //dd($location);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);
        return response()->download($pathToFileComment_1);
    }

    public function reassign(Request $request)
    {
        $request->validate([
            'id_ticket' => 'required',
            'department'  => 'required',
            'message'   => 'required'
        ]);

        //dd($request);
        //cek dulu apakah dept dan staff yg dipilih sama
        $cek_ticket = Ticket::where('tickets.id', $request->id_ticket)
            ->leftJoin('logs_tickets', function ($join) {
                $join->on('logs_tickets.id_ticket', '=', 'tickets.id');
                $join->where('logs_tickets.assign_status', '=', '1');
            })
            ->leftJoin('department', 'logs_tickets.assign_to_dept', '=', 'department.department_id')
            ->get();
        //dd($cek_ticket);

        if ($request->department == $cek_ticket[0]->assign_to_dept) {
            return redirect('/tickets/' . $request->id_ticket)->with('status', 'Failed Re-Assign Ticket, Ticket has been assigned to this Department');
        } else {
            $created_by = auth()->user()->id_employee;
            $created_date = Carbon::now();

            //update ticket status
            //cek status ticket dulu kalau posisinya hold maka tidak mengubah status
            if ($request->status_ticket == '3') {
                $ticket_status = '3';
            } else {
                $ticket_status = '1';
            }

            $update_ticket = Ticket::where('id', $request->id_ticket)
                ->update([
                    'status'    => $ticket_status
                ]);

            //insert log success assign ticket
            $desc_success_assign = 'Success Re-Assign Ticket';
            $assign_dept = Department::where('department_id', $request->department)
                ->get();
            $message_assign = 'Assign To: ' . $assign_dept[0]->department_name . ' (' . $request->message . ')';

            $log_assign = log::create([
                'id_ticket' => $request->id_ticket,
                'create_by' => $created_by,
                'description' => $desc_success_assign,
                'message'   => $message_assign
            ]);
            //insert log success assign ticket

            //update assign status semua jadi nol berdasarkan id_ticket
            $assign_status = '0';
            LogTicket::where('id_ticket', $request->id_ticket)
                ->update([
                    'assign_status' => $assign_status
                ]);
            //end update assign status nol

            //insert logs_ticket (log assign)
            $id_log = $log_assign->id;
            $preclosed_status = '0';
            $assign_status = '1';

            $log_ticket = LogTicket::create([
                'id_ticket' => $request->id_ticket,
                'id_log' => $id_log,
                'assign_by' => $created_by,
                'assign_to_dept' => $request->department,
                'assign_date' => $created_date,
                'assign_status' => $assign_status,
                'preclosed_status' => $preclosed_status,
                'for_ca' => $request->for_ca
            ]);

            // dd($log_ticket);
            //end insert logs_ticket (log assign)

            if ($log_assign && $log_ticket && $update_ticket) {

                //Audit Log
                $username = auth()->user()->email;
                $ipAddress = $_SERVER['REMOTE_ADDR'];
                $location = '0';
                $access_from = Browser::browserName();
                $activity = 'Reassign Ticket';

                //dd($activity);
                $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

                // initial ReassignMail
                $noticket = Ticket::where('id', $log_ticket->id_ticket)->first();
                $company_name = Customer::where('cid', $noticket->cid)->first();
                $subcategory = MappingIncident::where('id', $noticket->type_incident)->first();

                $created = User::select('department_name', 'department_emails.email as deptmail')
                    ->where('users.email', $username)
                    ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                    ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                    ->first();

                // jangan lupa ini aktifin pas mau production guys
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;

                $assigndept = Department::select('department_name', 'email')
                    ->where('department_id', $request->department)
                    ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                    ->first()->department_name;

                // jangan lupa ini aktifin pas mau production guys (kondisi checbox for_ca)
                $mailDept = $request->for_ca ? 9999 : $request->department;

                $assignMail = DepartmentEmail::select('email')
                ->where('id_department', $mailDept)
                ->first()->email;

                // nah kalo yang example ini matiin ya pas mau production
                // $createdMail = 'enquity.ekayekti@napinfo.co.id'; // example
                // $assignMail = 'hardy.prabowo@napinfo.co.id';

                $data = [
                    'no_ticket' => $noticket->no_ticket,
                    'cust_name' => $company_name->company_name,
                    'category' => 'External',
                    'subcategory' => $subcategory->name_incident,
                    'status_complain' => $noticket->status_complain,
                    'link_respond' => $noticket->status_link_respond,
                    'notes' => $noticket->remarks,
                    'message' => $request->message,
                    'created_by' => $createddept,
                    'assign_to' => $assigndept,
                    'receipt' => [
                        'createdMail' => $createdMail,
                        'assignMail' => $assignMail,
                    ]
                ];
                // dd($data);
                Mail::to($assignMail)->send(new AssignMail($data));

                return redirect('/tickets/' . $request->id_ticket)->with('status', 'Success Re-Assign Ticket !');
            } else {
                return redirect('/tickets/' . $request->id_ticket)->with('status', 'Failed Re-Assign Ticket !');
            }
        }
    }

    public function returnTicket(Request $request,  $id)
    {
        //dd($id);
        $request->validate([
            'message' => 'required',
        ]);

        //dd($request);
        $delete_logticket = LogTicket::destroy('id', $id);


        //di cek dulu kalau yg retur adalah creator seharusnya table logs_ticket bersih
        $cek_begin_assign = LogTicket::where('id_ticket', $request->id_ticket)->get();
        $count_row = count($cek_begin_assign);
        //dd($count_row);

        if ($count_row >= '1') {
            //cari row terakhir
            $cek_lastlogs_ticket = LogTicket::where('id_ticket', $request->id_ticket)
                ->orderby('id', 'desc')
                ->take(1)
                ->get();

            //dd($cek_lastlogs_ticket);

            $id_last = $cek_lastlogs_ticket[0]->id;

            //update assign_status jadi 1
            $update_status_assign = LogTicket::where('id', $id_last)
                ->update([
                    'assign_status' => '1',
                    'preclosed_status' => '0'
                ]);
        }

        //insert log success return ticket
        $desc_success_create = 'Success Return Ticket';
        $created_by = auth()->user()->id_employee;

        $log_history = log::create([
            'id_ticket' => $request->id_ticket,
            'create_by' => $created_by,
            'description' => $desc_success_create,
            'message'   => $request->message
        ]);
        //insert log success create ticket

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'Return Ticket';

        //dd($location);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

        return redirect('/tickets/' . $request->id_ticket)->with('status', 'Success Return Ticket !');
    }
    //test push ulang
    public function holdTicket(Request $request, $id)
    {
        $request->validate([
            'hold_date_from'  => 'required|date|before_or_equal:today',
            'hold_time_from' => 'required|date_format:H:i|before_or_equal:today',
            'hold_date'  => 'required|date|after_or_equal:today',
            'hold_time' => 'required',
            'message'   => 'required'
        ]);

        $created_by = auth()->user()->id_employee;
        $email = auth()->user()->email;
        $created_date = $request->hold_date_from . " " . $request->hold_time_from;
        $hold_date = $request->hold_date . " " . $request->hold_time;
        // dd($email);

        //update ticket status & hold status
        $ticket_status = '3';
        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'hold_by' => $created_by,
                'hold_created_date' => $created_date,
                'hold_date' => $hold_date,
                'hold_category' => $request->category_hold,
                'hold_message' => $request->message,
                'unhold_by' => '',
                'unhold_date' => 'null'
            ]);

        //insert log success hold ticket
        $desc_success_hold = 'Success Hold Ticket';

        $log_hold = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_hold,
            'message'   => $request->message
        ]);

        //insert log_Hold success hold ticket
        $holds_log = LogHold::create([
            'id_ticket' => $id,
            'hold_by' => $email,
            'hold_created_date' => $created_date,
            'hold_date_until' => $hold_date,
            'hold_category' => $request->category_hold,
            'hold_message' => $request->message
        ]);

        if ($update_ticket && $log_hold && $holds_log) {
            //Audit Log
            Notification::route(MicrosoftTeamsChannel::class, null)
                ->notify(new CreateHoldNotif());

            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Hold Ticket';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/tickets/' . $id)->with('status', 'Success Hold Ticket !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed Hold Ticket !');
        }
    }

    public function unholdTicket($id, $idHold)
    {
        $created_by = auth()->user()->id_employee;
        $email = auth()->user()->email;
        $created_date = Carbon::now();

        // ini validasi kalo duration log_hold minus

        $hold_created = LogHold::where('id', $idHold)->first();
        if ($created_date < $hold_created->hold_created_date) {
            return redirect('/tickets/' . $id)->with('statusError', 'Failed Unhold, Hold Created Time must be after Current Time');
        }
        // dd($idHold);

        //update ticket status & unhold status
        $ticket_status = '1';
        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'unhold_by' => $created_by,
                'unhold_date' => $created_date
            ]);

        //insert log success unhold ticket
        $desc_success_unhold = 'Success Unhold Ticket';
        $message_unhold = 'Unhold By: ' . auth()->user()->prefix_name;
        $log_unhold = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_unhold,
            'message' => $message_unhold,
        ]);


        //update log hold multiple unhold ticket
        $update_holds_log = LogHold::where('id', $idHold)
            ->update([
                'id_ticket' => $id,
                'unhold_by' => $email,
                'unhold_date' => $created_date
            ]);

        //duration HOLD
        $log_holds = LogHold::where('log_holds.id', $idHold)
            ->select('*')
            ->orderBy('log_holds.id', 'desc')
            ->get();


        $start = strtotime($log_holds[0]->hold_created_date);
        $end = strtotime($log_holds[0]->unhold_date);
        $duration = $end - $start;


        //uodate duration hold
        $update_holds_log = LogHold::where('id', $idHold)
            ->update([
                'duration' => $duration
            ]);


        if ($update_ticket && $log_unhold && $update_holds_log) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'UnHold Ticket';

            //dd($activity);
            $this->auditLogs($username, $ipAddress, $location, $access_from, $activity);

            return redirect('/tickets/' . $id)->with('status', 'Success unhold Ticket !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed unhold Ticket !');
        }
    }

    public function closeRequest(Request $request,  $id)
    {
        // dd($request);
        $request->validate([
            'cid'   => 'required',
            'ticket_date'   => 'required',
            'finish_date' => 'required|date|before_or_equal:today',
            'finish_time' => 'required',
            'message'   => 'required',
        ]);

        //dd($outage_reason);
        $created_by = auth()->user()->id_employee;
        $created_date = $request->req_time;
        $closed_date = Carbon::now();
        $start_date = $request->start_date . " " . $request->start_time;
        $resolved_date = $request->finish_date . " " . $request->finish_time;

        //dd($start_date);

        //update ticket status & hold status
        $ticket_status = '4';

        // get hold duration total
        $totals = LogHold::where('id_ticket', $id)
            ->select('duration')
            ->get()->toArray();
        $totalArray = [];
        foreach ($totals as $total) {
            $totalArray[] = $total['duration'];
        }
        $HoldTotal = array_sum($totalArray);

        //duration ticket - duration Holdtotal
        $start = strtotime($created_date);
        $end = strtotime($resolved_date);
        $duration = ($end - $start) - $HoldTotal;
        // dd($start, $end, $HoldTotal, $duration);

        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $hour = gmdate('H', $total_second);
        $temp_minutes = ($total_second % 3600 / (60));  // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $minute = gmdate('i', $total_second);
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = gmdate('s', $total_second);
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // end duration
        //end duration
        // bug fix sementara duration ticket total
        if ($minutes > 15) {
            $minutes_sementara = $minutes - 15;
        } else {
            $minutes_sementara = $minutes;
        }
        // dd($hours, $minutes, $second);

        //new Duration
        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'closed_by' => $created_by,
                'closed_date' => $closed_date,
                'start_date' => $created_date,
                'resolved_date' => $resolved_date,
                'closed_message' => $request->message,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
            ]);

        //update request selfcare status as closed
        $request_status = '3';

        if ($request->req_by == 'Selfcare') {
            //update status Selfcare Table
            $updateSelfcare = Selfcare::where('no_ticket', $request->no_ticket)
                ->update([
                    'status_request' => $request_status,
                ]);
            //end update status Selfcare Table
        }

        //insert log success hold ticket
        $desc_success_close = 'Success Close Ticket';

        $log_close = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_close,
            'message'   => $request->message
        ]);
        //insert log success hold ticket

        if ($update_ticket && $log_close) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Close Ticket';

            //dd($activity);
            $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

            // initial CloseMail
            $noticket = Ticket::where('id', $log_close->id_ticket)->first();
            $company_name = Customer::where('cid', $noticket->cid)->first();
            $subcategory = MappingIncident::where('id', $noticket->type_incident)->first();
            $ticketcreatedby = User::select('department_name', 'users.id_employee')
                ->where('users.id_employee', $noticket->created_by)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->first();

            $created = User::select('department_name', 'department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();

            // jangan lupa ini aktifin pas mau production guys
            $createdMail = $created->deptmail;
            $createddept = $created->department_name;

            // $assigndept = Department::select('department_name', 'email')
            //     ->where('department_id', $log_close->department)
            //     ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            //     ->first()->department_name;

            // jangan lupa ini aktifin pas mau production guys (kondisi checbox for_ca)
            // $mailDept = $for_ca ? 9999 : $request->department;

            // $assignMail = DepartmentEmail::select('email') 
            // ->where('id_department', $mailDept)
            // ->first()->email;

            // nah kalo yang example ini matiin ya pas mau production
            // $createdMail = 'enquity.ekayekti@napinfo.co.id'; // example
            // $assignMail = 'hardy.prabowo@napinfo.co.id';

            $data = [
                'no_ticket' => $noticket->no_ticket,
                'cust_name' => $company_name->company_name,
                'category' => $noticket->category,
                'subcategory' => $subcategory->name_incident,
                'status_complain' => $noticket->status_complain,
                'link_respond' => $noticket->status_link_respond,
                'created_by' => $createddept,
                'assign_to' => $ticketcreatedby->department_name,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                'closed_date' => $closed_date,
                'receipt' => [
                    'createdMail' => $createdMail,
                    // 'assignMail' => $assignMail,
                ]
            ];
            // dd($data);
            Mail::to($createdMail)->send(new ClosedMail($data));

            return redirect('/tickets/' . $id)->with('status', 'Success Closed Ticket !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed Closed Ticket !');
        }
    }

    public function close(Request $request,  $id)
    {
        // dd($request);
        // dd(implode(', ', $request->solved_by));
        $request->validate([
            'cid'   => 'required',
            'ticket_date'   => 'required',
            'finish_date' => 'required|date|before_or_equal:today',
            'finish_time' => 'required',
            'baseonrfo_hour' => 'required_with:baseonrfo_minute',
            'baseonrfo_minute' => 'required_with:baseonrfo_hour',
            'responsible' => 'required',
            'solved_by' => 'required|array',
            'outage_reason1' => 'required',
            'outage_sub1' => 'required',
            'action_close1' => 'required',
            // 'solved_by1' => 'required',
            'outage_loc' => 'required',
            'message'   => 'required',
        ]);

        $outage_reason1 = $request->outage_reason1;
        $outage_reason2 = $request->outage_reason2;
        // dd($outage_reason2);
        //$outage_reason
        //update status ticket
        if ($request->outage_reason2 == '') {
            $outage_reason = $request->outage_reason1;
            $outage_sub = $request->outage_sub1;
            $action_close = $request->action_close1;
            // $solved_by = $request->solved_by1;
        } else {
            $outage_reason = $outage_reason1 . " & " . $outage_reason2;
            $outage_sub = $request->outage_sub1 . " & " . $request->outage_sub2;
            $action_close = $request->action_close1 . " & " . $request->action_close2;
            // $solved_by = $request->solved_by1 . " & " . $request->solved_by2;
        }
        //dd($outage_reason);
        $created_by = auth()->user()->id_employee;
        $created_date = $request->req_time;
        $closed_date = Carbon::now();
        $start_date = $request->start_date . " " . $request->start_time;
        $resolved_date = $request->finish_date . " " . $request->finish_time;

        //dd($start_date);

        //update ticket status & hold status
        $ticket_status = '4';

        //nomor RFO

        //format tanggal
        $month_ticket = substr($created_date, 5, 2);
        $year_ticket = substr($created_date, 0, 4);


        if ($request->gen_rfo == '1') {
            $rfo = $id . '/NAP-INFO/' . $month_ticket . '/' . $year_ticket;
        } else {
            $rfo = 'NON RFO';
        }
        //end nomor RFO

        // get hold duration total
        $totals = LogHold::where('id_ticket', $id)
            ->select('duration')
            ->get()->toArray();
        $totalArray = [];
        foreach ($totals as $total) {
            $totalArray[] = $total['duration'];
        }
        $HoldTotal = array_sum($totalArray);

        //duration ticket - duration Holdtotal
        $start = strtotime($created_date);
        $end = strtotime($resolved_date);
        $duration = ($end - $start) - $HoldTotal;
        // dd($start, $end, $HoldTotal, $duration);

        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $hour = gmdate('H', $total_second);
        $temp_minutes = ($total_second % 3600 / (60));  // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $minute = gmdate('i', $total_second);
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = gmdate('s', $total_second);
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        // end duration
        //end duration
        // bug fix sementara duration ticket total
        if ($minutes > 15) {
            $minutes_sementara = $minutes - 15;
        } else {
            $minutes_sementara = $minutes;
        }
        // dd($hours, $minutes, $second);


        //final category
        if ($request->final_category == '') {
            $final_category = $request->category;
            $sub_final_category = $request->sub_category;
        } else {
            $final_category = $request->final_category;
            $sub_final_category = $request->sub_final_category;
        }

        //validasi array solvedby
        if (count($request->solved_by) !== count(array_unique($request->solved_by))) {
            return redirect('/tickets/' . $id)->with('statusError', 'Please Input Another Department (Solved By Cannot be Input With the Same Department) !');
        } else {
            $validate_solved_by = implode(', ', $request->solved_by);
        }

        //new Duration
        $update_ticket = Ticket::where('id', $id)
            ->update([
                'status' => $ticket_status,
                'closed_by' => $created_by,
                'closed_date' => $closed_date,
                'start_date' => $created_date,
                'resolved_date' => $resolved_date,
                'based_rfo_date' => $request->baseonrfo_hour . ' hours ' . $request->baseonrfo_minute . ' minutes ',
                'outage_reason' => $outage_reason,
                'outage_sub' => $outage_sub,
                'outage_location' => $request->outage_loc,
                'action_close' => $action_close,
                'responsible' => $request->responsible,
                'solved_by' => $validate_solved_by,
                'closed_message' => $request->message,
                'rfo' => $rfo,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                'final_category' => $final_category,
                'final_type_incident' => $sub_final_category
            ]);

        // dd($update_ticket);

        //update request selfcare status as closed
        $request_status = '3';

        if ($request->req_by == 'Selfcare') {
            //update status Selfcare Table
            $updateSelfcare = Selfcare::where('no_ticket', $request->no_ticket)
                ->update([
                    'status_request' => $request_status,
                ]);
            //end update status Selfcare Table
        }

        //insert log success hold ticket
        $desc_success_close = 'Success Close Ticket';

        $log_close = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_close,
            'message'   => $request->message
        ]);
        //insert log success hold ticket

        if ($update_ticket && $log_close) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Close Ticket';

            //dd($activity);
            $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

            // initial CloseMail
            $noticket = Ticket::where('id', $log_close->id_ticket)->first();
            $company_name = Customer::where('cid', $noticket->cid)->first();
            $subcategory = MappingIncident::where('id', $noticket->type_incident)->first();
            $ticketcreatedby = User::select('department_name', 'users.id_employee')
                ->where('users.id_employee', $noticket->created_by)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->first();

            $created = User::select('department_name', 'department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();

            // jangan lupa ini aktifin pas mau production guys
            $createdMail = $created->deptmail;
            $createddept = $created->department_name;

            //comment analisa fara bug 22Jan24
            //$assigndept = Department::select('department_name', 'email')
            //->where('department_id', $log_close->department)
            //->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
            //->first()->department_name;

            // jangan lupa ini aktifin pas mau production guys (kondisi checbox for_ca)
            //$mailDept = $for_ca ? 9999 : $request->department; //comment analisa fara bug 22Jan24

            // $assignMail = DepartmentEmail::select('email')
            // ->where('id_department', $mailDept)
            // ->first()->email;

            // nah kalo yang example ini matiin ya pas mau production
            // $createdMail = 'enquity.ekayekti@napinfo.co.id'; // example
            // $assignMail = 'hardy.prabowo@napinfo.co.id';

            $data = [
                'no_ticket' => $noticket->no_ticket,
                'cust_name' => $company_name->company_name,
                'category' => $noticket->category,
                'subcategory' => $subcategory->name_incident,
                'status_complain' => $noticket->status_complain,
                'link_respond' => $noticket->status_link_respond,
                'created_by' => $createddept,
                'assign_to' => $ticketcreatedby->department_name,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                'closed_date' => $closed_date,
                'receipt' => [
                    'createdMail' => $createdMail,
                    // 'assignMail' => $assignMail,
                ]
            ];
            // dd($data);
            Mail::to($createdMail)->send(new ClosedMail($data));
            return redirect('/tickets/' . $id)->with('status', 'Success Closed Ticket !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed Closed Ticket !');
        }
    }

    public function preclosed(Request $request,  $id)
    {

        // dd($request,$id);
        $request->validate([
            'message' => 'required',
        ]);

        $employee = Employee::where('int_emp_id', $request->user)->get();

        $dept = $employee[0]->int_emp_department;

        $log_ticket = LogTicket::where('id_ticket', $id)
            ->where('assign_status', '=', '1')
            ->where('assign_to_dept', $dept)
            ->get();

        $created_date = Carbon::now();
        $created_by = auth()->user()->id_employee;
        $created_name = auth()->user()->name;

        $logtick = Ticket::where('id', $id)->first();
        $assigndate = LogTicket::where('id_ticket', $logtick->id)->first();

        //duration
        $start = strtotime($assigndate->updated_at);
        $end = strtotime($created_date);
        $duration = $end - $start;
        // dd($duration);

        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60));  // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        //end duration

        // dd($hours . ' hours ' . $minutes . ' minutes ');

        $update_log_ticket = LogTicket::where('id', $log_ticket[0]->id)
            ->update([
                'preclosed_status' => '1',
                'preclosed_date' => $created_date,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                'preclosed_message' => $request->message,
            ]);

        $log_close = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => 'Preclosed by: ' . $created_name,
            'duration' => $hours . ' hours ' . $minutes . ' minutes ',
            'message'   => $request->message
        ]);

        if ($update_log_ticket) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Preclose Ticket';

            //dd($activity);
            $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

            // initial PrecloseMail
            $noticket = Ticket::where('id', $log_close->id_ticket)->first();
            $company_name = Customer::where('cid', $noticket->cid)->first();
            $subcategory = MappingIncident::where('id', $noticket->type_incident)->first();
            $ticketcreatedby = User::select('department_name', 'users.id_employee')
                ->where('users.id_employee', $noticket->created_by)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->first();

            $created = User::select('department_name', 'department_emails.email as deptmail')
                ->where('users.email', $username)
                ->leftJoin('department', 'users.id_department', '=', 'department.department_id')
                ->leftJoin('department_emails', 'department_emails.id_department', '=', 'department.department_id')
                ->first();

            if (env('APP_ENV') != 'production') {
                $createdMail = 'hardy.prabowo@napinfo.co.id'; // example
                $createddept = $created->department_name;
            }
            else {
                $createdMail = $created->deptmail;
                $createddept = $created->department_name;
            }

            $data = [
                'no_ticket' => $noticket->no_ticket,
                'cust_name' => $company_name->company_name,
                'category' => $noticket->category,
                'subcategory' => $subcategory->name_incident,
                'status_complain' => $noticket->status_complain,
                'link_respond' => $noticket->status_link_respond,
                'notes' => $noticket->remarks,
                'message' => $request->message,
                'created_by' => $createddept,
                'assign_to' => $ticketcreatedby->department_name,
                'receipt' => [
                    'createdMail' => $createdMail,
                    // 'assignMail' => $assignMail,
                ]
            ];
            // dd($data);
            Mail::to($createdMail)->send(new PreclosedMail($data));
            return redirect('/tickets/' . $id)->with('status', 'Success Pre Closed Ticket !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed Pre Closed Ticket !');
        }
    }

    public function ViewFile(Request $request,  $topology_file,  $cust)
    {
        $file = base64_decode($topology_file);
        $customer = base64_decode($cust);

        //Audit Log
        $username = auth()->user()->email;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $location = '0';
        $access_from = Browser::browserName();
        $activity = 'View Topology File';

        //dd($activity);
        $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

        return view('topology.view',  compact('file',  'customer'));
    }
    public function EditRFOHead(Request $request,  $id)
    {
        //dd($request);

        if ($request->finish_date == '' && $request->finish_time == '') {
            $resolved_date = $request->resolved_date_old;
        } else {
            $resolved_date = $request->finish_date . " " . $request->finish_time;
        }

        if ($request->link_respond == '') {
            $slr = $request->slr_old;
        } else {
            $slr = $request->link_respond;
        }

        if ($request->sub_link_respond == '') {
            $sub_slr = $request->sub_slr_old;
        } else {
            $sub_slr = $request->sub_link_respond;
        }

        if ($request->outage_loc == '') {
            $outage_loc = $request->outage_loc_old;
        } else {
            $outage_loc = $request->outage_loc;
        }

        if ($request->message == '') {
            $message = $request->closed_message_old;
        } else {
            $message = $request->message;
        }

        if ($request->outage_reason1 == '' && $request->outage_reason2 == '') {
            $outage_reason = $request->outage_reason_old;
            $outage_sub = $request->outage_sub_old;
            $action_close = $request->action_close_old;
        } else {
            $outage_reason1 = $request->outage_reason1;
            $outage_reason2 = $request->outage_reason2;

            if ($request->outage_reason2 == '') {
                $outage_reason = $request->outage_reason1;
                $outage_sub = $request->outage_sub1;
                $action_close = $request->action_close1;
            } else {
                $outage_reason = $outage_reason1 . " & " . $outage_reason2;
                $outage_sub = $request->outage_sub1 . " & " . $request->outage_sub2;
                $action_close = $request->action_close1 . " & " . $request->action_close2;
            }
        }


        $created_by = auth()->user()->id_employee;
        $created_date = $request->req_time;
        //$resolved_date=$request->finish_date." ".$request->finish_time;

        //duration
        $start = strtotime($created_date);
        $end = strtotime($resolved_date);
        $hold = strtotime($request->hold_date);
        $unhold = strtotime($request->unhold_date);
        $duration = ($end - $start) - ($unhold - $hold);

        $total_second = $duration; // 177300 second
        $total_minutes = ($duration / 60); // 2955 minute
        $temp_minutes = ($total_second % 3600 / (60));  // 177300 sec % 3600 = 900 sec / 60 = 15 menit
        $hours = (($total_minutes - $temp_minutes) / 60); // 2955 min - 15 min  = 2940 / 60 = 49 jam
        $second = (($temp_minutes * 60) % 60); // 15 menit * 60 = 900 sec % 60 = 0 sec
        $minutes = ((($temp_minutes * 60) - $second) / 60); // 15 menit * 60 = 900 second - 0 sec = 900 / 60 = 15 menit
        //end duration

        $update_ticket = Ticket::where('id', $id)
            ->update([
                'resolved_date' => $resolved_date,
                'outage_reason' => $outage_reason,
                'outage_sub' => $outage_sub,
                'outage_location' => $outage_loc,
                'action_close' => $action_close,
                'closed_message' => $message,
                'duration' => $hours . ' hours ' . $minutes . ' minutes ',
                'status_link_respond' => $slr,
                'sub_status_link_respond' => $sub_slr,
            ]);

        //insert log success hold ticket
        $desc_success_edit = 'Success Edit RFO by Head Dept';

        $log_edit_rfo = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_edit,
            'message'   => $message
        ]);
        //insert log success hold ticket

        if ($update_ticket && $log_edit_rfo) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Edit RFO';

            //dd($activity);
            $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

            return redirect('/tickets/' . $id)->with('status', 'Success Edit RFO !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed Edit RFO !');
        }
    }

    public function EditRFOStaff(Request $request,  $id)
    {
        //dd($request);

        if ($request->link_respond == '') {
            $slr = $request->slr_old;
        } else {
            $slr = $request->link_respond;
        }

        if ($request->sub_link_respond == '') {
            $sub_slr = $request->sub_slr_old;
        } else {
            $sub_slr = $request->sub_link_respond;
        }

        if ($request->outage_loc == '') {
            $outage_loc = $request->outage_loc_old;
        } else {
            $outage_loc = $request->outage_loc;
        }

        if ($request->message == '') {
            $message = $request->closed_message_old;
        } else {
            $message = $request->message;
        }

        if ($request->outage_reason1 == '' && $request->outage_reason2 == '') {
            $outage_reason = $request->outage_reason_old;
            $outage_sub = $request->outage_sub_old;
            $action_close = $request->action_close_old;
        } else {
            $outage_reason1 = $request->outage_reason1;
            $outage_reason2 = $request->outage_reason2;

            if ($request->outage_reason2 == '') {
                $outage_reason = $request->outage_reason1;
                $outage_sub = $request->outage_sub1;
                $action_close = $request->action_close1;
            } else {
                $outage_reason = $outage_reason1 . " & " . $outage_reason2;
                $outage_sub = $request->outage_sub1 . " & " . $request->outage_sub2;
                $action_close = $request->action_close1 . " & " . $request->action_close2;
            }
        }

        $created_by = auth()->user()->id_employee;

        $update_ticket = Ticket::where('id', $id)
            ->update([
                'outage_reason' => $outage_reason,
                'outage_sub' => $outage_sub,
                'outage_location' => $outage_loc,
                'action_close' => $action_close,
                'closed_message' => $message,
                'status_link_respond' => $slr,
                'sub_status_link_respond' => $sub_slr,
            ]);

        //insert log success hold ticket
        $desc_success_edit = 'Success Edit RFO by Staff';

        $log_edit_rfo = log::create([
            'id_ticket' => $id,
            'create_by' => $created_by,
            'description' => $desc_success_edit,
            'message'   => $message
        ]);
        //insert log success hold ticket

        if ($update_ticket && $log_edit_rfo) {
            //Audit Log
            $username = auth()->user()->email;
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $location = '0';
            $access_from = Browser::browserName();
            $activity = 'Edit RFO';

            //dd($activity);
            $this->auditLogs($username,  $ipAddress,  $location,  $access_from,  $activity);

            return redirect('/tickets/' . $id)->with('status', 'Success Edit RFO !');
        } else {
            return redirect('/tickets/' . $id)->with('status', 'Failed Edit RFO !');
        }
    }
}