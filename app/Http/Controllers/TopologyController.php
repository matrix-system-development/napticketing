<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Traits\AuditLogsTrait;
use Browser;
use Stevebauman\Location\Facades\Location;

class TopologyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuditLogsTrait;
    public function index(Request $request)
    {
        $customers = Customer::get();

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Master topology';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

        return view('topology.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'topology_cid' => 'required',
            'topology_file1' => 'mimes:png,jpg,jpeg|max:10048',
            'topology_file2' => 'mimes:png,jpg,jpeg|max:10048',
            'topology_file3' => 'mimes:png,jpg,jpeg|max:10048',
        ]);

        $cek_cust=Customer::where('cid','=',$request->topology_cid);
        $count_cust=$cek_cust->count();
        if($count_cust > 0){

            //$image = base64_encode(file_get_contents($request->file('topology_file')));

            //upload file
            $destinationPath = 'attachment_topology/';
            if($request->file('topology_file1')){
                $file1 = $request->file('topology_file1');
                $extension = $file1->getClientOriginalExtension();
                $file_name1 = $request->topology_cid.'_a.'.$extension;
                $store1 = $request->file('topology_file1')->storeAs('attachment_topology', $file_name1);
                //$file1->move($destinationPath, $file_name1);
            }
            else{
                $file_name1="";
            }

            if($request->file('topology_file2')){
                $file2 = $request->file('topology_file2');
                $extension = $file2->getClientOriginalExtension();
                $file_name2 = $request->topology_cid.'_b.'.$extension;
                $store2 = $request->file('topology_file2')->storeAs('attachment_topology', $file_name2);
                //$file2->move($destinationPath, $file_name2);
            }
            else{
                $file_name2="";
            }

            if($request->file('topology_file3')){
                $file3 = $request->file('topology_file3');
                $extension = $file3->getClientOriginalExtension();
                $file_name3 = $request->topology_cid.'_c.'.$extension;
                $store3 = $request->file('topology_file3')->storeAs('attachment_topology', $file_name3);
                //$file3->move($destinationPath, $file_name3);
            }
            else{
                $file_name3="";
            }
            
            $update_cust=Customer::where('cid',$request->topology_cid)
                ->update([
                'topology_file1' => $file_name1,
                'topology_file2' => $file_name2,
                'topology_file3' => $file_name3
            ]);

            //Audit Log
            $username= auth()->user()->email; 
            $ipAddress=$_SERVER['REMOTE_ADDR'];
            $location='0';
            $access_from=Browser::browserName();
            $activity='Add Topology';

            //dd($location);
            $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);

            return redirect('/masters/topology/')->with('status','Success Update Topology');
        }
        else{
            return redirect('/masters/topology/')->with('status','CID Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "hai";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ViewFile(Request $request,$topology_file,$cust)
    {
        //dd('haid');
        $file=base64_decode($topology_file);
        //dd($file);
        $customer=base64_decode($cust);

        //Audit Log
        $username= auth()->user()->email; 
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        $location='0';
        $access_from=Browser::browserName();
        $activity='View Topology File';

        //dd($location);
        $this->auditLogs($username,$ipAddress,$location,$access_from,$activity);
        return view('topology.view',compact('file','customer'));
    }
}
