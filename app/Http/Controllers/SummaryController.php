<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SummaryController extends Controller
{
    public function sumMatrix(Request $request)
    {
        if ($request->date_start == '' || $request->date_finish == '') {
            $now = Carbon::now();
            $date_now = Carbon::now()->lastOfMonth();
            $date_start2 = Carbon::now()->startOfMonth()->subMonth(1);
        } else {
            $request->validate([
                'date_start' => 'required|date',
                'date_finish' => 'required|date|after_or_equal:date_start'
            ]);

            $now = Carbon::now();
            $date_now = $request->date_finish;
            $date_start2 = $request->date_start;
        }

        //dd($date_start2,$date_now);
        $dept_login = auth()->user()->id_department;

        $dropdowns['ticket_type'] = DB::table('tickets')
            ->select('ticket_type')
            ->groupBy('ticket_type')
            ->orderBy('ticket_type', 'asc')
            ->get();

        $summary = Ticket::select(
            "category",
            DB::raw("count(*) as total"),
            DB::raw("count(case when status = 1 then 1 else null end) as in_progress"),
            DB::raw("count(case when status = 3 then 1 else null end) as hold"),
            DB::raw("count(case when status = 2 then 1 else null end) as unassigned"),
            DB::raw("count(case when status = 4 then 1 else null end) as closed"),
            DB::raw("count(case when DATE_FORMAT(created_date, '%y-%m-%d') = DATE_FORMAT(current_timestamp, '%y-%m-%d') and status <> 4 then 1 else null end) as new_ticket")
        )
            ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$date_start2, $date_now])
            ->orderBy("category", "asc")
            ->groupBy("category")
            ->get();
        //dd($summary);

        return view('summary_matrix', compact('summary', 'date_start2', 'date_now', 'dropdowns'));
    }
}
