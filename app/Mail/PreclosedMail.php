<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PreclosedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('[' . $this->data['no_ticket'] . ' - PRECLOSE TICKET] ' . ($this->data['cust_name'] ?? '') . ' - ' . $this->data['status_complain'] . ' - ' . $this->data['category'] . '  - ' . $this->data['subcategory'] . '')->view('ticket_external.preclose');

        foreach ($this->data['receipt'] as $key => $value) {
            $this->to($value);
        }

        return $this;
    }
}