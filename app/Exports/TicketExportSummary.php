<?php

namespace App\Exports;

use App\Models\Ticket;


use App\Exports\TEChildExport;
use App\Exports\TESummaryExport;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class TicketExportSummary implements FromArray, WithMultipleSheets
{
    protected $sheets;
    protected $date_start;
    protected $date_finish;
    protected $type;
    protected $dept_login;

    public function __construct($date_start, $date_finish, $type, $dept_login)
    {
        // $this->sheets = $sheets;
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->type = $type;
        $this->dept_login = $dept_login;
    }

    public function array(): array
    {
        return $this->sheets;
    }

    public function sheets(): array
    {
        $sheets = [
            new TESummaryExportSummary($this->date_start, $this->date_finish, $this->type, $this->dept_login),
            new TEChildExportSummary($this->date_start, $this->date_finish)
        ];

        return $sheets;
    }
}
