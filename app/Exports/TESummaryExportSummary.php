<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class TESummaryExportSummary implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $date_start;
    protected $date_finish;
    protected $type;
    protected $dept_login;

    function __construct($date_start, $date_finish, $type, $dept_login)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->type = $type;
        $this->dept_login = $dept_login;
        // dd($date_start, $date_finish, $type);
    }

    public function collection()
    {
        $dept_login = $this->dept_login;
        if ($this->type == 'all') {
            $query = DB::table('tickets')
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
                ->select(
                    'no_ticket',
                    'category',
                    'c.name_incident as name_incident',
                    'final_category',
                    'd.name_incident as final_name_incident',
                    'responsible', //new
                    'request',
                    'company_name',
                    'customers.customer_code', //new
                    'tickets.cid',
                    'a_end',
                    'b_end',
                    'network_type',
                    'network_owner',
                    'status_link_respond',
                    'sub_status_link_respond',
                    'service',
                    'rfo',
                    'tickets.duration',
                    'based_rfo_date',
                    //IN SLA/OVER SLA
                    DB::raw('(CASE
                WHEN customers.customer_code LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) < 24 THEN "IN SLA"
                WHEN customers.customer_code LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) > 24 THEN "OVER SLA"
                WHEN customers.customer_code NOT LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) < 4 THEN "IN SLA"
                WHEN customers.customer_code NOT LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) > 4 THEN "OVER SLA"
                END) AS sla_status'),
                    'duration_mtc',
                    'outage_reason',
                    'outage_sub',
                    'action_close',
                    'solved_by',
                    'a.int_emp_name as created_by',
                    'created_date',
                    // 'start_date',
                    DB::raw('(CASE
                WHEN category = "Maintenance" THEN "0000-00-00 00:00:00"
                ELSE start_date
                END) AS start_date'),
                    'b.int_emp_name as closed_by',
                    'closed_date',
                    'resolved_date',
                    'start_date_mtc',
                    'finish_date_mtc',
                    'regional',
                    'city',
                    DB::raw('(CASE
                WHEN status = "1" THEN "In Progress"
                WHEN status = "3" THEN "Hold"
                WHEN status = "4" THEN "Closed"
                ELSE "Unassign"
                END) AS status'),
                    DB::raw('(CASE
                WHEN status_mtc = "1" THEN "Open"
                WHEN status_mtc = "2" THEN "Reschedule"
                WHEN status_mtc = "3" THEN "Cancel"
                ELSE "Closed"
                END) AS status_mtc'),
                )
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('employee as a', 'tickets.created_by', '=', 'a.int_emp_id')
                ->leftJoin('employee as b', 'tickets.closed_by', '=', 'b.int_emp_id')
                ->leftJoin('logs_tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as e', 'logs_tickets.assign_to_dept', '=', 'e.department_id')
                ->leftJoin('department as f', 'employee.int_emp_department', '=', 'f.department_id')
                ->where(function ($query) use ($dept_login) {
                    $query->where('e.department_id', $dept_login) //dept yang di assign
                        ->orWhere('f.department_id', $dept_login); //dept yg created
                })
                ->groupby('tickets.no_ticket')
                ->orderBy('tickets.id', 'desc')
                ->get();
            //dd($query);
        } else {
            $query = DB::table('tickets')
                ->where('tickets.ticket_type', $this->type)
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
                ->select(
                    'no_ticket',
                    'category',
                    'c.name_incident as name_incident',
                    'final_category',
                    'd.name_incident as final_name_incident',
                    'responsible', //new
                    'request',
                    'company_name',
                    'customers.customer_code', //new
                    'tickets.cid',
                    'a_end',
                    'b_end',
                    'network_type',
                    'network_owner',
                    'status_link_respond',
                    'sub_status_link_respond',
                    'service',
                    'rfo',
                    'tickets.duration',
                    'based_rfo_date',
                    //IN SLA/OVER SLA
                    DB::raw('(CASE
                WHEN customers.customer_code LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) < 24 THEN "IN SLA"
                WHEN customers.customer_code LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) > 24 THEN "OVER SLA"
                WHEN customers.customer_code NOT LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) < 4 THEN "IN SLA"
                WHEN customers.customer_code NOT LIKE "%C09%" AND TIME_TO_SEC(SUBSTRING_INDEX(tickets.duration, " ", 1)) > 4 THEN "OVER SLA"
                END) AS sla_status'),
                    'duration_mtc',
                    'outage_reason',
                    'outage_sub',
                    'action_close',
                    'solved_by',
                    'a.int_emp_name as created_by',
                    'created_date',
                    // 'start_date',
                    DB::raw('(CASE
                WHEN category = "Maintenance" THEN "0000-00-00 00:00:00"
                ELSE start_date
                END) AS start_date'),
                    'b.int_emp_name as closed_by',
                    'closed_date',
                    'resolved_date',
                    'start_date_mtc',
                    'finish_date_mtc',
                    'regional',
                    'city',
                    DB::raw('(CASE
                WHEN status = "1" THEN "In Progress"
                WHEN status = "3" THEN "Hold"
                WHEN status = "4" THEN "Closed"
                ELSE "Unassign"
                END) AS status'),
                    DB::raw('(CASE
                WHEN status_mtc = "1" THEN "Open"
                WHEN status_mtc = "2" THEN "Reschedule"
                WHEN status_mtc = "3" THEN "Cancel"
                ELSE "Closed"
                END) AS status_mtc'),
                )
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('employee as a', 'tickets.created_by', '=', 'a.int_emp_id')
                ->leftJoin('employee as b', 'tickets.closed_by', '=', 'b.int_emp_id')
                ->leftJoin('logs_tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->leftJoin('employee', 'tickets.created_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as e', 'logs_tickets.assign_to_dept', '=', 'e.department_id')
                ->leftJoin('department as f', 'employee.int_emp_department', '=', 'f.department_id')
                ->where(function ($query) use ($dept_login) {
                    $query->where('e.department_id', $dept_login) //dept yang di assign
                        ->orWhere('f.department_id', $dept_login); //dept yg created
                })
                ->groupby('tickets.no_ticket')
                ->orderBy('tickets.id', 'desc')
                ->get();
        }
        //dd($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'No Ticket',
            'Category',
            'Sub Category',
            'Final Category',
            'Final Sub Category',
            'Responsible', //new
            'Request By',
            'Customer Name',
            'Customer Code', //new
            'Cid',
            'a end',
            'b end',
            'Network type',
            'Network_owner',
            'Status link reported',
            'Sub status link reported',
            'Service',
            'RFO',
            'Duration ticket',
            'Based on RFO Date',
            'IN SLA/OVER SLA', //new
            'Duration maintenance',
            'Outage reason',
            'Sub outage reason',
            'Resolution',
            'Solved By',
            'cc create ticket by',
            'Start Time', //change name
            'Reporting Time',
            'cc close ticket by',
            'Close Ticket Time', //change name
            'Resolving Time',
            'Start date Maintenance',
            'Finish date Maintenance',
            'Regional',
            'City',
            'Status Ticket',
            'Status Maintenance',
        ];
    }

    public function title(): string
    {
        return 'All Ticket';
    }
}