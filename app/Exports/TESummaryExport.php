<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class TESummaryExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $from;
    protected $until;
    protected $category;
    protected $summary;

    function __construct($summary, $from, $until, $category)
    {
        $this->summary = $summary;
        $this->from = $from;
        $this->until = $until;
        $this->category = $category;
        // dd($from, $until, $type);
    }

    public function collection()
    {
        if ($this->summary == 'all') {
            $query = DB::table('tickets')
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
                ->where('tickets.category', $this->category)
                ->select(
                    'no_ticket',
                    'category',
                    'c.name_incident as name_incident',
                    'final_category',
                    'd.name_incident as final_name_incident',
                    'request',
                    'company_name',
                    'tickets.cid',
                    'a_end',
                    'b_end',
                    'network_type',
                    'network_owner',
                    'status_link_respond',
                    'sub_status_link_respond',
                    'service',
                    'rfo',
                    'duration',
                    'based_rfo_date',
                    'duration_mtc',
                    'outage_reason',
                    'outage_sub',
                    'action_close',
                    'solved_by',
                    'a.int_emp_name as created_by',
                    'created_date',
                    // 'start_date',
                    DB::raw('(CASE 
                WHEN category = "Maintenance" THEN "0000-00-00 00:00:00"
                ELSE start_date
                END) AS start_date'),
                    'b.int_emp_name as closed_by',
                    'closed_date',
                    'resolved_date',
                    'start_date_mtc',
                    'finish_date_mtc',
                    'regional',
                    'city',
                    DB::raw('(CASE 
                WHEN status = "1" THEN "In Progress" 
                WHEN status = "3" THEN "Hold"
                WHEN status = "4" THEN "Closed"  
                ELSE "Unassign" 
                END) AS status'),
                    DB::raw('(CASE 
                WHEN status_mtc = "1" THEN "Open" 
                WHEN status_mtc = "2" THEN "Reschedule"
                WHEN status_mtc = "3" THEN "Cancel"  
                ELSE "Closed" 
                END) AS status_mtc'),
                )
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('employee as a', 'tickets.created_by', '=', 'a.int_emp_id')
                ->leftJoin('employee as b', 'tickets.closed_by', '=', 'b.int_emp_id')
                ->groupby('tickets.no_ticket')
                ->orderBy('tickets.id', 'desc')
                ->get();
            //dd($query);
        } else {
            $query = DB::table('tickets')
                ->where('tickets.category', $this->category)
                ->where('tickets.status', $this->summary)
                ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
                ->select(
                    'no_ticket',
                    'category',
                    'c.name_incident as name_incident',
                    'final_category',
                    'd.name_incident as final_name_incident',
                    'request',
                    'company_name',
                    'tickets.cid',
                    'a_end',
                    'b_end',
                    'network_type',
                    'network_owner',
                    'status_link_respond',
                    'sub_status_link_respond',
                    'service',
                    'rfo',
                    'duration',
                    'based_rfo_date',
                    'duration_mtc',
                    'outage_reason',
                    'outage_sub',
                    'action_close',
                    'solved_by',
                    'a.int_emp_name as created_by',
                    'created_date',
                    // 'start_date',
                    DB::raw('(CASE 
                WHEN category = "Maintenance" THEN "0000-00-00 00:00:00"
                ELSE start_date
                END) AS start_date'),
                    'b.int_emp_name as closed_by',
                    'closed_date',
                    'resolved_date',
                    'start_date_mtc',
                    'finish_date_mtc',
                    'regional',
                    'city',
                    DB::raw('(CASE 
                WHEN status = "1" THEN "In Progress" 
                WHEN status = "3" THEN "Hold"
                WHEN status = "4" THEN "Closed"  
                ELSE "Unassign" 
                END) AS status'),
                    DB::raw('(CASE 
                WHEN status_mtc = "1" THEN "Open" 
                WHEN status_mtc = "2" THEN "Reschedule"
                WHEN status_mtc = "3" THEN "Cancel"  
                ELSE "Closed" 
                END) AS status_mtc'),
                )
                ->leftJoin('customers', 'tickets.cid', '=', 'customers.cid')
                ->leftJoin('mapping_incident as c', 'tickets.type_incident', '=', 'c.id')
                ->leftJoin('mapping_incident as d', 'tickets.final_type_incident', '=', 'd.id')
                ->leftJoin('employee as a', 'tickets.created_by', '=', 'a.int_emp_id')
                ->leftJoin('employee as b', 'tickets.closed_by', '=', 'b.int_emp_id')
                ->groupby('tickets.no_ticket')
                ->orderBy('tickets.id', 'desc')
                ->get();
        }
        //dd($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'No Ticket',
            'Category',
            'Sub Category',
            'Final Category',
            'Final Sub Category',
            'Request By',
            'Customer Name',
            'Cid',
            'a end',
            'b end',
            'Network type',
            'Network_owner',
            'Status link reported',
            'Sub status link reported',
            'Service',
            'RFO',
            'Duration ticket',
            'Based on RFO Date',
            'Duration maintenance',
            'Outage reason',
            'Sub outage reason',
            'Resolution',
            'Solved By',
            'cc create ticket by',
            'Processing Time',
            'Reporting Time',
            'cc close ticket by',
            'Closing Time',
            'Resolving Time',
            'Start date Maintenance',
            'Finish date Maintenance',
            'Regional',
            'City',
            'Status Ticket',
            'Status Maintenance',
        ];
    }

    public function title(): string
    {
        return 'All Ticket';
    }
}
