<?php

namespace App\Exports;

use App\Models\Ticket;
use App\Models\LogTicket;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogsAssignBulkExportSummary implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $date_start;
    protected $date_finish;
    protected $type;
    protected $dept_login;

    function __construct($date_start, $date_finish, $type, $dept_login)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->type = $type;
        $this->dept_login = $dept_login;
    }

    public function collection()
    {
        $dept_login = $this->dept_login;
        if ($this->type == 'all') {
            $query = DB::table('logs_tickets')
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
                ->select(
                    'no_ticket',
                    DB::raw('(CASE 
            WHEN status = "1" THEN "In Progress" 
            WHEN status = "3" THEN "Hold"
            WHEN status = "4" THEN "Closed"  
            ELSE "Unassign" 
            END) AS status'),
                    DB::raw('CONCAT(int_emp_name," - ",a.department_name) as assign_by_dept'),
                    'b.department_name as assign_to_dept',
                    'assign_date',
                    DB::raw('(CASE 
                WHEN preclosed_status = "1" THEN "Pre-Closed"  
                ELSE "Active" 
                END) AS preclosed_status'),
                    'preclosed_date'
                )
                ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'employee.int_emp_department', '=', 'a.department_id')
                ->leftJoin('department as b', 'logs_tickets.assign_to_dept', '=', 'b.department_id')
                ->leftJoin('tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->leftJoin('department as c', 'logs_tickets.assign_to_dept', '=', 'c.department_id')
                ->leftJoin('department as d', 'employee.int_emp_department', '=', 'd.department_id')
                ->where(function ($query) use ($dept_login) {
                    $query->where('c.department_id', $dept_login) //dept yang di assign
                        ->orWhere('d.department_id', $dept_login); //dept yg created
                })
                ->orderBy('tickets.id', 'desc')
                ->get();
            //dd($query);
        } else {
            $query = DB::table('logs_tickets')
                ->where('tickets.ticket_type', $this->type)
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
                ->select(
                    'no_ticket',
                    DB::raw('(CASE 
            WHEN status = "1" THEN "In Progress" 
            WHEN status = "3" THEN "Hold"
            WHEN status = "4" THEN "Closed"  
            ELSE "Unassign" 
            END) AS status'),
                    DB::raw('CONCAT(int_emp_name," - ",a.department_name) as assign_by_dept'),
                    'b.department_name as assign_to_dept',
                    'assign_date',
                    DB::raw('(CASE 
                WHEN preclosed_status = "1" THEN "Pre-Closed"  
                ELSE "Active" 
                END) AS preclosed_status'),
                    'preclosed_date'
                )
                ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'employee.int_emp_department', '=', 'a.department_id')
                ->leftJoin('department as b', 'logs_tickets.assign_to_dept', '=', 'b.department_id')
                ->leftJoin('tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->leftJoin('department as c', 'logs_tickets.assign_to_dept', '=', 'c.department_id')
                ->leftJoin('department as d', 'employee.int_emp_department', '=', 'd.department_id')
                ->where(function ($query) use ($dept_login) {
                    $query->where('c.department_id', $dept_login) //dept yang di assign
                        ->orWhere('d.department_id', $dept_login); //dept yg created
                })
                ->orderBy('tickets.id', 'desc')
                ->get();
        }
        //dd($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'no ticket',
            'ticket status',
            'assign by',
            'assign to department',
            'assign date',
            'preclosed status',
            'preclosed date',
        ];
    }
}
