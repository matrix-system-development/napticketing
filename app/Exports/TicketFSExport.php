<?php

namespace App\Exports;

use App\Models\fsTicket;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TicketFSExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $date_start;
    protected $date_finish;
    function __construct($date_start, $date_finish)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $query = fsTicket::whereBetween(DB::raw("(STR_TO_DATE(created_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
            ->select(
                'no_ticket',
                'req_no',
                'ref_no',
                'cid',
                'ticket_type',
                'category',
                'sub_category',
                'status',
                'notes',
                'created_by',
                DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i:%s")'),
                DB::raw('DATE_FORMAT(fs_last_update, "%Y-%m-%d %H:%i:%s")'),
            )
            ->orderBy('id', 'asc')
            ->get();

        return $query;
    }

    public function headings(): array
    {
        return [
            'No Ticket NAP',
            'No Request FS',
            'Reference No',
            'CID',
            'Type',
            'Category',
            'Sub Category',
            'Status',
            'Notes',
            'Created By',
            'Created Date',
            'Last Updated',
        ];
    }
}
