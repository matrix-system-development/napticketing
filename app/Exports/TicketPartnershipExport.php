<?php

namespace App\Exports;

use App\Models\TicketPartnership;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TicketPartnershipExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $date_start;
    protected $date_finish;
    function __construct($date_start, $date_finish)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $query = TicketPartnership::whereBetween(DB::raw("(STR_TO_DATE(ticket_partnerships.created_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
            ->select(
                'ticket_partnerships.network_owner',
                'ticket_partnerships.requestor',
                'ticket_partnerships.sales_inquiry',
                'ticket_partnerships.request_third_party_time',
                'ticket_partnerships.partnership_response_time',
                'ticket_partnerships.feedback_third_party_time',
                'ticket_partnerships.third_party_response_time',
                'customers.company_name',
                'customers.a_end',
                'customers.b_end',
                'customers.capacity',
                'customers.capacity_international',
                'ticket_partnerships.spesific_requirement',
                'ticket_partnerships.currency',
                'ticket_partnerships.otc',
                'ticket_partnerships.mrc',
                'ticket_partnerships.term_of_contract',
                DB::raw('(CASE 
                WHEN ticket_partnerships.status = "1" THEN "Follow Up" 
                WHEN ticket_partnerships.status = "2" THEN "Cover"
                WHEN ticket_partnerships.status = "3" THEN "Purchase Order"  
                WHEN ticket_partnerships.status = "4" THEN "Not Cover"  
                WHEN ticket_partnerships.status = "5" THEN "No Response"  
                WHEN ticket_partnerships.status = "6" THEN "Inquiry"  
                WHEN ticket_partnerships.status = "7" THEN "Feedback"  
                WHEN ticket_partnerships.status = "8" THEN "Req 3rd Party"  
                ELSE "Undefined" 
                END) AS status'),
                'users.name',
                DB::raw('DATE_FORMAT(ticket_partnerships.created_at, "%Y-%m-%d %H:%i:%s")')
            )
            ->leftJoin('customers', 'ticket_partnerships.id_cust' , 'customers.id')
            ->leftJoin('users', 'ticket_partnerships.created_by' , 'users.id_employee')
            ->orderBy('ticket_partnerships.id', 'asc')
            ->get();

        return $query;
    }

    public function headings(): array
    {
        return [
            '3rd Party',
            'Requestor',
            'Inquiry From Sales',
            'Request to 3rd Party',
            'Partnership Response time',
            'Feedback From 3rd Party',
            '3rd Party Response Time',
            'Proposed by (Customer)',
            'A-End',
            'B-End',
            'Capacity',
            'Capacity International',
            'Special Requirements',
            'Currency',
            'OTC',
            'MRC',
            'Terms of Contract',
            'Status',
            'Partnership',
            'Ticket Created At'
        ];
    }
}
