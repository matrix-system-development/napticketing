<?php

namespace App\Exports;

use App\Models\Ticket;


use App\Exports\TEChildExport;
use App\Exports\TESummaryExport;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class TicketExport implements FromArray, WithMultipleSheets
{
    // protected $sheets;
    protected $from;
    protected $until;
    protected $category;
    protected $summary;

    public function __construct($summary, $from, $until, $category)
    {
        // $this->sheets = $sheets;
        $this->from = $from;
        $this->until = $until;
        $this->category = $category;
        $this->summary = $summary;
    }

    public function array(): array
    {
        return $this->sheets;
    }

    public function sheets(): array
    {
        $sheets = [
            new TESummaryExport($this->summary, $this->from, $this->until, $this->category),
            new TEChildExport($this->summary, $this->from, $this->until)
        ];

        return $sheets;
    }
}
