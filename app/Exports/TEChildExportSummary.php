<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class TEChildExportSummary implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $date_start;
    protected $date_finish;

    function __construct($date_start, $date_finish)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
    }

    public function collection()
    {
        $query = DB::table('log_holds')
            // ->where('tickets.status', '3')
            ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
            ->select(
                'tickets.no_ticket',
                'log_holds.hold_by',
                'log_holds.hold_created_date',
                'log_holds.hold_date_until',
                'log_holds.hold_category',
                'log_holds.hold_message',
                'log_holds.unhold_by',
                'log_holds.unhold_date',
                DB::raw("TIMEDIFF(log_holds.unhold_date,log_holds.hold_created_date)AS Durationhold")
            )
            ->leftJoin('tickets', 'tickets.id', '=', 'log_holds.id_ticket')
            ->groupby('log_holds.id')
            ->orderBy('log_holds.id', 'asc')
            ->get();
        return $query;
    }

    public function headings(): array
    {
        return [
            'No Ticket',
            'Hold By',
            'Hold Created',
            'Hold Until',
            'Hold Category',
            'Hold Message',
            'Unhold By',
            'Unhold Date',
            'Duration'
        ];
    }

    public function title(): string
    {
        return 'Log Holds';
    }
}
