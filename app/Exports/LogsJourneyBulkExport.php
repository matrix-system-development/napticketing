<?php

namespace App\Exports;

use App\Models\Ticket;
use App\Models\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogsJourneyBulkExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $from;
    protected $until;
    protected $category;
    protected $summary;

    public function __construct($summary, $from, $until, $category)
    {
        // $this->sheets = $sheets;
        $this->from = $from;
        $this->until = $until;
        $this->category = $category;
        $this->summary = $summary;
    }

    public function collection()
    {
        if ($this->summary == 'all') {
            $query = DB::table('logs')
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
                ->select(
                    'no_ticket',
                    'int_emp_name',
                    'department_name',
                    'description',
                    'message',
                    'logs.created_at'
                )
                ->leftJoin('employee', 'logs.create_by', '=', 'employee.int_emp_id')
                ->leftJoin('department', 'employee.int_emp_department', '=', 'department.department_id')
                ->leftJoin('tickets', 'logs.id_ticket', '=', 'tickets.id')
                ->orderBy('logs.id_ticket', 'desc')
                ->orderBy('logs.id', 'desc')
                ->get();
            //dd($query);
        } else {
            $query = DB::table('logs')
            ->where('tickets.category', $this->category)
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
                ->select(
                    'no_ticket',
                    'int_emp_name',
                    'department_name',
                    'description',
                    'message',
                    'logs.created_at'
                )
                ->leftJoin('employee', 'logs.create_by', '=', 'employee.int_emp_id')
                ->leftJoin('department', 'employee.int_emp_department', '=', 'department.department_id')
                ->leftJoin('tickets', 'logs.id_ticket', '=', 'tickets.id')
                ->orderBy('logs.id_ticket', 'desc')
                ->orderBy('logs.id', 'desc')
                ->get();
        }
        //dd($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'no ticket',
            'created by',
            'department',
            'description',
            'message',
            'created date'
        ];
    }
}
