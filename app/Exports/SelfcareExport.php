<?php

namespace App\Exports;


use App\Models\Selfcare;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SelfcareExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $date_start;
    protected $date_finish;
    function __construct($date_start, $date_finish)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $query = DB::table('selfcares')
            ->select(
                'selfcares.no_request',
                'selfcares.no_ticket',
                'selfcares.cust_code',
                'selfcares.cust_email',
                'selfcares.report_type',
                'selfcares.report_category',
                'selfcares.notes',
                'selfcares.billing_period',
                DB::raw(
                    '(CASE
                    WHEN status_request = "0" THEN "OPEN" 
                    WHEN status_request = "1" THEN "IN PROGRESS" 
                    WHEN status_request = "2" THEN "CANCEL"
                    WHEN status_request = "3" THEN "CLOSED"  
                    ELSE "ERROR STATUS" 
                    END
                ) as status'
                ),
                'selfcares.cancel_notes',
                'selfcares.canceled_at as cancel_date',
                'selfcares.created_at as reporting_date',
                'b.created_at as processing_date',
                'b.resolved_date as resolving_date',
                'b.closed_date as closing_date'
            )
            ->whereBetween(DB::raw("(STR_TO_DATE(selfcares.created_at,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
            ->leftJoin('tickets as b', 'selfcares.no_ticket', '=', 'b.no_ticket')
            ->get();

        return $query;
    }

    public function headings(): array
    {
        return [
            'no request',
            'no ticket',
            'cust code',
            'cust email',
            'report type',
            'report category',
            'notes',
            'billing period',
            'status',
            'cancel notes',
            'cancel date',
            'reporting date',
            'processing date',
            'resolving date',
            'closing date',
        ];
    }
}
