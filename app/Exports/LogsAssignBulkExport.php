<?php

namespace App\Exports;

use App\Models\Ticket;
use App\Models\LogTicket;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogsAssignBulkExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $from;
    protected $until;
    protected $category;
    protected $summary;

    public function __construct($summary, $from, $until, $category)
    {
        // $this->sheets = $sheets;
        $this->from = $from;
        $this->until = $until;
        $this->category = $category;
        $this->summary = $summary;
    }

    public function collection()
    {
        if ($this->summary == 'all') {
            $query = DB::table('logs_tickets')
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
                ->select(
                    'no_ticket',
                    DB::raw('(CASE 
            WHEN status = "1" THEN "In Progress" 
            WHEN status = "3" THEN "Hold"
            WHEN status = "4" THEN "Closed"  
            ELSE "Unassign" 
            END) AS status'),
                    DB::raw('CONCAT(int_emp_name," - ",a.department_name) as assign_by_dept'),
                    'b.department_name as assign_to_dept',
                    'assign_date',
                    DB::raw('(CASE 
                WHEN preclosed_status = "1" THEN "Pre-Closed"  
                ELSE "Active" 
                END) AS preclosed_status'),
                    'preclosed_date'
                )
                ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'employee.int_emp_department', '=', 'a.department_id')
                ->leftJoin('department as b', 'logs_tickets.assign_to_dept', '=', 'b.department_id')
                ->leftJoin('tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->orderBy('tickets.id', 'desc')
                ->get();
            //dd($query);
        } else {
            $query = DB::table('logs_tickets')
                ->where('tickets.category', $this->category)
                ->where('tickets.status', $this->summary)
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
                ->select(
                    'no_ticket',
                    DB::raw('(CASE 
            WHEN status = "1" THEN "In Progress" 
            WHEN status = "3" THEN "Hold"
            WHEN status = "4" THEN "Closed"  
            ELSE "Unassign" 
            END) AS status'),
                    DB::raw('CONCAT(int_emp_name," - ",a.department_name) as assign_by_dept'),
                    'b.department_name as assign_to_dept',
                    'assign_date',
                    DB::raw('(CASE 
                WHEN preclosed_status = "1" THEN "Pre-Closed"  
                ELSE "Active" 
                END) AS preclosed_status'),
                    'preclosed_date'
                )
                ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
                ->leftJoin('department as a', 'employee.int_emp_department', '=', 'a.department_id')
                ->leftJoin('department as b', 'logs_tickets.assign_to_dept', '=', 'b.department_id')
                ->leftJoin('tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
                ->orderBy('tickets.id', 'desc')
                ->get();
        }
        //dd($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'no ticket',
            'ticket status',
            'assign by',
            'assign to department',
            'assign date',
            'preclosed status',
            'preclosed date',
        ];
    }
}
