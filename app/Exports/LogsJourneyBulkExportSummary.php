<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogsJourneyBulkExportSummary implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $date_start;
    protected $date_finish;
    function __construct($date_start, $date_finish, $type)
    {
        $this->date_start = $date_start;
        $this->date_finish = $date_finish;
        $this->type = $type;
    }

    public function collection()
    {
        if ($this->type == 'all') {
            $query = DB::table('logs')
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
                ->select(
                    'no_ticket',
                    'int_emp_name',
                    'department_name',
                    'description',
                    'message',
                    'logs.created_at'
                )
                ->leftJoin('employee', 'logs.create_by', '=', 'employee.int_emp_id')
                ->leftJoin('department', 'employee.int_emp_department', '=', 'department.department_id')
                ->leftJoin('tickets', 'logs.id_ticket', '=', 'tickets.id')
                ->orderBy('logs.id_ticket', 'desc')
                ->orderBy('logs.id', 'desc')
                ->get();
            //dd($query);
        } else {
            $query = DB::table('logs')
                ->where('tickets.ticket_type', $this->type)
                ->whereBetween(DB::raw("(STR_TO_DATE(tickets.created_date,'%Y-%m-%d'))"), [$this->date_start, $this->date_finish])
                ->select(
                    'no_ticket',
                    'int_emp_name',
                    'department_name',
                    'description',
                    'message',
                    'logs.created_at'
                )
                ->leftJoin('employee', 'logs.create_by', '=', 'employee.int_emp_id')
                ->leftJoin('department', 'employee.int_emp_department', '=', 'department.department_id')
                ->leftJoin('tickets', 'logs.id_ticket', '=', 'tickets.id')
                ->orderBy('logs.id_ticket', 'desc')
                ->orderBy('logs.id', 'desc')
                ->get();
        }
        //dd($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            'no ticket',
            'created by',
            'department',
            'description',
            'message',
            'created date'
        ];
    }
}
