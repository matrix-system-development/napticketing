<?php

namespace App\Exports;

use App\Models\Ticket;
use App\Models\LogTicket;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogAssignExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $id_ticket;
    function __construct($id_ticket)
    {
        $this->id_ticket = $id_ticket;
    }

    public function collection()
    {
        $query = DB::table('logs_tickets')
            ->where('logs_tickets.id_ticket', '=', $this->id_ticket)
            ->select(
                'no_ticket',
                DB::raw('(CASE 
            WHEN status = "1" THEN "In Progress" 
            WHEN status = "3" THEN "Hold"
            WHEN status = "4" THEN "Closed"  
            ELSE "Unassign" 
            END) AS status'),
                DB::raw('CONCAT(int_emp_name," - ",a.department_name) as assign_by_dept'),
                'b.department_name as assign_to_dept',
                'assign_date',
                DB::raw('(CASE 
                WHEN preclosed_status = "1" THEN "Pre-Closed"  
                ELSE "Active" 
                END) AS preclosed_status'),
                'preclosed_date'
            )
            ->leftJoin('employee', 'logs_tickets.assign_by', '=', 'employee.int_emp_id')
            ->leftJoin('department as a', 'employee.int_emp_department', '=', 'a.department_id')
            ->leftJoin('department as b', 'logs_tickets.assign_to_dept', '=', 'b.department_id')
            ->leftJoin('tickets', 'logs_tickets.id_ticket', '=', 'tickets.id')
            ->orderBy('tickets.id', 'desc')
            ->get();

        return $query;
    }

    public function headings(): array
    {
        return [
            'no ticket',
            'ticket_status',
            'assign by',
            'assign to department',
            'assign date',
            'preclosed status',
            'preclosed date',
        ];
    }
}
