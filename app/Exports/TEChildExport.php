<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class TEChildExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $from;
    protected $until;
    protected $summary;

    function __construct($summary, $from, $until)
    {
        $this->summary = $summary;
        $this->from = $from;
        $this->until = $until;
    }

    public function collection()
    {
        $query = DB::table('log_holds')
            // ->where('tickets.status', $this->summary)
            ->whereBetween(DB::raw("(STR_TO_DATE(created_date,'%Y-%m-%d'))"), [$this->from, $this->until])
            ->select(
                'tickets.no_ticket',
                'log_holds.hold_by',
                'log_holds.hold_created_date',
                'log_holds.hold_date_until',
                'log_holds.hold_category',
                'log_holds.hold_message',
                'log_holds.unhold_by',
                'log_holds.unhold_date',
                DB::raw("TIMEDIFF(log_holds.unhold_date,log_holds.hold_created_date)AS Durationhold")
            )
            ->leftJoin('tickets', 'tickets.id', '=', 'log_holds.id_ticket')
            ->groupby('log_holds.id')
            ->orderBy('log_holds.id', 'asc')
            ->get();
        return $query;
    }

    public function headings(): array
    {
        return [
            'No Ticket',
            'Hold By',
            'Hold Created',
            'Hold Until',
            'Hold Category',
            'Hold Message',
            'Unhold By',
            'Unhold Date',
            'Duration'
        ];
    }

    public function title(): string
    {
        return 'Log Holds';
    }
}
