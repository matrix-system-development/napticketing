<?php

namespace App\Notifications;

use App\Models\Ticket;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notification;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsMessage;

class CreateHoldNotif extends Notification
{
    public function __construct()
    {
        //
    }
    public function via($notifiable)
    {
        return [MicrosoftTeamsChannel::class];
    }

    public function toMicrosoftTeams($notifiable)
    {
        $holds = Ticket::where('status', '3')
            ->select(DB::raw("(GROUP_CONCAT(no_ticket SEPARATOR '\n ')) as `noticket`"))
            ->first();
        $hold = $holds->noticket;
        // dd($hold);

        return MicrosoftTeamsMessage::create()
            ->to(config('services.microsoft_teams.hold_url'))
            ->type('success')
            ->title('Ticket yang sedang di hold')
            ->content("**$hold**");
        // dd($hold);
    }
}
