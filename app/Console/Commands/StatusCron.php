<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\fsTicket;
use App\Models\Rule;
use App\Models\LogToken;
use App\Models\LogApi;
use Illuminate\Support\Facades\Http;

class StatusCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateStatus:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Status Ticket FS Periodic';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //cari ticket yang bukan closed/canceled
        $checkticket=fsTicket::whereNotIn('status',['Canceled','Closed'])->get();

        //login fs
        //call url,username,password for api fs
        $rule=Rule::where('rule_name','FS Login')->first();
        $url_fslogin=$rule->rule_value;

        $rule_username=Rule::where('rule_name','username fs')->first();
        $username_fs=$rule_username->rule_value;

        $rule_password=Rule::where('rule_name','password fs')->first();
        $password_fs=$rule_password->rule_value;

        //login post to FS
        $response = Http::post($url_fslogin, [
            'username' => $username_fs,
            'password' => $password_fs,
        ]);

        $api_name='FS Login';

        if($response->failed()){
            $description_log= $response['title']." - ".$response['traceId'];

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '0',
            ]);
        }
        else{
            $token=$response['token'];
            $description_log='Success Login';

            //insert log token
            $log_token=LogToken::create([
                'access_token' => $token,
                'partner' => 'FiberStar',
            ]);

            //insert log api
            $log_api=LogApi::create([
                'api_name' => $api_name,
                'description' => $description_log,
                'conn_status' => '1',
            ]);
        }
        //end login FS

        foreach($checkticket as $data){
            $reqno=$data['req_no'];
            //get detail ticket
            $rule_check=Rule::where('rule_name','FS Check by Request Number')->first();
            $check_fs=$rule_check->rule_value;
            //dd($check_fs.$reqno);

            $response_check=Http::withToken($token)
            ->get($check_fs.$reqno);

            if(isset($response_check['ticketNumber'])){ ///jika ticket sudah di proses oleh FS
                //dd($response_check->json());
                $description_log="Success Check Detail"." ".$reqno;
                $update_fsticket=fsTicket::where('req_no',$reqno)
                ->update([
                    'status' => $response_check['currentLatestStatus'],
                ]);

                //update fs_last_update
                foreach ($response_check['ticketHistory'] as $history) {
                    if ($response_check['currentLatestStatus']==$history['status']) {
                        $update_fsticket=fsTicket::where('req_no',$reqno)
                        ->update([
                            'fs_last_update' => date('Y-m-d H:i:s',strtotime($history['processedAt'])),
                        ]);
                    }
                }
    
                //insert log api
                $log_api=LogApi::create([
                    'api_name' => 'FS Check by Request Number:Cron',
                    'description' => $description_log,
                    'conn_status' => '1',
                ]);
    
                $fstickets=fsTicket::where('req_no',$reqno)->first();
                //dd($fstickets);
                //dd($response_check->json());
                $ticketNumber=$response_check['ticketNumber'];
                $workOrderNumber=$response_check['workOrderNumber'];
                $workOrderStatus=$response_check['workOrderStatus'];
            }
            else{
                //dd($response_check->json());
                $description_log=$reqno." ".$response_check['message'];
    
                //insert log api
                $log_api=LogApi::create([
                    'api_name' => 'FS Check by Request Number:Cron',
                    'description' => $description_log,
                    'conn_status' => '1',
                ]);
            }
        }
    }
}
