<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\CreateHoldNotif;
use Illuminate\Support\Facades\Notification;

class NotifHoldCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifhold:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Notification::route(MicrosoftTeamsChannel::class, null)
            ->notify(new CreateHoldNotif());
    }
}
